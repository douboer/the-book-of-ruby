[# The Book Of Ruby <small class="h5 text-muted">Huw Collingbourne</small>](# The Book Of Ruby <small class="h5 text-muted">Huw Collingbourne</small>)
[目录概述](## 目录概述)
[学习资料](## 学习资料)
[关于作者](# 关于作者)
[序言](# 序言)
[Ruby 入门](## Ruby 入门)
[如何阅读本书](### 如何阅读本书)
[深入探索](### 深入探索)
[文本格式约定](### 文本格式约定)
[什么是 Ruby ？](### 什么是 Ruby ？)
[什么是 Rails ？](### 什么是 Rails ？)
[下载 Ruby](### 下载 Ruby)
[获取示例程序源码](### 获取示例程序源码)
[运行 Ruby 程序](### 运行 Ruby 程序)
[Ruby 的库文档](### Ruby 的库文档)
[第一章](# 第一章)
[字符串、数字、类和对象](## 字符串、数字、类和对象)
[获取并保存输入信息](### 获取并保存输入信息)
[字符串与内嵌表达式](### 字符串与内嵌表达式)
[数字](### 数字)
[测试条件语句：if ... then](### 测试条件语句：if ... then)
[局部变量与全局变量](### 局部变量与全局变量)
[类与对象](### 类与对象)
[实例变量](### 实例变量)
[消息、方法与多态](### 消息、方法与多态)
[构造方法——new 与 initialize](### 构造方法——new 与 initialize)
[查看对象](### 查看对象)
[第二章](# 第二章)
[类的层次结构、属性与变量](## 类的层次结构、属性与变量)
[超类与子类](### 超类与子类)
[向超类传递参数](### 向超类传递参数)
[访问器方法](### 访问器方法)
['Set' 访问器](### 'Set' 访问器)
[属性的读与写](### 属性的读与写)
[调用超类方法](### 调用超类方法)
[类变量](### 类变量)
[深入探索](## 深入探索)
[超类](### 超类)
[类中的常量](### 类中的常量)
[局部类](### 局部类)
[第三章](# 第三章)
[字符串和范围](## 字符串和范围)
[用户自定义字符串分隔符](### 用户自定义字符串分隔符)
[反引号](### 反引号)
[字符串处理](### 字符串处理)
[连接](#### 连接)
[字符串赋值](#### 字符串赋值)
[字符串索引](#### 字符串索引)
[删除换行符 chop 与 chomp](#### 删除换行符 chop 与 chomp)
[格式化字符串](#### 格式化字符串)
[深入探索](## 深入探索)
[范围](### 范围)
[范围迭代器](### 范围迭代器)
[HereDocs](### HereDocs)
[字符串字面量](### 字符串字面量)
[第四章](# 第四章)
[数组与哈希表](## 数组与哈希表)
[数组（Array）](### 数组（Array）)
[创建数组](#### 创建数组)
[多维数组](#### 多维数组)
[数组迭代](#### 数组迭代)
[数组索引](#### 数组索引)
[数组拷贝](#### 数组拷贝)
[数组比较](#### 数组比较)
[数组排序](#### 数组排序)
[比较值](#### 比较值)
[数组方法](#### 数组方法)
[哈希表（Hash）](### 哈希表（Hash）)
[创建哈希表](#### 创建哈希表)
[哈希表索引](#### 哈希表索引)
[哈希表拷贝](#### 哈希表拷贝)
[哈希表排序](#### 哈希表排序)
[哈希表方法](#### 哈希表方法)
[深入探索](## 深入探索)
[以数组方式操作哈希表](### 以数组方式操作哈希表)
[附加和连接](### 附加和连接)
[矩阵和向量](### 矩阵和向量)
[Sets](### Sets)
[第五章](# 第五章)
[循环（Loop）和迭代器（Iterator）](## 循环（Loop）和迭代器（Iterator）)
[for 循环](### for 循环)
[多迭代参数](### 多迭代参数)
[块（Blocks）](### 块（Blocks）)
[while 循环](### while 循环)
[while 修饰符](### while 修饰符)
[until 循环](### until 循环)
[循环（Loop）](### 循环（Loop）)
[深入探索](## 深入探索)
[Enumerable 模块](### Enumerable 模块)
[自定义比较](### 自定义比较)
[第六章](# 第六章)
[条件语句](## 条件语句)
[If..Then..Else](### If..Then..Else)
[And..Or..Not](### And..Or..Not)
[If..Elsif](### If..Elsif)
[Unless](### Unless)
[If 与 Unless 修饰符](### If 与 Unless 修饰符)
[Case 语句](### Case 语句)
[=== 方法](### === 方法)
[其它的 Case 语法](### 其它的 Case 语法)
[深入探索](## 深入探索)
[布尔（Boolean）测试](### 布尔（Boolean）测试)
[否定](### 否定)
[布尔运算中的怪象](### 布尔运算中的怪象)
[Catch 与 Throw](### Catch 与 Throw)
[第七章](# 第七章)
[方法（Methods）](## 方法（Methods）)
[类方法](### 类方法)
[类变量](### 类变量)
[类方法的用途？](### 类方法的用途？)
[Ruby 构造方法：new 还是 initialize？](### Ruby 构造方法：new 还是 initialize？)
[单例方法](### 单例方法)
[单例类](### 单例类)
[重写方法（Overriding）](### 重写方法（Overriding）)
[public, private 和 protected](### public, private 和 protected)
[深入探索](## 深入探索)
[子类中的 protected 和 private](### 子类中的 protected 和 private)
[突破 private 方法的隐私限制](### 突破 private 方法的隐私限制)
[单例类方法](### 单例类方法)
[嵌套方法](### 嵌套方法)
[方法名称](### 方法名称)
[第八章](# 第八章)
[传递参数和返回值](## 传递参数和返回值)
[1. 实例方法](### 1. 实例方法)
[2. 类方法](### 2. 类方法)
[3. 单例方法](### 3. 单例方法)
[返回值](### 返回值)
[返回多个值](### 返回多个值)
[默认参数和多参数](### 默认参数和多参数)
[赋值和参数传递](### 赋值和参数传递)
[整数是特殊的](### 整数是特殊的)
[进出原则](### 进出原则)
[并行赋值](### 并行赋值)
[深入探索](## 深入探索)
[引用或值传参](### 引用或值传参)
[赋值是拷贝还是引用？](### 赋值是拷贝还是引用？)
[什么时候两个对象是相同的？](### 什么时候两个对象是相同的？)
[括号避免歧义](### 括号避免歧义)
[第九章](# 第九章)
[异常处理](## 异常处理)
[Rescue](### Rescue)
[Ensure](### Ensure)
[Else](### Else)
[Error 编号](### Error 编号)
[Retry](### Retry)
[Raise](### Raise)
[深入探索](## 深入探索)
[省略 begin 和 end](### 省略 begin 和 end)
[Catch...Throw](### Catch...Throw)
[第十章](# 第十章)
[Blocks, Procs and Lambdas](## Blocks, Procs and Lambdas)
[什么是 Block？](### 什么是 Block？)
[换行值得注意](### 换行值得注意)
[匿名函数](### 匿名函数)
[看起来眼熟？](### 看起来眼熟？)
[块和数组](### 块和数组)
[Procs 与  Lambdas](### Procs 与  Lambdas)
[创建块对象](### 创建块对象)
[什么是闭包？](### 什么是闭包？)
[Yield](### Yield)
[块之中的块](### 块之中的块)
[传递命名的 Proc 参数](### 传递命名的 Proc 参数)
[优先级规则](### 优先级规则)
[块作为迭代器](### 块作为迭代器)
[深入探索](## 深入探索)
[从方法中返回块](### 从方法中返回块)
[块与实例变量](### 块与实例变量)
[块与局部变量](### 块与局部变量)
[第十一章](# 第十一章)
[符号（Symbols）](## 符号（Symbols）)
[符号与字符串](### 符号与字符串)
[符号和变量](### 符号和变量)
[为什么使用符号？](### 为什么使用符号？)
[深入探索](## 深入探索)
[什么是符号？](### 什么是符号？)
[第十二章](# 第十二章)
[模块（Modules）和混入（Mixins）](## 模块（Modules）和混入（Mixins）)
[模块像一个类...](### 模块像一个类...)
[模块方法](### 模块方法)
[模块作为命名空间](### 模块作为命名空间)
[模块的“实例方法”](### 模块的“实例方法”)
[包含模块或混入（Mixins）](### 包含模块或混入（Mixins）)
[命名冲突](### 命名冲突)
[Alias 方法](### Alias 方法)
[谨慎使用 Mix-in！](### 谨慎使用 Mix-in！)
[从文件中包含模块](### 从文件中包含模块)
[深入探索](## 深入探索)
[模块与类](### 模块与类)
[预定义模块](### 预定义模块)
[Kernel](#### Kernel)
[Math](#### Math)
[Comparable](#### Comparable)
[作用域解析](### 作用域解析)
[模块函数](### 模块函数)
[扩展对象](### 扩展对象)
[第十三章](# 第十三章)
[Files 与 IO](## Files 与 IO)
[打开和关闭文件](### 打开和关闭文件)
[文件和目录...](### 文件和目录...)
[复制文件](### 复制文件)
[目录查询](### 目录查询)
[关于递归的讨论](### 关于递归的讨论)
[根据大小排序](### 根据大小排序)
[深入探索](## 深入探索)
[简单递归](### 简单递归)
[第十四章](# 第十四章)
[YAML](## YAML)
[转换成 YAML](### 转换成 YAML)
[嵌套序列](### 嵌套序列)
[保存 YAML 数据](### 保存 YAML 数据)
[保存时忽略变量](### 保存时忽略变量)
[一个文件中多个文档](### 一个文件中多个文档)
[YAML 数据库](### YAML 数据库)
[YAML 冒险游戏](### YAML 冒险游戏)
[深入探索](## 深入探索)
[YAML 的简要指南](### YAML 的简要指南)
[第十五章](# 第十五章)
[Marshal](## Marshal)
[保存与加载数据](### 保存与加载数据)
[保存时忽略变量](### 保存时忽略变量)
[保存单例对象](### 保存单例对象)
[YAML 与单例对象](### YAML 与单例对象)
[深入探索](## 深入探索)
[Marshal 版本号](### Marshal 版本号)
[第十六章](# 第十六章)
[正则表达式](## 正则表达式)
[进行匹配](### 进行匹配)
[匹配组](### 匹配组)
[MatchData](### MatchData)
[前后匹配](### 前后匹配)
[贪婪匹配](### 贪婪匹配)
[字符串方法](### 字符串方法)
[文件操作](### 文件操作)
[深入探索](## 深入探索)
[正则表达式](### 正则表达式)
[第十七章](# 第十七章)
[线程（Threads）](## 线程（Threads）)
[创建线程](### 创建线程)
[运行线程](### 运行线程)
[主线程](### 主线程)
[线程状态](### 线程状态)
[确保线程执行](### 确保线程执行)
[线程优先级](### 线程优先级)
[主线程优先级](### 主线程优先级)
[互斥](### 互斥)
[深入探索](## 深入探索)
[传递执行权给其它线程](### 传递执行权给其它线程)
[第十八章](# 第十八章)
[调试与测试](## 调试与测试)
[IRB - 交互式 Ruby](### IRB - 交互式 Ruby)
[调试](### 调试)
[单元测试](### 单元测试)
[深入探索](## 深入探索)
[单元测试时可用的断言](### 单元测试时可用的断言)
[换行很重要](### 换行很重要)
[图形化调试器](### 图形化调试器)
[第十九章](# 第十九章)
[Ruby On Rails](## Ruby On Rails)
[首先安装 Rails](### 首先安装 Rails)
[A）自己安装...](#### A）自己安装...)
[B）或者使用'一体化'安装器...](#### B）或者使用'一体化'安装器...)
[MVC - 模型，视图，控制器](### MVC - 模型，视图，控制器)
[第一个 Ruby On Rails 应用](### 第一个 Ruby On Rails 应用)
[创建一个 Rails 应用](### 创建一个 Rails 应用)
[创建控制器](### 创建控制器)
[无法找到数据库？](### 无法找到数据库？)
[剖析简单的 Rails 应用](### 剖析简单的 Rails 应用)
[创建视图](### 创建视图)
[Rails 标记](### Rails 标记)
[让我们创建一个博客](### 让我们创建一个博客)
[创建数据库](#### 创建数据库)
[脚手架](#### 脚手架)
[迁移](#### 迁移)
[Partial](#### Partial)
[试一试](#### 试一试)
[深入探索](## 深入探索)
[MVC](### MVC)
[Model](#### Model)
[View](#### View)
[Controller](#### Controller)
[Rails 文件夹](### Rails 文件夹)
[其它 Ruby 框架](### 其它 Ruby 框架)
[第二十章](# 第二十章)
[动态编程](## 动态编程)
[自修改程序](### 自修改程序)
[eval 魔法](### eval 魔法)
[特殊类型的 eval](### 特殊类型的 eval)
[添加变量和方法](### 添加变量和方法)
[在运行时创建类](### 在运行时创建类)
[绑定](### 绑定)
[Send](### Send)
[移除方法](### 移除方法)
[处理未定义方法的调用](### 处理未定义方法的调用)
[在运行时写程序](### 在运行时写程序)
[深入探索](## 深入探索)
[冻结对象](### 冻结对象)
[附录](# 附录)
[附录 A：使用 RDOC 记录 Ruby](## 附录 A：使用 RDOC 记录 Ruby)
[附录 B：为 Ruby On Rails 安装 MySQL](## 附录 B：为 Ruby On Rails 安装 MySQL)
[下载 MySQL](### 下载 MySQL)
[安装 MySQL](### 安装 MySQL)
[配置 MySQL](### 配置 MySQL)
[附录 C：进一步阅读](## 附录 C：进一步阅读)
[书籍](### 书籍)
[E-Books](### E-Books)
[附录 D：Web 站点](## 附录 D：Web 站点)
[Ruby 和 Rails 信息](### Ruby 和 Rails 信息)
[附录 E：Ruby 和 Rails 的开发软件](## 附录 E：Ruby 和 Rails 的开发软件)
[集成开发环境 IDE/编辑器](### 集成开发环境 IDE/编辑器)
[Web 服务器](### Web 服务器)
[数据库](### 数据库)
[附录 F：Ruby 实现](## 附录 F：Ruby 实现)
