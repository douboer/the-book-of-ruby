

# The Book Of Ruby

***

随着开源社区的兴盛，作为一名开发者，我们在日常工作和生活中接触的开源技术也越来越多，未来开源已成为趋势和潮流。其中，开源书籍也成为了提供给我们开发者学习的免费并且宝贵的资源。在此之前，很多开发者都将自己所感兴趣领域的开源书籍、文档翻译成了中文，分享给国内的开发者参考学习，这似乎也算是对开源社区的一部分贡献吧。

《The Book Of Ruby》是一本与 **Ruby 语言** 编程知识相关的开源书籍，市面上有这本书的纸质出版书（英文版），而且 Windows 平台下安装了 Ruby 语言之后，这本书的 pdf 文件（英文版）也会随 API 文档存放于 Ruby 根目录的 `doc/` 目录下（目前发现，在安装 Ruby 2.3 还是 2.4 版本之后不再附带该 pdf 文档）。

我与 Ruby 结缘也算是一种巧合，源于一个网络游戏。事实上之前我非常喜欢玩《梦幻西游》这款网易公司出品的经典网游，但是随着自己慢慢长大，尤其是进入大学之后，学业上的事情比较繁重，就没有精力再继续玩下去了。然而，《梦幻西游》是一代人的回忆，也是我青春中的一段美好经历，10 年后它依然在，玩家已经换了一批又一批，它也早都不是当年的模样了。

在我读高中期间，偶尔闲暇的时候会上网搜索一下有关《梦幻西游》的信息，一次偶然间发现了民间单机版梦幻西游，下载下来后感觉还不错，而且民间制作单机版网游的人还不少。后来在大学期间，又接触了一批新的梦幻西游单机游戏，这次明显比高中时候接触的那些质量要好得多。这一次，激发我的探索欲，我接触到了 66rpg 论坛和 RPG Maker XP 游戏制作工具，后来也尝试着自己修改 RPG Maker XP 的脚本代码，而这些脚本代码正是由 Ruby 编写的。

Ruby 可以说是除过大学课程里面的 C 语言以外，我接触到并且用来写了大量代码的第一个高级语言，当然后来我也喜欢上了这门语言。Ruby 是一门纯粹的面向对象的脚本语言，可能它的运行效率会慢一点，但它的开发效率是极高的（Ruby 时代提倡敏捷开发）。更重要的是，它的面向对象的编程思想和设计哲学非常值得热爱编程的开发者去学习和借鉴。

Ruby 在前几年因为 ROR（Ruby on Rails）在 Web 开发方面大放异彩，事实上 GitHub 平台就是基于 ROR 构建的。遗憾的是，Ruby 在国内一直不温不火，据说在国外还是比较火的，虽然 ROR 框架现在用的少了，但 Ruby 语言在很多方面还是能看到它的踪影的，例如前端 Sass 工具就是用 Ruby 写的。Ruby 的创造者是一名日本人，也是首个亚洲人创造的编程语言，可能是由于偏见以及诸多原因，让 Ruby 没能成为一门主流语言。但是，Ruby 是一个集合了诸多早期其它编程语言（Perl、Smalltalk、Lisp 等）优点的高级语言，其中的设计哲学有很多地方值得我们去学习和体会。

国内的大多数开发者也许还不了解 Ruby，据说用过 Ruby 的人大多数认为它是对程序员友好的，同时我个人也觉得官方的 API 文档做的也是非常的好，而且从 Ruby 中领略到的面向对象的思维以及元编程的技巧是对开发者充满诱惑的，所以我推荐大家去学习 Ruby，它也许是一个很不错的工具。

在国内 Ruby 的学习资料是稀缺的，事实上 Ruby 相比其它语言（Java、PHP、C++ 等）的英文学习资料也是很少的，但这并不代表人们不认可它，近些年来 Ruby 在全球编程语言排名中一直位居 10 位左右，表现还不错。《The Book Of Ruby》这本书的最新版出版时间距今（2016年）已经约有六、七年了，但它的参考价值仍然是巨大的，全书共二十章，400 多页。该书不是简单的讨论了 Ruby 的语法，而是作者（Huw Collingbourne）带领你步步深入到 Ruby 之中，了解它的核心思想。

因此，我打算将这本书翻译成中文版，一方面是给自己找个事情做，能从中了解到更多编程思想和设计哲学，也方便以后自己去查阅，毕竟中文对我来说还是阅览速度更快的；另一方面呢，将该项目放到 GitHub 平台上，如果国内有 Ruby 的开发者感兴趣可以作为参考，这也算是为开源社区做一点点贡献吧。

## 目录概述

全书共 20 章节，下面是每一章节的内容概要：

- 第一章：字符串、数字、类和对象 - 获取输入和输出，字符串和内嵌表达式，数字和测试语句 if...then，局部变量和全局变量，类和对象，实例变量，消息、方法与多态性，构造方法与对象初始化，查看对象信息。
- 第二章：类的层次结构、属性与变量 - 超类与子类，访问器方法，属性读写，调用超类方法，类变量。
- 第三章：字符串和 Range - 字符串分隔符，字符串处理方法，Range，Range 迭代器。
- 第四章：数组与 Hash - 常用处理方法。
- 第五章：循环和迭代器 - for 循环，多参数迭代，代码块，while 循环，until 循环，loop 循环。
- 第六章：条件语句 - if...then...else，and...or...not，if...elsif，unless，case 语句，=== 方法，catch 与 throw。
- 第七章：方法 - 类方法，类变量，构造方法，单例方法，单例类，重写方法，public、private 和 protected 方法。
- 第八章：参数传递与返回值 - 实例方法，类方法，单例方法，返回值，返回多个值，默认参数和多参数，整数，进出原则，并行赋值，引用传值。
- 第九章：异常处理 - rescue，ensure，else，error 编号，retry，raise。
- 第十章：Block、Proc 和 Lambda - 匿名函数，proc 与 lambda，闭包，yield，嵌套块，优先级规则，块中实例变量，块中局部变量。
- 第十一章：符号 - 符号与字符串，符号和变量，为什么使用符号？
- 第十二章：模块和 mixin - 模块与类，模块方法，命名空间，包含模块，alias 方法，作用域解析符。
- 第十三章：文件与 IO - 打开和关闭文件，文件和目录，赋值文件，目录查询，递归，排序。
- 第十四章：Yaml - 转换成 yaml，嵌套序列，保存 yaml 数据，一个文件包含多个 yaml 文档，yaml 数据库。
- 第十五章：Marshal - 保存与加载数据，保存单例对象，yaml 与单例对象。
- 第十六章：正则表达式 - 匹配组，前后匹配，贪婪匹配，字符串方法，文件操作。
- 第十七章：线程 - 创建线程，运行线程，主线程，线程状态，线程优先级，主线程优先级，互斥。
- 第十八章：调试与测试 - irb、调试、单元测试、断言。
- 第十九章：Ruby On Rails - 安装 RoR、第一个 RoR 应用，创建控制器，创建视图，Rails 标记，MVC。
- 第二十章：动态编程 - 自修改程序，eval，动态添加变量和方法，运行时创建类，绑定，send，移除方法。

## 学习资料

最后，我向大家推荐一些有关 Ruby 的学习资料。

- 中文书籍
  - 《Ruby基础教程》，[日] 高桥征义 后藤裕藏，何文斯 译
  - 《Ruby元编程》，[意] Paolo Perrotta，廖志刚 译
- 中文论坛
  - <a href="http://ruby-china.org/" target="_blank">Ruby China</a>
- Ruby 官网
  - <a href="http://www.ruby-lang.org/zh_cn/" target="_blank">Ruby</a>

# 关于作者

<div class="text-center mb-3">
   <img class="d-inline-block" width="300" height="300" src="./images/author.png" />
</div>

**Huw Collingbourne** 是 SapphireSteel 软件 (http://www.sapphiresteel.com/) 的技术总监，同时也是基于 Visual Studio 的 **'Ruby In Steel'** Ruby 和 Rails 集成开发环境（IDE）以及基于 Adobe Flex 的 **'Amethyst'** 集成开发环境的开发人员。Huw 还是英国著名的技术作家，在一些计算机杂志上，例如 Computer Shopper、PC Pro and PC Plus，撰写了许多有关 C#、Delphi、Java、Smalltalk 以及 Ruby 的评论和编程专栏。另外，他也是免费的电子书《The Little Book Of Ruby》的作者，以及在线计算杂志 Bitwise (www.bitwisemag.com) 的编辑。Huw 拥有剑桥大学的英语硕士学位。

# 序言

***

## Ruby 入门

正因为你现在在阅读一本关于 Ruby 的书，我认为你不需要我去利用 Ruby 语言的优点说服你接受它这个假设是完全成立的。所以，我将采取特别的方式开始，需要注意的是：许多人被 Ruby 吸引是因为其简单的语法和易用性。然而，他们错了。Ruby 的语法一眼看去很简单，但是当你对它了解得越来越多时你会意识到，相反地，它是非常复杂的。事实就是，对于不熟悉 Ruby 的程序员来说却有很多陷阱。

在这本书中，我的目标是引导你摆脱这些陷阱，带领你穿越 Ruby 的语法和类库的浪潮。在探索的过程中会经历平坦的高速路以及有些曲折颠簸的小路。在旅途结束时，你应该能够安全、有效的使用 Ruby 了，而不会被沿途的任何意外所绊倒。

本书中使用的 Ruby 版本主要集中于 1.8.x 版本。虽然 Ruby 的 1.9 版本可能已经发布了，但 1.8 版本的 Ruby 仍然远远没有被广泛的使用。Ruby 的 1.9 版本可能被视为 2.0 版本的铺垫。大多数人同意 Ruby 1.9 版本的语法接近 1.8 版本，但你应该注意其中的一些差异，完全兼容是不可能的。

> 译注：目前（2016年11月）Ruby 的版本已经更新到 2.4（开发版），而稳定版为 2.3.x 。

### 如何阅读本书

这本书被分成了多个小部分。每一章都会介绍一个主题，而此又细分为多个子话题。并且，每个编程主题中都会伴随一个或多个彼此独立，直接就能运行的 Ruby 程序。

如果你想跟随一个结构良好的教程学习，那就按顺序阅读每个章节。如果你更喜欢学习到实用的方法，你也可以先运行程序，在你需要解释说明的时候再去参考文本。当然，如果你已经有了一些 Ruby 的实践经验，你可以按任意的顺序选择你觉得对你有用的章节进行阅读。在本书中没有大篇幅的相关联的应用程序代码或主题讨论，所以你不必担心由于没有按顺序阅读而导致你错过了一些重点。

### 深入探索

除了第一章，每一章都有一个部分叫做“深入探索”。在这部分我们将更深入地探索 Ruby 更具体的某些方面（包括一些我刚才提到的可能历经的曲折）。在大多数情况下，你可以直接跳过深入探索这部分，继续学习你今后必须要用到的知识。另一方面来说的话，在深入探索这一部分我们经常会接触到关于 Ruby 内部运作机制的知识，所以，如果你跳过这部分可能会错过一些非常有趣的东西。

### 文本格式约定

在这本书中，任何 Ruby 源代码都是像下面这样写的：

	def saysomething
  	  puts("Hello")
	end

当有一个示例程序伴随代码时，程序名将显示在页面右侧的一个框中，像这样：

<div class="code-file clearfix"><span>helloname.rb</span></div>

说明注释（通常是为在文本中提到的一些要点提供一些提示或给予一个更深入的解释）显示在这样一个方框中：

<div class="note">
	<b>这是一个解释性说明。</b>如果你想的话可以跳过它，不过你这么做的话，可能会错过一些有趣的东西。
</div>

### 什么是 Ruby ？

Ruby 是一种跨平台解释型语言，它具有许多与其他“脚本”语言（如 Perl 和 Python ）相同的特性。第一眼看上去它具有类似 Pascal 风格的英语语法。它是完全面向对象（object oriented）的，并且与著名的纯 OO 语言的鼻祖 Smalltalk 语言有很多共同点。据说，对 Ruby 语言的开发具有影响力的语言有：Perl、Smalltalk、Eiffel、Ada 和 Lisp。Ruby 语言是由 Yukihiro Matsumoto （通常称为 “Matz”，译注：日籍，中文译名为松本行弘）开发创造的，并于 1995 年首次发布。

### 什么是 Rails ？

目前，围绕 Ruby 最高的热度可以归功于一个 Web 开发框架，Rails —— 通常称为 “Ruby On Rails” 。Rails  是一个令人印象深刻的框架，但它并不代表 Ruby 的全部。事实上，你在没有首先精通 Ruby 的情况下直接去接触 Rails 开发框架，你可能会发现你最终创建的应用程序（applications），连你自己都不能理解（实际上，这在 Ruby On Rails 新手中太常见了）。学习 Ruby 是理解 Rails 的必要前提条件。

### 下载 Ruby

你可以在 http://www.ruby-lang.org 下载最新版本的 Ruby 。确保你下载了二进制文件（不仅仅是源代码）。在 PC 上，你可以使用 Ruby Installer for Windows 安装 Ruby ：http://rubyinstaller.rubyforge.org/wiki/wiki.pl 。

或者，如果你使用的是 Ruby In Steel IDE，你可以使用网站的下载页面上提供的 Ruby In Steel “一体化安装程序”安装 Ruby，Rails，Ruby In Steel 和所有其他需要使用的工具：http://www.sapphiresteel.com/ 。

### 获取示例程序源码

本书每章中的所有程序都可以从 http://www.sapphiresteel.com/The-Book-Of-Ruby 下载 Zip 文件获取。当你解压缩这些程序包之后，你会发现它们是被按章节分到一组目录中的。使用 Ruby In Steel （由本书作者的公司开发的 Visual Studio IDE）的好处就是，你可以将在一个项目树分支中的每个章节的源程序作为一个 Visual Studio solutions 全部加载到  Ruby In Steel For Visual Studio 2008 中。如果你正在使用其它编辑器或者 IDE ，你需要一个一个的加载每个 Ruby 程序。使用 Ruby In Steel for Visual Studio 2005 的用户可以（通过 <em>文件 新建/打开</em> 菜单）导入或者转换该项目。


### 运行 Ruby 程序

经常在包含有你的 Ruby 程序的源目录中打开一个命令行窗口是非常有用的。假设 Ruby 解释器（interpreter）在你的系统中路径是正确的，你就可以通过（在命令行）输入 **ruby &lt;filename&gt;** 来运行你的程序：

	ruby 1helloworld.rb

如果你使用的是 Ruby In Steel ，你可以在交互式控制台通过按 <kbd>CTRL+F5</kbd> 运行你的程序，或者按 <kbd>F5</kbd> 调试程序。

### Ruby 的库文档

这本书涵盖了标准 Ruby 库（Standard Ruby Library）中的许多类和方法，但并不是全部。因此，在某些时候你需要参考 Ruby 所使用的所有类的文档。幸运的是，Ruby 类库包含有被提取编译为易于浏览的多种合适格式的嵌入式文档。例如，请参考被放在 Web 网站上的在线文档：http://www.ruby-doc.org/core/ 。

或者，你也可以按字母顺序浏览：http://www.ruby-doc.org/stdlib/ 。

上面这些页面都包含有如何下载离线文档的说明。还有一个页面，你可以从其中下载多种格式、版本和语言的文档：http://www.ruby-doc.org/downloads 。

OK，这些序言已经足够了，让我们开始吧。是时候直接去阅读第一章了。

***

<p class="text-center">
	<b>《The Book Of Ruby》由 SapphireSteel 软件赞助，并提供开发了 Ruby In Steel IDE for Visual Studio 。</b><br />
	<a href="http://www.sapphiresteel.com">http://www.sapphiresteel.com</a>
</p>

# 第一章

***

## 字符串、数字、类和对象

关于 Ruby 语言，首先要知道的是它是易于使用的。为了证明这一点，让我们来看一看经典的 "Hello world" 程序代码：

<div class="code-file clearfix"><span>1helloworld.rb</span></div>

	puts 'hello world'

这就是完整的代码。用到了一个 `puts` 方法和一个 `'Hello world'` 字符串。没有文件头或者类定义，也不需要导入其他代码和 `main` 方法。这真的就是那么简单。（自己）加载这段代码，**1helloworld.rb**，试着运行一下。

### 获取并保存输入信息

首先将一个提示字符串输出后（这里是命令行窗口），显然下一步就是获取一个字符串。正如你可能猜到的，Ruby 为此提供的是 `gets` 方法。**2helloname.rb** 这段程序提示用户输入他们的名字，假设输入的是 'Fred'，随后将显示一句问候语："Hello Fred"。

<div class="code-file clearfix"><span>2helloname.rb</span></div>

	print( 'Enter your name: ' )
	name = gets()
	puts( "Hello #{name}" )

虽然说这仍然非常地简单，但有一些重要的细节需要说明。首先，注意我输出提示的时候使用的是 `print` 方法而不是 `puts` 方法。这是因为 `puts` 方法会在末尾自动添加一个换行符，但 `print` 方法则不会；而当前我希望光标和提示能在同一行显示。

在下一行，当用户按下 <kbd>Enter</kbd> 键时，我使用 `gets()` 方法读取用户的输入并以字符串类型保存。该字符串会被赋值给 `name` 变量（variable）。我没有预先声明该变量，也没有指定它的类型。在 Ruby 中，你可以根据需要去创建变量，并且 Ruby 会自动去推断该变量的类型。现在我将一个字符串赋值给了 `name`，因此 Ruby 推断 `name` 变量的类型一定是字符串（String）。

<div class="note">
	<b>注意：Ruby 是大小写敏感的。</b>一个名为 <code>myvar</code> 的变量和名为 <code>myVar</code> 的变量是不同的。一个和示例程序中 <code>name</code> 一样的变量，它的名字必须以小写字母开头（如果以大写字母开头，Ruby 会认为它是一个常量（constant），关于常量在后面的章节我会详细说明。）
</div>

顺便说一下，`gets()` 方法的括号是可选的，它与 `print` 和 `puts` 方法用来包围字符串的括号是一样的，如果你移除了括号，仍然会得到相同的结果。但是，括号可以帮助你解决某些语义冲突，并且在某些情况下，如果你省略它们，解释器将会发出警告。

### 字符串与内嵌表达式

在我们的示例代码中，最后一行是相当有趣的。

	puts( "Hello #{name}" )

这里的 `name` 变量被嵌入到字符串（String）本身中。这是通过将变量放置于两个花括号中并在花括号前面加一个 # 字符实现，也就是 `#{}` 。这种嵌入式表达式仅限于使用双引号分隔的字符串中起作用。如果你尝试在单引号分隔的字符串中使用它，该变量将不会被执行（解释），恰恰显示的将会是字符串 **'Hello #{name}'**。

不仅仅只有变量可以嵌入到双引号分隔的字符串中。你也可以嵌入非打印（转义）字符，例如换行符 `\n` 和制表符 `\t` 。你甚至也可以嵌入程序代码和数学表达式。让我们假设你拥有一个方法 `showname` ，它的返回值为字符串 'Fred'。

下面这个字符串在执行过程中将会调用 `showname` 方法，因此，最终结果将会显示为 "Hello Fred"：

	puts "Hello #{showname}"

看你是否能弄清楚下面这段程序将会显示什么结果：

<div class="code-file clearfix"><span>3string_eval.rb</span></div>

	puts("\n\t#{(1 + 2) * 3}\nGoodbye")

现在运行一下 **3string_eval.rb** 程序看看你对了吗。

### 数字

数字（Numbers）和字符串一样容易使用。例如，你想基于税率值和合计值来计算一些东西的销售价格或者总的合计值。为此，你需要将合计值乘以合适的税率并将结果加上合计值。假设合计值为 100 美元，税率为 17.5% ，这个 Ruby 程序会进行计算并显示结果：

<div class="code-file clearfix"><span>4calctax.rb</span></div>

	subtotal = 100.00
	taxrate = 0.175
	tax = subtotal * taxrate
	puts "Tax on $#{subtotal} is $#{tax}, so grand total is $#{subtotal+tax}"

显然，如果这个程序可以计算不同的合计值的话，相比于计算相同的合计值是更有用的。这是一个简单的可以提示用户输入合计值的计算程序：

	taxrate = 0.175
	print "Enter price (ex tax): "
	s = gets
	subtotal = s.to_f
	tax = subtotal * taxrate
	puts "Tax on $#{subtotal} is $#{tax}, so grand total is $#{subtotal+tax}"

这里的 `s.to_f` 是 String 类的一个方法，它会尝试将该字符串转换成一个浮点数。例如，字符串 "145.45" 将被转换成浮点数 145.45 。如果字符串不能被转换，将会返回 0.0 。所以，对于 **"Hello world".to_f** 将会返回 0.0 。

<div class="note">
	<dl>
		<dt>注释</dt>
		<dd>本书附带的许多示例源代码都有会被 Ruby 解释器忽略的注释。注释可以放置于 <b>#</b> 字符之后，该字符之后的一行文本都将会被视为注释：</dd>
	</dl>

	# this is a comment

	puts( "hello" ) # this is also a comment

如果你想注释掉多行文本你可以在文本的首行添加 **=begin** 以及在末行添加 **=end**（**=begin** 与 **=end** 必须左对齐顶格写）：

	=begin
	  This is a
	  multiline
	  comment
	=end
</div>

### 测试条件语句：if ... then

上面的税率值计算代码的问题是允许负的合计值和税率，这种情况在政府看来可能是不利的。因此，我需要测试负数，如果出现负数将其置为 0 。这是我的新版代码：

<div class="code-file clearfix"><span>5taxcalculator.rb</span></div>

	taxrate = 0.175
	print "Enter price (ex tax): "
	s = gets
	subtotal = s.to_f

	if (subtotal < 0.0) then
	subtotal = 0.0
	end

	tax = subtotal * taxrate
	puts "Tax on $#{subtotal} is $#{tax}, so grand total is $#{subtotal+tax}"

Ruby 中的 `if` 测试语句与其他编程语言中的 `if` 相似。注意，这里的括号也是可选的，`then` 也一样。但是，你如果在测试条件之后没有换行符的情况下继续写代码，那么 `then` 不能省略：

	if (subtotal < 0.0) then subtotal = 0.0 end

将所有代码写在同一行不会增加代码的清晰度，我会避免这么写。我长期习惯于 Pascal 书写风格所以导致我经常在 `if` 条件之后添加 `then`，然而这真的是不需要的，你可以将其看成我的一个癖好。`if` 代码块末尾的 `end` 关键字不是可选的，忘记添加它的话你的代码将不会运行。

### 局部变量与全局变量

在前面的示例中，我将值赋给了变量，例如 `subtotal`、`tax` 和 `taxrate` 。这些以小写字母开头的变量都是局部变量（Local variables），这意味着它们只存在于程序的特定部分。换句话说，它们被限制一个定义明确的作用域（scope）内。这是一个实例：

<div class="code-file clearfix"><span>variables.rb</span></div>

	localvar = "hello"
	$globalvar = "goodbye"

	def amethod
	  localvar = 10
	  puts(localvar)
	  puts($globalvar)
	end

	def anotherMethod
	  localvar = 500
	  $globalvar = "bonjour"
	  puts(localvar)
	  puts($globalvar)
	end

这里有三个名为 `localvar` 的局部变量，一个在 main 作用域内被赋值为 "hello" ;其它的两个分别在独立的方法作用域内被赋值为整数（Integers）：因为每一个局部变量都有不同的作用域，赋值并不影响在其它作用域中同名的局部变量。你可以通过调用方法来验证：

	amethod           #=> localvar = 10
	anotherMethod     #=> localvar = 500
	amethod           #=> localvar = 10
	puts( localvar )  #=> localvar = "hello"

另一方面，一个以 **$** 字符开头的全局变量拥有全局作用域。当在一个方法中对一个全局变量进行赋值，同时也会影响程序中其它任意作用域中的同名全局变量：

	amethod          #=> $globalvar = "goodbye"
	anotherMethod    #=> $globalvar = "bonjour"
	amethod          #=> $globalvar = "bonjour"
	puts($globalvar) #=> $globalvar = "bonjour"

### 类与对象

现在先跳过其余的 Ruby 语法，例如类型（type）、循环（loops）、模块（modules）等等（不要怕，我们会很快回过头来），让我们迅速去看看如何创建类（class）和对象（object）。

<div class="note">
	<dl>
		<dt>类、对象和方法</dt>
		<dd>
			类是对象的大纲蓝图，它定义对象包含的数据以及行为方式。许多不同的对象可以从单一的类创建，所以你可能有一个 Cat 类（<code>class</code>），但是有三个 cat 对象（<code>objects</code>）：<em>tiddles</em>、<em>cuddles</em> 和 <em>flossy</em>。一个方法（<code>method</code>）就像一个定义在类中的函数或子例程。
		</dd>
	</dl>
</div>

Ruby 是面向对象（object oriented）的似乎没什么特别可说的，现代所有的语言不是如此吗？好吧，说一点。大多数现代的“面向对象”语言（Java、C++、C#、Object  Pascal 等等）或多或少都具有面向对象编程（OOP）的特性。另一方面，Ruby 是纯粹面向对象的。事实上，除非你使用过 Smalltalk 或  Eiffel （比 Ruby 更纯粹的面向对象的语言），否则 Ruby 就是你曾经使用过的语言中最面向对象的语言。从简单的数字和字符串到复杂的文件和模块，每一块数据都被视为一个对象。并且你用对象做的每一件事都是通过方法来完成，甚至“运算符”（operators）也是一个方法，例如加 + 和减 - 。看下面这个程序：

	x = 1 + 2

这里的 `+` 是 Fixnum (Integer) 对象 1 的一个方法，值 2 被传入该方法；结果 3 被返回并赋值给 x 对象。顺便地说一下，运算符 `=` 是“使用对象做任何事情都是通过方法来完成”这条规则的罕见例外。赋值运算符是一个内置的东西（这不是一个术语，我没有添加）并且它不是用来完成任何事情的一个方法。

现在让我们来看看如何创建我们自己的对象。和大多数其它 OOP（面向对象编程）的语言一样，一个 Ruby 对象由类来定义，这个类就像一个从中构建多个单个对象的蓝图。例如，这个类定一只狗：

	class Dog
      def set_name( aName )
        @myname = aName
      end
	end

注意，类的定义以关键字 `class`（全部小写）和类名开始，并且类名必须以大写字母开头。这个类包含一个 `set_name` 方法，它需要传入一个参数 `aName`，方法体则是将 `aName` 赋值给一个 `@myname` 变量。

### 实例变量

以 `@` 符号开头的变量就是“实例变量”——这意味着它们属于单独的对象或者类的实例。实例变量不需要提前声明。我可以通过调用类的 `new` 方法来创建 Dog 类的实例（即 dog 对象）。在这里我创建两个两个 dog 对象（注意，虽然类名是以大写字母开头的，而实例对象名则是以小写字母开头的）：

	mydog = Dog.new
	yourdog = Dog.new

目前，这两只狗还没有名字。所以，接下来我将要做的是调用 `set_name` 方法来给它们起个名字：

	mydog.set_name( 'Fido' )
	yourdog.set_name( 'Bonzo' )

现在每只狗都有了名字，但是我以后需要通过某些途径能获知它们的名字。我该怎么办？我不能在对象内部获取 `@name` 变量，因为每个对象的内部细节只能被它自己所知道。这是纯粹的面向对象的根本：每个对象内部的数据是私有的。每个对象都有其对应的被定义的输入（例如，`set_name` 方法）和输出接口。只有对象自身才能让它的内部状态变得混乱，外部世界是不能做到的。这被称为“数据隐藏”，并且它是“封装”（encapsulation）原理的一部分。

<div class="note">
	<dl>
		<dt>封装（Encapsulation）</dt>
		<dd>
			在 Ruby 中，封装并不像最初它出现时的那么严格地被遵守，有一些不好的技巧可以让你使一个对象内部变得混乱。为了清楚起见（并确保你和我不会有恶梦），现在我们默默的了解下面这些语言的特性。
		</dd>
	</dl>
</div>

因为我们需要每一只狗都能知道它的名字，让我们给 Dog 类提供一个 `get_name` 方法：

	def get_name
  	  return @myname
	end

这里的 `return` 关键字是可选的。当它被省略时，Ruby 会返回最后一个表达式的值。

为了清楚起见（并为了避免发生意外的结果），我习惯于明确的返回我所期望的值。

最后，我们可以让狗拥有说话的能力。这是最终的类定义：

	class Dog
	  def set_name( aName )
		@myname = aName
	  end

	  def get_name
		return @myname
	  end

	  def talk
		return 'woof!'
	  end
	end

现在，我们可以创建一个 dog 对象，给它命名、显示它的名字并且让它说话：

	mydog = Dog.new
	mydog.set_name( 'Fido' )
	puts(mydog.get_name)
	puts(mydog.talk)

<div class="code-file clearfix"><span>6dogs.rb</span></div>

我已经在 **6dogs.rb** 这个文件中编写了这个代码的扩展版本。这个文件也包含了一个类似于 Dog 类的 Cat 类，除过 `talk` 方法不同，很自然的它的返回值是 miaow 而不是 woof 。

<div class="note">
	<p ><b>糟糕！</b>这个程序似乎包含一个错误。</p>
	<p >
		名为 <code>someotherdog</code> 的对象从未给它的 <code>@name</code> 变量赋值。幸运的是，在我们要显示这只狗的名字时 Ruby 并不会发生错误，而只会打印“nil”。我们将很快看到一个简单的方式来确保这样的错误不再发生...
	</p>
</div>

### 消息、方法与多态

顺便的说一句，这是一个基于经典的 Smalltalk 示例程序的例子，说明了如何将相同的“消息”（例如 `talk`）发送给不同的对象（例如 cats 和 dogs），并且每个不同的对象会对相同的消息使用它们自己特有的方法（这里是 `talk` 方法）产生不同的响应。这种不同的类拥有相同的方法的能力有一个面向对象的名字“多态”——这个词可以不用记住。

当你运行一个程序，例如 **6dogs.rb** ，它的代码是顺序执行的。但是，直到类的实例（即对象）被后面的代码创建类的代码本身不会被执行。你会发现，我经常将类定义与程序运行时就会被执行的独立自由地代码混合着写。这可能不是你想写一个应用程序的主要方式，但这仅仅是尝试，而且它非常方便。

<div class="note">
	<p><strong>什么是自由独立的代码？</strong></p>
	<p>
		如果 Ruby 真的是一个面向对象的语言，你可能会因为我们可以写“自由浮动”的方法而感到奇怪。事实上被证明的是，当你运行一个程序时，Ruby 会创建一个 <code>main</code> 对象并且任何出现在其内部的代码不是自由浮动的，实际上是在 <code>main</code> 对象内部运行。你可以很容易的验证这一点，创建一个新的源文件，添加这些代码然后运行它来查看输出信息：
	</p>

	puts self
	puts self.class
</div>

我的程序有一个明显的缺陷就是 Cat 和 Dog 类是高度重复的。也许更有意义的做法是，创建一个包含 `get_name` 和 `set_name` 方法的 Animal 类，并且它有两个仅仅包含特定行为——woofing 或 miaowing——的后代类 Cat 和 Dog。我们将在下一章中找到如何做到这一点。

### 构造方法——new 与 initialize

现在，来看看另一个用户自定义类的例子。加载 **7treasure.rb** ，这是制作了一个冒险游戏。它包含两个类 Thing（东西） 和 Treasure（宝藏），Thing 类与 Cat 和 Dog 类特别的相似，除了它不包含 woof 或者 miaow。

Treasure 类没有 `get_name` 和 `set_name` 方法，相反地它包含一个名为 `initialize` 的方法，这个方法接受两个参数并将参数值分配给 `@name` 和 `@description` 变量：

<div class="code-file clearfix"><span>7treasure.rb</span></div>

	def initialize( aName, aDescription )
  	  @name = aName
  	  @description = aDescription
	end

当一个类包含名为 `initialize` 的方法，它会在使用 `new` 方法创建对象时自动地被调用。使用 `initialize` 来设置一个对象的实例变量的值是不错的主意。

这相对于使用方法（例如 `set_name`）设置每个实例变量的值有两个明显的好处。首先，一个复杂的类可能包含许多实例变量，你可以通过一个 `initialize` 方法设置它们全部的值，而不是通过许多独立的“set”方法。其次，如果这些变量在对象创建时都被自动的初始化，你就不会以空的变量结束程序（例如在前面的程序中我们尝试显示 `someotherdog` 的名字时会返回 <em>nil</em> 值）。

最后，我创建了一个名为 `to_s` 的方法用来返回一个表示宝物对象的字符串。这个 `to_s` 方法名不是随意的，相同的方法名已被在 Ruby 标准对象库中使用。实际上，`to_s` 方法被定义在 Object 类中，该类是其它类的祖先。通过重新定义 `to_s` 方法，我添加了新的行为，这比默认的方法更适合于 Treasure 类。换句话说，我已经“覆盖”（overridden）了它的 `to_s` 方法。

`new` 方法可以创建一个对象，所以它可以被认为是对象的“构造方法”。然而，你通常不应该实现你自己的 `new` 方法（这是可能的，但它通常不可取）。相反，当你想要执行任何“设置”操作（例如为对象的内部变量赋值）时，应在 `initialize` 方法中完成，Ruby 会在一个新对象创建后立即执行 `initialize` 方法。

<div class="note">
	<dl>
		<dt>垃圾回收（Garbage Collection，GC）</dt>
		<dd >
			在许多语言中（例如 C++ 和 Delphi for Win32），销毁任何已经创建并且不再需要的对象是程序员的职责。换句话说，对象被赋予析构函数以及构造函数。在 Ruby 中，你不必做这些了，因为 Ruby 有一个内置的“垃圾回收器”，它会在你的程序不再引用对象时销毁它并回收内存。
		</dd>
	</dl>
</div>

### 查看对象

顺便提一下，Treasure 对象 **t1** 内部使用了 `inspect` 方法：

	t1.inspect

`inspect` 是为所有的 Ruby 对象定义的，它将返回一个包含人类可读的表示该对象的字符串。在本例中，它显示这样一些信息：

	#<Treasure:0x28962f8 @description="an Elvish weapon forged of gold", @name="Sword">

它是以类名 Treasure 开始，后面跟随一个数字，这是 Ruby 内部用来识别特定对象的的识别码；随后就是对象的名字和变量的值。

Ruby 还提供了<code>p</code>方法作为打印和查看对象细节的快捷语法，像这样：

<div class="code-file clearfix"><span>p.rb</span></div>

	p(anobject)

来看看如何用 `to_s` 方法将各种对象以及测试将一个 Treasure 对象在没有重写 `to_s` 方法的情况下转换成字符串，场试运行 **8to_s.rb** 程序：

<div class="code-file clearfix"><span>8to_s.rb</span></div>

	puts(Class.to_s)     #=> Class
	puts(Object.to_s)    #=> Object
	puts(String.to_s)    #=> String
	puts(100.to_s)       #=> 100
	puts(Treasure.to_s)  #=> Treasure

正如你将看到的，当 `to_s` 方法被调用时，类（如 Class,、Object、String 以及 Treasure ）只是简单的返回它们的名字；而一个对象，例如 Treasure 对象 **t** ，返回与 `inspect` 方法返回值一样的对象标识符：

	t = Treasure.new( "Sword", "A lovely Elvish weapon" )
	puts(t.to_s)
    	#=> #<Treasure:0x3308100>
	puts(t.inspect)
    	#=> #<Treasure:0x3308100 @name="Sword", @description="A lovely Elvish weapon">

虽然 **7treasure.rb** 程序可能作为一个游戏的基础代码包含了不同类型的对象，其代码仍然是重复的。毕竟，为什么 Thing 类有一个 name 而 Treasure 也要包含一个 name 呢？如果把 Treasure 视为一类东西可能是更有意义的。在一个完整的游戏中，其它对象，例如 Rooms（房间） 和 Weapons（武器）可能也是其它的一类东西。是时候应该在一个合适的类层次上开始工作了，这就是我们下一章将要讲到的...

# 第二章

***

## 类的层次结构、属性与变量

在上一节结束时我们创建两个类：Thing 和 Treasure，尽管事实上这两个类共享了一些功能（特别是两者都包含 'name'），但它们是没有联系的。

现在，这两个类的重复看起来是不值一提的。但是，当你开始写一些复杂的程序时，你的类将会包含大量的变量和方法；你真的想将同样的事情一遍又一遍的重复吗。

对于其中一个类是其它（祖先）类的特殊类型结构来说创建一个类层次是更有意义的，这种情况下它会自动继承（inherit）祖先类的特征。例如，在我们简单的冒险游戏中，Treasure 是 Thing 的一个特殊的类型，因此 Treasure 就会继承 Thing 类的特征。

<div class="note">
	<b>类层次——祖先（Ancestors）和后代（Descendants）：</b>在这本书中，我会经常提及“后代”类继承（inherit）自它们祖先类，这些术语意味着“相关”类之间的一种类似于家庭的关系。Ruby 中每一个类只有一个父亲，然而，它可能在一个很长很大的家庭树中，有许多代的父母、祖父母等等...
</div>

Thing 类的特征通常被定义在它内部，Treasure 类则会自动地“继承” Thing 类所有的的特性。所以，我们不需要再次对这些特征进行编码，而是额外添加一些 Treasures 类特有的特性。

通常地规则是，在创建类层次结构时，具有更多通用特征的类要比具有更多特殊特征的类层次更高一些。所以，只有一个 *name* 和 *description* 的 Thing 类是具有 *name*、*description* 以及 *value* 的 Treasure 类的祖先；Thing 类也可能是一些其它的特殊类的祖先，例如具有 *name*、*description* 以及 *exits* 的 Room 类。

<div class="note">
	<dl>
		<dt><strong>一个父亲，有多个孩子...</strong></dt>
		<dd>
			<div class="text-center py-1">
				<img src="./images/chapter2_Class.png" />
			</div>
			Thing 类具有 <i>name</i> 和 <i>description</i> （在 Ruby 程序中，它们可能是内部变量 <code>@name</code> 和 <code>@description</code>）。Treasure 和 Room 类都派生自 Thing 类，所以它们自动地继承了 <i>name</i> 和 <i>description</i> 。Treasure 类添加了一个 <i>value</i>，所以它就具有 <i>name</i>、<i>description</i> 和 <i>value</i> ；Room 类添加了一个 <i>exits</i>，所以它就具有 <i>name</i>、<i>description</i> 和 <i>exits</i> 。
		</dd>
	</dl>
</div>

<div class="code-file clearfix"><span>1adventure.rb</span></div>

来让我们看看如何在 Ruby 中创建一个后代类。加载 **1adventure.rb** 程序，这只需要定义一个具有两个实例变量（instance variables）`@name` 和 `@description` 的 Thing 类，它们的值可以在创建新的 Thing 对象时在 `initialize` 方法中赋值。

实例变量通常不能（也不应该）被外部直接访问，这是为了遵循上一章讲到的封装（ encapsulation）原则。为了获得我们所需要的每个变量的值，我们需要一个 get 访问器方法，例如 `get_name`；为了给某个变量赋一个新值，我们也需要一个 set 访问器方法，例如 `set_name`。

### 超类与子类

现在来看看 Treasure 类，注意声明方式：

	class Treasure < Thing

尖括号 `<` 表示 Treasure 是 Thing 的一个子类（后代类），因此它从 Thing 类继承数据（变量）和行为（方法）。因为 `get_name`、`set_name`、`get_description` 以及 `set_description` 方法已经存在于 Thing 类中，所以不需要在后代类（Treasure）中再编写了。

Treasure 类还有额外的数据，也就是它的 value（`@value`），我已经为它编写了 *get* 和 *set* 访问器了。当创建一个新的 Treasure 对象时，它的 `initialize` 方法会被自动调用。一个 Treasure 对象有三个变量（`@name`、`@description` 和 `@value`）需要初始化，所以它的 `initialize` 方法有三个参数。前两个参数使用 `super` 关键字传递给超类（Thing）的 `initialize` 方法，以便 Thing 类的 `initialize` 方法可以处理它们：

	super(aName, aDescription)

当在方法中使用时，`super` 关键字调用在祖先类或超类中的与当前所在方法的同名方法。如果 `super` 关键字没有指定任何参数，当前方法的所有参数将会传递给祖先类的方法。但是，像本例中一样，提供了一个特定的参数列表时（这里是 `aName` 和 `aDescription`），那么只有这些参数会被传递给祖先类的方法。

### 向超类传递参数

当调用超类的方法时，括号是很重要的！如果参数列表为空，并且没有使用括号，那么所有的参数都将传递给超类。但是，参数列表为空，并且使用了括号时，将不会给超类传递任何参数。

<div class="code-file clearfix"><span>super_args.rb</span></div>

	# This passes a, b, c to the superclass
	def initialize(a, b, c, d, e, f)
	  super(a, b, c)
	end

	# This passes a, b, c to the superclass
	def initialize(a, b, c)
	  super
	end

	# This passes no arguments to the superclass
	def initialize(a, b, c)
	  super()
	end

<div class="note">
	要更好的了解 <code>super</code> 关键字的使用，请参阅本章末尾的<b>深入探索</b>部分。
</div>

### 访问器方法

虽然这些类在冒险游戏中运行的足够好，但它们仍然是累赘的，因为设置了大量的 *get* 和 *set* 访问器方法。让我们看看有什么补救措施。

替换掉 `@description` 实例变量的两个不同的方法，`get_description` 和 `set_description`。

	puts(t1.get_description)
	t1.set_description("Some description")

取值和赋值也许更好一些，对于一个简单的变量使用下面的方式进行取值和赋值：

	puts(t1.description)
	t1.description = "Some description"

为了能够做到这一点，我们需要修改 Treasure 类的定义。实现这一点的方法是重写 `@description` 的访问器方法：

	def description
	  return @description
	end

	def description=(aDescription)
	  @description = aDescription
	end

<div class="code-file clearfix"><span>accessors1.rb</span></div>

我已经在 **accessors1.rb** 中添加了类似于上面的访问器。*get* 访问器被称为 `description`，*set* 访问器被称为 `description=`（即就是，将等号（=）附加到 *get* 访问器方法名后面）。现在，就可以将一个新的字符串进行赋值了：

	t.description = "a bit faded and worn around the edges"

你可以像这样取值：

	puts(t.description)

### 'Set' 访问器

当你想要以这种方式编写 set 访问器时，必须将 `=` 字符附加到方法名后面，而不仅仅是将其放置于方法名和参数之间。

所以，这样是正确的：

	def name=(aName)

但这样是错误的：

	def name = (aName)

### 属性的读与写

事实上，有一个更简单的方式实现相同的功能。所有你要做的就是使用两个特殊的方法，`attr_reader` 和 `attr_writer`，后跟一个符号（symbol）：

	attr_reader :description
	attr_writer :description

你可以添加这些代码到你的类定义中：

	class Thing
	  attr_reader :description
	  attr_writer :description
		# maybe some more methods here…
	end

使用一个符号并调用 `attr_reader` 方法将会为命名与该符号（`:description`）相匹配的实例变量（`@description`）创建一个 *get* 访问器。

调用 `attr_writer` 方法类似的将会为实例变量创建一个 *set* 访问器。实例变量被认为是一个对象的“属性”（attributes），这就是为什么 `attr_reader` 和 `attr_writer` 方法是这样命名的。

<div class="note">
	<dl>
		<dt>符号（Symbols）</dt>
		<dd>
			在 Ruby 中，一个符号是以冒号开头的（例如，<code>:description</code>）。<strong>Symbol</strong> 类在 Ruby 类库中定义，用来表示解释器中的命名。当你将一个或多个符号作为参数传递给 <code>attr_reader</code>（这是一个 <strong>Module</strong> 类的方法）时，Ruby 会创建一个实例变量和一个 <em>get</em> 访问器方法。此访问器方法返回相应变量的值；实例变量和访问器方法都会使用该符号命名。所以，<code>attr_reader(:description)</code> 会创建一个名为 <code>@description</code> 的实例变量，以及一个名为 <code>description()</code> 的访问器方法。
		</dd>
	</dl>
</div>

<div class="code-file clearfix"><span>accessors2.rb</span></div>

**accessors2.rb** 程序包含一些属性读取器的示例。Thing 类为 `@name` 属性明确定义了 *get* 访问器方法，编写这样一个完整的方法的好处是，可以让你进行额外的处理，而不是简单的对属性的值进行读写。这里的 *get* 访问器使用 `String.capitalize` 方法返回 `@name` 的字符串值及其初始值的大写形式。

	def name
	  return @name.capitalize
	end

当为 `@name` 属性进行赋值时，我不需要进行特殊的处理，所以我可以这么写：

	attr_writer :name

`@description` 属性不需要特殊处理，所以我使用 `attr_reader` 和 `attr_writer` 去获取和设置 `@description` 变量的值：

	attr_reader :description
	attr_writer :description

<div class="note">
	<dl>
		<dt>Attributes or Properties?</dt>
		<dd>
			不要对术语感到困惑，在 Ruby 中，"Attribute" 相当于许多编程语言中的 "Propertie"。
		</dd>
	</dl>
</div>

当你想允许对一个变量同时可以进行读和写操作时，`attr_accessor` 方法提供了替代 `attr_reader` 和 `attr_writer` 方法的简洁语法。我已经使用它来访问 Treasure 类中的 value 属性：

	attr_accessor :value

这等同于：

	attr_reader :value
	attr_writer :value

前面我说过，使用一个符号并调用 `attr_reader` 实际上会创建一个名字与符号相同的变量。`attr_accessor` 方法也是一样的。

在 Thing 类的代码中，因为 `initialize` 方法显式创建了变量，所以这并不明显。然而，Treasure 类没有在它的 `initialize` 方法中引用 `@value` 变量。只存在 `@value` 访问器的定义：

	attr_accessor :value

在我的源文件代码的底部，每个 Treasure 对象创建后都单独设置了 value 的值。

	t1.value = 800

即使从没有正式的声明，`@value` 变量却是真实存在的，我们能够使用 *get* 访问器获取其数值。

	t1.value

要绝对地确定属性访问器真的已经创建了 `@value`，你可以使用 `inspect` 方法查看对象的内部。我在这个程序的最后两行代码就是这么做的：

	puts "This is treasure1: #{t1.inspect}"
	puts "This is treasure2: #{t2.inspect}"

<div class="code-file clearfix"><span>accessors3.rb</span></div>

属性访问器可以同时初始化超过一个属性，你可以传递一个用逗号分隔的符号列表：

	attr_reader :name, :description
	attr_writer(:name, :description)
	attr_accessor(:value, :id, :owner)

和往常一样，在 Ruby 中参数列表的括号是可选的，但在我看来（为了清楚起见）括号是必选的。

<div class="code-file clearfix"><span>2adventure.rb</span></div>

现在让我们看看如何将属性读写用在我们的冒险游戏中。加载 **2adventure.rb** 程序。你会看到我在 Thing 类中创建了两个可读的属性：`name` 和 `description`。我同时也让 `description` 是可写的。然而，我不打算改变任何一个 Thing 对象的 name，所以 `name` 是不可写的：

	attr_reader(:name, :description)
	attr_writer(:description)

我创建了一个用来返回描述 Treasure 对象的字符串的 `to_s` 方法。回想一下，所有的 Ruby 类都有一个标准的 `to_s` 方法，`Thing.to_s` 方法覆盖（替换）了默认的方法。当您希望实现适合特定类类型的新行为时，你可以覆盖现有的方法。

### 调用超类方法

我已经决定我的游戏会有两个派生自 Thing 的后代类。Treasure 类添加了一个可读写的 `value` 属性；注意它的 `initialize` 方法中调用了超类方法，是为了在初始化新变量 `@value` 之前初始化 `name` 和 `description` 属性：

	super(aName, aDescription)
	@value = aValue

在这里如果我省略了对超类方法的调用，那么 `name` 和 `description` 属性将永远不会被初始化。这是因为 `Treasure.initialize` 覆盖了 `Thing.initialize`，所以当一个 Treasure 对象被创建时，`Thing.initialize` 代码不会自动被执行。

另一方面，`Room` 类也派生自 Thing，目前没有 `initialize` 方法；所以当一个新的 Room 对象被创建时，Ruby 会从类层次中逐层向上寻找该方法。第一个 `initialize` 方法在 Thing 中被发现；所以一个 Room 对象的 `name` 和 `description` 属性在这里被初始化。

### 类变量

在这个程序中还有一些其他的有趣的东西。在顶级类 Thing 中你会看到：

	@@num_things = 0

此变量的名字是以两个 **@** 字符开头的，`@@num_things` 定义这是一个“类变量”（class variable）。到目前为止，我们在类中使用的变量都是实例变量，以单个 **@** 开头，例如 `@name`。而每个类的新对象（实例）会将自己的值分配给自己的实例变量，然而所有派生自特定类的对象共享相同的类变量。我已经给 `@@num_things` 赋值为 0 以确保其是有意义的。

在这里，`@@num_things` 类变量用来记录在游戏运行的 Thing 对象的数量。它只是在每个新对象被创建时在它的初始化方法中简单的递增类变量（通过 `+=1`）：

	@@num_things +=1

如果你接着看我的代码，你会看到我创建一了包含 rooms 的数组（array）的 Map 类。这也包括一个标准的 `to_s` 方法，可以打印出数组中每个 room 的信息。不要担心 Map 类的实现，我们将在后面章节讨论数组及其方法。

滚动到代码文件的底部，然后运行程序看看我是如何创建和初始化所有的对象以及使用类变量，`@@num_things`，来记录已创建的 Thing 对象的数量。

<div class="note">
	<dl>
		<dt class="text-center"><strong>类变量与实例变量</strong></dt>
		<dd>
			<div class="text-center">
				<img src="./images/chapter2_Class_Variables.png" />
			</div>
			对于 Thing 类，如有三个不同的 Thing 对象（实例），<code>@name</code> 作为实例变量，因各个对象（实例）的不同而取值不同；并且各个对象之间修改 <code>@name</code> 的值并不会相互影响，是独立的。Thing 类中有一个 <code>@@num_things</code> 类变量，它被三个对象共享，其中一个修改 <code>@@num_things</code> 的值时，其它对象访问到 <code>@@num_things</code> 的值也会发生改变。所以说，实例变量 <code>@name</code> 是对象独有的，而类变量 <code>@@num_things</code> 是被所有对象共享的。
		</dd>
	</dl>
</div>

## 深入探索

### 超类

<div class="code-file clearfix"><span>super.rb</span></div>

要理解 super 关键字的工作原理，请看我们的示例程序，**super.rb**。它包含五个相关类：Thing 类其他所有类的祖先；从 Thing 类衍生到 Thing2 类，再从 Thing2 类衍生到 Thing3 类，然后依次是 Thing4 和 Thing5 类。

让我们仔细看看这个层次中的前三个类：Thing 类有两个实例变量，`@name` 和 `@description` ；Thing2 类也定义了 `@fulldescription`（一个包含 `@name` 与 `@description` 的字符串）；Thing3 类则添加了另一个变量 `@value`。

这三个类各自包含一个 `initialize` 方法，用来在创建新对象时设置变量的值；它们各自也有一个名为 `aMethod` 的方法，用来改变一个或多个变量的值。后代类 Thing2 和 Thing3 在它们的方法中都使用了 `super` 关键字。

<div class="note">
	在命令窗口运行 <b>super.rb</b> 。去按位测试代码，键入 1 到 5的一个数字，当有提示时或者键入 'q' 退出。
</div>

在这段代码的底部，我写了一个 "main" 循环，这将在你运行该程序时执行。不要担心不懂这些语法，我们将在以后的章节中学习循环。我已经将 `test1` 到 `test5` 添加进了循环，所以你可以很容易地运行包含在不同方法中的代码。首次运行此程序时，在提示符处键入数字 `1` 然后按 Enter 键。 这将运行包含这两行代码的test1方法：

	t = Thing.new( "A Thing", "a lovely thing full of thinginess" )
	t.aMethod( "A New Thing" )

第一行在这里创建并初始化 Thing 对象，第二行调用它的 `aMethod` 方法。因为 Thing 类并非派生自其它类（事实上，所有的 Ruby 类都派生自 Object 类，它是其它所有类的祖先类），所以在这里并没有其它的事情发生。当调用 `Thing.initialize` 和 `Thing.aMethod` 方法时，输出调用了 `inspect` 方法来显示对象的内部结构。`inspect` 方法可以被所有对象调用，是个调试的工具方法。在这里，它给我们显示一个十六进制的数字，来表示这个拥有 `@name` 和 `@description` 字符串变量的特定对象。

现在，在提示符下，输入 `2` 来运行 `test2` 包含的代码，创建一个 Thing2 对象 `t2` 并调用 `t2.aMethod` 方法：

	t2 = Thing2.new( "A Thing2", "a Thing2 thing of great beauty" )
	t2.aMethod( "A New Thing2", "a new Thing2 description" )

仔细看看输出，你会看到即使 `t2` 是一个 Thing2 对象，首先被调用的却是 Thing 类的 `initialize` 方法。想理解为什么会这样，看看 Thing2 类的 `initialize` 方法的代码：

	def initialize(aName, aDescription)
	  super
	  @fulldescription = "This is #{@name}, which is #{@description}"
	  puts("Thing2.initialize: #{self.inspect}\n\n")
	end

这里使用了 `super` 关键字调用了 Thing2 类的祖先类或者说"超类" 的 `initialize` 方法。你可以从声明中看到 Thing2 类的超类是 Thing 类：

	class Thing2 < Thing

在 Ruby 中， 当 `super` 关键字本身单独使用时（即不带任何参数），它将会把当前方法（这里是 `Thing2.initialize`）所有参数传递给在其超类中的同名方法（这里是 `Thing.initialize`）。或者，你可以显式的指定 `super` 后的参数列表。因此，在本例中以下代码将具有相同的效果：

	super(aName, aDescription)

虽然允许单独使用 `super` 关键字，但是通常为了清楚起见，应该明确指定要传递给超类方法的参数列表。无论如何，如果你想要传递有限的参数，则需要一个明确的参数列表。例如，Thing2 的 `aMethod` 方法仅将 `Name` 参数传递给其超类 Thing1 的 `initialize` 方法：

这解释了为什么 `@description` 变量在 `Thing2.aMethod` 方法被调用时为何不会改变。

现在，你看看 Thing3 类，你会看到这增加了一个更多的变量，`@value`。`initialize` 方法传递了两个参数 `aName` 和 `aDescription` 给它的超类 Thing2。反过来，正如我们所看到的，Thing2 类的 `initialize` 同样地将这些参数传递给了它的超类 Thing 的 `initialize` 方法。

程序运行时，在提示符下输入 `3` 以查看输出。这是此次执行的代码：

	t3 = Thing3.new("A Thing 3", "a Thing3 full of Thing and Thing2iness",500)
	t3.aMethod( "A New Thing3", "and a new Thing3 description",1000)

注意执行流程是如何在类层次结构中向上传递的，在 Thing 中的 `initialize` 和 `aMethod` 方法在执行前匹配 Thing2 和 Thing3 中的同名方法。

到目前为止，我做的例程中它们并没有强制重写超类的方法。这只有当你想添加一些新行为的时候才会需要。Thing4 省略了 `initialize` 方法，但实现了 `aMethod` 方法。

输入 `4` 执行下面的代码：

	t4 = Thing4.new( "A Thing4", "the nicest Thing4 you will ever see", 10 )
	t4.aMethod

当你运行它，发现第一个可用的 `initialize` 被调用时， Thing4 对象被创建。这恰好是 `Thing3.initialize`，同时也会再次调用其祖先类 Thing2 和 Thing 的 `initialize` 方法。然而，Thing4 实现的 `aMethod` 方法没有调用它的超类方法，因此其它祖先类中的方法会被忽略，这里将立即执行。

最后，Thing5 继承自 Thing4，不会引入任何新的数据或方法。在提示符处输入 `5` 以执行以下代码：

	t5 = Thing5.new("A Thing5", "a very simple Thing5", 40)
	t5.aMethod

这次你会看到，调用 `new` 方法会导致 Ruby 回溯类层次结构，直到找到第一个 `initialize` 方法。而这存在于 Thing3 中（同样也是 Thing2 和 Thing3 的初始化方法）。然而，`aMethod` 方法的第一个实现在 Thing4 中，它没有调用 `super`，所以从这里就结束了。

<div class="code-file clearfix"><span>superclasses.rb</span></div>

<div class="note">
	<p>
		所有的 Ruby 类都是继承自 Object 类。
	</p>
	<p>
		Object 类本身没有超类，任何尝试获取它的超类行为都会返回 nil 。
	</p>

	begin
	  x = x.superclass
	  puts(x)
	end until x == nil
</div>

### 类中的常量

有时可能需要访问在类中声明的常量（以大写开头）。让我们假设你有下面这个类：

<div class="code-file clearfix"><span>classconsts.rb</span></div>

	class X
	  A = 10

	  class Y
	  end
	end

为了访问常量 `A`，你需要使用特殊范围运算符 `::`，像这样：

	X::A

类名是常量，所以这个操作符允许你访问其他类中的类，这使得可以从“嵌套”类创建对象，如类 `X` 中的类 `Y`：

	ob = X::Y.new

### 局部类

在 Ruby 中，定义一个类在同一个地方是不必要的，如果你愿意，你可以在程序的不同地方定义同一个类。当一个类派生自特定的超类，每个后续的类定义部分使用 `<` 操作符指定超类是可选的。

这里我创建了两个类，A 以及 派生自 A 的 B：

<div class="code-file clearfix"><span>partial_classes</span></div>

	class A
	  def a
		puts( "a" )
	  end
	end

	class B < A
	  def ba1
		puts( "ba1" )
	  end
	end

	class A
	  def b
		puts( "b" )
	  end
	end

	class B < A
	  def ba2
		puts( "ba2" )
	  end
	end

现在，如果我创建一个 B 对象，A 和 B 的所有方法都是可用的：

	ob = B.new
	ob.a
	ob.b
	ob.ba1
	ob.ba2

你还可以将功能添加到 Ruby 的标准类中，例如 Array：

	class Array
	  def gribbit
		puts( "gribbit" )
	  end
	end

这会将 `gribbit` 方法添加到 Array 类中，以便现在可以执行以下代码：

	[1,2,3].gribbit

# 第三章

***

## 字符串和范围

到目前为止，我已经在我的程序中使用了很多字符串（String）。事实上，在本书中的第一个程序中就有字符串。这里再给出来：

	puts 'hello world'

在第一个程序中字符串使用单引号作为分隔符，我在第二个程序中则使用了双引号来替换：

	print('Enter your name: ')
	name = gets()
	puts("Hello #{name}")

<div class="code-file clearfix"><span>1strings.rb</span></div>

双引号字符串要比单引号字符串做更多的工作。尤其地，即便字符串是代码它也有能力去执行。要执行代码的话，请使用 `#` 字符与大括号将其包含进去。

在上面的示例中，`#{name}` 在双引号字符串中指示 Ruby 获取 `name` 变量的值并将其插入到字符串中。所以，如果 `name` 等于 "Fred"，将会显示 "Hello Fred"。**1strings.rb** 示例程序提供了双引号字符串中嵌入式表达式的更多示例。

双引号字符串不仅能够执行获取属性或者变量的值，例如 `ob.name`，还有诸如 `2*3` 的表达式，方法调用 `ob.ten`（**ten** 是一个方法名）以及转义字符换行符 "\n" 和制表符 "\t" 都能被识别执行。

单引号字符串则不会执行这些。并且，单引号字符串可以使用反斜杠表示下一个字符仅仅表示其字面意思。当一个单引号字符串中包含单引号时，这是非常有用的：

	'It\'s my party'

假设名为 `ten` 的方法返回值为10，你可以写出下面的代码：

	puts("Here's a tab\t a new line\n a calculation #{2*3} and a method-call #{ob.ten}")

由于这是双引号字符串，因此将执行嵌入式表达式，并显示以下内容：

	Here's a tab	a new line
	a calculation 6 and a method-call 10

接下来，让我们看当使用单引号时会发生什么：

	puts('Here\'s a tab\t a new line\n a calculation #{2*3} and a method-call #{ob.ten}')

这一次，嵌入式表达式将不会被执行，所以将显示：

	Here's a tab\t a new line\n a calculation #{2*3} and a method-call #{ob.ten}

### 用户自定义字符串分隔符

如果由于某些原因，使用单双引号不方便，例如你的字符串包含很多的引号，而你不想总是使用反斜杠去转义，那么你也可以通过其它方式去分割字符串。

<div class="code-file clearfix"><span>2strings.rb</span></div>

双引号的标准替代分隔符是 **%Q** 和 **/** 或者 **%/** 和 **/** ，然而单引号则为 **%q** 和 **/** 。因此：

	%Q/This is the same as a double-quoted string./
	%/This is also the same as a double-quoted string./
	%q/And this is the same as a single-quoted string/

你甚至可以定义自己的字符串分隔符。它们必须是非字母数字字符,可以包含非打印字符，比如换行符和通常在 Ruby 中有特殊含义的字符，例如 `#`。你选择的字符应该放在 **%Q** 或 **%q** 之后，并且应该确保终止字符串的是同样的字符。如果你使用的分隔符是一个开括号，相应的在字符串结尾处应该使用闭括号，像这样：

	%Q[This is a string]

<div class="code-file clearfix"><span>3strings.rb</span></div>

你可以在示例程序 **3strings.rb** 中发现许多种字符串分隔符。不用说，有时候使用一些深奥的字符（比如换行符和星号）分割字符串很有用，但在许多情况下这些方式的缺点可能会掩盖掉其优点。

### 反引号

一个其它类型的字符串值得特别提及：一个由反引号括起来的字符串——也就是在键盘左上角的向内指向的引号字符 <code>`</code>。

Ruby 认为任何由反引号括起来的都是一个可以使用 `print` 或 `puts` 方法传递给操作系统执行的命令。到目前为止，你可能已经猜到 Ruby 提供了不仅仅一种方式去实现这些。事实证明，`%x/some command/` 与 <code>`somecommand`</code> 具有相同的效果，当然 `%x{some command}` 也是如此。例如，在 Windows 操作系统上，如下所示的三行代码都是将命令 **dir** 传递给操作系统执行，显示目录列表：

<div class="code-file clearfix"><span>4backquotes.rb</span></div>

	puts(`dir`)
	puts(%x/dir/)
	puts(%x{dir})

你也可以在双引号字符串中嵌入命令，如下所示：

	print( "Goodbye #{%x{calc}}" )

如果你这么做，要小心的是，命令首先会被执行。你的 Ruby 程序会进行等待，直到开始的进程终止。在这种情况下，计算器将先弹出来。你可以做一些计算，只有当你关闭计算器的时候，字符串 “Goodbye” 才会显示。

### 字符串处理

在结束字符串这个话题之前，我们将快速查看一看字符串的处理操作。

#### 连接

<div class="code-file clearfix"><span>string_concat.rb</span></div>

你可以使用 `<<` 或 `+`，或者说在中间放置一个空格来连接字符串。这里有三个字符串连接示例，在每一种情况下 `s` 都被赋值为字符串 “Hello World” ：

	s = "Hello " << "world"
	s = "Hello " + "world"
	s = "Hello " "world"

但是请注意，当你使用 `<<` 方法时，你可以直接附加一个整数（0到255），而不必先将它转换为字符串；当使用 `+` 或者空格时，整数必须使用 `to_s` 方法先转换为字符串。

<div class="note">
<p class="h4" style="font-weight: bold;">关于逗号</p>
<p>
	你有时可能会看到 Ruby 代码使用逗号分割来分割字符串或者其它类型数据。在某些情况下，这些逗号似乎有连接字符串的效果。例如，下面的代码一眼看上去似乎是创建和显示了一个由三个子字符串加整数组成的字符串：
</p>

	s4 = "This ", "is", " not a string!", 10
	print("print (s4):", s4, "\n")

<p>
	事实上，用逗号分割的列表是创建了一个数组——一个基于字符串的有序列表。<b>string_concat.rb</b> 程序包含的示例证明了这一点。
</p>
<p>
	请注意，当你将一个数组传递给一个方法比如<code>puts</code>，数组中的每个元素将被单独处理。你可以像下面一样将<code>x</code>传递给<code>puts</code>：
</p>

	puts(x)

<p>在这种情况下，输出将会是：</p>

	This
	is
	not a string!
	10

<p>我们将在下一章中更深入的研究数组。</p>
</div>

#### 字符串赋值

Ruby 的 String 类提供了许多有用的字符串处理方法。大多数的方法将会创建一个新的字符串对象。例如，在下面的代码中，第二行左侧的 `s` 与右侧的 `s` 不是同一个对象：

	s = "hello world"
	s = s + "!"

<div class="code-file clearfix"><span>string_assign.rb</span></div>

一些字符串方法实际上是在不创建新的字符串对象的情况下改变其本身。这些方法通常以感叹号结尾（例如，`capitalize!`）。

如果有疑问，可以使用 `object_id` 方法来检查对象。我已经在 **string_assign.rb** 程序中提供了几个不需要创建新字符串对象的操作示例。运行它们并且在字符串操作执行完之后检查 `s` 的 `object_id`。

#### 字符串索引

你可以将字符串视为一个字符数组，并使用方括号以及索引查找在该数组中特定索引处的字符。Ruby 中的字符串和数组索引是从 0 开始的。所以，要将字符串 `s` “Hello world” 中的 “e” 替换为 “a”，你应该在索引 1 处赋值一个新的字符：

	s[1] = 'a'

但是，如果你要在一个字符串中使用索引查找特定位置的字符，Ruby 不会返回这个字符本身，返回的是该字符的 ASCII 值：

	s = "Hello world"
	puts(s[1])  # prints out 101 – the ASCII value of 'e'

为了获得实际的字符，可以这么做：

	s = "Hello world"
	puts(s[1,1])  # prints out 'e'

这将告诉 Ruby 将字符串中索引 1 处的字符返回。如果你想要返回索引 1 处开始的3个字符，你可以输入：

	puts(s[1,3])  # prints "ell"

这告诉 Ruby 返回从索引 1 处开始接下来的3个字符。或者，你也可以使用双点符号 “范围（range）”来表示:

	puts(s[1..3])  # also prints "ell"

<div class="note">
	关于范围（Ranges），你可以参阅本章末尾的<strong>深入探索</strong>部分。
</div>

字符串还可以使用负值索引，在这种情况下，-1 代表最后一个字符的位置，并且可以再次指定要返回的字符数：

	puts(s[-1,1])  # prints 'd'
	puts(s[-5,1])  # prints 'w'
	puts(s[-5,5])  # prints "world"

<div class="code-file clearfix"><span>string_index.rb</span></div>

使用减号指定范围时，起始和终止索引必须均用负值：

	puts(s[-5..5])  # this prints an empty string!
	puts(s[-5..-1])  # prints "world"

<div class="code-file clearfix"><span>string_methods.rb</span></div>

最后，你可能需要尝试一些可用于操作字符串的标准方法。这些方法包括改变字符串大小写，反转字符串，插入子字符串，删除重复字符等等。我在 **string_methods.rb** 提供了几个示例。

#### 删除换行符 chop 与 chomp

一些方便的字符串处理方法值得提及一下。`chop` 与 `chomp` 方法可用于从字符串的末尾删除字符。`chop` 方法会返回一个字符串，这个字符串是删除最后一个字符得到的，但如果在字符串末尾处发现了回车符和换行符 “\r\n” 则删除一个回车符或换行符之后返回字符串。`chomp` 方法返回一个字符串，这个字符串是删除了末尾一个回车符或换行符得到的。

这些方法在你需要删除用户输入或者文件输入的换行符时是非常有用的。例如，当你使用 `gets` 方法读取一行文本时，它将返回该文本以及在末尾的“记录分隔符”，默认地通常是换行符。

<div class="note">
	<p class="h4" style="font-weight: bold;">记录分隔符- $/</p>
	<p>
		Ruby 预定义了一个变量 <code>$/</code> 来作为“记录分隔符”。这个变量被用于一些方法中，例如 <code>gets</code> 和 <code>chomp</code> 中。<code>gets</code> 方法读入一个字符串时将会包含一个记录分隔符。<code>chomp</code> 方法将会返回一个字符串，该字符串末尾存在记录分隔符时删除后返回，不存在时直接返回未修改的原始字符串。如果你愿意，可以重新定义记录分隔符，如下所示：
	</p>

	$/="*"  # the "*" character is now the record separator

当你重新定义了记录分隔符之后，新的字符或字符串现在将会在一些方法中被使用，例如 `gets` 和 `chomp` 方法。例如：

	$/= "world"
	s = gets()  # user enters "Had we but world enough and time…"
	puts(s)  # displays "Had we but world"

</div>

你可以使用 `chop` 和 `chomp` 方法来删除换行符。在大多数情况下，`chomp` 方法是最好的，因为它不会删除最后的字符，除非末尾是记录分隔符，而 `chop` 方法无论最后一个字符是什么都会删除。这里是一些示例：

<div class="code-file clearfix"><span>chop_chomp.rb</span></div>

	# Note: s1 includes a carriage return and linefeed
	s1 = "Hello world
	"
	s2 = "Hello world"
	s1.chop  # returns "Hello world"
	s1.chomp # returns "Hello world"
	s2.chop  # returns "Hello worl" – note the missing "d"!
	s2.chomp # returns "Hello world"

`chomp` 方法允许你指定要用做分隔符的字符或字符串：

	s2.chomp("rld")  # returns "Hello wo"

#### 格式化字符串

Ruby 提供了 `v` 方法来打印包含以百分号 **%** 开头的说明符的“格式化字符串”。格式化字符串之后可以是一个或多个用逗号分隔的数据项；数据项的列表应与格式说明符的类型相匹配。实际数据项替换字符串中匹配的说明符，并相应地进行格式化。这是一些常见的格式说明符：

	%d – decimal number
	%f – floating point number
	%o – octal number
	%p – inspect object
	%s – string
	%x – hexadecimal number

你可以通过在浮点数说明符 **%f** 前面放置一个点号来控制浮点精度。例如，这将显示两位浮点值：

	printf("%0.02f", 10.12945)  # displays 10.13

## 深入探索

### 范围

在 Ruby 中，Range 是一个表示指定的起始和终止值之间一组数的类。通常，使用整数定义范围，但也可以使用其它有序值（比如浮点数或字符）来定义范围。值可以是负的，尽管如此，你应该注意起始值应该始终小于终止值。这有一些示例：

<div class="code-file clearfix"><span>ranges.rb</span></div>

	a = (1..10)
	b = (-10..-1)
	c = (-10..10)
	d = ('a'..'z')

你也可以使用三个点而不是两个点指定范围：这将创建一个不包含终止值范围：

	d = ('a'..'z')   # this two-dot range = 'a'..'z'
	e = ('a'...'z')   # this three-dot range = 'a'..'y'

你可以使用 `to_a` 方法创建一个由范围定义的数的数组：

	(1..10).to_a

请注意，由于两个浮点数之间的数不是有限的，所以 `to_a` 方法并没有为浮点数所定义。

<div class="code-file clearfix"><span>str_range.rb</span></div>

你甚至可以创建字符串范围，你需要非常的小心，这样做的话最终可能会超出你的预想。例如，看看你是否知道这个范围指定的值：

	str_range = ('abc'..'def')

一眼看去，从"abc"到"def"这个范围的值可能不多。实际上，这个范围之间的值不少于 2110 个！它们具体如下："abc"、"abd"、"abe"等等，直到"a"结束；然后再从"b"开始，"baa"、"bab"、"bac"等等。准确的说，这种范围是不常用的，最好要非常小心或者直接不使用。

### 范围迭代器

你可以使用范围从起始值迭代到终止值。例如，以下是将数字从1到10打印出来的方法：

<div class="code-file clearfix"><span>for_to.rb</span></div>

	for i in (1..10) do
  	  puts( i )
	end

### HereDocs

虽然你可以在单双引号之间写跨行的长字符串，但许多 Ruby 程序员更喜欢使用名为"heredoc"这种替代类型的字符串。hererdoc 是一个以特定结束标记开始的文本块，这可以是你自己选择的结束标记。这里，我选择了 EODOC 作为结束标记：

<div class="code-file clearfix"><span>heredoc.rb</span></div>

	hdoc1 = <<EODOC

这告诉 Ruby，直到遇到结束标记时所有行都是单个字符串。这个字符串被赋值给 `hdoc1` 变量。这是一个完整的例子：

	hdoc1 = <<EODOC
	I wandered lonely as a #{"cloud".upcase},
	That floats on high o'er vale and hill...
	EODOC

默认情况下，heredocs 被视为双引号字符串，所以表达式 `#{"cloud".upcase}` 将会被执行。如果你想要一个 heredoc 被视为单引号字符串，在单引号之间指定结束标记：

	hdoc2 =  <<'EODOC'
	I wandered lonely as a #{"cloud".upcase},
	That floats on high o'er vale and hill...
	EODOC

默认情况下，heredoc 的结束标记必须与左侧对齐。如果你想缩进，应该在指定结束标记时使用 `<<-` 来代替 `<<`：

	hdoc3 = <<-EODOC
	I wandered lonely as a #{"cloud".upcase},
	That floats on high o'er vale and hill...
    	EODOC

你可以自己选择适当的结束标记，使用保留字也是可以的（但这似乎不是明智的做法！）。

	hdoc4 = <<def
	I wandered lonely as a #{"cloud".upcase},
	That floats on high o'er vale and hill...
	def

一个被赋值为 heredoc 的变量也可以像其它字符串变量一样使用：

	puts(hdoc1)

### 字符串字面量

如本章前面所述，你可以选择使用 **%q/** 和 **/** 来分割单引号字符串，以及使用 **%Q/** 和 **/** 来分割双引号字符串。

Ruby 提供了类似的方法来定义反引号字符串、正则表达式（regular expressions）、符号（symbols）和数组（arrays）的分隔符。以这种方式定义字符串数组特别的有用，这样避免了为每个元素都要输入字符串分隔符。这里是一些字符串字面量分隔符的参考：

	%q/   /
	%Q/   /
	%/   /
	%w/   /
	%W/   /
	%r|   |
	%s/   /
	%x/   /

请注意，你可以选择要使用的分隔符。我选择了 **/**，而正则表达式则使用了 **|**（因为 **/** 是一个常规的正则表达式分隔符），同样地我也可以使用方括号、星号、& 或者其它符号（例如 `%W*dog cat＃{1 + 2}*` 或 `%s&dog&`）。这是一些示例：

<div class="code-file clearfix"><span>literals.rb</span></div>

	p %q/dog cat #{1+2}/  #=> "dog cat \#{1+2}"
	p %Q/dog cat #{1+2}/  #=> "dog cat 3"
	p %/dog cat #{1+2}/  #=> "dog cat 3"
	p %w/dog cat #{1+2}/  #=> ["dog", "cat", "\#{1+2}"]
	p %W/dog cat #{1+2}/  #=> ["dog", "cat", "3"]
	p %r|^[a-z]*$|  #=> /^[a-z]*$/
	p %s/dog/ #=> :dog
	p %x/vol/ #=> " Volume in drive C is OS [etc...]"

# 第四章

***

## 数组与哈希表

到目前为止，我们一般一直在使用单个对象。 在这一章中我们将会了解如何创建对象列表。 我们首先来看最常见的列表结构类型——数组。

### 数组（Array）

<div class="code-file clearfix"><span>array0.rb</span></div>

<div class="note">
	<p class="h4" style="font-weight: bold">什么是数组(Array）？</p>

数组是每个元素都可以被索引到的有序集合。在 Ruby 中，（与许多其它语言不同）一个 Array 可以包含不同类型的元素，例如字符串、整数和浮点数，甚至是方法的返回值。

	a1 = [1, 'two', 3.0, array_length(a0)]

数组中的第一个项目的索引为 0，这意味着数组中的最后一个元素的索引为数组中元素总数减 1。如上所示，给定一个数组 `a1`，然后访问其第一个和最后一个元素：

	a1[0]  # returns 1st item (at index 0)
	a1[3]  # returns 4th item (at index 3)

</div>

我们已经使用了数次数组，例如在第二章的 **2adventure.rb** 中我们使用了一个数组来存储房间的地图：

	mymap = Map.new([room1, room2, room3])

#### 创建数组

与其他许多编程语言一样，Ruby 使用方括号来界定数组。你可以使用逗号分隔多个值很容易的创建一个数组，并将其赋给一个变量。

	arr = ['one','two','three','four']

<div class="code-file clearfix"><span>array1.rb</span></div>

与 Ruby 中其它的东西一样，数组也是对象。你可能会猜到，正如字符串一样，它由 `Array` 类定义，索引从 0 开始。你可以将索引放在方括号中得到相应元素，如果索引无效，将会返回 `nil`：

	arr = ['a', 'b', 'c']
	puts(arr[0]) # shows "a"
	puts(arr[1]) # shows "b"
	puts(arr[2]) # shows "c"
	puts(arr[3]) # nil

<div class="code-file clearfix"><span>array2.rb</span></div>

在数组中混合数据类型是被允许的，甚至也可以包含一些产生值的表达式。假设你创建了这个方法：

	def hello
	  return "hello world"
	end

你可以这样声明一个数组：

	x = [1+2, hello, `dir`]

这里，第一个元素是整数 3，第二个元素是字符串 “hello world”（由 `hello` 方法返回）。如果你在 Windows 上运行，第三个数组将是一个包含目录列表的字符串。这是因为 <code>`dir`</code> 反引号字符串是可以被操作系统执行的命令（见第三章）。因此，数组中的最后一个位置将被 **dir** 命令返回的文件名字符串填充。如果你是运行在不同的操作系统上，这时候应该替换一个合适的命令。

<div class="code-file clearfix"><span>dir_array.rb</span></div>

<div class="note">
	<p><b>创建一个文件名的数组</b></p>
	<p>
		许多 Ruby 类有返回值为数组的方法。例如，Dir 类用来执行在磁盘上目录操作，拥有<code>entries</code>方法。传递给该方法一个目录名称，将会返回一个包含文件名列表的数组。
	</p>

	Dir.entries('C:\\') # returns an array of files in C:\

</div>

如果你要创建一个包含单引号字符串的数组，但是输入所有引号又很麻烦，一种简洁的方式就是使用 `%w` 和将不带引号的字符串以空格分隔放入圆括号中的形式表示（或者使用 `%W` 表示双引号字符串，如第三章所述）：

<div class="code-file clearfix"><span>array2.rb</span></div>

	y = %w(this is an array of strings)

你也可以使用通常的构造器来（new）创建一个数组，你可以同时将一个整数传递给构造方法，来创建一个特定大小（每个元素值为 `nil`）的数组。当然，你也可以传递两个参数，第一个参数指定数组大小，第二个参数指定要放入数组中的元素：

	a = Array.new # an empty array
	a = Array.new(2) # [nil,nil]
	a = Array.new(2, "hello world")  # ["hello world", "hello world"]

#### 多维数组

要创建一个多维数组，你可以先创建一个数组，然后再将其它数组作为元素放入该数组中。例如，这将创建一个包含两个数组元素的数组。

	a = Array.new(2)
	a[0]= Array.new(2,'hello')
	a[1]= Array.new(2,'world')

<div class="note">
	<p>
		你还可以将数组对象作为参数传递给数组的 <code>new</code> 方法来创建多维数组。不过要注意，虽然在传递数组参数时不适用圆括号是可以的，但你如果不在方法名和参数之间加入空格，Ruby 将认为这是一个语法错误，所以在传递参数时，请一定要使用圆括号。
	</p>
	<p>
		也可以使用方括号将数组嵌套在一起。这是创建了一个包含四个数组元素的 2 维数组，每个数组元素包含四个整数元素：
	</p>
</div>

	a = [   [1,2,3,4],
			[5,6,7,8],
			[9,10,11,12],
			[13,14,15,16]  ]

在上面显示的代码中，我将四个子数组分别放在不同行中，这并不是强制性的，但这样的写法有助于构建多元化的数组结构，通过将每个子数组显示为一行，类似电子表格中的行。当谈到数组中的数组时，可以很方便的将每个子数组引用为外层数组的行。

<div class="code-file clearfix"><span>multi_array.rb</span></div>

有关更多的使用多维数组的示例，请加载 **multi_array.rb** 程序。首先创建了包含另外两个数组的多维数组 `multiarr`，而这两个数组中在多维数组中的索引分别为 0 和 1。

	multiarr = [['one','two','three','four'],[1,2,3,4]]

#### 数组迭代

你可以使用 `for` 循环来遍历数组访问数组中的元素，循环将会遍历位于索引 0 和 1 处的子数组两个元素：

	for i in multiarr
	  puts(i.inspect)
	end

将会输出：

	>["one", "two", "three", "four"]
	[1, 2, 3, 4]

那么，你如何子数组中的元素呢？如果元素数量是固定的，你可以指定多个不同迭代变量，这时将会匹配子数组中对应索引位置的元素。

这两个子数组有四个元素，所以你可以使用四个迭代变量：

	for (a,b,c,d) in multiarr
	  print("a=#{a}, b=#{b}, c=#{c}, d=#{d}\n" )
	end

<div class="note">
	<p class="h4"><b>迭代器和 for 循环</b></p>

`for` 循环中的代码对每一个迭代元素进行执行，语法可以总结如下：

	for <one or more variables> in <expression> do
	  <code to run>
	end

当提供多个变量时，会将这些变量传递给代码里面的 `for...end` 块，如同给方法传递参数一样。在这里，你可以将 `(a,b,c,d)` 作为四个参数进行初始化，每一次匹配 `for` 循环所遍历的多维数组 `multiarr` 的每一行：

	for (a,b,c,d) in multiarr
	  print("a=#{a}, b=#{b}, c=#{c}, d=#{d}\n" )
	end

我们将在下一章中更深入地研究 `for` 循环和其他迭代器。
</div>

<div class="code-file clearfix"><span>multi_array2.rb</span></div>

您还可以使用 for 循环来单独迭代每个子数组中的所有元素：

	for s in multiarr[0]
	  puts(s)
	end
	for s in multiarr[1]
	  puts(s)
	end

以上两种技术（多个迭代变量和多个 `for` 循环）都需要满足两个条件：a）你需要知道多维数组有几行或者几列；b）每个子数组都包含相同数量的元素。

为了更灵活的迭代多维数组，你可以使用嵌套的 `for` 循环。一个外部循环遍历每一行，内部循环则遍历当前行中的元素。这种技术在子数组有不同数量元素时都可以正常运行：

	for row in multiarr
	  for item in row
		puts(item)
	  end
	end

#### 数组索引

与字符串一样（参见第三章），你可以使用负数从末尾开始索引元素，也可以使用范围来索引：

<div class="code-file clearfix"><span>array_index.rb</span></div>

	arr = ['h','e','l','l','o',' ','w','o','r','l','d']

	print( arr[0,5] )  #=> "hello"
	print( arr[-5,5 ] ) #=> "world"
	print( arr[0..4] ) #=> "hello"
	print( arr[-5..-1] ) #=> "world"

注意，与字符串一样，当提供两个整数以返回一个来自数组的连续几项的元素，第一个整数作为起始索引，第二个则是元素数目（并非终止索引）：

	arr[0,5]  # returns 5 chars - ["h", "e", "l", "l", "o"]

<div class="code-file clearfix"><span>array_assign.rb</span></div>

你也可以利用索引来进行数组中元素的赋值，例如，我们首先创建一个空的数组，然后对索引为 0，1 和 3 的位置进行赋值，而没有赋值的索引为 2 的位置将填充一个默认值 `nil`：

	arr = []

	arr[0] = [0]
	arr[1] = ["one"]
	arr[3] = ["a", "b", "c"]

	# arr now contains:
	# [[0], ["one"], nil, ["a", "b", "c"]]

同样地，你也可以使用范围，负索引等：

	arr2 = ['h','e','l','l','o',' ','w','o','r','l','d']

	arr2[0] = 'H'
	arr2[2,2] = 'L', 'L'
	arr2[4..6] = 'O','-','W'
	arr2[-4,4] = 'a','l','d','o'

	# arr2 now contains:
	# ["H", "e", "L", "L", "O", "-", "W", "a", "l", "d", "o"]

#### 数组拷贝

<div class="code-file clearfix"><span>array_copy.rb</span></div>

注意，如果你使用赋值运算符 `=` 将一个数组变量赋值给另一个变量时，你实际上只是将该数组的引用赋值给了另一个变量，并没有真正复制该数组。你可以使用 `clone` 方法来为该数组创建一个副本：

	arr1 = ['h','e','l','l','o',' ','w','o','r','l','d']
	arr2 = arr1
		# arr2 is now the same as arr1. Change arr1 and arr2 changes too!
	arr3 = arr1.clone
		# arr3 is a copy of arr1. Change arr1 and arr2 is unaffected

#### 数组比较

<div class="code-file clearfix"><span>array_compare.rb</span></div>

关于比较运算符 `<=>` 需要额外说几句。这里我们比较两个数组，称之为 `arr1` 和 `arr2`；如果 `arr1` 小于 `arr2`，则返回 -1; 如果 `arr1` 和 `arr2` 相等，它返回 0; 如果 `arr2` 大于 `arr1`，则返回 1。但是，Ruby 是如何确定一个数组是“大于”还是“小于”另一个数组？事实证明，Ruby 会将两个数组中相同索引位置上的元素进行比较。当遇到两个元素值不相等时，将返回其比较结果。换句话说，如果进行了这种比较：

	[0, 10, 20] <=> [0, 20, 20]

将会返回 -1（第一个数组小于第二个数组），因为在索引为 1 时第一个数组中的值（10）小于第二个数组中的值（20）。

如果要比较字符串数组，则对字符串的 ASCII 值进行比较。如果一个数组比另一个数组长，并且两个数组中的元素都相等，那么较长的数组被认为“更大”。但是，如果短数组中的元素值有比长数组的元素值大的，则认为短数组更大。

#### 数组排序

<div class="code-file clearfix"><span>array_sort.rb</span></div>

`sort` 方法使用比较运算符 `<=>` 来比较相邻的数组元素。该运算符在许多 Ruby 类中都有定义，包括数组（Array）、字符串（String）、浮点数（Float）、日期（Date）和 Fixnum。但是，sort 运算并没有为所有类定义（也就是说，派生出其它所有类的 Object 类中 sort 没有定义）。其中令人遗憾的是，它不能用于对包含 `nil`  值的数组进行排序。但是，这个可以通过定义你自己的排序例程来解决。通过给 sort 方法发生一个块（block）来实现。我们将在第 10 章详细介绍块（blocks）。现在，只需要知道这里的块（block）是一段决定了 `sort` 方法如何进行元素比较的代码就足够了。

这是我的 `sort` 例程：

	arr.sort {
	  |a,b|
		a.to_s <=> b.to_s
	}

这里的 `arr` 代表一个数组，变量 `a` 和 `b` 代表两个连续的元素。我已经使用 `to_s` 方法将每个变量转换成了字符串；这样就会将 `nil` 转换成一个排序时认为更小的空字符串。注意，虽然我的 block 定义了数组的排序顺序，但不会改变数组元素自身。所以，`nil` 依然为 `nil`，整数（integers）依然为整数。字符串的转换操作只用于实现元素比较，不会改变数组元素。

#### 比较值

这个比较运算符 `<=>`（实际上是一个方法）在 Ruby 名为 Comparable 的模块中定义的。现在，你可以将模块（module）视为一种可重用的排序代码库。我们将在第 12 章中更详细地研究模块。

你可以在自己的类中包含（include）Comparable 模块。这样你就可以覆盖掉 `<=>` 方法，去实现特定类型对象之间比较的准确方式。例如，你可能想子类化 Array，以便仅基于两个数组的长度进行比较，而不是数组中的每个元素值（如前所述，这是默认的）。下面来看看如何做到这一点：

<div class="code-file clearfix"><span>comparisons.rb</span></div>

	class MyArray < Array
	  include Comparable

	  def <=> ( anotherArray )
		self.length <=> anotherArray.length
	  end
	end

现在，你可以初始化两个 MyArray 对象：

	myarr1 = MyArray.new([0,1,2,3])
	myarr2 = MyArray.new([1,2,3,4])

你可以使用在 MyArray 类中定义的 `<=>` 方法来进行比较：

					   # Two MyArray objects
	myarr1 <=> myarr2  # returns 0

返回 0 表示两个数组相等（因为我们的 `<=>` 方法仅根据长度来进行比较是否相等）。另一方面，我们也可以用相同的整数初始化两个标准数组（Arrays），用 Array 类自己的 `<=>` 方法来执行比较：

				   # Two Array objects
	arr1 <=> arr2  # returns -1

这里的 -1 代表第一个数组小于第二个数组，因为 Array 类的 `<=>` 比较得出 `arr1` 中的元素数值小于 `arr2` 中相同索引位置上的元素数值。

但是，如果你想直接使用“小于”、“等于”、“大于”这些常规运算符进行比较：

	<   # less than
	==  # equal to
	>   # greater than

在 MyArray 类中，我们可以在不编写任何额外代码的情况下进行比较。这是由于已包含在 MyArray 类中的 Comparable 模块自动提供了这三种比较方法; 每种方法都根据 `<=>` 方法的定义进行比较。因为我们的 `<=>` 方法基于元素数量进行判断，所以 '<' 方法在第一个数组较短时返回 true，`==` 在两个数组长度相等时返回 true，`>` 方法在第二个数组较短时返回 true。

	p( myarr1 < myarr2 )  #=> false
	p( myarr1 == myarr2 ) #=> true

但是，标准 Array 类不包含 Comparable 模块。因此，如果您尝试使用 `<`，`==` 或 `>` 比较两个普通数组，Ruby 将显示一个错误消息，告诉您该方法未定义。

事实证明，很容易将这三种方法添加到 Array 的子类中。 所有你要做的就是包含（include）Comparable 模块，像这样：

	class Array2 < Array
	  include Comparable
	end

现在 Array2 类将基于 Array 的 `<=>` 方法进行比较，也就是比较数组中的每一个元素，而不是数组的长度。假设有 Array2 对象，`arr1` 和 `arr2`，用之前我们用于 `myarr1` 和 `myarr2` 的同样的数组进行初始化，我们可以看到这些结果：

	p( arr1 < arr2 )  #=> true
	p( arr1 > arr2 )  #=> false

#### 数组方法

<div class="code-file clearfix"><span>array_methods.rb</span></div>

一些标准数组方法会修改数组本身，而不是返回修改了的数组副本。这些不仅包括那些标有末尾感叹号的方法，例如 `flatten!` 和 `compact!`，还有 `<<` 方法通过添加右边的数组到左边的数组来改变原数组，`clear` 方法会移除数组中的所有元素，以及 `delete` 和 `delete_at` 方法将会移除所选元素。

### 哈希表（Hash）

虽然数组提供了一种好的方式来通过数字索引集合中的元素，但有时候以其它方式进行索引会更方便。例如，如果你正在创建一个食谱集合，那么按名称索引每个食谱会更有意义，例如 “Rich Chocolate Cake” 和 “Coq au Vin”，而不是数字 23、87 等等。

Ruby 有一个类可以让你做到这一点。它被称为哈希（Hash）。在其它语言中被称为字典（Dictionary）。就像真正的字典一样，条目由一些唯一键（在字典中，这将是一个单词）索引，该键与一个值相关联（在字典中，这将是单词的定义）。

#### 创建哈希表

<div class="code-file clearfix"><span>hash1.rb</span></div>

就像数组一样，您可以通过创建 Hash 类的新实例来创建一个哈希表：

	h1 = Hash.new
	h2 = Hash.new("Some kind of ring")

上面的两个例子都创建了一个空的哈希表。Hash 对象始终具有一个默认值，即在给定索引处未找到特定值时返回的值。在这些例子中，`h2` 用 “Some kind of ring” 作为初始化的默认值，`h1` 没有指定初始化的默认值，因此其默认值为 `nil`。

创建 Hash 对象后，可以使用类似数组的语法向其添加元素，也就是说将索引放在方括号中，使用 `=` 号来赋值。这里明显的区别是，对于数组来说索引必须是整数; 而对于哈希表，它可以是任何唯一的数据项：

	h2['treasure1'] = 'Silver ring'
	h2['treasure2'] = 'Gold ring'
	h2['treasure3'] = 'Ruby ring'
	h2['treasure4'] = 'Sapphire ring'

通常，键（key）可以是数字，或者如上面的代码中那样是字符串。 但是，原则上键可以是任何类型的对象。

<div class="note">
	<p class="h4"><b>唯一键（keys）？</b></p>

给哈希表分配键（key）时要小心。如果你在一个 Hash 中使用了两次同样的键（key），你最终将覆盖原来的值。这就像为数组中的同一索引赋值两次一样。思考这个例子：

	h2['treasure1'] = 'Silver ring'
	h2['treasure2'] = 'Gold ring'
	h2['treasure3'] = 'Ruby ring'
	h2['treasure1'] = 'Sapphire ring'

在这里 “treasure1” 键使用了两次。因此，原来的值 “Silver ring” 被 “Sapphire ring” 替换，该 Hash 为：

	{"treasure1" => "Sapphire ring", "treasure2" => "Gold ring", "treasure3" => "Ruby ring"}
</div>

给定一些类 `X`，以下的赋值是合法的：

	x1 = X.new('my Xobject')
	h2[x1] = 'Diamond ring'

有一种创建哈希表并初始化一些键值对的简写方式。只需要在键（key）后跟 `=>` 和关联的值（value），每个键值对使用逗号分割，整体放在一对花括号中：

	h1 = {  'room1'=>'The Treasure Room',
			'room2'=>'The Throne Room',
			'loc1'=>'A Forest Glade',
			'loc2'=>'A Mountain Stream' }

#### 哈希表索引

要访问值，将键（key）放在方括号中：

	puts(h1['room2']) #=> "The Throne Room"

如果指定不存在的键，则返回默认值。回想一下，之前我们没有为 `h1` 指定默认值，但为 `h2` 指定了默认值：

	p(h1['unknown_room'])  #=> nil
	p(h2['unknown_treasure'])  #=> 'Some kind of ring'

使用 `default` 方法获取默认值，并用 `default=` 方法可以设置默认值（有关 *get* 和 *set* 存取器方法的详细信息，请参见第 2 章）：

	p(h1.default)
	h1.default = 'A mysterious place'

#### 哈希表拷贝

<div class="code-file clearfix"><span>hash2.rb</span></div>

与数组一样，可以将一个 Hash 变量赋值给另一个变量，在这种情况下两个变量将引用相同的 Hash，使用任何一个变量的修改都会影响到该 Hash：

	h4 = h1
	h4['room1'] = 'A new Room'
	puts(h1['room1']) #=> 'A new Room'

如果你希望两个变量引用拥有相同元素的不同的 Hash 对象，使用 `clone` 方法创建一个新副本：

	h5 = h1.clone
	h5['room1'] = 'An even newer Room'
	puts(h1['room1']) #=> 'A new room' (i.e. its value is unchanged)

#### 哈希表排序

<div class="code-file clearfix"><span>hash_sort.rb</span></div>

和数组一样，你可能会发现 Hash 的 `sort` 方法也存在一些问题。该方法期望处理的键（keys）具有相同的数据类型，因此，你将一个使用数字索引和另一个使用字符串索引的数组合并后，你无法对该 Hash 进行正确排序。和数组一样，解决该问题的方法是通过编写一些代码来进行自定义类型的比较并将其传递给 `sort` 方法。你可能会定义一个方法，如下所示：

	def sorted_hash( aHash )
	  return aHash.sort {
		|a,b|
		  a.to_s <=> b.to_s
	  }
	end

这将会根据 Hash 中每个键（key）的字符串（`to_s`）形式进行排序。实际上，Hash 的 `sort` 方法是将 Hash 转换为嵌套数组 *\[key，value\]* 后使用 Array 的 `sort` 方法对它们进行排序。

#### 哈希表方法

<div class="code-file clearfix"><span>hash_methods.rb</span></div>

Hash 类有许多内置方法。例如，哈希表 `aHash` 通过元素的键来删除元素使用 `aHash.delete( someKey )`。测试一个键（key）或者（value）是否存在则使用 `aHash.has_key?( someKey )` 和 `aHash.has_value?( someValue )`。要返回一个使用原始哈希表的值（values）作为键（keys）创建的新哈希表，并将其键作为值使用则通过 `aHash.invert` 方法；返回一个填充了哈希键（keys）或其值（values）的数组使用 `aHash.keys` 和 `aHash.values`，等等。

**hash_methods.rb** 中有许多这些方法的示例。

## 深入探索

### 以数组方式操作哈希表

<div class="code-file clearfix"><span>hash_ops.rb</span></div>

Hash 的 `keys` 和 `values` 方法都返回一个数组，以便您可以使用各种数组方法来处理它们。以下是一些简单的例子：

	h1 = {'key1'=>'val1', 'key2'=>'val2', 'key3'=>'val3', 'key4'=>'val4'}
	h2 = {'key1'=>'val1', 'KEY_TWO'=>'val2', 'key3'=>'VALUE_3', 'key4'=>'val4'}

	p( h1.keys & h2.keys ) 	   # set intersection (keys)
	#=> ["key1", "key3", "key4"]

	p( h1.values & h2.values ) # set intersection (values)
	#=> ["val1", "val2", "val4"]

	p( h1.keys+h2.keys )  	   # concatenation
	#=> [ "key1", "key2", "key3", "key4", "key1", "key3", "key4", "KEY_TWO"]

	p( h1.values-h2.values )   # difference
	#=> ["val3"]

	p( (h1.keys << h2.keys) )  # append
	#=> ["key1", "key2", "key3", "key4", ["key1", "key3", "key4", "KEY_TWO"]]

	p( (h1.keys << h2.keys).flatten.reverse )  # 'un-nest' arrays and reverse
	#=> ["KEY_TWO", "key4", "key3", "key1", "key4", "key3", "key2", "key1"]

### 附加和连接

请注意两种数组连接是有区别的，使用 `+` 是将第二个数组的每个元素添加到第一个数组中，使用 `<<` 是将第二个数组作为第一个数组的最后一个元素添加。

<div class="code-file clearfix"><span>append_concat.rb</span></div>

	a =[1,2,3]
	b =[4,5,6]
	c = a + b  #=> c=[1, 2, 3, 4, 5, 6] a=[1, 2, 3]
	a << b     #=> a=[1, 2, 3, [4, 5, 6]]

另外，`<<` 会改变第一个数组（接收者，receiver），而 `+` 会返回一个新的数组，原数组（接收者，receiver）保持不变。

<div class="note">
	<p class="h4"><b>接受者（Receivers），消息（Messages）和方法（Methods）</b></p>

在面向对象术语中，方法所属的对象被称为接收者 **receiver**。这表示不像面向过程的语言中那样“调用函数”（calling functions），而是将“消息”（messages）发送给对象。例如，消息 `+1` 可能会发送给一个整数（integer）对象，而消息 `reverse` 则可能会发送给一个字符串（string）对象。“接收”消息的对象（receives）试图找到响应消息的方式（即“方法”，`method`）。例如，字符串（string）对象拥有一个 `reverse` 方法，所以它可以响应 `reverse` 消息，而整数（integer）对象没有该方法，也就不能响应。

</div>

在使用 `<<` 方法附加数组后，如果你想将每个附加数组的元素都添加到 receiver 数组，而不是将整个附加数组嵌套在 receiver 数组中，你可以使用 `flatten` 方法：

	a=[1, 2, 3, [4, 5, 6]]
	a.flatten  #=> [1, 2, 3, 4, 5, 6]

### 矩阵和向量

Ruby 提供了 Matrix 类，它包含多个行（rows）和列（columns），其每个值都可以表示为向量（vector）（Ruby 也提供 Vector 类）。Matrices 允许您执行矩阵运算。例如，给出两个 Matrix 对象，`m1` 和 `m2`，你可以在矩阵中添加每个对应单元格的值，如下所示：

<div class="code-file clearfix"><span>matrix.rb</span></div>

	m3 = m1+m2

### Sets

Set 类实现了没有重复值的无序集合（collection）。你可以使用数组来初始化一个 `Set`，在这种情况下，重复的元素将会被忽略：

*例如：*

<div class="code-file clearfix"><span>sets.rb</span></div>

	s1 = Set.new( [1,2,3, 4,5,2] )
	s2 = Set.new( [1,1,2,3,4,4,5,1] )
	s3 = Set.new( [1,2,100] )
	weekdays = Set.new( %w( Monday, Tuesday, Wednesday, Thursday,
			Friday, Saturday, Sunday ) )

你可以使用 `add` 方法来添加新的值：

	s1.add( 1000 )

`merge` 方法可以将两个集合（Set）的值组合在一起：

	s1.merge(s2)

你可以使用 `==` 来判断是否相等。包含相同值的两个集合（sets）（记住在创建集合时将删除重复项）被认为是相等的：

	p( s1 == s2 )  #=> true

# 第五章

***

## 循环（Loop）和迭代器（Iterator）

大部分程序都是与重复行为相关的。也许你希望你的程序发出十次嘟嘟（beep）声，读取一个长文件的更多行直到用户按下某个键，显示一个警告信息。Ruby 提供了许多执行此类重复行为的方式。

### for 循环

在许多编程语言中，当你想将一些代码运行一定的次数时你可以把它放在 `for` 循环中。在大多数语言中，为 `for` 循环可以提供一个初始化的变量作为起始值，该起始值在每个循环中递增 1，循环到直到它满足某个特定的结束值。当满足结束值时，`for` 循环停止运行。这是用 Pascal 写的常规类型的 `for` 循环：

	(* This is Pascal code, not Ruby! *)
	for i := 1 to 3 do
		writeln( i );

<div class="code-file clearfix"><span>for_loop.rb</span></div>

您可能还记得上一章中 Ruby 的 `for` 循环根本不是这样的！我们会给 `for` 循环一个元素列表，而不是一个起始值和结束值，然后逐个迭代它们，将每个元素值依次分配给一个循环变量，直到它到达列表的末尾。

例如，这是一个 `for` 循环，它迭代数组中的项目，并依次显示：

	# This is Ruby code…
	for i in [1,2,3] do
		puts( i )
	end

`for `循环更像是其它编程语言中提供的 'for each' 迭代器。循环迭代的项目不必是整数（integers）。这也可以...

	for s in ['one','two','three'] do
		puts( s )
	end

Ruby 的作者描述 `for` 循环是集合类型，例如 Arrays、Sets、Hashes 和 Strings（字符串实际上是一个字符集合）中实现的 `each` 方法的语法糖（syntax sugar）。为了便于比较，这是上面的 `for` 循环使用 `each` 方法进行重写的：

<div class="code-file clearfix"><span>each_loop.rb</span></div>

	[1,2,3].each do |i|
		puts( i )
	end

正如你所看到的，并没有太大的区别。要将 `for `循环转换为 `each` 迭代器，所要做的就是删除 `for` 和 `in`，并将 `.each` 附加到数组中。然后把迭代变量 `i` 放在 `do` 后的两条竖线中。比较一下其它示例，看看 `for` 循环和 `each` 迭代器的相似程度：

<div class="code-file clearfix"><span>for_each.rb</span></div>

	# --- Example 1 ---
	# i) for
	for s in ['one','two','three'] do
		puts( s )
	end

	# ii) each
	['one','two','three'].each do |s|
		puts( s )
	end

	# --- Example 2 ---
	# i) for
	for x in [1, "two", [3,4,5] ] do puts( x ) end

	# ii) each
	[1, "two", [3,4,5] ].each do |x| puts( x ) end

顺便提一下，请注意，`do` 关键字在跨越多行的 `for` 循环中是可选的，但是当它写在一行上时是必须的：

	# Here the "do" keyword can be omitted
	for s in ['one','two','three']
		puts( s )
	end

	# But here it is required
	for s in ['one','two','three'] do puts( s ) end

<div class="code-file clearfix"><span>for_to.rb</span></div>

<div class="note">
	<p class="h4"><b>如何编写一个“普通（normal）”的 for 循环...</b></p>

如果你习惯了常规类型的 `for` 循环，你可以随时通过使用 Ruby 中的 `for` 循环迭代范围中的值来实现。例如，这显示了如何使用一个 `for` 循环变量从 1 到 10 计数，并在每次循环过程中显示其值：

	for i in (1..10) do
		puts( i )
	end
</div>

<div class="code-file clearfix"><span>for_each2.rb</span></div>

此示例显示了 `for` 和 `each` 两者如何用于迭代范围内的值：

	# for
	for s in 1..3
		puts( s )
	end

	# each
	(1..3).each do |s|
		puts(s)
	end

顺便提一下，要注意在使用 `each` 方法时，范围（range）表达式，例如 `1..3` 必须使用圆括号包围，否则 Ruby 会假设你试图使用整数（一个 Fixnum）作为 `each` 方法的最终值，而不是整个表达式（范围，Range）。range 用在 `for` 循环中时圆括号是可选的。

### 多迭代参数

<div class="code-file clearfix"><span>multi_array.rb</span></div>

你可能还记得在上一章中我们使用了一个带有多个循环变量的 `for` 循环。我们这样做是为了迭代一个多维数组。在每次进入 `for` 循环中，一个变量将被赋值为外层数组中的一行数据（子数组）：

	# Here multiarr is an array containing two "rows"
	# (sub-arrays) at index 0 and 1
	multiarr = [
		['one','two','three','four'],
		[1,2,3,4]
	]

	# This for loop runs twice (once for each "row" of multiarr)
	for (a,b,c,d) in multiarr
		print("a=#{a}, b=#{b}, c=#{c}, d=#{d}\n" )
	end

上面的循环将会打印出：

	a=one, b=two, c=three, d=four
	a=1, b=2, c=3, d=4

我们可以使用 `each` 方法来迭代四个元素的数组，将四个“块参数” `a`，`b`，`c`，`d` 传入 `do` 和 `end` 限定的块中：

	multiarr.each do |a,b,c,d|
	  print("a=#{a}, b=#{b}, c=#{c}, d=#{d}\n" )
	end

<div class="note">
	<p class="h4"><b>块参数（Block Parameters）</b></p>

在 Ruby 中，迭代器的主体称为“块”（block），在块顶部的两个竖直线中声明的任何变量都称为“块参数”（block parameters）。在某种程度上，块的工作方式类似于函数（function），块参数的工作方式类似于函数的参数列表（argument list）。`each` 方法运行块（block）内的代码，并将集合（例如数组，`multiarr`）提供的参数传递给块。在上面的示例中，`each` 方法重复地将有四个元素的数组传递给块，并且这四个数组内的元素初始化为四个块参数 `a`，`b`，`c`，`d`。除了迭代集合之外，块还可以用于其它方面。 我将在第 10 章中对块（block）进行更多说明。
</div>

### 块（Blocks）

<div class="code-file clearfix"><span>block_syntax.rb</span></div>

Ruby 有一种用于限定块的替代语法。你可以不使用 `do..end`，而是像这样使用花括号 `{..}`：

	# do..end
	[[1,2,3], [3,4,5], [6,7,8]].each do
	  |a,b,c|
	  puts( "#{a}, #{b}, #{c}" )
	end

	# curly braces {..}
	[[1,2,3], [3,4,5], [6,7,8]].each {
	  |a,b,c|
	  puts( "#{a}, #{b}, #{c}" )
	}

无论你使用哪个块限定符，都必须确保开放限定符，`'{'` 或 `'do'` 与 `each` 方法放在同一行。 在 `each` 和开放块限定符之间插入一个换行符是错误的语法。

### while 循环

Ruby 也有一些其它的循环结构。这是一个 `while` 循环：

	while tired
	  sleep
	end

或者，以另一种方式：

	sleep while tired

即使这两个示例的语法不同，它们也会执行相同的操作。在第一个示例中，`while` 和 `end` 之间的代码（这里是一个名为 `sleep` 方法的调用）会在布尔测试（在这里，是一个名为 `tired` 的方法的返回值）为 true 时执行。与 `for` 循环一样，关键字 `do` 可选的可以放置于出现在不同行的测试条件与要执行的循环体代码中间，当测试条件与循环代码出现在同一行时关键字 `do` 则是必须的。

### while 修饰符

在第二个版本的循环中（`sleep while tired`），要执行的循环代码（`sleep`）优先于测试条件（`while tired`）。该语法被称为“while 修饰符”（while modifie）。如果你想要使用此语法执行多个表达式，可以将它们放在 `begin` 和 `end` 关键字之间：

	begin
	  sleep
	  snore
	end while tired

<div class="code-file clearfix"><span>1loops.rb</span></div>

这个示例展示了各种替代语法：

	$hours_asleep = 0

	def tired
	  if $hours_asleep >= 8 then
	    $hours_asleep = 0
		return false
	  else
		$hours_asleep += 1
		return true
	  end
	end

	def snore
	  puts('snore....')
	end

	def sleep
	  puts("z" * $hours_asleep )
	end

	while tired do sleep end   # a single-line while loop

	while tired                # a multi-line while loop
	  sleep
	end

	sleep while tired          # single-line while modifier

	begin                      # multi-line while modifier
	  sleep
	  snore
	end while tired

上面的最后一个示例（多行 `while` modifier）需要多加注意，因为它引入了一些重要的新特性。当使用 `begin` 和 `end` 限定的代码块优先于 `while` 测试时，该代码总是至少执行一次。在其它类型的 `while` 循环中，代码可能永远都不会执行，除非布尔测试开始为 true。

<div class="note">
	<p class="h4"><b>确保循环至少执行一次</b></p>

通常 `while` 循环会执行 0 次或多次，因为布尔测试*先于*循环体执行；如果布尔测试在开始时就返回 false，则循环体内的代码永远不会运行。

但是，当 `while` 循环属于 `begin` 和 `end` 包裹的代码块类型时，循环将执行 1 次或多次，因为循环体内的代码*先于*布尔表达式执行。
</div>

<div class="code-file clearfix"><span>2loops.rb</span></div>

<div class="note">
要了解这两种类型的 <code>while</code> 循环的行为差异，请运行 <b>2loops.rb</b>。

这些示例应该有助于阐明该问题：

	x = 100

	# The code in this loop never runs
	while (x < 100) do puts('x < 100') end

	# The code in this loop never runs
	puts('x < 100') while (x < 100)

	# But the code in loop runs once
	begin puts('x < 100') end while (x < 100)
</div>

### until 循环

Ruby 也有一个 `until` 循环，可以被认为是 *'while not'* 循环。它的语法和选项与应用于 `while` 的那些相同——即测试条件与循环体代码可以放置于同一行中（此时 `do` 关键字是必须的），或者也可以放在不同行中（这时 `do` 是可选的）。

还有一个 `until` 修饰符，可以让你将循环体代码放置于测试条件之前，以及可选的是可以将循环体代码包含在 `begin` 和 `end` 之间来确保循环体代码块至少运行一次。

<div class="code-file clearfix"><span>until.rb</span></div>

这里有一些 `until` 循环的简单示例：

	i = 10

	until i == 10 do puts(i) end # never executes

	until i == 10                # never executes
	  puts(i)
	  i += 1
	end

	puts(i) until i == 10        # never executes

	begin                        # executes once
	  puts(i)
	end until i == 10

`while` 和 `until` 循环都可以像 `for` 循环一样用于迭代数组和其他集合。例如，这是迭代数组中所有元素的方法：

	while i < arr.length
	  puts(arr[i])
	  i += 1
	end

	until i == arr.length
	  puts(arr[i])
	  i +=1
	end

### 循环（Loop）

<div class="code-file clearfix"><span>3loops.rb</span></div>

**3loops.rb** 中的示例应该看起来都很熟悉 - 除了最后一个：

	loop {
	  puts(arr[i])
	  i+=1

	  if (i == arr.length) then
	    break
	  end
	}

这里使用 `loop` 方法来重复地执行花括号内的代码块。这就像我们之前在 `each` 方法中使用的迭代器块一样。同样地，我们可以选择块的界定符 - 花括号或者 `do` 和 `end`：

	puts( "\nloop" )
	i=0

	loop do
	  puts(arr[i])
	  i+=1

	  if (i == arr.length) then
	    break
	  end
	end

这段代码通过递增计数器变量 `i` 来遍历数组 `arr`，当 `(i == arr.length)` 条件求值为 true 时，跳出循环。你必须以这种方式跳出循环，因为不同于 `while` 或 `until`
，`loop` 方法执行测试条件以确定是否继续循环。 没有 `break`，它将永远循环。

## 深入探索

Hashes, Arrays, Ranges 和 Sets 都包含（include）了一个名为 Enumerable 的 Ruby 模块（module）。模块是一种代码库（我将在第 12 章中更多地讨论模块）。在第 4 章中，我使用了 Comparable 模块为数组添加比较方法，例如 `<` 和 `>`。你可能还记得我是通过继承 Array 类并将 Comparable 模块 "including" 到子类中来完成此操作：

	class Array2 < Array
	  include Comparable
	end

### Enumerable 模块

<div class="code-file clearfix"><span>enum.rb</span></div>

Enumerable 模块已经被包含进了 Ruby 的 Array 类中，它提供了很多有用的方法，例如 `include?` 方法会在数组中找到一个特定的值时返回 true，`min` 方法则会返回最小的元素值，`max` 方法返回最大的元素值，`collect` 方法会创建一个由块（block）返回的值组成的新数组。

	arr = [1,2,3,4,5]
	y = arr.collect{ |i| i }     #=> y = [1, 2, 3, 4]
	z = arr.collect{ |i| i * i } #=> z = [1, 4, 9, 16, 25]

	arr.include?( 3 ) #=> true
	arr.include?( 6 ) #=> false
	arr.min           #=> 1
	arr.max           #=> 5

<div class="code-file clearfix"><span>enum2.rb</span></div>

只要其它集合类包含 Enumerable 模块，就可以使用这些相同的方法。Hash 就是一个这样的类。但请记住，Hash 中的元素索引是没有顺序的，因此当你使用 `min` 和 `max` 方法时，将根据其数值返回最小和最大元素值 - 当元素值为字符串时，其数值由键（key）中字符的 ASCII 码确定。

### 自定义比较

但是我们假设你更喜欢 `min` 和 `max` 根据一些其它标准（比如字符串的长度）返回元素？最简单的方法是在块（block）内定义比较的本质。这与我在第 4 章中定义的排序块类似。你可能还记得我们通过将块（block）传递给 `sort` 方法来对 Hash（此处为变量 `h`）进行排序，如下所示：

	h.sort{ |a,b| a.to_s <=> b.to_s }

两个参数 `a` 和 `b` 表示来自 Hash 的两个元素，使用 `<=>` 比较方法进行比较。我们可以类似地将块（block）传递给 `max` 和 `min` 方法：

	h.min { |a,b| a[0].length <=> b[0].length }
	h.max { |a,b| a[0].length <=> b[0].length }

当 Hash 将元素传递给块时，它会以包含键值对（key-value）的数组形式传递。所以，如何一个 Hash 包含这样的元素...

	{"one"=>"for sorrow", "two"=>"for joy"}

...两个块参数，`a` 和 `b` 将会被初始化为两个数组：

	a = ["one", "for sorrow"]
	b = ["two", "for joy"]

这解释了为什么我在为 `max` 和 `min` 方法定义的自定义比较中特意比较的是两个块参数中位于索引 0 处的首个元素：

	a[0].length <=> b[0].length

这确保了比较是基于哈希中的*键*（keys）的。

如果你要比较*值*（values），而不是键（keys），只需要将数组的索引设置为 1：

<div class="code-file clearfix"><span>enum3.rb</span></div>

	p( h.min {|a,b| a[1].length <=> b[1].length } )
	p( h.max {|a,b| a[1].length <=> b[1].length } )

当然，你可以在块中定义其他类型的自定义比较。例如，假设你希望字符串 'one'，'two'，'three' 等按照我们说它们的顺序进行执行。这样做的一种方法是创建一个有序的字符串数组：

	str_arr=['one','two','three','four','five','six','seven']

现在，如果一个 Hash，`h` 包含这些字符串作为键（key），则块可以使用 `str_array` 作为键的引用以确定最小值和最大值：

	h.min { |a,b| str_arr.index(a[0]) <=> str_arr.index(b[0])}
	#=> ["one", "for sorrow"]

	h.max { |a,b| str_arr.index(a[0]) <=> str_arr.index(b[0])}
	#=> ["seven", "for a secret never to be told"]

上面所有的示例都使用了 Array 和 Hash 类的 `min` 和 `max` 方法。请记住，是 Enumerable 模块给这些类提供了这些方法。

在某些情况下，能够将诸如 `max`，`min` 和 `collect` 之类的 Enumerable 方法应用于不是从现有的实现这些方法的类（例如 Array）中派生出来的类中是有用的。你可以在你的类中包含 Enumerable 模块，然后编写一个名为 `each` 的迭代器方法：

<div class="code-file clearfix"><span>include_enum1.rb</span></div>

	class MyCollection
	  include Enumerable

	  def initialize( someItems )
		@items = someItems
	  end

	  def each
		@items.each { |i|
		  yield( i )
		}
	  end
	end

在这里，你可以使用数组初始化一个 MyCollection 对象，该数组将存储在实例变量 `@items` 中。当你调用 Enumerable 模块提供的方法之一（例如 `min`，`max` 或 `collect`）时，这将“在幕后”（behind the scenes）调用 `each` 方法，以便一次获取一个数据。

	things = MyCollection.new(['x','yz','defgh','ij','klmno'])

	p( things.min )  #=> "defgh"
	p( things.max )  #=> "yz"
	p( things.collect{ |i| i.upcase } )
					 #=> ["X", "YZ", "DEFGH", "IJ", "KLMNO"]

<div class="code-file clearfix"><span>include_enum2.rb</span></div>

你可以类似地使用 `MyCollection` 类来处理数组，例如 Hashes 的键（keys）或值（values）。目前，`min` 和 `max` 方法采用基于数值执行比较的默认行为，因此基于字符的ASCII 值，'xy' 将被认为比 'abcd''更大'。如果你想执行一些其它类型的比较 - 例如，通过字符串长度来比较，以便 'abcd' 被认为大于 'xz' - 你可以覆盖 `min` 和 `max `方法：

<div class="code-file clearfix"><span>include_enum3.rb</span></div>

	def min
	  @items.to_a.min { |a,b| a.length <=> b.length }
	end

	def max
	  @items.to_a.max { |a,b| a.length <=> b.length }
	end

<div class="note">
	<p class="h4"><b>Each and Yield…</b></p>

那么，当 Enumerable 模块中的方法调用你编写的 `each` 方法时，真正发生了什么？事实证明，Enumerable 方法（`min`，`max`，`collect` 等）给 `each` 方法传递了一个代码块（block）。这段代码期望一次接收一个数据（即来自某种集合的每个元素）。你的 `each` 方法以块参数的形式为其提供该项，例如此处的参数 `i`：

	def each
	  @items.each{ |i|
		yield( i )
	  }
	end

关键字 `yield` 是一个特殊的 Ruby 魔术，它告诉代码运行传递给 `each` 方法的块 - 也就是说，运行 Enumerator 模块的 `min`，`max` 或 `collect` 方法传递的代码块。这意味着这些方法的代码块可以应用于各种不同类型的集合。你所要做的就是，i）在你的类中包含 Enumerable 模块；ii）编写 `each` 方法，确定 Enumerable 方法将使用哪些值。
</div>

# 第六章

***

## 条件语句

计算机程序，如生活本身，充满了等待要做的困难决定。如果我待在床上，可以多睡一会，但我不得不去上班；如果我去上班我会赚到一些钱，否则我将丢掉工作 - 等等...

我们在之前的程序中执行了一些 `if` 测试。举一个简单的例子，这是来自第一章的税收计算器：

	if (subtotal < 0.0) then
	  subtotal = 0.0
	end

在此程序中，将会提示用户输入一个值 `subtotal`，它将被用来计算应缴税额。如果用户错误的输入一个小于 0 的值，`if` 测试会发现这一点，因为测试 `(subtotal < 0.0)` 的计算结果为 true，这将会导致位于 `if` 测试语句和 `end` 关键字之间的代码被执行，这里将会把 `subtotal` 置为 0。

<div class="note">
	<p class="h4"><b>等号（=）与双等号（==）？</b></p>

与许多其它编程语言一样，Ruby 使用一个等号 `=` 来赋值，用两个等号 `==` 来测试值。
</div>

### If..Then..Else

<div class="code-file clearfix"><span>if_else.rb</span></div>

像这样的简单测试只会是两个可能的结果之一。要么运行一部分代码，要么不运行，取决于测试结果是否为 true。通常，你会需要有两种以上可能的结果。例如，假设你的程序在这一天为工作日时执行一种程序行为，如果是周末则执行不同的程序行为。你可以在 `if` 部分之后添加 `else` 部分来测试这些条件，如下所示：

	if aDay == 'Saturday' or aDay == 'Sunday'
	  daytype = 'weekend'
	else
	  daytype = 'weekday'
	end

这里的 `if` 条件很简单。它测试了两种可能性：1）变量 `aDay` 的值等于字符串 "Saturday"，或 2）等于字符串 "Sunday"。如果其中任何一个条件为真，则执行下一行代码：`daytype ='weekend'`; 在所有其它情况下，`else` 之后的代码将执行：`daytype ='weekday'`。


<div class="code-file clearfix"><span>if_then.rb</span></div>

<div class="note">
如果 <code>if</code> 测试和要执行的代码在不同行，关键字 <code>then</code> 是可选的。但是，当测试语句和要执行代码在同一行时，关键字 <code>then</code>（或者你喜欢更简洁的代码，一个冒号）是必要的：

	if x == 1 then puts( 'ok' ) end # with 'then'

	if x == 1 : puts( 'ok' ) end    # with colon

	if x == 1 puts( 'ok' ) end      # syntax error!
</div>

`if` 测试不仅限于两个条件的判断。例如，假设你的代码需要确定某一天是工作日还是节假日。所有的周内每一天都为工作日，所有的星期六都是假期，但周末只有你不加班时才是假期。这是我第一次尝试编写测试来判断所有的这些条件：

<div class="code-file clearfix"><span>and_or_wrong.rb</span></div>

	working_overtime = true

	if aDay == 'Saturday' or aDay == 'Sunday' and not working_overtime
	  daytype = 'holiday'
	  puts( "Hurrah!" )
	else
	  daytype = 'working day'
	end

不幸的是，这并没有达到预期的效果。请记住，星期六总是一天假期。但是，这段代码却认定星期六是工作日。这是因为 Ruby 接收的测试为：“如果这一天是星期六并且我不加班，或者这一天是周末并且我不加班”，但我真正的意思是：“如果这一天是星期六，或者这一天是周末并且我不加班”。解决这种歧义的最简单方法是在任意代码周围加上括号使其作为单个单元进行判断，如下所示：

<div class="code-file clearfix"><span>and_or.rb</span></div>

	if aDay == 'Saturday' or (aDay == 'Sunday' and not working_overtime)

### And..Or..Not

顺便说一下，Ruby 有两种不同的语法来测试布尔值（true/false）条件。在上面的示例中，我使用了英文风格的运算符：`and`，`or` 以及 `not`。如果你愿意，你可以使用类似其它语言中的一种替代运算符：`&&`（and）、`||`（or）以及 `!`（not）。

但是要小心，这两组运算符不是完全可以互换的。首先，它们具有不同的优先级，这意味着当在单个测试中使用多个运算符时，将会根据你使用的运算符以不同的顺序执行测试的各个部分。例如，看看这个测试：

<div class="code-file clearfix"><span>days.rb</span></div>

	if aDay == 'Saturday' or aDay == 'Sunday' and not working_overtime
	  daytype = 'holiday'
	end

假设布尔变量 `working_overtime` 为 true，如果变量 `aDay` 用字符串 'Saturday' 初始化，那么这个测试会成功吗？换句话说，如果 `aDay` 是 'Saturday'，`daytype` 会被赋值为 'holiday' 吗？答案是：不，它不会。测试将只会在 `aDay` 是 'Saturday' 或 'Sunday'，并且 `working_overtime` 不为 true 时成功。

思考下面这个测试：

	if aDay == 'Saturday' || aDay == 'Sunday' && !working_overtime
	  daytype = 'holiday'
	end

从表面上看，这与上一次测试相同; 唯一的区别是这次我使用了运算符的替代语法。然而，这个变化不仅仅是表面的，因为如果 `aDay` 是 'Saturday'，那么这个测试执行结果为 true，而 `daytype` 则会初始化为 'holiday'。这是因为 `||` 运算符的优先级高于 `or` 运算符。所以这个测试会在 `aDay` 是 'Saturday' ，或者不仅 `aDay` 是 'Sunday' 还要 `working_overtime` 不为 true 时成功。

有关详细信息，请参阅本章末尾的**深入挖掘**部分。作为一般原则，你最好决定你喜欢哪组运算符，坚持使用它们并使用括号来避免产生歧义。

### If..Elsif

毫无疑问，你总会遇到需要根据几种替代条件来采取不同的行为。这样做的一种实现方式是通过判断一个 `if` 测试，然后在关键字 `elsif` 之后再放置一系列其它测试条件。然后必须使用 `end` 关键字终止。

例如，这里我通过在 `while` 循环中反复获取用户输入信息，`if` 测试来判断用户是否输入了 'q'（我已经用 `chomp()` 方法从输入中删除了回车符）；如果输入的不是 'q' 则第一个 `elsif` 测试判断输入的整数值（`input.to_i`）是否大于 800；该测试失败后，下一个 `elsif` 测试判断整数值是否小于等于 800：

<div class="code-file clearfix"><span>if_elsif.rb</span></div>

	while input != 'q' do
	  puts("Enter a number between 1 and 1000 (or 'q' to quit)")
	  print("?- ")
	  input = gets().chomp()

	  if input == 'q'
	    puts( "Bye" )
	  elsif input.to_i > 800
		puts( "That's a high rate of pay!" )
	  elsif input.to_i <= 800
		puts( "We can afford that" )
	  end
	end

这个程序的问题在于，即使它要求用户输入一个 1 到 1000 的值，它也可能会接收到一个小于 1（当然，你如果想要一份负数的薪水，我很乐意为你提供一份工作！）或者大于 1000（在这种情况下，不要找我找工作！）的值。

我们可以通过重写两个 `elsif` 测试并添加一个 `else` 部分，如果所有前面的测试都失败则执行该部分，来解决这个问题：

<div class="code-file clearfix"><span>if_elsif2.rb</span></div>

	if input == 'q'
	  puts( "Bye" )
	elsif input.to_i > 800 && input.to_i <= 1000
	  puts( "That's a high rate of pay!" )
	elsif input.to_i <= 800 && input.to_i > 0
	  puts( "We can afford that" )
	else
	  puts( "I said: Enter a number between 1 and 1000!" )
	end

<div class="code-file clearfix"><span>if_else_alt.rb</span></div>

<div class="note">
Ruby 也有一种 <code>if..then..else</code> 的简写方式，用 <code>?</code> 替换掉 <code>if..then</code> 部分，并用一个 <code>:</code> 当作 <code>else</code> ...

&lt;Test Condition&gt; `?` &lt;if true do this&gt; `:` &lt;else do this&gt;

例如：

	x == 10 ? puts("it's 10") : puts( "it's some other number" )

当测试条件复杂时（如果使用多个 `and` 和 `or`），则应将其括在括号中。如果测试和代码跨越几行， `?` 必须与前一个条件放在同一行，并且 `:` 必须与紧跟在 `?` 之后的代码放在同一行。换句话说，如果你在 `?` 或者 `:` 之前添加换行符，你将得到一个语法错误。 这是正确的多行代码块的示例：

	(aDay == 'Saturday' or aDay == 'Sunday') ?
	daytype = 'weekend' :
	daytype = 'weekday
</div>

<div class="code-file clearfix"><span>days2.rb</span></div>

这有另一个示例，一个长的 `if..elsif` 序列，并且有 `else` 部分处理其它所有情况。这次的测试值 `i` 是一个整数：

	def showDay( i )
	  if i == 1 then puts("It's Monday" )
	  elsif i == 2 then puts("It's Tuesday" )
	  elsif i == 3 then puts("It's Wednesday" )
	  elsif i == 4 then puts("It's Thursday" )
	  elsif i == 5 then puts("It's Friday" )
	  elsif (6..7) === i then puts( "Yippee! It's the weekend! " )
	  else puts( "That's not a real day!" )
	  end
	end

请注意，我使用范围（range） `(6..7)` 来匹配代表星期六和星期天的两个整数值。这里的 `===` 方法（三个 `=` 字符）测试一个值（这里是 `i`）是否在范围（range）中。上面的示例：

	(6..7) === i

...可以重写为：

	(6..7).include?(i)

`===` 方法由 Object 类定义，并在后代类中重写。它的行为因所属类而异。我们将很快看到，它的一个基本用途是为测试语句提供有意义的判断。

### Unless

<div class="code-file clearfix"><span>unless.rb</span></div>

Ruby 也可以执行 `unless` 测试，这与 `if` 测试完全相反：

	unless aDay == 'Saturday' or aDay == 'Sunday'
	  daytype = 'weekday'
	else
	  daytype = 'weekend'
	end

`unless` 是表达 'if not' 的一种替代方式。下面的代码与上面示例等同：

	if !(aDay == 'Saturday' or aDay == 'Sunday')
	  daytype = 'weekday'
	else
	  daytype = 'weekend'
	end

### If 与 Unless 修饰符

你可能还记得第 5 章中提到的 `while` 循环的替代语法。替换这样的写法：

	while tired do sleep end

...我们可以这样写：

	sleep while tired

这种将 `while` 关键字放在循环代码和测试条件之间的替代语法称为 'while 修饰符'（while modifier）。事实上，Ruby 也提供了 `if` 和 `unless` 修饰符。这是一些示例：

<div class="code-file clearfix"><span>if_unless_mod.rb</span></div>

	sleep if tired

	begin
	  sleep
	  snore
	end if tired

	sleep unless not tired

	begin
	  sleep
	  snore
	end unless not tired

当你在某些测试条件为 true 时要重复执行一些明确的操作时，这种简洁的语法是很有用的。例如，在常量 `DEBUG` 为 true 时你的代码可能需要输出一些调试信息。

	puts( "somevar = #{somevar}" ) if DEBUG

<div class="code-file clearfix"><span>constants.rb</span></div>

<div class="note">
	<p class="h4"><b>常量（Constants）</b></p>

Ruby 中的常量以大写字母开头。 类名就是常量。你可以使用 `constants` 方法获取所有已定义常量的列表：

	Object.constants

Ruby 提供了 `const_get` 和 `const_set` 方法来获取和设置特定的以符号命名的常量的值（标识符前面带有冒号，如 `:RUBY_VERSION`）。

请注意，与许多其它编程语言中的常量不同，Ruby 中的常量可以为其分配新的值：

	RUBY_VERSION = "1.8.7"
	RUBY_VERSION = "2.5.6"

上面给 `RUBY_VERSION` 常量重新赋值会产生一个 '已初始化的常量'（already initialized constant）的警告（warning）- 但不是错误（error）！
</div>

### Case 语句

当你需要根据单个变量的值采取各种不同的操作时，多个 `if..elsif` 测试是冗长且重复的。

`case` 语句提供了更简洁的替代方案。以单词 `case` 开始，后跟要测试的变量名称。然后是一系列 `when` 片段，每一片段都指定一个“触发值”（trigger），后跟要执行的代码。

仅当测试变量等于触发（trigger）值时，此代码才会执行：

<div class="code-file clearfix"><span>case.rb</span></div>

	case( i )
	  when 1 : puts("It's Monday" )
	  when 2 : puts("It's Tuesday" )
	  when 3 : puts("It's Wednesday" )
	  when 4 : puts("It's Thursday" )
	  when 5 : puts("It's Friday" )
	  when (6..7) : puts( "Yippee! It's the weekend! " )
	  else puts( "That's not a real day!" )
	end

在上面的示例中，我使用冒号将每个 `when` 测试与要执行的代码分隔开。与类 C 语言中的 `case` 语句不同，当匹配到一个片段时，不需要输入一个 `break` 关键字来防止继续进入后面其余的片段中匹配。在 Ruby 中，一旦匹配到，`case` 语句就会结束：

	case( i )
	  when 5 : puts("It's Friday" )
		puts("...nearly the weekend!")
	  when 6 : puts("It's Saturday!" )
		# the following never executes
	  when 5 : puts( "It's Friday all over again!" )
	end

你可以在一个 `when` 测试中包含多行代码，你也可以包含多个用逗号分割的值来触发同一个 when 代码块，像这样：

	when 6, 7 : puts( "Yippee! It's the weekend! " )

<div class="code-file clearfix"><span>case2.rb</span></div>

`case` 语句中的条件不一定是一个简单的变量; 它也可以是这样的表达式：

	case( i + 1 )

你还可以使用非整数（non-integer）类型，例如字符串（string）。如果在一个 `when` 片段中指定了多个触发值，则它们可能具有不同的类型 - 例如，包含字符串和整数：

	when 1, 'Monday', 'Mon' : puts( "Yup, '#{i}' is Monday" )

这是一个较长的例子，说明了上面提到的一些语法元素：

<div class="code-file clearfix"><span>case3.rb</span></div>

	case( i )
	  when 1 : puts("It's Monday" )
	  when 2 : puts("It's Tuesday" )
	  when 3 : puts("It's Wednesday" )
	  when 4 : puts("It's Thursday" )
	  when 5 then puts("It's Friday" )
		puts("...nearly the weekend!")
	  when 6, 7
		puts("It's Saturday!" ) if i == 6
		puts("It's Sunday!" ) if i == 7
		puts( "Yippee! It's the weekend! " )
	  # the following never executes
	  when 5 : puts( "It's Friday all over again!" )
	  else puts( "That's not a real day!" )
	end

### === 方法

如前所述，`case` 语句中的 `when` 测试的对象使用 `===` 方法判断。因此，例如当整数（integer）作为范围（range）的一个组成部分时，`===` 方法返回 true；当 case 语句中的整型变量构成范围表达式的一部分时，`when` 测试返回 true：

	when (6..7) : puts( "Yippee! It's the weekend! " )

如果对特定对象的 `===` 方法的作用有疑问，请参阅该对象所属类的 Ruby 文档。

### 其它的 Case 语法

`case` 语句有一种其它的形式，就像一系列 `if..then..else` 语句的简写形式。每个 `when` 部分都可以执行一些任意测试并执行一行或多行代码。`case` 变量不是必要的。每个 `when` 片段都会返回一个值，就像方法（method）一样，它是最后一段代码的结果。可以将此值分配给 `case` 语句之前的变量：

<div class="code-file clearfix"><span>case4.rb</span></div>

	salary = 2000000
	season = 'summer'
	happy = case
	when salary > 10000 && season == 'summer':
	puts( "Yes, I really am happy!" )
	'Very happy' #=> This value is "returned"
	when salary > 500000 && season == 'spring' : 'Pretty happy'
	else puts( 'miserable' )
	end
	puts( happy ) #=> "Very happy"

## 深入探索

### 布尔（Boolean）测试

	and &&

这些运算符只有在判断左侧结果为 true 时，会继续判断右侧，`and` 的优先级比 `&&` 低。

	or ||

这些运算符只有在判断左侧结果为 false 时，会继续判断右侧，`or` 的优先级比 `||` 低。

	not !

布尔值的否操作，即值为 false 时返回 true，值为 true 时返回 false。

使用两种不同的布尔运算符时要小心。由于优先级的差异，测试将以不同的顺序进行判断，并可能产生不同的结果。

思考以下代码：

<div class="code-file clearfix"><span>boolean_ops.rb</span></div>

	# Example 1
	if (1==3) and (2==1) || (3==3) then
	  puts('true')
	else
	  puts('false')
	end

	# Example 2
	if (1==3) and (2==1) or (3==3) then
	  puts('true')
	else
	  puts('false')
	end

这些看起来可能是一样的。实际上，示例 1 将打印 'false' ，而示例 2 将打印 true。这完全是因为 `or` 比 `||` 优先级低的事实。因此，示例 1 中的测试是：如果 1 等于 3 [*false*] 并且（要么 2 等于 1 ，要么 3 等于 3）[*true*]。由于这两个必要的条件中有一个是 false，所以整个测试返回 false。

现在来看示例 2，其测试是：（如果 1 等于 3 ，并且 2 等于 1）[*false*]，或者 3 等于 3 [*true*]。这次，我们仅需要两个测试中一个成功即可；第二个测试判断为 true，所以整个测试返回 true 。

在这样的测试中，运算符优先级的副作用可能会导致非常模糊的错误。你可以通过使用括号来清楚的表达测试的含义来避免这些错误。在这里，我重写了上面的示例 1 和 2；在每种情况下，添加一对括号都会反转测试返回的布尔值：

	# Example 1 (b) – now returns true
	if ((1==3) and (2==1)) || (3==3) then
	  puts('true')
	else
	  puts('false')
	end

	# Example 2 (b) – now returns false
	if (1==3) and ((2==1) or (3==3)) then
	  puts('true')
	else
	  puts('false')
	end

### 否定

否定运算符 `!` 可以在表达式的开头使用，或者你可以在一个表达的左侧和右侧中间使用 `!=`（不等于）运算符：

	!(1==1)  #=> false
	1 != 1   #=> false

或者，你可以用 `not` 代替 `!`：

	not(1==1)

### 布尔运算中的怪象

<div class="code-file clearfix"><span>eccentricities.rb</span></div>

请注意，Ruby 的布尔（boolean）运算符有时会以一种奇怪且不可预测的方式运行。例如：

	puts( (not( 1==1 )) )            # This is ok
	puts( not( 1==1 ) )              # This is a syntax error

	puts( true && true && !(true) )  # This is ok
	puts( true && true and !(true) ) # This is a syntax error

	puts( ((true) and (true)) )      # This is ok
	puts( true && true )             # This is ok
	puts( true and true )            # This is a syntax error

在多数情况下，可以通过统一使用同一类型的运算符（要么用 `and`，`or`，`not`，要么用 `&&`，`||`，`!`）来避免这些问题，而不是混合地使用两者。另外，推荐经常使用括号。

### Catch 与 Throw

Ruby 提供了一对方法 `catch` 和 `throw`，可用于在满足某些条件时中断（break）代码块的执行。这是 Ruby 中与其它一些编程语言中的 `goto` 最接近的等价语法。该代码块必须以 `catch` 后跟一个符号（symbol）（即以冒号开头的唯一标识符）开头，例如 `:done` 或 `:finished`。代码块本身可以用大括号限定，也可以用关键字 `do` 和 `end` 限定，如下所示：

	# think of this as a block called :done
	catch(:done){
	  # some code here
	}

	# and this is a block called :finished
	catch(:finished) do
	  # some code here
	end

在块内，你可以使用一个符号（symbol）作为参数调用 `throw`。通常，当满足某些特定条件时，你将可以调用 `throw` 来跳过块中的所有剩余的未执行代码。例如，让我们假设该块包含这样一些代码，提示用户输入一个数字，用某个值来除以该数字，然后继续对结果进行大量其它的复杂计算。显然，如果用户输入 0，则后面的计算都不能完成，因此你可以通过跳出块来跳过这些计算，并继续执行块后的任何代码。这是这样做的一种方式：

<div class="code-file clearfix"><span>catch_throw.rb</span></div>

	catch(:finished) do
	  print('Enter a number: ')
	  num = gets().chomp.to_i
	  if num == 0 then
	    throw :finished # if num is 0, jump out of the block
	  end
	    # Here there may be hundreds of lines of
	    # calculations based on the value of num
	    # if num is 0 this code will be skipped
	end

	# the throw method causes execution to
	# jump to here – outside of the block
	puts("Finished")

实际上，你可以在块外面调用 `throw`，像这样:

	def dothings( aNum )
	  i = 0
	  while true
		puts("I'm doing things...")
		i += 1
		throw(:go_for_tea) if (i == aNum)
			# throws to end of go_to_tea block
	  end
	end

	catch(:go_for_tea) { # this is the :go_to_tea block
	  dothings(5)
	}

并且你可以将 `catch` 块嵌套在其它的 `catch` 块中，像这样：

	catch(:finished) do
	  print('Enter a number: ')
	  num = gets().chomp.to_i
	  if num == 0 then throw :finished end
	  puts( 100 / num )

	  catch(:go_for_tea) {
		dothings(5)
	  }

	  puts("Things have all been done. Time for tea!")
	end

与其它编程语言中的 `gotos` 和 jumps 一样，在 Ruby 应该非常谨慎地使用 `catch` 和 `throw`，因为它们会破坏代码的逻辑，并且可能会引入难以发现的错误。

# 第七章

***

## 方法（Methods）

我们在本书中使用了很多方法（methods）。总的来说，它们并不是特别复杂的东西，因此你可能会想那么该章节关于方法的一切东西却如此之长。正如我们将要发现的一样，关于方法的远不止你眼前看到的那些。

### 类方法

到目前为止我们使用的方法都是'实例方法'（instance methods）。实例方法属于类（class）的特定实例（instance） - 换句话说，属于单个对象（object）。也可以编写“类方法”（class methods）。类方法属于类（class）本身。为了定义类方法，你可以在方法名称前面加上类名和句号：

<div class="code-file clearfix"><span>class_methods1.rb</span></div>

	class MyClass
	  def MyClass.classMethod
		puts( "This is a class method" )
	  end

	  def instanceMethod
		puts( "This is an instance method" )
	  end
	end

调用类方法时必须使用类名：

	MyClass.classMethod

特定对象不能调用类方法，同样的类也不能调用实例方法：

	MyClass.instanceMethod   #=> Error! This is an „undefined method‟
	ob.classMethod           #=> Error! This is an „undefined method‟

### 类变量

类方法可能会让你想到类变量（也就是名称以 `@@` 开头的变量）。你可能还记得我们之前在一个简单的冒险游戏中使用了类变量（参见第 2 章中的 **2adventure.rb**）来记录游戏中对象的总数; 每次创建一个新的 Thing 对象时，都会在 `@@num_things` 类变量中增加 1：

	class Thing
	  @@num_things = 0

	  def initialize(aName, aDescription)
		@@num_things +=1
	  end
	end

与实例变量（在从类派生的对象中）不同，类变量必须在首次声明时给出一个值：

	@@classvar = 1000   # class variables must be initialized

在类内初始化实例或类变量只会影响类本身存储的值。类变量（class variable）既可用于类本身，也可用于从该类创建的对象。但是，每个实例变量都是唯一的；每个对象都有属于自己的任何实例变量的副本 - 而类本身也可能有自己的实例变量。

<div class="note">
	<p class="h4"><b>类变量、实例变量以及方法的总结</b></p>

实例变量以 `@` 开头：

	@myinstvar    # instance variable

类变量以 `@@` 开头：

	@@myclassvar  # class variable

实例方法由以下定义：`def` <*MethodName*>

	def anInstanceMethod
	  # some code
	end

类方法则由以下定义：`def` <*ClassName*>.<*MethodName*>

	def MyClass.aClassMethod
	  # some code
	end
</div>

<div class="code-file clearfix"><span>class_methods2.rb</span></div>

要了解一个类怎样才会拥有实例变量，请查看 **class_methods2.rb** 程序。这里声明并初始化了一个类变量和一个实例变量：

	@@classvar = 1000
	@instvar = 1000

它定义了一个类方法 `classMethod`，将这两个变量递增 10，还有一个实例方法 `instanceMethod`，将两个变量递增 1。请注意，我还为实例变量 `@instvar` 赋了值。我之前说过，初始值通常不会以这种方式分配给实例变量。该规则的例外是将值赋给*类本身*的实例变量，而不是从该类派生的对象的实例变量。不久之后，这个区别将变得更加明显。

我编写了几行代码来创建 MyClass 类的三个实例（`ob` 变量在每个循环中初始化一个新实例），然后调用类和实例方法：

	for i in 0..2 do
	  ob = MyClass.new
	  MyClass.classMethod
	  ob.instanceMethod
	  puts( MyClass.showVars )
	  puts( ob.showVars )
	end

我还编写了另一个类方法 `MyClass.showVars` 和一个实例方法 `showVars`，以便在每个循环中显示 `@instvar` 和 `@@classvar` 的值。当你运行代码时，将会显示的值：

	(class method) @instvar = 1010, @@classvar = 1011
	(instance method) @instvar = 1, @@classvar = 1011
	(class method) @instvar = 1020, @@classvar = 1022
	(instance method) @instvar = 1, @@classvar = 1022
	(class method) @instvar = 1030, @@classvar = 1033
	(instance method) @instvar = 1, @@classvar = 1033

你可能需要仔细查看这些结果才能看到发生了什么。总之，这就是发生的事情：类方法 `MyClass.classMethod` 和实例方法 `instanceMethod` 中的代码都在递增类和实例变量，`@@classvar` 和 `@instvar`。

你可以清楚地看到类变量通过这两种方法都在递增（无论何时创建新对象，类方法都会向 `@@classvar` 添加 10，实例方法为它添加 1）。但是，每当创建新对象时 `instanceMethod` 都会将其实例变量初始化为 1。这是预期的行为 - 因为每个对象都有自己的实例变量的副本，但所有对象共享一个唯一的类变量。

也许不太明显的是，类本身也有自己的实例变量 `@instvar`。这是因为，在 Ruby 中，类也是一个对象，因此可以包含实例变量，就像任何其它对象一样。MyClass 变量 `@instvar` 由类方法 `MyClass.classMethod` 递增：

	@instvar += 10

注意当实例方法 `showVars` 打印 `@instvar` 的值时，它打印存储在特定对象 `ob` 中的值; `ob` 的 `@instvar` 的值最初是 `nil`（并非像 MyClass 的变量 `@instvar` 一样初始值为 1000）并且此值在 `instanceMethod` 中递增 1。

当类方法 `MyClass.showVars` 打印 `@instvar` 的值时，它打印存储在类本身中的值（换句话说，MyClass 的 `@instvar` 与来自 `ob` 的 `@instvar` 是不同的变量）。但是当任一方法打印出类变量 `@@classvar` 的值时，值是一样的。

请记住，只会有一个类变量的副本，但可能会有许多实例变量的副本。如果这仍然令人困惑，请看看 **inst_vars.rb** 程序：

<div class="code-file clearfix"><span>inst_vars.rb</span></div>

	class MyClass
	  @@classvar = 1000
	  @instvar = 1000

	  def MyClass.classMethod
	    if @instvar == nil then
	      @instvar = 10
	    else
		  @instvar += 10
	    end
	  end

	  def instanceMethod
		if @instvar == nil then
		  @instvar = 1
		else
		  @instvar += 1
		end
	  end
	end

	ob = MyClass.new
	puts MyClass.instance_variable_get(:@instvar)

	puts( '--------------' )
	for i in 0..2 do
	  # MyClass.classMethod
	  ob.instanceMethod
	  puts("MyClass @instvar=#{MyClass.instance_variable_get(:@instvar)}")
	  puts("ob @instvar= #{ob.instance_variable_get(:@instvar)}")
	end

这一次，我们在一开始就创建了一个实例（`ob`），而不是通过循环每次创建一个新的对象实例。当 `ob.instanceMethod` 调用时，`@instvar` 增加 1。

在这里，在类和方法中我使用了一个小技巧，使用 Ruby 的 `instance_get_variable` 方法获取 `@instvar` 的值：

	puts("MyClass @instvar=#{MyClass.instance_variable_get(:@instvar)}")
	puts("ob @instvar= #{ob.instance_variable_get(:@instvar)}")

因为我们只增加属于对象 `ob` 的 `@instvar`，所以当 `for` 循环执行时，`@instvar` 的值从 1 上升到 3。但是属于 MyClass 类的 `@instvar` 永远不会增加; 它保持在初始值（1000）...

	1000
	--------------
	MyClass @instvar= 1000
	ob @instvar= 1
	MyClass @instvar= 1000
	ob @instvar= 2
	MyClass @instvar= 1000
	ob @instvar= 3

但现在，取消掉这一行的注释...

	MyClass.classMethod

现在调用一个类方法，它将 `@instvar` 增加 10。这次当你运行程序时，你会看到，像以前一样，`ob` 的 `@instvar` 变量在每次循环中增加 1 而 MyClass 的 `@instvar` 变量则会增加 10 ...

	1000
	--------------
	MyClass @instvar= 1010
	ob @instvar= 1
	MyClass @instvar= 1020
	ob @instvar= 2
	MyClass @instvar= 1030
	ob @instvar= 3

<div class="note">
	<p class="h4"><b>一个类是一个对象</b></p>

要理解这一点，只需记住*一个类是一个对象*（实际上，它是 `Class` 类的一个实例！）。MyClass '类对象'（class object）有自己的实例变量（`@instvar`），就像 `ob` 对象有自己的实例变量（在这里，也恰好称为 `@instvar`）。实例变量对于对象实例始终是唯一的 - 因此没有两个对象（甚至像 MyClass 这样恰好是一个类的对象！）可以共享一个实例变量。
</div>

### 类方法的用途？

但是，有人可能会问，为什么你想要创建一个类方法而不是更常用的实例方法呢？有两个主要原因：首先，类方法可以用作“准备运行的函数”，而省去了为了使用它而创建对象的麻烦；其次，它可以在那些需要在创建对象实例之前运行方法的场合使用。

有关将方法用作“准备运行函数”的几个示例，请查看 File 类。它的许多方法都是类方法。这是因为，在大多数情况下，你将使用它们对现有文件执行操作或返回信息。你不需要创建一个 File 对象来执行这些操作，将文件名作为参数传递给类方法即可。这里有一些示例：

<div class="code-file clearfix"><span>file_methods.rb</span></div>

	fn = 'file_methods.rb'

	if File.exist?(fn) then
	  puts(File.expand_path(fn))
	  puts(File.basename(fn))
	  puts(File.dirname(fn))
	  puts(File.extname(fn))
	  puts(File.mtime(fn))
	  puts("#{File.size(fn)} bytes")
	else
	  puts( "Can't find file!")
	end

在创建对象之前需要使用方法的情况下，类方法是至关重要的。最重要的例子是 `new` 方法。

你在每次创建对象时都会调用 `new` 方法。在创建对象实例之前，你显然无法调用其任一实例方法 - 因为你只能从已存在的对象中调用实例方法。当你使用 `new` 时，你正在调用类本身的方法并告诉类创建自己的新实例。

### Ruby 构造方法：new 还是 initialize？

负责使对象生成的方法称为构造方法。在 Ruby 中，构造方法称为 `new`。`new` 方法是一个类方法，一旦创建了一个对象，如果名为 `initialize` 的实例方法存在的话，就会运行它。

简而言之，`new` 方法是构造方法，并使用 `initialize` 方法在创建对象后立即初始化任意变量的值。但是为什么你不能编写自己的 `new` 方法并在其中初始化变量？让我们尝试一下：

<div class="code-file clearfix"><span>new.rb</span></div>

	class MyClass
	  def initialize(aStr)
		@avar = aStr
	  end

	  def MyClass.new(aStr)
		super
		@anewvar = aStr.swapcase
	  end
	end

	ob = MyClass.new("hello world")
	puts(ob)
	puts(ob.class)

在这里，我使用 `super` 关键字调用默认的 `new` 构造方法，超类的 `new` 方法。然后我创建了一个字符串实例变量 `@anewvar`。那么我最终会得到什么呢？正如你可能想的那样，不是包含几个字符串变量的新 MyClass 对象。请记住，Ruby 中的方法计算的最后一个表达式是该方法返回的值。这里 `new` 方法计算的最后一个表达式是一个字符串。所以，当我计算这...

	ob = MyClass.new("hello world")

... `MyClass.new` 返回一个字符串；并且它是这个被分配给 `ob` 的字符串（不是 MyClass 对象）。因为你不太可能想要做这样的事情，所以通常明智的做法是避免尝试覆盖（override）`new` 方法。

### 单例方法

<div class="code-file clearfix"><span>class_classes.rb</span></div>

单例方法（singleton method）是属于单个对象而不是整个类的方法。Ruby 类库中的许多方法都是单例方法。如前所述，这是因为每个类都是 Class 类型的对象。或者，简单地说：每个类的类都是 Class。所有类都是如此 - 无论是你自己定义的类还是 Ruby 类库提供的类：

	class MyClass
	end
	puts( MyClass.class )  #=> Class
	puts( String.class )   #=> Class
	puts( Object.class )   #=> Class
	puts( Class.class )    #=> Class
	puts( IO.class )       #=> Class

现在，一些类也有类方法 - 即属于 Class 对象本身的方法。从这个意义上讲，这些是 Class 对象的单例方法。实际上，如果你执行以下代码，将显示一个与 IO 类的类方法名称匹配的方法名称数组：

	p(IO.singleton_methods)

如前所述，当你编写自己的类方法时，可以通过在方法名前加上类的名称来实现：

	def MyClass.classMethod

事实证明，在为特定对象创建单例方法时，你可以使用类似的语法。这次你在方法名称前加上对象的名称：

	def myObject.objectMethod

<div class="code-file clearfix"><span>class_hierarchy.rb</span></div>

<div class="note">
	<p class="h4"><b>所有 Ruby 对象都是 Object 类的后代...</b></p>

...也包括 `Class` 类！初看起来很奇怪，实际上创建对象的每个类本身就是一个从 `Object` 类继承的对象。要证明这一点，请尝试 **class_hierarchy.rb** 程序：

	def showFamily( aClass )
	  if (aClass != nil) then
		puts( "#{aClass} :: about to recurse with aClass.superclass = #{aClass.superclass}" )
		showFamily( aClass.superclass )
	  end
	end
</div>

让我们看一个具体的示例。假设你有一个程序包含许多不同物种的生物对象（也许你是兽医，或者动物园管理员，或者像本书的作者一样，是一个热情的冒险游戏玩家）；每个生物都有一个名为 `talk` 的方法，它显示每个生物通常发出的声音。

这是我的 Creature 类和一些生物对象：

<div class="code-file clearfix"><span>singleton_meth1.rb</span></div>

	class Creature
	  def initialize( aSpeech )
		@speech = aSpeech
	  end

	  def talk
		puts( @speech )
	  end
	end

	cat = Creature.new("miaow")
	dog = Creature.new("woof")
	budgie = Creature.new("Who's a pretty boy, then!")
	werewolf = Creature.new("growl")

然后你突然意识到其中一个生物有额外的特殊行为。在满月之夜，狼人（werewolf）不仅会说话（咆哮）；它也会嚎叫（“How-oo-oo-oo-oo！”）。它确实需要一个 `howl` 方法。

你可以回头向 Creature 类添加一个这样的方法，但是你最终也会得到会嚎叫的狗，猫和虎皮鹦鹉 - 这并不是你想要的。你可以创建一个新的继承自 Creature 的 Werewolf 类，但是你只会有一个狼人（唉，它们是濒临灭绝的物种），所以为什么你要创建一个完整的类呢？一个 werewolf 对象除了它有一个 `howl` 方法之外，与其它生物对象都一样是不是更有意义？好吧，让我们通过给狼人（werewolf）提供它自己的单例方法来做到这一点。来吧：

	def werewolf.howl
	  puts("How-oo-oo-oo-oo!")
	end

哎呀，我们可以做得更好！它只会在满月时嚎叫（howls），所以让我们确定如果要求在新月时嚎叫，它就会咆哮（growls）。这是我的完成方法：

	def werewolf.howl
	  if FULLMOON then
		puts( "How-oo-oo-oo-oo!" )
	  else
		talk
	  end
	end

请注意，即使此方法已在 Creature 类之外声明，它也可以调用实例方法 `talk`。那是因为 `howl` 方法现在存在于 werewolf 对象内部，因此在该对象中具有与 `talk` 方法相同的作用域。然而，它不会存在于任何狼人（werewolf）的同伴生物之中; `howl` 方法属于它并仅属于它一个。尝试执行 `budgie.howl`，Ruby 会告诉你 `howl` 是一个未定义（undefined）的方法。

现在，如果你正在调试供自己使用的代码，那么由于未定义的方法导致程序奔溃可能是可以接受的；但如果你的程序在“终端用户”这个庞大而恶劣的环境中发生这样的情况，那绝对是不可接受的。

如果你认为未定义的方法可能是一个问题，你可以采取避免措施，在使用单例方法之前测试是否存在该单例方法。Object 类有一个 `singleton_methods` 方法，它返回一个包含单例方法名称的数组。你可以使用 Array 类的 `include?` 方法来测试方法名称是否包含在数组中。例如，在 **singleton_meth2.rb** 中，我编写了一个“打开盒子”的游戏，有许多 Box 对象，只有其中一个在打开时获得星星奖励。我已经将这个特殊的 Box 对象命名为 **star-prize** 并给它一个单例方法 `congratulate`：

<div class="code-file clearfix"><span>singleton_meth2.rb</span></div>

	starprize = Box.new( "Star Prize" )
	def starprize.congratulate
	  puts( "You've won a fabulous holiday in Grimsby!" )
	end

当 **starprize** 盒子被打开时 `congratulate` 方法会被调用。这段代码（其中 `item` 是一个 Box 对象）确保了当其它盒子被打开时这个方法（在其它对象中不存在）不会被调用。

	if item.singleton_methods.include?("congratulate") then
	  item.congratulate
	end

检查方法有效性的另一种方法是将该方法名称作为符号（以冒号开头的标识符）传递给 Object 类的 `respond_to?` 方法：

	if item.respond_to?(:congratulate) then
	  item.congratulate
	end

<div class="note">
我们将在第 20 章中讨论处理不存在的方法的另一种方式。
</div>

### 单例类

单例方法是属于单个对象的方法。另一方面，单例类（singleton class）则是定义单个对象的类。感到困惑？我也是。那么让我们仔细看看这些令人讨厌的东西...

假设你创建了几十个对象，每个对象都是 Object 类的一个实例。自然的，它们都可以访问 Object 类的常用方法，例如 `inspect` 和 `class`。但是现在你决定只想要一个特殊的对象（为了区别，让我们称之为 `ob`），它有一个特殊的方法（让我们称之为 `blather`）。

你不希望为此对象定义一个全新的类，因为你永远不会再创建更多拥有 `blather` 方法的对象。 所以你特别针对 `ob` 创建了一个类。

你无需为该类命名。你只需要通过在 class 关键字和对象名之间放置一个 `<<` 符号将它本身附加到 `ob` 上。然后你可以以通常的方式在类中添加代码：

<div class="code-file clearfix"><span>singleton_class.rb</span></div>

	ob = Object.new
	# singleton class
	class << ob
	  def blather( aStr )
		puts("blather, blather #{aStr}")
	  end
	end

现在 `ob`，并且只有 `ob`，不仅拥有 Object 类的所有常用方法；它也拥有了（这里只有 `blather` 方法，但原则上可以有更多）自己特殊的匿名类（anonymous class）中的方法：

	ob.blather( "weeble" )  #=> “blather, blather weeble”

如果你一直在密切关注，你可能已经注意到单例类（singleton class）似乎正在做一些与单例方法（singleton method）类似的事情。使用单例类，我可以创建一个对象，然后在匿名类中打包添加额外的方法。使用单例方法，我可以创建一个对象，然后逐个添加方法：

	ob2 = Object.new

	def ob2.blather( aStr ) # <= this is a singleton method
	  puts( "grippity, grippity #{aStr}" )
	end

	ob2.blather( "ping!" )  #=> grippity, grippity ping!

<div class="code-file clearfix"><span>singleton_class2.rb</span></div>

同样地，我可以重写 "star prize" 程序。在之前的版本中一个名为 `starprize` 的对象添加了一个单例方法，`congratulate`。我也可以很容易地创建一个包含 `congratulate` 方法的单例类：

	starprize = MyClass.new( "Star Prize" )

	class << starprize
	  def congratulate
		puts( "You've won a fabulous holiday in Grimsby!" )
	  end
	end

事实上，相似性不仅限于表面。上面代码的最终结果是 congratulate 成为 `starprize` 的单例方法，并且我已经使用此测试进行了验证：

	if item.singleton_methods.include?("congratulate")

<div class="note">
	<p class="h4"><b>单例方法，单例类 - 有什么区别？</b></p>

简单的说：区别不大。这两种语法提供了向特定对象添加方法，而不是将这些方法构建到其定义类中的不同实现方式。
</div>

### 重写方法（Overriding）

有时你可能想要重新定义某个类中已存在的方法。之前我们已经这样做过了，例如，我们创建了一些类它们自己的 `to_s` 方法来返回字符串的表示。从 Object 向下的每个 Ruby 类都有一个 `to_s` 方法。Object 类的 `to_s` 方法返回类名和对象唯一标识符的十六进制表示形式。但是，许多 Ruby 类都有自己特殊的 `to_s` 版本。例如，Array.to_s 连接并返回数组中的元素值。

当一个类中的方法替换祖先类中的同名方法时，它被称为“重写”（override）该方法。你可以重写标准类库中定义的方法，例如 `to_s`，以及你自己的类中定义的方法。如果你需要向现有方法添加一些新的行为，请记住在重写方法的开头使用 `super` 关键字调用超类的方法。这是一个例子：

<div class="code-file clearfix"><span>override.rb</span></div>

	class MyClass
	  def sayHello
		return "Hello from MyClass"
	  end

	  def sayGoodbye
		return "Goodbye from MyClass"
	  end
	end

	class MyOtherClass < MyClass
	  def sayHello     #overrides (and replaces) MyClass.sayHello
		return "Hello from MyOtherClass"
	  end

	  # overrides MyClass.sayGoodbye but first calls that method
	  # with super. So this version "adds to" MyClass.sayGoodbye
	  def sayGoodbye
		return super << " and also from MyOtherClass"
	  end

	  # overrides default to_s method
	  def to_s
		return "I am an instance of the #{self.class} class"
	  end
	end

### public, private 和 protected

在某些情况下，你可能希望限制方法的“可见性”（visibility），以确保定义方法的类之外的代码不能调用它们。

当你的类定义了它所需的各种“实用的”（utility）工具方法以执行它不打算公开使用的某些功能时，这将是很有用的。通过对这些方法施加访问限制，你可以阻止程序员将它们用于自己的恶意目的。这也意味着你将能够在以后阶段中更改这些方法的实现，而不必担心你将破坏其他人的代码。

Ruby 提供了三个级别的方法可访问性：

	public
	protected
	private

顾名思义，`public` 方法是最容易访问的，`private` 方法是最不易访问的。除非另有说明，否则你编写的所有方法都是公开（public）的。当一个方法是公共（public）的时，它可以被定义该对象的整个类之外的环境使用。

当方法是**私有**（`private`）的时，它只能由定义该对象的类内的其它方法使用。

一个**受保护**（`protected`）的方法通常以与私有方法相同的方式工作，但具有一个微小但重要的区别：除了对当前对象的方法可见之外，受保护的方法对于具有相同类型并且处于第一个对象作用域内的第二个对象也是可见的。

当你看一个可运行的示例时，私有和受保护方法之间的区别可能更容易理解。思考这个类：

<div class="code-file clearfix"><span>pub_prot_priv.rb</span></div>

	class MyClass

	  private
	  def priv
		puts( "private" )
	  end

	  protected
	  def prot
		puts( "protected" )
	  end

	  public
	  def pub
		puts( "public" )
	  end

	  def useOb( anOb )
		anOb.pub
		anOb.prot
		anOb.priv
	  end
	end

我已经声明了三个方法，每个方法都有一个级别的可访问性。这些级别是通过在一个或多个方法之前放置 `private`，`protected` 或 `public` 来设置的。在指定其它某些访问级别之前，指定的可访问性级别对所有后续方法保持有效。

<div class="note">

**注意**：`public`，`private` 和 `protected` 可能看起来像关键字。但事实上，它们是 Module 类的方法。

</div>

最后，我的类有一个公共方法 `useOb`，它将 `MyOb` 对象作为一个参数并调用该对象的三个方法 `pub`，`prot` 和 `priv`。现在让我们看看如何使用 MyClass 对象。首先，我将创建该类的两个实例：

	myob = MyClass.new
	myob2 = MyClass.new

现在，我尝试依次调用这三个方法...

	myob.pub  # This works! Prints out "public"
	myob.prot # This doesn't work! I get a 'NoMethodError'
	myob.priv # This doesn't work either - another 'NoMethodError'

从上面可以看出，公共（public）方法在调用的对象之外的环境中（正如预期的那样）是可见的。但私有（private）和受保护（protected）的方法都是不可见的。所以，受保护的方法的用途是什么？另一个示例应该有助于理解这一点：

	myob.useOb( myob2 )

这一次，我调用了 `myob` 对象的公共方法 `useOb`，并且我将第二个对象 `myob2` 作为参数传递给它。需要注意的重要一点是 `myob` 和 `myob2` 是同一个类的实例。现在，回想一下我之前说过的话：

> *除了对当前对象的方法可见之外，受保护的方法对于具有相同类型并且处于第一个对象作用域内的第二个对象也是可见的。*

这可能听起来像官方话（gobbledygook）。让我们看看是否可以分析一下来理解它。

在程序中，第一个 MyClass 对象（此处为 `myob`）在当 `myob2` 作为参数传递给它的方法时，在它的作用域内就有了第二个 MyClass 对象。当发生这种情况时，你可以认为 `myob2` 存在于 `myob` 内部。现在 `myob2` 共享了容器（containing）对象 `myob` 的作用域。在这种特殊情况下 - 当同一个类的两个对象在该类定义的作用域内时 - 该类的任何对象的受保护（protected）方法将变得可见。

在本例中，`myob2`（这里名为 `anob` 的参数 - '接收'了 `myob2`）的受保护方法 `prot` 变得可见并且可以被执行。然而，它的私有方法仍然是不可见的：

	def useOb( anOb )
	  anOb.pub
	  anOb.prot  # protected method can be called
	  anOb.priv  # but calling a private method results in an error
	end

## 深入探索

### 子类中的 protected 和 private

调用父类和子类的对象上的方法时，适用相同的访问规则。也就是说，当你传递给一个方法一个对象（作为一个参数），该对象与接收者对象（即方法所属的对象）具有相同的类，参数对象可以调用类的 public 和 protected 方法，但不能调用其 private 方法。

<div class="code-file clearfix"><span>protected.rb</span></div>

有关此示例，请查看 **protected.rb** 程序。在这里，我创建了一个名为 `myob` 的 MyClass 对象和一个 MyOtherClass 对象 `myotherob`，并且 MyOtherClass 继承自 MyClass。我尝试将 `myotherob` 作为参数传递给 `myob` 的公共方法，`shout`：

	myob.shout( myotherob )

但是 `shout` 方法在参数对象上调用 private 方法 `priv`：

	def shout( anOb ) # calls a private method
	  puts( anOb.priv( "This is a #{anOb.class} - hurrah" ) )
	end

这将不能运行！Ruby 解释 `priv` 方法是私有的。

同样，如果我反过来这样做 - 也就是说，通过将父类对象 `myob`作为参数传递，并在子类对象上调用方法 `shout`，我会得到同样的错误：

	myotherob.shout( myob )

MyClass 类还有另一个公共方法，`exclaim`。这次会调用一个 protected 方法，`prot`：

	def exclaim( anOb ) # calls a protected method
	  puts( anOb.prot( "This is a #{anOb.class} - hurrah" ) )
	end

现在，我可以将 MyClass 对象 `myob` 或 MyOtherClass 对象 `myotherob` 作为参数传递给 `exclaim` 方法，并且在调用 protected 方法时都不会发生错误：

	myob.exclaim( myotherob )  # This is OK
	myotherob.exclaim( myob )  # And so is this…

不用说，这仅在两个对象（接收器和参数）共享相同的继承链时才有效。如果发送不相关的对象作为参数，则无论其保护级别如何，你都无法调用接收器对象所属类的方法。

### 突破 private 方法的隐私限制

私有方法的重点在于它不能从它所属的对象作用域之外被调用。所以这将不起作用：

<div class="code-file clearfix"><span>send.rb</span></div>

	class X
	  private
	  def priv( aStr )
		puts("I'm private, " << aStr)
	  end
	end

	ob = X.new
	ob.priv( "hello" ) # This fails

然而，事实证明 Ruby 以一种叫做 `send` 方法的形式提供了一个'get out'子句（或者我应该说'get in'子句？）。`send` 方法调用方法名称与符号（一个以冒号开头的标识符，例如 `:priv`）相匹配的方法，该方法名称作为第一个参数传递给 `send`，如下所示：

	ob.send( :priv, "hello" )  # This succeeds

符号后面提供的任何参数（如字符串，"hello"）都以正常方式传递给指定的方法。

可以说使用 `send` 获取私有方法的公共访问权通常不是一个好主意（否则，为什么你首先将该方法设为私有的），所以应谨慎使用或根本不使用...

### 单例类方法

之前，我们通过将方法名称附加到类的名称后面来创建类方法（class method），如下所示：

	def MyClass.classMethod

有一种“快捷”语法。这是个示例：

<div class="code-file clearfix"><span>class_methods3.rb</span></div>

	class MyClass
	  def MyClass.methodA
		puts("a")
	  end

	  class << self
		def methodB
		  puts("b")
		end

		def methodC
		  puts("c")
		end
	  end
	end

这里，`methodA`，`methodB` 和 `methodC` 都是 MyClass 的类方法；`methodA` 是使用我们之前使用的语法声明的：

> def &lt;ClassName&gt;.&lt;methodname&gt;

但是,使用实例方法的语法声明了 `methodB` 和 `methodC`：

> def &lt;methodname&gt;

那么为什么它们最终成为类方法呢？这完全归结于方法声明已放在此代码中：

	class << self
	  # some method declarations
	end

这可能会让你想起用于声明单例类（singleton classe）的语法。例如，在 **singleton_class.rb** 程序中，你可能还记得我们首先创建了一个名为 `ob` 的对象，然后给它声明了一个自己的方法，`blather`：

	class << ob
	  def blather( aStr )
		puts("blather, blather #{aStr}")
	  end
	end

这里的 `blather` 方法是 `ob` 对象的单例方法（singleton method）。类似地，在 **class_methods3.rb** 程序中，`methodB` 和 `methodC` 方法是 `self` 的单例方法，而 `self` 恰好是 MyClass 类。我们可以类似地通过使用 `<<` 后跟类名来从类定义之外添加单例方法，如下所示：

	class << MyClass
	  def methodD
		puts( "d" )
	  end
	end

### 嵌套方法

你可以嵌套（nest）方法（将一个方法嵌套在另一个方法中）。这为你提供了一种将长方法划分为可重用块的方式。因此，例如，如果方法 `x` 需要在几个不同的点进行 `y` 计算，则可以将 `y` 方法放在 `x` 方法中：

<div class="code-file clearfix"><span>nested_methods.rb</span></div>

	class X

	  def x
		print( "x:" )

		def y
		  print("ha! ")
		end

		def z
		  print( "z:" )
		  y
		end

		y
		z
	  end

	end

嵌套方法默认在定义它们的作用域之外是不可见的。因此，在上面的示例中，虽然可以从 `x` 内部调用 `y` 和 `z`，但是任何其它代码都不能调用它们：

	ob = X.new
	ob.y #<= error
	ob.z # <= error

但是，当你运行一个包含嵌套方法的方法时，这些嵌套方法将被带入该方法之外的作用域内！

<div class="code-file clearfix"><span>nested_methods2.rb</span></div>

	class X
	  def x
		print( "x:" )

		def y
		  print("y:")
		end

		def z
		  print( "z:" )
		  y
		end
	  end
	end

	ob = X.new
	ob.x #=> x:
	puts
	ob.y #=> y:
	puts
	ob.z #=> z:y:

### 方法名称

最后一点，值得一提的是 Ruby 中的方法名称几乎总是以小写字符开头，如下所示：

	def fred

但是，这只是一个习惯约定，而非必须的。也可以用大写字母开头的方法名称，如下所示：

	def Fred

由于 `Fred` 方法看起来像一个常量（以大写字母开头），因此你需要在调用它时添加括号来告诉 Ruby，它是一个方法：

<div class="code-file clearfix"><span>method_names.rb</span></div>

	Fred   # <= Ruby complains "uninitialized" constant
	Fred() # <= Ruby calls the Fred method

总的来说，最好坚持使用以小写字符开头的方法名称的约定。

# 第八章

***

## 传递参数和返回值

在本章中，我们将研究传递参数和给方法返回值以及从方法获得返回值的许多效果和副作用。首先，让我们花点时间总结一下我们现在使用的方法类型：

<div class="code-file clearfix"><span>methods.rb</span></div>

### 1. 实例方法

实例方法（instance method）在类定义中声明，旨在供类的特定对象或“实例”（instance）使用，如下所示：

	class MyClass
	  # declare instance method
	  def instanceMethod
		puts( "This is an instance method" )
	  end
	end

	# create object
	ob = MyClass.new
	# use instance method
	ob.instanceMethod

### 2. 类方法

在这两种情况下，类方法（class method）可以在类定义中声明：a）类名在方法名之前；b）`class << self` 块包含'普通'（normal）的方法声明。无论哪种方式，类方法都是由类本身使用，而不是由特定对象使用，如下所示：

	class MyClass
	  # a class method
	  def MyClass.classmethod1
		puts( "This is a class method" )
	  end

	  # another class method
	  class << self
		def classmethod2
		  puts( "This is another class method" )
		end
	  end
	end

	# call class methods from the class itself
	MyClass.classmethod1
	MyClass.classmethod2

### 3. 单例方法

单例方法（singleton method）是添加到单个对象的方法，不能被其他对象使用。单例方法可以通过将方法名称附加到对象名称后跟一个点或者通过将“普通”（normal）方法定义放在 `<ObjectName> << self` 块中来定义，如下所示：

	# create object
	ob = MyClass.new

	# define a singleton method
	def ob.singleton_method1
	  puts( "This is a singleton method" )
	end

	# define another singleton method
	class << ob
	  def singleton_method2
		puts( "This is another singleton method" )
	  end
	end

	# use the singleton methods
	ob.singleton_method1
	ob.singleton_method2

### 返回值

在许多编程语言中，一个差异是函数（function）或方法（method）是否会将值返回给调用代码。例如，在 Pascal 中，函数（function）会返回一个值，但例程（procedure）不会。在 Ruby 中没有这样的区别差异。所有方法总是返回一个值，当然，你不一定使用它。

<div class="code-file clearfix"><span>return_vals.rb</span></div>

如果未指定返回值，则 Ruby 方法返回最后一个计算表达式的结果。思考这个方法：

	def method1
	  a = 1
	  b = 2
	  c = a + b # returns 3
	end

计算的最后一个表达式是 `a + b`，它恰好返回 3，因此这是该方法返回的值。有时候你可能不想要返回最后计算的表达式的结果。在这种情况下，你可以使用 `return` 关键字显式指定返回值：

	def method2
	  a = 1
	  b = 2
	  c = a + b
	  return b # returns 2
	end

方法不一定要进行任何赋值操作以提供返回值。一些简单的数据（即，对其自身求值），如果这恰好是方法中计算的最后一个操作，这将作为返回值。如果没有计算任何东西，则返回 `nil`：

	def method3
	  "hello" # returns "hello"
	end

	def method4
	  a = 1 + 2
	  "goodbye" # returns "goodbye"
	end

	def method5
	end # returns nil

我自己的编程习惯是编写尽可能清晰明确的代码。出于这个原因，每当我打算使用方法的返回值时，我更喜欢使用 `return` 关键字显式指定它；只有当我不打算使用返回值时，我才会省略它。但是，这不是强制性的 - Ruby 给了你选择的权利。

### 返回多个值

但是当你需要一个方法来返回多个值时呢？在其它编程语言中你可以通过传递一个引用（reference）参数（指向原始数据的指针）而不是值参数（数据的副本）来间接实现；当你更改了引用参数的值时，你在没有显式的返回值给调用代码的情况下就修改了原始值。

Ruby 没有对"引用"（by reference）和"值"（by value）进行区分，所以我们无法使用这种技术（大多数时候，尽管我们很快会看到规则的一些例外）。但是，Ruby 能够一次性返回多个值，如下所示：

<div class="code-file clearfix"><span>return_many.rb</span></div>

	def ret_things
	  greeting = "Hello world"
	  a = 1
  	b = 2.0
	  return a, b, 3, "four", greeting, 6 * 10
	end

多个返回值会被放入数组中。如果您要运行 `ret_things.class`，Ruby 会告诉你返回的对象是一个 Array。

但是，你可以显式的指定一个不同类型的集合，例如 Hash：

	def ret_hash
	  return {'a'=>'hello', 'b'=>'goodbye', 'c'=>'fare thee well'}
	end

### 默认参数和多参数

Ruby 允许你指定参数的默认值。可以使用通常的赋值运算符在方法的参数列表中指定默认值：

	def aMethod( a=10, b=20 )

如果将未赋值的变量传递给该方法，则将为其分配默认值。但是，如果传递了赋值的变量，则为其赋的值优先于默认值：

	def aMethod( a=10, b=20 )
	  return a, b
	end

	p( aMethod )        #=> displays: [10, 20]
	p( aMethod( 1 ))    #=> displays: [1, 20]
	p( aMethod( 1, 2 )) #=> displays: [1, 2]

在某些情况下，方法可能需要能够接收不确定数量的参数 - 例如，处理可变长度的项列表的方法。在这种情况下，您可以“删除”任意数量的尾随参数，然后在最后一个参数前面加上星号：

<div class="code-file clearfix"><span>default_args.rb</span></div>

	def aMethod( a=10, b=20, c=100, *d )
	  return a, b, c, d
	end

	p( aMethod( 1,2,3,4,6 ) ) #=> displays: [1, 2, 3, [4, 6]]

### 赋值和参数传递

大多数情况下，Ruby 方法有两个接入点 - 比如进出房间的门。参数列表提供了入口；返回值提供了出口。对输入参数的修改不会影响原始数据，原因很简单，当 Ruby 计算表达式时，该计算结果会创建一个新对象 - 因此对参数所做的任何更改只会影响新对象，而不会影响原始对象数据。但是这个规则有例外，我们稍后会看到这样的示例。

<div class="code-file clearfix"><span>in_out.rb</span></div>

让我们从最简单的情况开始：一个方法，它将获取一个值，其作为命名参数，并返回另一个值：

	def change( x )
	  x += 1
	  return x
	end

从表面上看，你可能会认为我们正在处理单个对象 `x`，这里：对象 `x` 进入 `change` 方法并返回同一个对象 `x`。事实上，情况并非如此。一个对象进入（参数），出来的是一个不同的对象（返回值）。你可以轻松验证这一点，使用 `object_id` 方法以显示程序中每个对象的唯一标识数字：

	num = 10
	puts( "num.object_id=#{num.object_id}" )
	num = change( num )
	puts( "num.object_id=#{num.object_id}" )

在调用 `change` 方法之前和之后，变量 `num` 的标识符是不同的。这表明，即使变量名保持不变，`change` 方法返回的 `num` 对象也不同于发送给它的 `num` 对象。

<div class="code-file clearfix"><span>method_call.rb</span></div>

方法调用本身与对象的更改无关。你可以通过运行 **method_call.rb** 来验证这一点。这只是将 `num` 对象传递给 `change` 方法并返回它：

	def nochange( x )
	  return x
	end

在这种情况下，返回之后的 `num` 与发送到方法之前的 `num` 的 `object_id` 相同。换句话说，进入方法的对象与再次出来的对象完全相同。这产生了一个必然的结论，即在 `change` 方法（`x += 1`）中有一些关于*赋值（assignment）*的行为导致创建了新的对象。

但是赋值行为本身并不能解释这个问题。如果只是为自己分配一个变量，则不会创建新对象...

<div class="code-file clearfix"><span>assignment.rb</span></div>

	num = 10
	num = num # a new num object is not created

那么，如果你为对象分配的值与已有的值相同怎么办？

	num = 10
	num = 10  # a new num object is not created

这表明单独的赋值必定不会创建新的对象。现在让我们尝试分配一个新值...

	num = 10
	num += 1  # this time a new num object is created

通过查看 `object_id`，我们可以确定当为现有的变量分配新值时，会创建一个新对象。

大多数数据项被视为是唯一的，因此一个字符串，"hello" 被认为与其它另一个字符串 "hello" 不同，以及一个浮点数 10.5 被认为与其它另一个浮点数 10.5 不同。因此，任何字符串（string）或浮点数（float）赋值操作都将创建一个新对象。

但是在处理整数（integer）时，只有当分配的值与前一个值不同时才会创建一个新对象。你可以在赋值运算符的右侧执行各种复杂的操作，但如果生成的值与原始值相同，则不会创建新对象...

	num = (((num + 1 - 1) * 100) / 100)  # a new object is not created!

<div class="code-file clearfix"><span>object_ids.rb</span></div>

### 整数是特殊的

在 Ruby 中，整数（integer，亦 Fixnum）具有固定的标识。数字 10 的每个实例或者赋值为 10 的每个变量将具有相同的 object_id。其它数据类型并非如此。浮点数（例如 10.5）或者字符串（例如 "hello world"）的每个实例都是具有唯一 object_id 的不同对象。请注意，当你为变量分配整数（integer）值时，该变量将具有整数值本身 object_id。但是，当你将某些其它类型的数据分配给变量时，即使每次分配的数据值本身相同，也会创建一个新对象：

	# 10 and x after each assignment are the same object
	puts( 10.object_id )
	x = 10
	puts( x.object_id )
	x = 10
	puts( x.object_id )

	# 10.5 and x after each assignment are 3 different objects!
	puts( 10.5.object_id )
	x = 10.5
	puts( x.object_id )
	x = 10.5
	puts( x.object_id )

但为什么这些都很重要？

答案是，因为该规则的一些罕见例外是相当重要的。正如我之前所说，大部分情况下，方法都有明确定义的入口和明确定义的出口。一旦参数进入方法，它就会进入一个封闭的空间。该方法之外的任何代码都无法了解对参数所做的任何更改，直到它以返回值的形式再次出来。事实上，这是“纯粹”（pure）的面向对象的深层秘密之一。原则上，方法的实现细节应该隐藏起来 - “封装”（encapsulated）。这可以确保对象外部的代码不能依赖于该对象内发生的事情。

### 进出原则

在大多数现代 OOP（面向对象编程）语言（如 Java 和 C#）中，封装（encapsulation）和信息隐藏并未严格执行。另一方面，在 Smalltalk 中 - 最著名和最有影响力的 OOP 语言 - 封装和信息隐藏是基本原则：如果将变量 `x` 发送到方法 `y`，并且在 `y` 内部更改 `x` 的值，则无法从方法外部获取 `x` 的更改值 - *除非方法显式返回该值*。

<div class="note">
	<p class="h4"><b>“封装”（Encapsulation）或“信息隐藏”（Information Hiding）？</b></p>

通常这两个术语可互换使用。但是，要进行挑选的话，则是存在差异的。

*封装（Encapsulation）*是指将对象的“状态”（state，其数据）与可能改变或询问其状态（方法）的操作组合在一起。

*信息隐藏（Information Hiding）*是指数据被封锁并且只能使用明确定义的路径进出访问这一事实 - 在面向对象的术语中，这意味着获取或返回值的“访问器方法”（accessor methods）。

在面向过程语言中，信息隐藏可能采取其它形式 - 例如，你可能必须定义接口以从代码“单元”或“模块”而不是对象中检索数据。

在 OOP 术语中，封装和信息隐藏几乎是同义词 - 真正的封装必然意味着隐藏了对象的内部数据。但是，许多现代的“OOP语言”，例如 Java，C#，C++ 和 Object Pascal，在强制执行信息隐藏的程度上（如果有的话）是非常宽容的。
</div>

通常，Ruby 遵循这个原则：参数进入方法，除非 Ruby 返回更改的值，否则无法从外部访问方法内的对该参数的任何更改：

<div class="code-file clearfix"><span>hidden.rb</span></div>

	def hidden( aStr, anotherStr )
	  anotherStr = aStr + " " + anotherStr
	  return aStr + anotherStr.reverse
	end

	str1 = "dlrow"
	str2 = "olleh"
	str3 = hidden(str1, str2) # str3 receives returned value
	puts( str1 )              # input args: original values unchanged
	puts( str2 )
	puts( str3 )              # returned value ( "dlrowhello world" )

事实证明，有时候传递给 Ruby 方法的参数可以像其它语言的'引用'（by reference）参数一样使用（也就是说，在方法内部进行的更改可能会影响方法之外的变量）。这是因为某些 Ruby 方法修改了原始对象，而不是生成值并将其分配给新对象。

例如，一些以感叹号结尾方法会修改原始对象。类似的，字符串的附加（append）方法 `<<` 将其右侧的字符串与左侧字符串连接起来的过程中没有创建新的对象：因此左侧字符串的值被修改，但字符串对象本身保留其原始的 `object_id`。

这样做的结果是，如果在方法中使用 `<<` 运算符而不是 `+` 运算符，则结果将更改：

<div class="code-file clearfix"><span>not_hidden.rb</span></div>

	def nothidden( aStr, anotherStr )
	  anotherStr = aStr << " " << anotherStr
	  return aStr << anotherStr.reverse
	end

	str1 = "dlrow"
	str2 = "olleh"
	str3 = nothidden(str1, str2)
	puts( str1 ) # input arg: changed ("dlrow ollehhello world")
	puts( str2 ) # unchanged
	puts( str3 ) # returned value("dlrow ollehhello world")

<div class="code-file clearfix"><span>str_reverse.rb</span></div>

**str_reverse.rb** 示例程序应该有助于解释清楚这一点。例如，这表明当你使用 `reverse` 方法时，不会对'接收对象'（receiver object，即 `str1` 这样的对象：`str1.reverse`）进行更改。但是当你使用 `reverse!` 方法对对象进行更改（使其字母顺序反转）。即便如此，也没有创建新对象：`str1` 是调用 `reverse!` 方法前后的同一个对象。

这里 `reverse` 像大多数 Ruby 方法一样运行 - 它产生一个值，为了使用该值，你必须将它分配给一个新对象。所以...

	str1 = "hello"
	str1.reverse

这里，`str1` 不受调用 `reverse` 的影响。它仍然具有值 'hello'，它仍然具有原始的 `object_id`。但是...

	str1 = "hello"
	str1.reverse!

这次，`str1` 改变了（变成了，'olleh'）。即便如此，也没有创建新对象：`str1` 具有与之前相同的 `object_id`。然后再次...

	str1 = "hello"
	str1 = str1.reverse

这次，`str1.reverse` 产生的值被分配给 `str1`。生成的值是一个新对象，因此 `str1` 现在被分配了反向字符串（'olleh'），现在它有一个新的 `object_id`。

有关字符串连接方法 `<<` 的示例，请参阅示例程序 **concat.rb**，像那些以 `!` 结尾的方法一样，会修改接收对象（receiver object）而不创建新对象：

<div class="code-file clearfix"><span>concat.rb</span></div>

	str1 = "hello"
	str2 = "world"
	str3 = "goodbye"
	str3 = str2 << str1

在这个示例中，`str1` 永远不会被修改，所以它始终具有相同的 `object_id`；`str2` 通过连接操作被修改。

但是，`<<` 运算符不会创建新对象，因此 `str2` 也会保留其原始 `object_id`。但 `str3` 最后是一个与开始不同的对象：这是因为它被赋值由这个表达式产生的值：`str2 << str1`。这个值恰好是 `str2` 对象本身，因此 `str3` 的 `object_id` 现在与 `str2` 的 `object_id` 相同（即 `str2` 和 `str3` 现在引用相同的对象）。

总之，以 `!` 结尾的方法，比如 `reverse!`，再加上一些其它方法，比如 `<<` 连接方法，会改变接收者对象（receiver object）本身的值。大多数其它方法不会修改接收对象的值，并且为了利用因调用方法而产生的任何新值，你必须将该值赋给变量（或将生成的值作为参数传递给一个方法）。

<div class="note">
	<p class="h4"><b>修改接收对象违背封装原则</b></p>

大多数方法看起来不是足够无害的，但事实上只有极少一部分方法会修改接受对象（receiver object）。但请注意：这种行为使你能够通过引用而不是显式的返回值来重新获取参数值。通过允许你的代码依赖于方法内部的实现细节，这样做会破坏封装（encapsulation）原则。这可能会导致不可预测的副作用，在我看来，应该避免这么做。
</div>

<div class="code-file clearfix"><span>side_effects.rb</span></div>

这是一个依赖于修改的参数值而不是显式的返回值，可能会对实现细节引入一些不必要的依赖关系的简单（但在实际编程中，可能是严重的）示例，请参阅 **side_effects.rb**。这里我们有一个名为 `stringProcess` 的方法，它接受两个字符串参数，将它们混淆并返回结果。假设练习的对象接收两个小写字符串并返回一个单独的字符串，该字符串会以空格分隔并且首字母和最后一个字母大写的方式组合这两个字符串。所以两个原始字符串可能是 "hello" 和 "world"，返回的字符串是 "Hello hellD"。

但现在我们有一个不耐烦的程序员，他不想使用返回值。他注意到在方法中进行的修改会改变进入参数的值。他注意到在方法中进行的修改会改变进入参数的值。哎呀！（他决定）他不妨使用参数自己来实现！然后他编写了一个非常复杂的文本处理系统，其中有数千块代码依赖于这两个参数被修改的值。

但是最初编写 `stringProcess` 方法的程序员现在觉得原来的实现是低效且不优雅的，因此决定重写代码，确信返回的值也不会变（如果 "hello" 和 "world" 作为参数发送，"Hello worlD" 将被返回）。

啊哈！但是新实现会导致输入参数的值在方法体内会被更改。所以，这位没有耐心的程序员的这个依赖于这些输入*参数*而不是返回值的文本处理系统，现在输入这些参数时会输出 "hello Dlrow"，而不是他期望的 "Hello worlD"（实际上，他的程序在处理莎士比亚（Shakespeare ）的作品时，最终划时代的艺术家会宣称："To
eb or ton to eb, that si the noitseuq..."）。这是一种意想不到的副作用，可以通过遵循单向输入（one-way-in）和单向输出（one-way-out）原则轻松避免。

### 并行赋值

我前面提到过，一个方法可以返回多个值，用逗号分隔。 通常，你需要将这些返回值分配给一组匹配变量。

在 Ruby 中，这可以通过并行赋值在单个操作中完成。这意味着你可以在赋值运算符的左侧使用多个变量，在右侧使用多个值。右侧的值将按顺序分配给左侧的变量，如下所示：

<div class="code-file clearfix"><span>parallel_assign.rb</span></div>

	s1, s2, s3 = "Hickory", "Dickory", "Dock"

这种能力不仅为你提供了进行多次赋值的快捷方式；它还允许你交换变量的值（你只需在赋值运算符的任一侧更改它们的顺序）：

	i1 = 1
	i2 = 2

	i1, i2 = i2, i1  #=> i1 is now 2, i2 is 1

并且你可以根据方法返回值进行多次赋值：

	def returnArray( a, b, c )
	  a = "Hello, " + a
	  b = "Hi, " + b
	  c = "Good day, " + c
	  return a, b, c
	end

	x, y, z = returnArray( "Fred", "Bert", "Mary" )

如果在左侧指定的变量多于赋值运算符右侧的值，则任何“尾随”变量都将分配为 `nil`：

	x, y, z, extravar = returnArray( "Fred", "Bert", "Mary" ) # extravar = nil

方法返回的多个值将放入数组中。当你将数组放在多变量赋值的右侧时，它的各个元素将分配给每个变量，如果提供的变量太多，则额外的将被分配为 `nil`：

	s1, s2, s3 = ["Ding", "Dong", "Bell"]

## 深入探索

### 引用或值传参

搜索互联网，你很快就会发现 Ruby 程序员经常会讨论 Ruby 是通过'值'（by value）还是'引用'（by reference）传递参数。

在诸如 Pascal 和 C 等许多面向过程编程语言及其衍生物中，通过值或通过引用传递参数之间存在明显的区别。

*'值'（by value）*参数是原始变量的副本；你可以将它传递给一个程序，修改它但是原始变量的值保持不变。

另一方面，*'引用'（by reference）*是一个指向原始变量的指针。当它传递给例程（procedure）时，你传递的不是新的副本，而是原始数据在内存中存储的地址引用。因此，在例程（procedure）中进行的任何更改都将对原始数据进行，并且必然会影响原始变量的值。

<div class="code-file clearfix"><span>arg_passing.rb</span></div>

实际上很容易解决这个问题。如果 Ruby 通过值传递，那么它会生成原始变量的副本，因此该副本将具有不同的 `object_id`。事实上，情况并非如此。尝试运行 **arg_passing.rb** 程序来证明这一点。

现在，很可能在某些情况下，参数的传递可以（“在幕后”）说是“按值”（by value）*实现*的。但是，这些实现细节应该是 Ruby 解释器和编译器的编写者而不是 Ruby 程序员的义务。事实很明显，如果你以'纯' OOP 方式编程 - 通过将参数传递给方法但只是随后使用这些方法返回的值 - 实现细节（通过值或通过引用）将不会给你带来任何意外后果。

然而，由于 Ruby 有时可以修改参数（例如使用如前所述的 `!` 或 `<<` 方法），一些程序员已经习惯使用参数本身的修改值（相当于在 C 中使用引用（By Reference）参数）而不是使用返回的值。在我看来，这是一种不好的做法。它使你的程序依赖于方法的实现细节，因此应该避免这么做。

### 赋值是拷贝还是引用？

我之前说过，当某个表达式产生一个值时会创建一个新对象。因此，例如，如果为名为 `x` 的变量分配新值，则赋值后的对象将与赋值之前的对象不同（即，它将具有不同的 `object_id`）：

	x = 10  # this x has one object_id
	x +=1   # and this x has a different one

但它不是因为赋值创建了新对象。而是新生成的值导致新对象被创建。在上面的示例中，`+=1` 是一个生成了新值的表达式（`x+=1` 等同于 `x=x+1`）。

简单的将一个变量赋值给另一个变量不会创建新对象。因此，假设你有一个名为 `num` 和另一个名为 `num2` 的变量。如果将 `num2` 赋值给 `num`，则两个变量都将引用同一个对象。你可以用 Object 类的 `equals?` 方法测试验证：

<div class="code-file clearfix"><span>assign_ref.rb</span></div>

	num = 11.5
	num2 = 11.5

	# num and num 2 are not equal
	puts( "num.equal?(num2) #{num.equal?(num2)}" )

	num = num2
	# but now they are equal
	puts( "num.equal?(num2) #{num.equal?(num2)}" )

<div class="code-file clearfix"><span>equal_tests.rb</span></div>

<div class="note">
	<p class="h4"><b>相等测试：`==` 或 `equal?`</b></p>

默认情况下（如 Ruby 的 Kernel 模块中所定义），当测试的两个对象都是同一个对象时，使用 `==` 的测试返回 `true`。因此，如果值相同但对象不同，它将返回 `false`：

	ob1 = Object.new
	ob2 = Object.new
	puts( ob1==ob2 ) #<= false

事实上，`==` 经常被诸如 String 之类的类重写，然后当值相同但对象不同时会返回 `true`：

	s1 = "hello"
	s2 = "hello"
	puts( s1==s2 ) #<= true

出于这个原因，当你想确定两个变量是否引用同一个对象时，最好使用 `equal?` 方法：

	puts( s1.equal?(s2) ) #<= false
</div>

### 什么时候两个对象是相同的？

作为一般规则，如果使用十个值初始化十个变量，则每个变量将引用一个不同的对象。例如，如果你创建两个这样的字符串...

<div class="code-file clearfix"><span>identical.rb</span></div>

	s1 = "hello"
	s2 = "hello"

...然后 `s1` 和 `s2` 将引用独立的对象。两个浮点数（float）也一样...

	f1 = 10.00
	f2 = 10.00

但是，如前所述，整数（integer）是不同的。创建具有相同值的两个整数，它们最终将引用相同的对象：

	i1 = 10
	i2 = 10

对于普通整数值而言就是 true。如果有疑问？请使用 `equal?` 方法测试两个变量或值是否引用完全相同的对象：

	10.0.equal?(10.0) # compare floats – returns false
	10.equal?(10)     # compare integers (Fixnums) – returns true

### 括号避免歧义

方法可以与局部变量共享相同的名称。例如，你可能有一个名为 `name` 的变量和一个名为 `name` 的方法。如果你习惯于在没有括号的情况下调用方法，那么这里是指方法还是变量是不明确的。括号再次避免含糊不清...

<div class="code-file clearfix"><span>parentheses.rb</span></div>

	greet = "Hello"
	name = "Fred"

	def greet
	  return "Good morning"
	end

	def name
	  return "Mary"
	end

	def sayHi( aName )
	  return "Hi, #{aName}"
	end

	puts( greet )            #<= Hello
	puts greet               #<= Hello
	puts( sayHi( name ) )    #<= Hi, Fred
	puts( sayHi( name() ) )  #<= Hi, Mary

# 第九章

***

## 异常处理

即使是精心编写的程序，有时也会遇到无法预料的错误。例如，如果编写需要从磁盘读取某些数据的程序，则可以假设指定的磁盘实际可用且数据有效。如果你的程序根据用户输入进行计算，则它假定输入适合用于计算。

虽然你可能会在一些潜在的问题出现之前尽可能的预料到 - 例如，通过编写代码来检查文件是否存在，然后再从中读取数据，或者在进行计算之前检查用户输入是否为数字 - 你永远无法提前预料到每个问题。

例如，用户可以在已经开始从 CD 中读取数据时移除 CD；或者，在你的代码尝试除以此值之前，某些模糊的计算可能会产生 0。当你知道在运行时（runtime）某些不可预见的情况可能导致你的代码被“中断”（break）时，你可以尝试使用“异常处理”（exception handling）来避免灾难。

“异常”（exception）是打包到对象中的错误。该对象是 Exception 类（或其后代之一）的一个实例。你可以通过捕获异常对象（Exception Object）来处理异常，可选地使用它包含的信息（比如打印相应的错误消息）并采取从错误中恢复所需的任何操作 - 可能通过关闭任何仍然打开的文件，或者分配合理的值给那些因错误计算而被分配了一些无意义的值的变量。

### Rescue

异常处理的基本语法可归纳如下：

	begin
	  # Some code which may cause an exception
	rescue <Exception Class>
	  # Code to recover from the exception
	end

下面是一个处理尝试除以零的异常的程序示例：

<div class="code-file clearfix"><span>exception1.rb</span></div>

	begin
	  x = 1/0
	rescue Exception
	  x = 0
	  puts( $!.class )
	  puts( $! )
	end

<div class="code-file clearfix"><span>div_by_zero.rb</span></div>

运行此代码时，除以零的尝试会导致异常。如果未处理（如示例程序 **div_by_zero.rb**），程序将崩溃。但是，通过将有问题的代码放在异常处理块（`begin` 和 `end` 之间）中，我已经能够在以 `rescue` 开头的部分中捕获异常。我做的第一件事是将变量 `x` 设置为有意义的值。接下来是这两个令人费解的语句：

	puts( $!.class )
	puts( $! )

在 Ruby 中，`$!` 是一个全局变量，为其分配了最后一个捕获的异常对象。打印 `$!.class` 会显示类名，这里是 "ZeroDivisionError"；单独打印变量 `$!` 会显示异常对象中包含的错误信息，这里是 "divided by 0"。

我一般都不太热衷于依赖全局变量，特别是当它们的'名字'与 `$!` 一样不具有描述性时。幸运的是，还有另一种选择。你可以通过将'关联运算符'（assoc operator），`=>` 放在异常的类名之后和变量名之前，将变量名与异常对象相关联：

<div class="code-file clearfix"><span>exception2.rb</span></div>

	rescue Exception => exc

你现在可以使用变量名称（此处为 `exc`）来引用 Exception 对象：

	puts( exc.class )
	puts( exc )

<div class="code-file clearfix"><span>exception_tree.rb</span></div>

<div class="note">
	<p class="h4"><b>Exceptions 有一个家族树（家谱）...</b></p>

要理解 `rescue` 子句如何捕获异常，只要记住，在 Ruby 中异常是对象，并且像所有其它对象一样，它们由一个类定义。此外，还有一个明确的“继承链”，就像所有 Ruby 对象都继承自 Object 类一样。
</div>

虽然看起来很明显，当你除以零时，你将得到一个 ZeroDivisionError 异常，在现实世界的代码中，有时候异常的类型不是那么可预测的。例如，假设你有一个基于用户提供的两个值进行除法计算的方法：

	def calc( val1, val2 )
	  return val1 / val2
	end

这可能会产生各种不同的异常。显然，如果用户输入的第二个值为 0，我们将得到 ZeroDivisionError。

但是，如果*第二个*值是字符串（string），则异常将是 TypeError，而*第一个*值是字符串时，它将是 NoMethodError（因为 String 类没有定义'除法运算符' `/`）。这里的 `rescue` 块处理所有可能发生的异常：

<div class="code-file clearfix"><span>multi_except.rb</span></div>

	def calc( val1, val2 )
	  begin
		result = val1 / val2
	  rescue Exception => e
		puts( e.class )
		puts( e )
		result = nil
	  end
	  return result
	end

通常，针对不同的异常采取不同的行为会很有用。你可以通过添加多个 `rescue` 块来实现。每个 `rescue` 子句都可以处理多个异常类型，异常类名用逗号分隔。这里我的 `calc` 方法在一个子句中处理 TypeError 和 NoMethodError 异常，并使用 catch-all 异常处理程序来处理其它所有异常类型：

<div class="code-file clearfix"><span>multi_except2.rb</span></div>

	def calc( val1, val2 )
	  begin
		result = val1 / val2
	  rescue TypeError, NoMethodError => e
		puts( e.class )
		puts( e )
		puts( "One of the values is not a number!" )
		result = nil
	  rescue Exception => e
		puts( e.class )
		puts( e )
		result = nil
	  end
	  return result
	end

<div class="code-file clearfix"><span>exception_tree.rb</span></div>

<div class="note">
	<p class="h4"><b>Object 类是所有异常类（exceptions）的最终祖先类。</b></p>

从 Object 类开始，派生出子类 Exception，然后是 StandardError，最后是更具体的异常类型，例如 ZeroDivisionError。如果你愿意，你可以编写一个 `rescue` 子句来处理 Object 类，因为 Object 是所有对象的祖先，这样确实会成功匹配一个异常对象：

	# This is possible...
	rescue Object => exc

但是，尽可能匹配 Exception 类的相关后代类通常更有用。作为更好的措施，附加一个处理 StandardError 或 Exception 对象的 `rescue` 子句是很有用的，以防止你没考虑到的异常类型被漏掉。你可以运行 `exception_tree.rb` 程序来查看 ZeroDivisionError 异常的家族树（继承链）。
</div>

在处理多个异常类型时，应始终让 `rescue` 子句先处理特定类型的异常，然后使用 `rescue` 子句处理通用类型的异常。

当特定类型异常（例如 TypeError）处理完时，`begin..end` 异常块将会退出，因此执行流程不会“进入”通用类型的 `rescue` 子句。但是，如果 `rescue` 子句首先处理通用类型的异常，那么它将处理所有类型的异常，因此任何用来处理更具体的类型的异常子句都将永远不会执行。

例如，如果我在 `calc` 方法中颠倒了 `rescue` 子句的顺序，首先放置了通用的 Exception 处理程序，这将匹配所有的异常类型，因此特定的 TypeError 和 NoMethodError 异常处理子句永远都不会运行：

<div class="code-file clearfix"><span>multi_except_err.rb</span></div>

	# This is incorrect...
	rescue Exception => e
	  puts( e.class )
	  puts( e )
	  result = nil
	rescue TypeError, NoMethodError => e
	  puts( e.class )
	  puts( e )
	  puts( "Oops! This message will never be displayed!" )
	  result = nil
	end

### Ensure

无论是否发生异常（Exception），你可能会在某些情况下采取某些特定操作。例如，每当你处理某种不可预测的输入/输出时 - 例如，在使用磁盘上的文件和目录时 - 总是有可能位置（磁盘或目录）或数据源（文件）根本不存在或者可能发生其它类型的问题 - 例如当你尝试写入时磁盘已满，或者尝试读取时可能包含一个错误类型的数据。

无论你是否遇到任何问题，你可能需要执行一些最终的“清理”（cleanup）过程 - 例如登录到特定的工作目录或关闭先前打开的文件。你可以通过在 `begin..rescue` 代码块后跟随一个以 `ensure` 关键字开头的另一个块的来执行此操作。`ensure` 块中的代码将始终会执行 - 无论之前是否发生异常。

最后，我想确保我的工作目录（由 `Dir.getwd` 提供）始终恢复到其原始位置。我通过在 `startdir` 变量中保存原始目录并再次在 `ensure` 块中将其作为工作目录来完成此操作：

<div class="code-file clearfix"><span>ensure.rb</span></div>

	startdir = Dir.getwd

	begin
	  Dir.chdir( "X:\\" )
	  puts( `dir` )
	rescue Exception => e
	  puts e.class
	  puts e
	ensure
	  Dir.chdir( startdir )
	end

现在让我们看看如何处理从文件中读取错误数据的问题。如果数据损坏，或者你不小心打开了错误的文件，或者很简单 - 你的程序代码包含错误（bug）时，则可能会发生这种情况。

这里我有一个文件 **test.txt**，包含六行内容。前五行是数字（numbers）；第六行不是。我的代码会打开此文件并读入所有六行内容：

<div class="code-file clearfix"><span>ensure2.rb</span></div>

	f = File.new( "test.txt" )

	begin
	  for i in (1..6) do
		puts("line number: #{f.lineno}")
		line = f.gets.chomp
		num = line.to_i
		puts( "Line '#{line}' is converted to #{num}" )
		puts( 100 / num )
	  end
	rescue Exception => e
	  puts( e.class )
	  puts( e )
	ensure
	  f.close
	  puts( "File closed" )
	end

这些行作为字符串读入（使用 `gets`），尝试将它们转换为整数（使用 `to_i`）。转换失败时不会产生错误；Ruby 会返回值 0。

问题出现在下一行代码中，它尝试按转换后的数字进行除法运算。输入文件的第六行包含字符串 "six"，当尝试转换为整数时产生 0 - 并且当在除法运算中使用该值时不可避免地会导致错误发生。

在外部打开数据文件后，无论是否发生错误我都想确保文件会关闭。例如，如果我只通过将 `for` 循环中的范围编辑为 `(1..5)` 来读取前五行，那么就没有异常。我仍然想要关闭该文件。

但是将文件关闭代码（`f.close`）放在 `rescue` 子句中并不好，因为在这种情况下它不会被执行。然而，通过将它放在 `ensure` 子句中，无论是否发生异常，我都可以确定该文件将被关闭。

### Else

如果说 `rescue` 部分在发生错误时执行，而 `ensure` 无论是否发生错误都会执行，那么我们怎么才能只有在没有*发生*错误时指定执行某些代码？

这样做的方法是在 `rescue` 部分之后和 `ensure` 部分之前添加一个可选的 `else` 子句（如果有的话），如下所示：

	begin
			# code which may cause an exception
	rescue [Exception Type]
	else 	# optional section executes if no exception occurs
	ensure  # optional exception always executes
	end

这是一个示例：

<div class="code-file clearfix"><span>else.rb</span></div>

	def doCalc( aNum )
	  begin
		result = 100 / aNum.to_i
	  rescue Exception => e # executes when there is an error
		result = 0
		msg = "Error: " + e
	  else  # executes when there is no error
		msg = "Result = #{result}"
	  ensure  # always executes
		msg = "You entered '#{aNum}'. " + msg
	  end
	  return msg
	end

### Error 编号

如果你之前运行了 `ensure.rb` 程序并且你正密切关注，你可能已经发现了一些异常情况当你尝试登录不存在的驱动器（例如，在我的系统上可能是 "X:\" 驱动器）。通常，当一个异常发生时，异常类是特定命名类型的实例，如 ZeroDivisionError 或 NoMethodError。然而，在这种情况下，类异常显示为：

	Errno::ENOENT

事实证明，Ruby 中存在各种各样的 `Errno` 错误。试试 **disk_err.rb**。这里定义了一个方法 `chDisk`，它尝试登录由字符 `aChar` 标识的磁盘。因此，如果你传递 "A" 作为 `chDisk` 的参数，它将尝试登录 A:\ 驱动器。我调用了三次 `chDisk` 方法，每次都传递一个不同的字符串：

<div class="code-file clearfix"><span>disk_err.rb</span></div>

	chDisk( "D" )
	chDisk( "X" )
	chDisk( "ABC" )

在我的电脑上，D:\ 是我的 DVD 驱动器。目前它是空的，当我的程序尝试登录它时，Ruby 返回此类型的异常：

	Errno::EACCES

我的 PC 上没有 X:\ 驱动器，当我尝试登录时，Ruby 会返回此类型的异常：

	Errno::ENOENT

在最后一个示例中，我传递一个字符串参数 "ABC" 作为无效的磁盘标识符，Ruby 返回此类型的异常：

	Errno::EINVAL

此类型的错误是 SystemCallError 类的后代。你可以通过取消注释代码行来轻松的验证这一点，以显示 **disk_err.rb** 源代码中指示的类的族。

实际上，这些类包含底层操作系统返回的整数错误值。这里 `Errno` 是包含匹配相应整数错误值的常量（例如 `EACCES` 和 `ENOENT`）的模块的名称。

要查看 `Errno` 常量的完整列表，请运行以下命令：

	puts( Errno.constants )

要查看任何给定常量的相应数值，请将 `::Errno` 追加到常量名称后面，如下所示：

	Errno::EINVAL::Errno

<div class="code-file clearfix"><span>errno.rb</span></div>

以下代码可用于显示所有 `Errno` 常量的列表及其数值：

	for err in Errno.constants do
	  errnum = eval( "Errno::#{err}::Errno" )
	  puts( "#{err}, #{errnum}" )
	end

### Retry

如果你认为错误情况可能是暂时的或者可以被纠正（由用户），你可以使用关键字 `retry` 重新运行 `begin..end` 块中的所有代码，如此示例中如果发生 ZeroDivisionError 等错误则会提示用户重新输入一个值：

<div class="code-file clearfix"><span>retry.rb</span></div>

	def doCalc
	  begin
		print( "Enter a number: " )
		aNum = gets().chomp()
		result = 100 / aNum.to_i
	  rescue Exception => e
		result = 0
		puts( "Error: " + e + "\nPlease try again." )
		retry #  retry on exception
	  else
		msg = "Result = #{result}"
	  ensure
		msg = "You entered '#{aNum}'. " + msg
	  end
	  return msg
	end

当然，存在这样的危险：错误可能不像你想象的那样是暂时的，如果你使用 `retry`，你必须要提供明确定义的退出（exit）条件，以确保代码在固定次数的尝试后停止执行。

例如，你可以在 `begin` 子句中递增一个局部变量（如果这样做，请确保它在任何可能产生异常的代码之前递增，因为一旦发生异常，那些剩下的预先为 `rescue` 子句关联的代码将被跳过！）。然后在 `rescue` 部分测试该变量的值，如下所示：

	rescue Exception => e
	  if aValue < someValue then
		retry
	  end

这是一个完整的示例，其中我测试名为 `tries` 的变量的值，以确保在异常处理块退出之前在不出错的情况下尝试重新运行代码不超过三次：

	def doCalc
	  tries = 0
	  begin
		print( "Enter a number: " )
		tries += 1
		aNum = gets().chomp()
		result = 100 / aNum.to_i
	  rescue Exception => e
		msg = "Error: " + e
		puts( msg )
		puts( "tries = #{tries}" )
		result = 0
		if tries < 3 then # set a fixed number of retries
		  retry
		end
	  else
		msg = "Result = #{result}"
	  ensure
		msg = "You entered '#{aNum}'. " + msg
	  end
	  return msg
	end

### Raise

有时你可能希望将异常保持为“活动的”（alive），即使它已被异常处理块捕获。例如，这可用于推迟异常的处理 - 通过将其传递给其他方法。你可以使用 `raise` 方法执行此操作。但是，你需要注意，一旦异常被抛出（raised），就需要重新处理该异常，否则可能导致程序崩溃。这是一个简单的示例，它引发了一个 ZeroDivisionError 异常，并将异常传递给一个名为 `handleError` 的方法：

<div class="code-file clearfix"><span>raise.rb</span></div>

	begin
	  divbyzero
	rescue Exception => e
	  puts( "A problem just occurred. Please wait..." )
	  x = 0
	  begin
		raise
	  rescue
		handleError( e )
	  end
	end

这里 `divbyzero` 是一个方法的名称，在该方法中进行除零操作，`handleError` 是一个打印该异常的更详细的信息的方法：

	def handleError( e )
	  puts( "Error of type: #{e.class}" )
	  puts( e )
	  puts( "Here is a backtrace: " )
	  puts( e.backtrace )
	end

请注意，这里使用了 `backtrace` 方法，该方法显示一个字符串数组 - 显示发生错误所在的文件名和行号，在本例中为调用生成错误的 `divbyzero` 方法所在的行。

<div class="code-file clearfix"><span>raise2.rb</span></div>

即使程序代码本身没有引起异常，你也可以专门抛出（raise）异常以强制执行错误条件。单独调用 `raise` 会抛出 RuntimeError 类型的异常（或全局变量 `$!` 中的任何异常）：

	raise # raises RuntimeError

默认情况下，这将没有与之关联的描述性消息。你可以将消息添加为参数，如下所示：

	raise "An unknown exception just occurred!"

你可以抛出特定类型的错误...

	raise ZeroDivisionError

你还可以创建特定异常类型的对象，并使用自定义消息对其进行初始化...

	raise ZeroDivisionError.new( "I'm afraid you divided by Zero" )

<div class="code-file clearfix"><span>raise3.rb</span></div>

当然，如果标准异常类型不符合你的要求，你可以通过继承现有异常类来创建新的异常类型。为你的类提供 `to_str` 方法，以便为它们提供默认信息。

	class NoNameError < Exception
	  def to_str
		"No Name given!"
	  end
	end

这是一个如何抛出自定义异常的示例：

	def sayHello( aName )
	  begin
		if (aName == "") or (aName == nil) then
		  raise NoNameError
		end
	  rescue Exception => e
		puts( e.class )
		puts( "message: " + e )
		puts( e.backtrace )
	  else
		puts( "Hello #{aName}" )
	  end
	end

## 深入探索

### 省略 begin 和 end

在方法，类或模块中捕获异常时，你可以选择省略 `begin` 和 `end`。例如，以下所有内容都是合法的：

<div class="code-file clearfix"><span>omit_begin_end.rb</span></div>

	def calc
		result = 1/0
	  rescue Exception => e
		puts( e.class )
		puts( e )
		result = nil
	  return result
	end

	class X
		@@x = 1/0
	  rescue Exception => e
		puts( e.class )
		puts( e )
	end

	module Y
		@@x = 1/0
	  rescue Exception => e
		puts( e.class )
		puts( e )
	end

在上面显示的所有情况中，如果以通常的方式将 `begin` 和 `end` 关键字放在异常处理代码块的开头和结尾，则异常处理也会起作用。

### Catch...Throw

在某些语言中，可以使用关键字 `catch` 捕获异常，使用关键字 `throw` 来抛出异常。虽然 Ruby 提供了 `catch` 和 `throw` 方法，但它们与异常处理没有直接关系。相反，`catch` 和 `throw` 用于在满足某些条件时跳出已定义的代码块。当然，在发生异常时，你也可以使用 `catch` 和 `throw` 来跳出代码块（尽管这可能不是处理错误的最优雅方式）。

<div class="code-file clearfix"><span>catch_except.rb</span></div>

	catch(:finished) {
	  print( 'Enter a number: ' )
	  num = gets().chomp.to_i
	  begin
		result = 100 / num
	  rescue Exception => e
		throw :finished  # jump to end of block
	  end
	  puts("The result of that calculation is #{result}" )
	} # end of :finished catch block

有关 `catch` 和 `throw` 的更多信息，请参见第 6 章。

# 第十章

***

## Blocks, Procs and Lambdas

当程序员谈论“块”（Block）时，它们通常意味着一些独立的“代码块”（chunks）。但是，在 Ruby 中一个 block 是特殊的。它是一个代码单元，有点像方法，但与方法不同的是它没有名称。为了有效的使用 block，您需要了解它们的特殊性和原因。这就是本章的全部内容...

### 什么是 Block？

思考这些代码：

<div class="code-file clearfix"><span>1blocks.rb</span></div>

	3.times do |i|
	  puts( i )
	end

很明显，这段代码实际上会执行三次。在每次连续的循环中 `i` 的值可能不是那么的明显。实际上，在这种情况下，`i` 的值将为 0,1 和 2。这是上面代码的另一种形式。这一次，block 由花括号限定，而不是由 `do` 和 `end`：

	3.times { |i|
	  puts( i )
	}

根据 Ruby 文档，`times` 是 Integer 类的一个方法（让我们称 Integer 为 `int`），它迭代一个块 `int` 次，并且传入从 0 到 `int` -1 的值'。所以，这里 block 中的代码运行 3 次；第一次运行时，变量 `i` 其值为 0；在后序循环中，`i` 将增加 1 直到达到最终值 2（即 `int`-1）。

请注意，上面的两个代码示例在功能上是相同的。Block 可以用花括号或 `do` 和 `end` 关键字括起来，程序员可以根据个人喜好使用任一语法。

<div class="note">

**注意：**一些 Ruby 程序员喜欢在当块的整个代码只有单行时用花括号来限定，而当块跨越多行时使用 `do..end`。我个人的意见是要统一，不管代码布局如何，我通常在分隔块时都使用花括号。通常，你选择的限定符对代码的行为没有任何影响 - 但请参阅本章后面有关“优先级规则”（precedence rules）的部分。
</div>

如果你熟悉类似 C 的语言（如 C# 或 Java），你应该会使用 Ruby 的花括号，就像在这些语言中一样，只需将独立的“块”（blocks）代码组合在一起 - 例如，条件计算结果为 true 时要执行的代码块。但情况并非如此。在 Ruby 中，块是一种特殊的结构，只能在非常特殊的情况下使用。

### 换行值得注意

Block 的开（opening）限定符必须与其关联的方法放在同一行。

这些是可以的...

	3.times do |i|
	  puts( i )
	end

	3.times { |i|
	  puts( i )
	}

但这些包含了语法错误...

	3.times
	do |i|
	  puts( i )
	end

	3.times
	{ |i|
	  puts( i )
	}

### 匿名函数

Ruby 的 block 可以被视为一种匿名函数（nameless function）或方法（method），并且其最常用于提供迭代列表项或范围值的一种方式。如果你之前从未遇到过匿名函数，这可能听起来像官方话（gobbledygook）。幸运的是，到本章结束时，事情会变得更加清晰。让我们回顾一下前面给出的简单示例。我说一个 block 就像一个匿名函数。以此块为例：

	{ |i|
	  puts( i )
	}

如果它作为普通的 Ruby 方法来编写，它看起来可能像这样：

	def aMethod( i )
	  puts( i )
	end

要调用该方法三次并将从 0 到 2 的值传递给它，我们可能会这样写：

	for i in 0..2
	  aMethod( i )
	end

当你创建一个匿名方法（即 block），在竖线之间声明的变量（例如 `|i|`）可以像命名方法的参数一样对待。我们将这些变量称为“块参数”（block parameters）。

再看看我之前的示例：

	3.times { |i|
	  puts( i )
	}

整数的 `times` 方法将从 0 到具体整数值减去 1 的值传递给块。

所以：

	3.times{ |i| }

...非常像：

	for i in 0..2
	  aMethod( i )
	end

主要区别在于第二个示例必须调用其它一些命名方法来处理 `i` 的值，而第一个示例使用匿名方法（花括号之间的代码）来处理 `i`。

### 看起来眼熟？

既然你现在知道 Block 是什么了，你可能会注意到你之前见过它们很多次。

例如，我们之前使用 `do..end` 块来迭代范围（ranges）：

	(1..3).each do |i|
	  puts(i)
	end

我们还使用了 `do..end` 块来迭代数组（参见第 5 章中的 **for_each2.rb**）：

	arr = ['one','two','three','four']
	arr.each do |s|
	  puts(s)
	end

我们通过将它传递给 `loop` 方法来重复执行一个 block（参见第 5 章中的 **3loops.rb**）：

	i=0
	loop {
	  puts(arr[i])
	  i+=1
	  if (i == arr.length) then
		break
	  end
	}

上面的 `loop` 示例在两方面是很明显的：1）它没有要迭代的项目列表（例如数组或范围值）；2）它非常难看。这两个特点并非完全不相关！`loop` 方法是 Kernel 类的一部分，它可以“直接”（automatically）用于你的程序。由于它没有'结束值'（end value），它将永远执行该 block，除非你明确地使用 `break` 关键字中断它。通常有更优雅的方式来执行这种迭代 - 通过迭代一系列有限范围的值。

### 块和数组

块（Blocks）通常用于迭代数组（Array）。 因此，Array 类提供了许多可以传递块的方法。

一个有用的方法 `collect`；会将数组的每个元素传递给一个 block，并创建一个新数组以包含该 block 返回的每个值。例如，这里的一个 block 会被传入一个数组中的每个整数（每个整数被分配给变量 `x`），它将其值加倍并返回它。

`collect` 方法会创建一个包含了每个按顺序返回的整数的新数组：

<div class="code-file clearfix"><span>2blocks.rb</span></div>

	b3 = [1,2,3].collect{|x| x*2}

上面的示例返回这个数组：`[2,4,6]`。

在下一个示例中，block 返回原始字符串每个首字母大写的一个版本：

	b4 = ["hello","good day","how do you do"].collect{|x| x.capitalize }

所以 `b4` 现在是...

	["Hello", "Good day", "How do you do"]

Array 类的 `each` 方法看起来与 `collect` 类似；它也依次传递每个数组元素以由 block 处理。但是，与 `collect` 不同，`each` 方法不会创建包含返回值的新数组：

	b5 = ["hello","good day","how do you do"].each{|x| x.capitalize }

这一次，`b5` 没有变化...

	["hello", "good day", "how do you do"]

回想一下，有些方法 - 特别是以感叹号（`!`）结尾的方法 - 实际上改变了原始对象而不是产生新值。如果你想使用 `each` 方法来将原始数组中的字符串首字母大写，你可以使用 `capitalize!` 方法：

	b6 = ["hello","good day","how do you do"].each{|x| x.capitalize! }

所以 `b6` 现在是...

	["Hello", "Good day", "How do you do"]

经过一番思考，你还可以使用 block 来迭代字符串中的字符（characters）。首先，你需要从字符串中拆分每个字符。这可以使用 String 类的 `split` 方法完成，如下所示：

	"hello world".split(//)

`split` 方法基于分隔符将字符串划分为子字符串，并返回包含这些子字符串的数组。这里 `//` 是一个定义零长度字符串的正则表达式；这具有返回单个字符的效果，因此我们最终创建了包含字符串中所有字符的数组。我们现在可以迭代这个字符数组，返回每个字符的大写版本：

	a = "hello world".split(//).each{ |x| newstr << x.capitalize }

因此，在每次迭代时，大写字符将附加到 `newstr`，并显示以下内容...

	H
	HE
	HEL
	HELL
	HELLO
	HELLO
	HELLO W
	HELLO WO
	HELLO WOR
	HELLO WORL
	HELLO WORLD

因为我们在这里使用 `capitalize` 方法（结尾没有 `!` 字符），所以数组中的字符 `a` 保持原样，全部小写，因为 `capitalize` 方法不会改变接收对象（这里接收对象是传入 block 的字符）。

但请注意，如果你使用 `capitalize!` 方法修改原始字符，此代码将不会运行。这是因为 `capitalize!` 如果没有进行任何更改则会返回 `nil`，因此当遇到空格字符时，将返回 `nil`，并且我们尝试向字符串 `newstr` 追加（`<<`）一个 `nil` 值将会失败。

你还可以使用 `each_byte` 方法对字符串进行首字母大写转换。这将遍历字符串的字符，将每个字节（byte）传递给 block。这些字节采用了 ASCII 码的形式。因此，"hello world" 将以这些数值的形式传递：`104` `101` `108` `108` `111` `32` `119` `111` `114` `108` `100`

显然，你不能将整数大写，所以我们需要将每个 ASCII 值转换为一个字符。String 的 `chr` 方法执行此操作：

	a = "hello world".each_byte{|x| newstr << (x.chr).capitalize }

### Procs 与  Lambdas

到目前为止，在我们的示例中块（blocks）都和方法（methods）一起使用。这是必须的，因为匿名块在 Ruby 中不能独立存在。例如，你不能像这样创建一个独立的块：

	{|x| x = x*10; puts(x)}

这是"Ruby中的所有内容都是对象"这一规则的例外情况之一。块显然不是一个对象。每个对象都是从类创建的，你可以通过调用其 `class` 方法来查找对象的类。

例如，使用 Hash 对象执行此操作，会显示类名 "Hash"：

	puts({1=>2}.class)

但是，尝试对块执行此操作，你只会得到一条错误消息：

	puts({|i| puts(i)}.class) #<= error!

<div class="note">
	<p class="h4"><b>Block 还是 Hash？</b></p>

Ruby 使用花括号来限定 block 和 Hash。那你（以及 Ruby）怎么能分辨哪个是哪个？答案基本上就是当它看起来像 Hash 时它是 Hash，否则它就是一个 block。Hash 看起来是在花括号中包含了键值（key-value）对...

	puts( {1=>2}.class ) #<= Hash

...或者，当它们是空（empty）的：

	puts( {}.class ) #<= Hash

但是，又一次的，当你省略括号时，将会出现歧义。这是一个空的 Hash 还是与 `puts` 方法相关联的一个 block？

	puts{}.class

坦率地说，我不得不承认我不知道这个问题的答案，我也无法让 Ruby 告诉我。Ruby 认为这是有效的语法，但事实上，在代码执行时不会显示任何内容。所以，这个...

	print{}.class

...打印为 `nil`（你不会注意到实际的 `nil` 类，即 Nil-Class，而是 `nil` 本身）。如果你发现所有的这些令人困惑（就像我一样！），请记住，通过明确地使用括号可以避免这一点：

	print( {}.class ) #<= Hash
</div>

### 创建块对象

<div class="code-file clearfix"><span>proc_create.rb</span></div>

虽然默认情况下块（blocks）可能不是对象，但它们可以"变成"对象。有三种方式可以创建块对象并将它们分配给变量 - 具体方法如下：

	a = Proc.new{|x| x = x*10; puts(x) }
	b = lambda{|x| x = x*10; puts(x) }
	c = proc{|x| x.capitalize! }

请注意，在上述三种情况下，你最终都会创建一个 Proc 类的实例 - 这是 Ruby 为块提供的'对象包装器'（object wrapper）。让我们仔细看看创建 Proc 对象的三种方法。首先，你可以调用 `Proc.new` 创建一个对象，并将块作为参数传递给它：

<div class="code-file clearfix"><span>3blocks.rb</span></div>

	a = Proc.new{|x| x = x*10; puts(x)}

你可以通过 `a` 引用使用 Proc 类的 `call` 方法执行块中的代码，并将一个或多个参数（匹配块参数）传递给块；在上面的代码中，你可以传递一个整数，如 100，这将被分配给块变量 `x`：

	a.call(100)

你也可以通过调用 `lambda` 或 `proc` 方法来创建 Proc 对象。这些方法（由 Kernel 类提供）是相同的。名称 `lambda` 取自 Scheme（Lisp）语言，是用于描述匿名方法或“闭包”（closure）的术语。

使用 `Proc.new` 创建 Proc 对象和使用 `proc` 或 `lambda` 方法创建 Proc 对象之间有一个重要的区别 - `Proc.new` 不检查传递给块的数字或参数是否与块参数的数量匹配 - 但 `proc` 和 `lambda` 会做检查：

<div class="code-file clearfix"><span>proc_lamba.rb</span></div>

	a = Proc.new{|x,y,z| x = y*z; puts(x) }
	a.call(2,5,10,100) # This is not an error

	b = lambda{|x,y,z| x = y*z; puts(x) }
	b.call(2,5,10,100) # This is an error

	puts('---Block #2---' )
	c = proc{|x,y,z| x = y*z; puts(x) }
	c.call(2,5,10,100) # This is an error

<div class="code-file clearfix"><span>block_closure.rb</span></div>

### 什么是闭包？

'闭包'（Closure）是一类函数的名称，这些函数能够在创建块的作用域（将其视为块的“原作用域”，native scope）内存储（即“封闭”，enclose）局部变量的值。Ruby 的块是闭包。要理解这一点，请看这个示例：

	x = "hello world"

	ablock = Proc.new { puts( x ) }

	def aMethod( aBlockArg )
	  x = "goodbye"
	  aBlockArg.call
	end

	puts( x )
	ablock.call
	aMethod( ablock )
	ablock.call
	puts( x )

这里，在 `ablock` 作用域的内局部变量 `x` 的值是 "hello world"。但是，在 `aMethod` 方法中，名为 `x` 的局部变量具有值，'goodbye'。尽管如此，当 `ablock` 被传递给 `aMethod` 并在 `aMethod` 的作用域内调用时，它打印出 "hello world"（即，在块的 'native scope' 中 `x` 的值，而不是在 `aMethod` 的作用域内 `x` 的值 'goodbye'）。

<div class="note">

有关闭包（closures）的更多信息，请参阅本章末尾的**“深入探索”**。
</div>

### Yield

让我们看看还有更多在使用中的块。**4blocks.rb** 程序引入了一些新东西 - 即一种执行传递给方法的匿名块的方式。这是使用关键字 `yield` 完成的。在第一个示例中，我定义了这个简单的方法：

<div class="code-file clearfix"><span>4blocks.rb</span></div>

	def aMethod
	  yield
	end

它实际上没有任何自己的代码。相反，它期望接收一个块并且 `yield` 关键字会让块执行。这是我传递一个块的方式：

	aMethod{ puts( "Good morning" ) }

请注意，这次块不作为命名参数传递。尝试在圆括号之间传递块是错误的，如下所示：

	aMethod( { puts( "Good morning" ) } ) # This won't work!

相反，我们只是将块放在我们传递它的方法右侧旁边，就像我们在本章的第一个示例中所做的那样。该方法接收没有声明命名参数的块，并用 `yield` 调用块。

这是一个稍微有用的示例：

	def caps( anarg )
	  yield( anarg )
	end

	caps( "a lowercase string" ){ |x| x.capitalize! ; puts( x ) }

这里 `caps` 方法接收一个参数 `anarg`，并将此参数传递给匿名块，然后由 `yield` 执行。当我调用 `caps` 方法时，我使用通常的参数传递语法传递一个字符串参数（"a lowercase string"）。匿名块在参数列表*之后*传递。当 caps 方法用字符串参数调用 `yield(anarg)` 时，"a lowercase string" 会被传递给该块，它被分配给块变量 `x`，它将其首字母大写并用 `puts(s)` 显示它。

### 块之中的块

我们已经看到了如何使用块来迭代数组。在下一个示例中，我使用一个块来迭代一个字符串数组，依次将每个字符串分配给块变量 `s`。然后将第二个块传递给 `caps` 方法，以便将字符串的首字母大写：

	["hello","good day","how do you do"].each{
	  |s|
	  caps( s ){ |x| x.capitalize!
		puts( x )
	  }
	}

输出结果：

	Hello
	Good day
	How do you do

### 传递命名的 Proc 参数

到目前为止，我们已经通过匿名的（在这种情况下使用 `yield` 关键字执行块）或以命名参数的形式将块传递给例程（procedures），在这种情况下，它使用 `call` 方法执行。 还有另一种传递块的方法。当方法的参数列表中的最后一个参数前面有一个 `＆` 符号时，它被认为是一个 Proc 对象。这使你可以选择使用与将块传递给迭代器时相同的语法将匿名块传递给例程。然而，例程本身可以接收块作为命名参数。运行 **5blocks.rb** 以查看此示例。

<div class="code-file clearfix"><span>5blocks.rb</span></div>

首先，这里提醒我们已经看到过传递块的两种方式。 该方法有三个参数，a，b，c：

	def abc( a, b, c )
	  a.call
	  b.call
	  c.call
	  yield
	end

我们用三个命名参数调用这个方法（这里恰好是块，但原则上可以是任何东西）加上一个未命名的块：

	abc(a, b, c ){ puts "four" }

`abc` 方法使用 `call` 方法执行命名的块参数，使用 `yield` 关键字执行未命名的块：

	a.call  #<= call block a
	b.call  #<= call block b
	c.call  #<= call block c
	yield  #<= yield unnamed block: { puts "four" }

下一个方法 `abc2` 接收单个参数，`＆d`：

	def abc2( &d )

此处的 ＆ 符是重要的，因为它表示 `＆d` 参数是一个块。但是，我们不需要将此块作为命名参数发送。相反，我们只需将其附加到方法名称后即可传递未命名的块：

	abc2{ puts "four" }

`abc2` 方法不使用 `yield` 关键字，而是使用参数名称（没有 ＆ 符号）执行块：

	def abc2( &d )
	  d.call
	end

你可以将 ＆ 符号参数视为块参数的类型检查（type-checked）。也就是说，＆ 参数被正式声明，因此与匿名块（那些 'yielded'）不同，块不会在'未通知'（unannounced）的情况下到达方法。但与普通参数（没有 ＆ 符号）不同，它们必须匹配块。你不能将其它类型的对象传递给 `abc2`：

	abc2( 10 ) # This won‟t work!

除了指定第四个正式参数 `(＆d)` 之外，`abc3` 方法与 `abc` 方法基本相同：

	def abc3( a, b, c, &d)

参数 `a`，`b` 和 `c` 被调用，而参数 `＆d` 可以被调用（call）或产生（yield），如你所愿：

	def abc3( a, b, c, &d)
	  a.call
	  b.call
	  c.call
	  d.call #<= block &d
	  yield #<= also block &d
	end

这意味着调用代码必须将三个普通参数和一个块（这可能是匿名的）传递给此方法：

	abc3(a, b, c){ puts "five" }

当接收方法没有匹配的命名参数时，你还可以使用前缀 ＆ 符号将命名块传递给方法，如下所示：

	abc3(a, b, c, &myproc )

当一个 ＆ 符号块变量传递给一个方法时（如上面的代码所示），它可能会被生成（yielded）。这提供了传递匿名块或 Proc 对象的选项：

	xyz{ |a,b,c| puts(a+b+c) }
	xyz( &myproc )

但要小心！请注意，在上面的一个示例中，我使用了块参数（`|a, b, c|`），其名称与我之前分配给 Proc 对象的三个局部变量的名称相同：`a`，`b`，`c`：

	a = lambda{ puts "one" }
	b = lambda{ puts "two" }
	c = proc{ puts "three" }

	xyz{ |a,b,c| puts(a+b+c) }

现在，原则上块参数应仅在块本身内可见。但是，事实证明，对块参数的赋值可以初始化在块的原作用域（native scope，请参阅本章前面的“什么是闭包？”）内具有与块参数相同名称的任何局部变量的值。

尽管 `xyz` 方法中的变量被命名为 `x`，`y` 和 `z`，但事实证明，该方法中的整数赋值实际上是对块中变量 `a`，`b` 和 `c` 进行的。

	{ |a,b,c| puts(a+b+c) }

...传递 `x`，`y` 和 `z` 的值：

	def xyz
	  x = 1
	  y = 2
	  z = 3
	  yield( x, y, z ) # 1,2,3 assigned to block parameters a,b,c
	end

因此，一旦块中的代码运行，块的原作用域（native scope，也是我的程序的 main 作用域）中的变量 `a`，`b` 和 `c` 就会被块变量的值初始化：

	xyz{ |a,b,c| puts(a+b+c) }
	puts( a, b, c ) # displays 1, 2, 3

为了更清楚这一点，请尝试 **6blocks.rb** 中的简单程序：

<div class="code-file clearfix"><span>6blocks.rb</span></div>

	a = "hello world"

	def foo
	  yield 100
	end

	puts( a )
	foo{ |a| puts( a ) }

	puts( a ) #< a is now 100

这是一个容易陷入 Ruby 的陷阱之一的例子。作为一般规则，当变量共享相同的作用域（例如，在此处程序的 main 作用域内声明的块）时，最好使其名称唯一，以避免任何不可预见的副作用。

请注意，此处描述的块作用域适用于 Ruby 1.8.x 以上的版本（包含此版本），在编写本文时，它可能被认为是 Ruby 的“标准”（standard）版本。Ruby 1.9 中正在对作用域进行更改，并将其合并到 Ruby 2.0 中。有关作用域的更多信息，请参阅本章末尾的“深入探索”部分中的“块和局部变量”。

### 优先级规则

花括号内的块比 `do` 和 `end` 中的块具有更高的优先级（precedence）。让我们看看这在实践中意味着什么。思考这两个例子：

	foo bar do |s| puts( s ) end
	foo bar{ |s| puts(s) }

这里，`foo` 和 `bar` 是方法。那么块传递给哪个方法？事实证明，`do..end` 块将被传递给最左边的方法 `foo`，而花括号中的块将被发送到最右边的方法 `bar`。这是因为花括号具有更高的优先级。思考这个程序...

<div class="code-file clearfix"><span>precedence.rb</span></div>

	def foo( b )
	  puts("---in foo---")
	  a = 'foo'
	  if block_given?
		puts( "(Block passed to foo)" )
		yield( a )
	  else
		puts( "(no block passed to foo)" )
	  end
	  puts( "in foo, arg b = #{b}" )
	  return "returned by " << a
	end

	def bar
	  puts("---in bar---")
	  a = 'bar'
	  if block_given?
		puts( "(Block passed to bar)" )
		yield( a )
	  else
		puts( "(no block passed to bar)" )
	  end
	  return "returned by " << a
	end

	foo bar do |s| puts( s ) end  # 1) do..end block

	foo bar{ |s| puts(s) }        # 2) {..} block

这里 `do..end` 块的优先级较低，方法 `foo` 优先。这意味着 `bar` 和 `do..end` 块都传递给 `foo`。因此，这两个表达式是等价的：

	foo bar do |s| puts( s ) end
	foo( bar ) do |s| puts( s ) end

另一方面，花括号块具有更高的优先级，因此它尝试立即执行并传递给第一个可能的接收方法 `(bar)`。然后将结果（即 `bar` 返回的值）作为参数传递给 `foo`；但这一次，`foo` 本身并没有收到块。因此，以下两个表达式是等效的：

	foo bar{ |s| puts(s) }
	foo( bar{ |s| puts(s) } )

如果你对这一切感到困惑，不要难过因为实际上并不是你一个人感到困惑！Ruby 块的行为远非透明的。潜在的歧义是由于在 Ruby 中参数列表周围的括号是可选的。从上面给出的替代版本中可以看出，当使用括号时，模糊性消失了。

<div class="note">
	<p class="h4"><b>提示...</b></p>

一个方法可以使用 `block_given?` 方法测试它是否已经收到一个块。你可以在 **precedence.rb** 程序中找到相关示例。
</div>

### 块作为迭代器

如前所述，Rub y中块的主要用途之一是提供可以传递范围或项列表的迭代器。许多标准类（如 Integer 和 Array）都有方法可以提供块来迭代元素。例如：

	3.times{ |i| puts( i ) }

	[1,2,3].each{|i| puts(i) }

当然，你可以创建自己的迭代器方法，以便为块提供一系列值。在 **iterate1.rb** 程序中，我定义了一个简单的 `timesRepeat` 方法以执行指定次数的块代码。这类似 Integer 类的 `times` 方法，除了它从索引 1 而不是索引 0 开始的事实（这里显示变量 `i` 是正为了证明这一事实）：

<div class="code-file clearfix"><span>iterate1.rb</span></div>

	def timesRepeat( aNum )
	  for i in 1..aNum do
		yield i
	  end
	end

以下是如何调用此方法的示例：

	timesRepeat( 3 ){ |i| puts("[#{i}] hello world") }

我还创建了一个 `timesRepeat2` 方法来迭代数组：

	def timesRepeat2( aNum, anArray )
	  anArray.each{ |anitem|
		yield( anitem )
	  }
	end

这可以通过以下方式调用：

	timesRepeat2( 3, ["hello","good day","how do you do"] ){ |x| puts(x) }

事实上，如果一个对象本身包含它自己的迭代器方法，那么它将更好（面向对象的灵魂更真实）。我在下一个示例中实现了这一点。在这里，我创建了 MyArray，作为 Array 的子类：

<div class="code-file clearfix"><span>iterate2.rb</span></div>

	class MyArray < Array

在创建新的 MyArray 对象时，它使用数组初始化：

	def initialize( anArray )
	  super( anArray )
	end

它向上依赖于它自己的（*self*）`each` 方法，它由它的祖先 Array 提供，以迭代数组中的所有元素，并使用 Integer 的 times 方法执行此操作一定次数。这是完整的类定义：

	class MyArray < Array
	  def initialize( anArray )
		super( anArray )
	  end

	  def timesRepeat( aNum )
		aNum.times{ # start block 1...
		  | num |
		  self.each{ # start block 2...
			| anitem |
			yield( "[#{num}] :: '#{anitem}'" )
		  } # ...end block 2
		} # ...end block 1
	  end
	end

请注意，由于我使用了两个迭代器（`aNum.times` 和 `self.each`），因此 `timesRepeat` 方法包含两个嵌套块。这是一个如何使用它的示例...

	numarr = MyArray.new( [1,2,3] )
	numarr.timesRepeat( 2 ){ |x| puts(x) }

这将输出以下内容：

	[0] :: '1'
	[0] :: '2'
	[0] :: '3'
	[1] :: '1'
	[1] :: '2'
	[1] :: '3'

在 **iterate3.rb** 中，我自己设置了为包含任意数量的子数组的数组定义迭代器的问题，其中每个子数组具有相同数量的项。换句话说，它像一个具有固定行数和固定列数的表或矩阵。例如，这里是一个具有三个“行”（rows，子数组）和四个“列”（columns，元素）的多维数组：

<div class="code-file clearfix"><span>iterate3.rb</span></div>

	multiarr =
	[	['one','two','three','four'],
		[1, 2, 3, 4 ],
		[:a, :b, :c, :d ]
	]

我已经尝试了三个替代版本。第一个版本受到限制，只能使用在预定义数量（这里是索引 [0] 和 [1]）的"行数"（rows）中：

	multiarr[0].length.times{|i|
	  puts(multiarr[0][i], multiarr[1][i])
	}

第二个版本通过迭代 `multiarr` 的每个元素（或'行'，row）然后通过获取行长度并使用 Integer 的 `times` 方法和该值迭代该行中的每个元素来绕过此限制：

	multiarr.each{ |arr|
	  multiarr[0].length.times{|i|
		puts(arr[i])
	  }
	}

第三个版本反转这些操作：外部块沿着行 0 的长度迭代，内部块获得每行中索引 `i` 的元素：

	multiarr[0].length.times{|i|
	  multiarr.each{ |arr|
		puts(arr[i])
	  }
	}

虽然版本 2 和版本 3 以类似的方式工作，但你会发现它们以不同的顺序迭代这些元素项目。运行该程序以验证。你可以尝试创建自己的 Array 子类并添加像这样的迭代器方法 - 一个按顺序迭代行的方法（如上面的版本 2）和一个按顺序遍历列的方法（如版本 3）。

## 深入探索

### 从方法中返回块

早些时候，我解释过 Ruby 中的块可能视为“闭包”（closures）。闭包可以说是封闭声明它的“环境”（environment）。或者，换句话说，它将局部变量的值从其原始作用域带入不同的作用域。我之前给出的示例显示了名为 `ablock` 的块如何捕获局部变量 `x` 的值...

<div class="code-file clearfix"><span>block_closure.rb</span></div>

	x = "hello world"
	ablock = Proc.new { puts( x ) }

...然后它就能够将该变量“携带”到不同的作用域内。例如，这里将块传递给 `aMethod`。当在该方法内部调用 `ablock` 时，它运行代码 `puts(x)`。这里显示，"hello world" 而不是 "goodbye"...

	def aMethod( aBlockArg )
	  x = "goodbye"
	  aBlockArg.call #<= displays "hello world"
	end

在这个特定的例子中，这种行为似乎对好奇心没有太大吸引力。实际上，可以更具创造性地使用块/闭包。

例如，你可以*在方法内*创建一个块并将该块返回给调用代码，而不是创建一个块并将其发送到方法。如果创建块的方法碰巧接收参数，则可以使用该参数初始化块。

这为我们提供了一种从同一“块模板”（block template）创建多个块的简单方法，每个块的实例都使用不同的数据进行初始化。例如，在这里我创建了两个块，分配给变量 `salesTax` 和 `vat`，每个块根据不同的值（0.10）和（0.175）计算结果：

<div class="code-file clearfix"><span>block_closure2.rb</span></div>

	def calcTax( taxRate )
	  return lambda{
		|subtotal|
		subtotal * taxRate
	  }
	end

	salesTax = calcTax( 0.10 )
	vat = calcTax( 0.175 )

	print( "Tax due on book = ")
	print( salesTax.call( 10 ) )  #<= prints: 1.0

	print( "\nVat due on DVD = ")
	print( vat.call( 10 ) )       #<= prints: 1.75

### 块与实例变量

块的一个不太明显的特性是它们使用变量的方式。如果一个块可能真的被视为匿名函数或方法，那么从逻辑上讲，它应该能够：1）包含它自己的局部变量；2）能够访问该块所属的对象的实例变量。

我们先来看实例变量（instance variables）。加载 **closures1.rb** 程序。这提供了块等同于闭包的另一个例子 - 通过捕获创建它的作用域中的局部变量的值。这里我使用 `lambda` 方法创建了块：

<div class="code-file clearfix"><span>closures1.rb</span></div>

	aClos = lambda{
	  @hello << " yikes!"
	}

这个块将一个字符串 "yikes!" 附加到一个实例变量 `@hello`。请注意，在这个过程中，之前没有为 `@hello` 分配任何值。

但是，我创建了一个单独的方法 `aFunc`，它为一个名为 `@hello` 的变量赋值：

	def aFunc( aClosure )
	  @hello = "hello world"
	  aClosure.call
	end

当我将块传递给该方法（`aClosure` 参数）时，`aFunc` 方法将引入 `@hello` 。我现在可以使用 `call` 方法执行块内代码。当然 `@hello` 变量包含字符串 "hello world"。通过调用块也可以使用方法之外相同的变量。实际上，现在，通过反复调用块，我最终会反复追加字符串 "yikes!" 到 `@hello`：

	aFunc(aClos)  #<= @hello = “hello world yikes!”
	aClos.call    #<= @hello = “hello world yikes! yikes!”
	aClos.call    #<= @hello = “hello world yikes! yikes! yikes!”
	aClos.call    # ...and so on
	aClos.call

如果你认为这并不是太令人惊讶。毕竟，`@hello` 是一个实例变量，因此它存在于一个对象的作用域内。当我们运行 Ruby 程序时，会自动创建一个名为 `main` 的对象。所以我们应该期望在该对象（我们的程序）中创建的任何实例变量可用于其中的所有内容。

现在出现的问题是：如果要将块发送到某个*其它*对象的方法会发生什么？如果该对象有自己的实例变量 `@hello`，那么该块会使用哪个变量 - 来自创建块的作用域内的 `@hello`，还是来自调用该块的对象作用域内的 `@hello`？让我们尝试一下。我们将使用与以前相同的块，除了这次它将显示有关块所属对象和 `@hello` 值的一些信息：

	aClos = lambda{
	  @hello << " yikes!"
	  puts("in #{self} object of class #{self.class}, @hello = #{@hello}")
	}

现在从新类（X）创建一个新对象，并为它提供一个接收我们的块 `b` 的方法，并调用该块：

	class X
	  def y( b )
		@hello = "I say, I say, I say!!!"
		puts( " [In X.y]" )
		puts("in #{self} object of class #{self.class}, @hello = #{@hello}")
		puts( " [In X.y] when block is called..." )
		b.call
	  end
	end

	x = X.new

要测试它，只需将块 `aClos` 传递给 `x` 的 `y` 方法：

	x.y( aClos )

这就是显示的内容：

	[In X.y]
	in #<X:0x32a6e64> object of class X, @hello = I say, I say, I say!!!
	[In X.y] when block is called...
	in main object of class Object, @hello = hello world yikes! yikes! yikes! yikes! yikes! yikes!

因此，很明显，块在创建它的对象（main）的作用域内执行，并保留该对象的实例变量，即使在调用块的对象的作用域内有一个具有相同名称和不同值的实例变量。

### 块与局部变量

现在让我们看看块/闭包（block/closure）如何处理局部变量（local variables）。加载 **closures2.rb** 程序。首先，我声明一个变量 `x`，它对程序本身的上下文来说是局部的：

<div class="code-file clearfix"><span>closures2.rb</span></div>

	x = 3000

第一个块/闭包称为 `c1`。每次我调用这个块时，它会获取块本身外部定义的 `x` 值（3000）并返回 `x+100`：

	c1 = lambda{
	  return x + 100
	}

这个块没有块参数（也就是说，竖条之间没有'块局部'（block local）变量）所以当用变量 `someval` 调用它时，该变量被丢弃，未使用。换句话说，`c1.call(someval)` 与 `c1.call()` 具有相同的效果。所以当你调用块 `c1` 时，它返回 `x+100`（即 3100），然后将该值赋给 `someval`。

	someval=1000
	someval=c1.call(someval); puts(someval) #<= someval is now 3100
	someval=c1.call(someval); puts(someval) #<= someval is now 3100

<div class="note">

**注意：**如上所示，你可以将调用放在块中并将其传递给 Integer 的 `times` 方法，而不是重复调用 `c1`，如下所示：

	2.times{ someval=c1.call(someval); puts(someval) }

但是，因为它可能很难在只有一个块（例如这里的 `c1` 块）的情况下工作，以至于我故意避免使用比这个程序中更多本应该必要的块！
</div>

第二个块名为 `c2`。这声明了一个'块参数'（block parameter），`z`。这也返回一个值：

	c2 = lambda{
	  |z|
	  return z + 100
	}

但是，这次返回值可以重复使用，因为块参数就像一个方法的传入参数 - 所以当 `someval` 的值在被赋值为 `c2` 的返回值之后被更改时，这个更改的值随后作为参数传入：

	someval=1000
	someval=c2.call(someval); puts(someval) #<= someval is now 1100
	someval=c2.call(someval); puts(someval) #<= someval is now 1200

乍一看，第三个块 `c3` 与第二个块 `c2` 几乎相同。实际上，唯一的区别是它的块参数被称为 `x` 而不是 `z`：

	c3 = lambda{
	  |x|
	  return x + 100
	}

块参数的名称对返回值没有影响。和以前一样，`someval` 首先被赋值 1100（即，它的原始值 1000，加上块中添加的 100）然后，当第二次调用块时，`someval` 被赋值为 1200（其先前的值 1100，加上在块内分配的 100）。

但现在看一下局部变量 `x` 的值会发生什么。在该单元的顶部分配了 3000。只需给块参数指定相同的名称 `x`，我们就改变了局部变量 `x` 的值。它现在具有值 1100，即块参数 `x` 在调用 `c3` 块时最后具有的值：

	x = 3000

	c3 = lambda{
	  |x|
	  return x + 100
	}

	someval=1000
	someval=c3.call(someval); puts(someval)
	someval=c3.call(someval); puts(someval)
	puts( x )  #<= x is now 1100

顺便提一下，即使块局部变量和块参数可以影响块外部的类似命名的局部变量，块变量本身也不会在块之外存在。你可以使用 `defined?` 关键字对此进行验证，以尝试显示变量的类型（如果确实已定义）：

	print("x=[#{defined?(x)}],z=[#{defined?(z)}]")

Ruby 的创造者 Matz，他将块内局部变量的作用域描述为“抱歉的”（regrettable）。特别是，他认为在一个块中使局部变量对包含该块的方法不可见是错误的。有关此示例，请参阅 **local_var_scope.rb**：

<div class="code-file clearfix"><span>local_var_scope.rb</span></div>

	def foo
	  a = 100
	  [1,2,3].each do |b|
		c = b
		a = b
		print("a=#{a}, b=#{b}, c=#{c}\n")
	  end
	  print("Outside block: a=#{a}\n") # Can't print #{b} and #{c} here!!!
	end

这里，块参数 `b` 和块局部变量 `c` 只有在块本身内部时才可见。该块可以访问这些变量并作用于变量 `a`（`foo` 方法的局部）。但是，在块之外，`b` 和 `c` 是不可访问的，只有`a` 是可见的。

只是为了增加迷惑性，块局部变量 `c` 和块参数 `b` 在上面的示例中都不能在块外部访问，但是当你用一个`for` 块迭代时可以访问它们，如下例所示：

	def foo2
	  a = 100
	  for b in [1,2,3] do
		c = b
		a = b
		print("a=#{a}, b=#{b}, c=#{c}\n")
	  end
	  print("Outside block: a=#{a}, b=#{b}, c=#{b}\n")
	end

在 Ruby 的未来版本中，在块内赋值的局部变量（与 `c` 一样）也将是块外部方法（例如 `foo`）的局部变量。形式上块参数（如 `b`）将是块的局部变量。

# 第十一章

***

## 符号（Symbols）

Ruby 的许多新人都被符号（symbols）弄糊涂了。符号（symbol）是一个标识符，其首个字符为冒号（:），所以 `:this` 是一个符号，`:that` 也是。事实上，符号并不复杂 - 在某些情况下，它们可能非常有用，我们很快就会看到。

让我们首先明确一个符号*不是*什么：它不是一个字符串，它不是一个常量，它也不是一个变量。简单地说，符号是除了自己的名称之外没有内在含义的标识符。而你可能会这样为变量赋值...

	name = "Fred"

你不能为符号赋值。名为 `:name` 的符号的值也为 `:name`。

<div class="note">

更多有关符号专门的说明，请参阅本章末尾的“深入探索”部分。
</div>

当然，我们之前使用过符号。例如，在第 2 章中，我们通过将符号传递给 `attr_reader` 和 `attr_writer` 方法来创建属性的读取器和修改器，如下所示：

	attr_reader( :description )
	attr_writer( :description )

你可能还记得上面的代码会使得 Ruby 创建一个 `@description` 实例变量以及一对名为 `description` 的 getter（reader）和 setter（writer）方法。Ruby 从字面量理解符号的值。它的值就是它的名字（`:description`）。`attr_reader` 和 `attr_writer` 方法会创建名称与该名称相匹配的变量和方法。

### 符号与字符串

一个常见的误解就是认为符号（symbol）是字符串的一种类型。毕竟，符号 `:hello` 与字符串 "hello" 非常相似不是吗？

事实上，符号与字符串完全不同。首先，每个字符串是不同的 — 因此，"hello"、"hello" 和 "hello" 是三个独立的对象，具有三个独立的 `object_id`s。

<div class="code-file clearfix"><span>symbol_ids.rb</span></div>

	puts( "hello".object_id ) # These 3 strings have 3 different object_ids
	puts( "hello".object_id )
	puts( "hello".object_id )

但是符号是唯一的，所以 `:hello`、`:hello` 和 `:hello` 都引用具有相同的 `object_id` 的对象。在这方面，符号与整数（integer）相比，要比字符串有更多的共同之处。你可能还记得，给定的整数值每次出现都引用相同的对象，因此 `10`、`10` 和 `10` 可以被认为是相同的对象，并且它们具有相同的 `object_id`：

<div class="code-file clearfix"><span>ints_and_symbols.rb</span></div>

	# These three symbols have the same object_id
	puts( :ten.object_id )
	puts( :ten.object_id )
	puts( :ten.object_id )

	# These three integers have the same object_id
	puts( 10.object_id )
	puts( 10.object_id )
	puts( 10.object_id )

或者你可以使用 `equal?` 方法测试其相等性：

<div class="code-file clearfix"><span>symbols_strings.rb</span></div>

	puts( :helloworld.equal?( :helloworld ) )    #=> true
	puts( "helloworld".equal?( "helloworld" ) )  #=> false
	puts( 1.equal?( 1 ) )                        #=> true

由于是唯一的，所以符号提供了明确的标识符。你可以将符号作为参数传递给方法，如下所示：

	amethod( :deletefiles )

方法可能包含测试传入参数的值的代码：

<div class="code-file clearfix"><span>symbols_1.rb</span></div>

	def amethod( doThis )
	  if (doThis == :deletefiles) then
		puts( 'Now deleting files...')
	  elsif (doThis == :formatdisk) then
		puts( 'Now formatting disk...')
	  else
		puts( "Sorry, command not understood." )
	  end
	end

符号还可用于提供字符串的可读性和整数的唯一性的 `case` 语句：

	case doThis
	  when :deletefiles : puts( 'Now deleting files...')
	  when :formatdisk : puts( 'Now formatting disk...')
	  else puts( "Sorry, command not understood." )
	end

声明符号的作用域不会影响其唯一性。思考以下...

<div class="code-file clearfix"><span>symbol_ref.rb</span></div>

	module One
	  class Fred
	  end
	  $f1 = :Fred
	end

	module Two
	  Fred = 1
	  $f2 = :Fred
	end

	def Fred()
	end

	$f3 = :Fred

这里，变量 `$f1`，`$f2` 和 `$f3` 在三个不同的作用域内分配了符号 `:Fred`：模块 One，模块 Two 和 'main' 作用域。我将在第 12 章中对模块（modules）进行更多说明。现在，只需将它们视为定义不同作用域的“命名空间”（namespaces）即可。然而每个变量引用着相同的符号 `:Fred`，并且具有相同的 `object_id`：

	# All three display the same id!
	puts( $f1.object_id )
	puts( $f2.object_id )
	puts( $f3.object_id )

即便如此，符号的“含义”（meaning）也会根据其作用域而变化。

换句话说，在模块 One 中，`:Fred` 引用类 `Fred`，在模块 Two 中，它引用常量 `Fred = 1`，在 main 作用域内引用 `Fred` 方法。

上一个程序的重写版本证实了这一点：

<div class="code-file clearfix"><span>symbol_ref2.rb</span></div>

	module One
	  class Fred
	  end
	  $f1 = :Fred
	  def self.evalFred( aSymbol )
		puts( eval( aSymbol.id2name ) )
	  end
	end

	module Two
	  Fred = 1
	  $f2 = :Fred
	  def self.evalFred( aSymbol )
		puts( eval( aSymbol.id2name ) )
	  end
	end

	def Fred()
	  puts( "hello from the Fred method" )
	end

	$f3 = :Fred

	One::evalFred( $f1 ) #=> displays the module::class name: One::Fred
	Two::evalFred( $f2 ) #=> displays the Fred constant value: 1
	method($f3).call 	 #=> calls Fred method: displays: "hello from the Fred method"

当然，由于变量 `$f1`，`$f2` 和 `$f3` 引用着相同的符号，因此你使用的变量是在任意地方指定的都是无关紧要的。以下产生完全相同的结果：

	One::evalFred( $f3 )
	Two::evalFred( $f1 )
	method($f2).call

### 符号和变量

<div class="code-file clearfix"><span>symbols_2.rb</span></div>

要了解符号（symbol）和标识符（例如变量名称）之间的关系，请查看我们的 **symbols_2.rb** 程序。首先将值 1 赋给局部变量 `x`。然后将符号 `:x` 赋给局部变量 `xsymbol`...

	x = 1
	xsymbol = :x

此时，变量 `x` 和符号 `:x` 之间没有明显的联系。我声明了一个方法，它只需要一些传入参数并使用 `p` 方法查看（inspects）和显示它。我可以使用变量和符号调用此方法：

	# Test 1
	amethod( x )
	amethod( :x )

这是该方法打印的数据结果：

	1
	:x

换句话说，`x` 变量的值是 1，因为那是分配给它的值，而 `:x` 的值是 `:x`。但是出现了有趣的问题：如果 `:x` 的值是 `:x` 并且这也是变量 `x` 的符号名称，是否可以使用符号 `:x` 来查找变量 `x` 的值？困惑？希望下一行代码能这些更清楚：

	# Test 2
	amethod( eval(:x.id2name))

这里，`id2name` 是 Symbol 类的一个方法。它返回与符号对应的名称或字符串（`to_s` 方法将执行相同的功能）；最终结果是，当给出符号 `:x` 作为参数时，`id2name` 返回字符串 "x"。Ruby 的 `eval` 方法（在 Kernel 类中定义）能够计算字符串中的表达式。在本例中，这意味着它找到字符串 "x" 并尝试将其作为可执行代码进行计算。它发现 `x` 是变量的名称，并且 `x` 的值是 1。所以值 1 传递给 `amethod`。你可以通过运行 **symbols2.rb** 和比较代码的输出结果来验证这一点。

<div class="note">

在第 20 章中更详细地解释了有关将数据作为代码来计算执行。
</div>

事情变得更加诡异。请记住，变量 `xsymbol` 已被赋予符号 `:x`...

	x = 1
	xsymbol = :x

这意味着如果我们 eval `:xsymbol`，我们可以获得分配给它的名称 - 即符号 `:x`。获得 `:x` 后我们可以继续计算它，给出 `x` 的值 - 即 1：

	# Test 3
	amethod( xsymbol ) #=> :x
	amethod( :xsymbol ) #=> :xsymbol
	amethod( eval(:xsymbol.id2name)) #=> :x
	amethod( eval( ( eval(:xsymbol.id2name)).id2name ) ) #=> 1

正如我们所见，当用于创建属性访问器（attribute accessors）时，符号可以引用方法名称。我们可以利用它将方法名称作为符号传递给 `method` 方法（是的，确实存在一个名为`'method'` 的方法），然后使用 `call` 方法调用指定的方法：

	#Test 4
	method(:amethod).call("")

`call` 方法允许我们传递参数，为了方便，我们可以通过计算符号来传递一个参数：

	method(:amethod).call(eval(:x.id2name))

如果这看起来很复杂，请看一下 **symbols_3.rb** 中的一个更简单的示例。这从以下赋值开始：

<div class="code-file clearfix"><span>symbols_3.rb</span></div>

	def mymethod( somearg )
	  print( "I say: " << somearg )
	end

	this_is_a_method_name = method(:mymethod)

这里 `method(mymethod)` 查找一个方法，该方法的名称由作为参数传递的符号（`:mymethod`）指定，如果找到，则返回具有相应名称的 Method 对象。在我的代码中，我有一个名为 `mymethod` 的方法，现在将其分配给变量 `this_is_a_method_name`。

运行此程序时，你将看到第一行输出打印了变量的值：

	puts( this_is_a_method_name ) #=> This displays: #<Method: Object#mymethod>

这表明变量 `this_is_a_method_name` 已被赋予了方法 `mymethod`，该方法绑定到 Object 类（所有方法都作为'独立'（freestanding）函数输入）。要仔细检查变量是否真的是 Method 类的一个实例，下一行代码会打印出它的类：

	puts( "#{this_is_a_method_name.class}" ) #=> This displays: Method

好吧，如果它真的是一个真正的方法，那么我们应该可以调用它，不是吗？为此，我们需要使用 `call` 方法。这就是最后一行代码的作用：

	this_is_a_method_name.call( "hello world" ) #=> This displays: I say: hello world

### 为什么使用符号？

Ruby 类库中的某些方法将符号（symbol）指定为参数。当然，如果你需要调用这些方法，则必须将符号传递给它们。但是，除了这些情况之外，没有绝对的要求你在自己的编程中使用符号。对于许多 Ruby 程序员来说，“常规”（conventional）数据类型（如字符串和整数）就足够了。

但是，符号确实在“动态”（dynamic）编程中占有特殊的地位。例如，Ruby 程序能够在运行时（runtime）通过在某个类的作用域内调用 `define_method` 来创建一个新方法，符号表示要定义的方法以及块表示该方法的代码：

<div class="code-file clearfix"><span>add_method.rb</span></div>

	class Array
	  define_method( :aNewMethod, lambda{ |*args| puts( args.inspect) } )
	end

执行上面的代码后，Array 类将获得一个名为 `aNewMethod` 的方法。你可以通过调用 `method_defined?` 并传入表示方法名称的符号来验证这一点：

	Array.method_defined?( :aNewMethod ) #=> returns: true

当然，你本身也可以调用该方法：

	[].aNewMethod( 1,2,3 ) #=> returns: [1,2,3]

你可以在运行时（runtime）以类似的方式删除现有的方法，在类中调用 `remove_method`，并传入提供被删除的方法的名称符号：

	class Array
	  remove_method( :aNewMethod )
	end

动态编程在需要程序仍在执行时修改 Ruby 程序本身的行为的应用程序中是非常有用的。例如，动态编程广泛用于 Rails 框架中。

## 深入探索

### 什么是符号？

之前，我说过符号（symbol）是一个标识符，其值就是它本身。从广义上讲，这描述了从 Ruby 程序员的角度来待看符号的行为方式。但它并没有告诉我们从 Ruby 解释器（interpreter）的角度来看，符号的字面意思是什么。实际上，符号是指向符号表（symbol table）的指针（pointer）。符号表是 Ruby 的已知标识符的内部列表 - 例如变量和方法名称。

如果你想深入了解 Ruby，你可以显示 Ruby 已知的所有符号，如下所示：

<div class="code-file clearfix"><span>allsymbols.rb</span></div>

	p( Symbol.all_symbols )

这将显示数千个符号，包括方法名称，例如 `:to_s` 和 `:reverse`，全局变量，例如：`$/` 和 `:$DEBUG`，类名称，例如 `:Array` 和 `:Symbol`。你可以使用数组索引限制显示的符号数量，如下所示：

	p( Symbol.all_symbols[0,10] )

但是你不能对符号进行排序，因为符号本身并不是连续的。显示符号排序列表的最简单方法是将它们转换为字符串并对其进行排序。在下面的代码中，我将 Ruby 已知的所有符号传递给一个块，该块将每个符号转换为一个字符串，并将字符串收集到一个新的数组中，该数组被分配给 `str_array` 变量。现在我可以对这个数组进行排序并显示结果：

	str_arr = Symbol.all_symbols.collect{ |s| s.to_s }
	puts( str_arr.sort )

# 第十二章

***

## 模块（Modules）和混入（Mixins）

在 Ruby 中，每个类只有一个直接的“父类”（parent），尽管每个父类可能有许多“子类”（children）。通过将类层次结构限制为单继承，Ruby 避免了那些允许多继承的编程语言（如 C++）中可能出现的一些问题。当类有很多父类和子类，而且它们的父类、子类还有其它父类和子类，你最终会面临一个难以理解的网络（或者“结”？），而不是你可能想要的整洁，有序的层次结构。

然而，有时候（多继承）对于与实现某些共享特征并不密切相关的类是有用的。例如，剑可能是一种武器（Weapon），但也会是一种珍宝（Treasure）；PC 可能是一种计算机（Computer），但也会是一种投资（Investment）等等。

但是，由于定义武器（Weapons）和珍宝（Treasures）或计算机（Computers）和投资（Investments）的类继承自不同的祖先类，因此它们的类层次结构使它们没有明显的方式来共享数据和方法。这个问题的 Ruby 解决方案由 Modules 提供。

### 模块像一个类...

模块（module）的定义看起来非常类似于类（class）。事实上，模块和类密切相关 -  `Module` 类是 `Class` 类的直接祖先。就像一个类一样，模块可以包含常量，方法和类。 这是一个简单的模块：

	module MyModule
	  GOODMOOD = "happy"
	  BADMOOD = "grumpy"

	  def greet
		return "I'm #{GOODMOOD}. How are you?"
	  end

	end

如你所见，它包含一个常量 `GOODMOOD` 和一个'实例方法'（instance method） `greet`。

### 模块方法

模块除了有实例方法（instance methods）之外，还可以具有模块方法（module methods）。就像类方法以类的名称为前缀一样，模块方法也以模块名称为前缀：

	def MyModule.greet
	  return "I'm #{BADMOOD}. How are you?"
	end

尽管它们有相似之处，但是有两个重要特征，类有而模块没有：**实例（instances）**和**继承（inheritance）**。类可以有实例（对象），超类（父类）和子类（子类）；模块没有这些。

<div class="note">

Module 类确实有一个超类 - 即 Object。 但是，你创建的任何命名模块都没有超类。有关模块（Modules）和类（Classes）之间关系的更详细说明，请参阅本章末尾的“**深入探索**”部分。
</div>

这为我们引出了下一个问题：如果你不能从模块创建一个对象，那么模块可以用来干什么？这可以用两个词来回答：**命名空间（namespaces）**和**混入（mixins）**。Ruby 的 'mixins' 机制提供了一种处理多重继承存在的问题的方法。我们很快就会遇到 mixins。首先，我们来看看命名空间（namespaces）。

### 模块作为命名空间

你可以将模块视为一种围绕一组方法，常量和类的命名“包装器”（wrapper）。模块内部的各种代码共享相同的“命名空间”（namespaces），因此它们彼此都可见，但对模块外部的代码不可见。

Ruby 类库定义了许多模块，如 Math 和 Kernel。Math 模块包含数学方法（例如 `sqrt` 以返回平方根）和常量（例如 `PI`）。Kernel 模块包含我们从一开始就使用的许多方法，例如 `print`，`puts` 和 `gets`。

假设我们前面看过的模块：

<div class="code-file clearfix"><span>modules1.rb</span></div>

	module MyModule
	  GOODMOOD = "happy"
	  BADMOOD = "grumpy"

	  def greet
		return "I'm #{GOODMOOD}. How are you?"
	  end

	  def MyModule.greet
		return "I'm #{BADMOOD}. How are you?"
	  end
	end

我们可以访问模块常量，就像我们使用 `::` 作用域解析运算符访问类常量一样，如下所示：

	puts(MyModule::GOODMOOD)

我们可以使用点表示法访问模块方法 - 即，指定模块名称后跟句点和方法名称。 以下会打印出来 "I'm grumpy. How are you?"：

	puts( MyModule.greet )

### 模块的“实例方法”

但是如何访问实例方法，`greet`？ 由于模块定义了一个封闭的命名空间，模块外的任何代码都无法“看到” `greet` 方法，所以这不起作用：

	puts( greet )

如果这是一个类而不是一个模块，我们当然可以使用 `new` 方法从类创建对象 - 每个单独的对象，类的每个'实例' - 都可以访问实例方法。但是你无法创建模块的实例。那么我们如何使用它们的实例方法呢？这是引入 mixins 的时候了...

### 包含模块或混入（Mixins）

对象可以通过使用 `include` 方法包含该模块来访问模块的实例方法。如果你要将 MyModule 包含到程序中，则该模块内的所有内容都会突然出现在当前作用域内。因此，现在可以访问 MyModule 的 `greet` 方法：

<div class="code-file clearfix"><span>modules2.rb</span></div>

	include MyModule
	puts( greet )

注意，只会包含实例方法。在上面的示例中，已经包含了 `greet`（实例）方法，但是 `MyModule.greet`（模块）方法没有被包含...

	module MyModule
	  GOODMOOD = "happy"
	  BADMOOD = "grumpy"

	  def greet
		return "I'm #{GOODMOOD}. How are you?"
	  end

	  def MyModule.greet
		return "I'm #{BADMOOD}. How are you?"
	  end
	end

正如它所包含的那样，`greet` 方法可以像使用当前作用域中的普通实例方法一样被使用...

	puts( greet )

包含模块的过程也称为“混入”（mixing in） - 这解释了为什么包含的模块通常被称为 "mixins"。将模块混入到类定义中时，从该类创建的任何对象都将能够使用被混入模块的实例方法，就像它们在类本身中定义一样。

<div class="code-file clearfix"><span>modules3.rb</span></div>

	class MyClass
	  include MyModule

	  def sayHi
		puts( greet )
	  end
	end

不仅这个类的方法可以访问 MyModule 模块的 `greet` 方法，而且从类中创建的任何对象也是如此：

	ob = MyClass.new
	ob.sayHi
	puts(ob.greet)

模块（Modules）可以被认为是离散的代码单元，可以简化可重用代码库的创建。另一方面，你可能更感兴趣的是使用模块作为实现多继承的替代方式。

回到我在本章开头提到的一个示例，让我们假设你有一个剑（Sword）类，它不仅是一种武器（Weapon），也是一种珍宝（Treasure）。或许 Sword 是 Weapon 类的后代（因此继承了 Weapon 的 `deadliness` 属性），但它也需要具有 Treasure 的属性（例如 `value` 和 `owner`），并且这是拥有魔法（MagicThing）的精灵之剑。如果你在 Treasure 和 MagicThing *模块*（`modules`）而不是*类*（`classes`）中定义这些属性，则 Sword 类将能够以“混入”（mix in）方式包含这些模块其方法或属性：

<div class="code-file clearfix"><span>modules4.rb</span></div>

	module MagicThing
	  attr_accessor :power
	end

	module Treasure
	  attr_accessor :value
	  attr_accessor :owner
	end

	class Weapon
	  attr_accessor :deadliness
	end

	class Sword < Weapon
	  include Treasure
	  include MagicThing
	  attr_accessor :name
	end

Sword 对象现在可以访问 Sword 类本身，它的祖先类 Weapon，以及它的混入（mixed-in）模块 Treasure 和 MagicThing 的方法和属性：

	s = Sword.new
	s.name = "Excalibur"
	s.deadliness = "fatal"
	s.value = 1000
	s.owner = "Gribbit The Dragon"
	s.power = "Glows when Orcs Appear"
	puts(s.name)
	puts(s.deadliness)
	puts(s.value)
	puts(s.owner)
	puts(s.power)

顺便提一下，无法从模块外部访问模块中作为局部变量的任何变量。即使模块内部的方法试图访问局部变量并且该方法是由模块外部的代码调用的 - 例如，当包含混入模块时，情况也是如此：

<div class="code-file clearfix"><span>mod_vars.rb</span></div>

	x = 1 						 # local to this program

	module Foo
	  x = 50 					 # local to module Foo

	  # This can be mixed in but the variable x won't then be visible
	  def no_bar
		return x
	  end

	  def bar
		@x = 1000
		return @x
	  end

	  puts( "In Foo: x = #{x}" ) # this can access the „module local‟ x
	end

	include Foo

	puts(x)
	puts( no_bar ) 				 # Error! This can't access the module-local variable
								 # needed by the no_bar method
	puts(bar)

请注意，*实例变量（instance variables）*可用于混入方法（例如 `bar`）。但是，即使在混入方法的当前作用域中存在具有相同名称的局部变量时，局部变量也不可用（因此，即使 `x` 在当前作用域中已经声明，`no_bar` 也无法访问名为 `x` 的变量）。

模块可以具有其自己的实例变量，这些变量仅仅属于模块“对象”。这些实例变量存在于模块方法的作用域内：

<div class="code-file clearfix"><span>inst_class_vars.rb</span></div>

	module X
	  @instvar = "X's @instvar"

	  def self.aaa
		puts(@instvar)
	  end
	end

	X.aaa #=> "X's @instvar"

但实例对象中引用的实例变量“属于”包含该模块的作用域：

	module X
	  @instvar = "X's @instvar"
	  def amethod
		@instvar = 10 			# creates @instvar in current scope
	    puts(@instvar)
	  end
	end

	include X

	X.aaa 					#=> X's @instvar
	puts( @instvar ) 		#=> nil
	amethod #=> 10
	puts( @instvar ) 		#=> 10
	@instvar = "hello world"
	puts( @instvar ) 		#=> "hello world"

类变量也会被混入，和实例变量一样，它们的值可以在当前作用域内重新分配：

	module X
	  @@classvar = "X's @@classvar"
	end

	include X

	puts( @@classvar ) 		#=> X's @@classvar
	@@classvar = "bye bye"
	puts( @@classvar ) 		#=> "bye bye"

你可以使用 `instance_variables` 方法获取实例变量的名称数组：

	p( X.instance_variables )
	p( self.instance_variables )


### 命名冲突

模块方法（特定的前缀为模块名称的那些方法）可以让你的代码免受意外命名冲突的影响。但是，模块中的实例方法没有这样的保护措施。假设你有两个模块 - 一个叫做 Happy，另一个叫做 Sad。它们每个都包含一个名为 `mood` 的模块方法和一个名为 `expression` 的实例方法。

<div class="code-file clearfix"><span>happy_sad.rb</span></div>

	module Happy
	  def Happy.mood # module method
		return "happy"
	  end

	  def expression # instance method
		return "smiling"
	  end
	end

	module Sad
	  def Sad.mood  # module method
		return "sad"
	  end

	  def expression # instance method
		return "frowning"
	  end
	end

现在，一个类 Person 包含了这两个模块：

	class Person
	  include Happy
	  include Sad
	  attr_accessor :mood

	  def initialize
		@mood = Happy.mood
	  end
	end

Person 类的 `initialize` 方法需要使用被包含模块之一的 `mood` 方法设置其 `@mood` 变量的值。实际上他们都有一个 `mood` 方法，但这没有问题；作为一个模块方法，`mood` 必须以模块名称开头，因此 `Happy.mood` 不会与 `Sad.mood` 混淆。

但 Happy 和 Sad 模块也都包含一个名为 `expression` 的方法。这是一个实例方法，当两个模块都包含在 Person 类中时，可以不带任何限定地调用 `expression` 方法：

	p1 = Person.new
	puts(p1.expression)

对象 `p1` 使用哪个 `expression` 方法？事实证明它使用最后定义的方法。在目前的情况下，这恰好是 Sad 模块中定义的方法，原因很简单，在 Happy 之后包含了 Sad 模块。如果更改包含顺序以在 Sad 之后包含 Happy，则 `p1` 对象将使用 Happy 模块中定义的 `expression` 方法的版本。

在开始创建大型而且复杂的模块并将其混入到你的常规基类中之前，请记住这个潜在的问题 - 即包含相同名称的实例方法将“覆盖”彼此。在我的小程序中发现问题可能是显而易见的。但在一个巨大的应用程序中它可能不那么明显！

### Alias 方法

当你使用来自多个模块有类似命名的方法时，避免歧义的一种方式是给这些方法一个“别名”（alias）。别名是具有新名称的现有方法的副本。你可以使用 `alias` 关键字后跟新名称，以及旧名称：

<div class="code-file clearfix"><span>alias_methods.rb</span></div>

	alias happyexpression expression

你还可以使用别名（alias）来创建一个已被覆盖方法的副本，以便你可以在定义被覆盖之前指定引用其版本：

	module Happy
	  def Happy.mood
		return "happy"
	  end

	  def expression
		return "smiling"
	  end
	  alias happyexpression expression
	end

	module Sad
	  def Sad.mood
		return "sad"
	  end

	  def expression
		return "frowning"
	  end
	  alias sadexpression expression
	end

	class Person
	  include Happy
	  include Sad
	  attr_accessor :mood

	  def initialize
		@mood = Happy.mood
	  end
	end

	p2 = Person.new
	puts(p2.mood)				#=> happy
	puts(p2.expression) 		#=> frowning
	puts(p2.happyexpression) 	#=> smiling
	puts(p2.sadexpression) 		#=> frowning

### 谨慎使用 Mix-in！

虽然每个类只能从继承自一个超类，但它可以混入（mix in）许多模块。实际上，完全允许将一批模块混入到另一批模块中，并将这些其它模块混入到类中，再将这些类混入到更多模块中。下面是一些代码的示例，这些代码将子类混入在模块中，甚至是混入到模块中的子类中。此代码已被有意简化。有关示例的完整代码，请参阅示例程序 **multimods.rb**：

<div class="code-file clearfix"><span>multimods.rb</span></div>

	module MagicThing # module
	  class MagicClass # class inside module
	  end
	end

	module Treasure   # module
	end

	module MetalThing
	  include MagicThing 			# mixin
	  class Attributes < MagicClass # subclasses class from mixin
	  end
	end

	include MetalThing				# mixin
	class Weapon < MagicClass 		# subclass class from mixin
	  class WeaponAttributes < Attributes # subclass
	  end
	end

	class Sword < Weapon # subclass
	  include Treasure 		# mixin
	  include MagicThing 	# mixin
	end

简而言之，虽然模块在使用时可能有助于避免与 C++ 一样的多重继承相关的一些复杂性，但它们仍可能被滥用。如果程序员真的想要创建复杂的类层次结构，并且在模块中的多个级别上具有难以理解的依赖性，那么他或她当然可以这样做。在 **multimods.rb** 中，我已经展示了用几行代码编写一个难以理解的程序是多么容易。想象一下，你可能将成千上万行这样的代码分散在数十个代码文件中！可以说，这不是我推荐的编程风格，因此你可能需要在混入模块之前仔细考虑一下。

### 从文件中包含模块

到目前为止，我已经混入了在单个源文件中定义的模块。通常，在单独的文件中定义模块并根据需要将它们混入更有用。要使用其它文件中的代码，你必须要做的第一件事是使用 `require` 方法加载该文件，如下所示：

	require( "testmod.rb" )

（可选）你可以省略文件扩展名：

	require( "testmod" )  # this works too

所需文件必须位于当前目录，搜索路径或预定义数组变量 `$:` 中列出的文件夹中。你可以使用常用的 array-append 方法 `<<` 向此数组变量添加元素，以这种方式：

	$: << "C:/mydir"

<div class="note">

全局变量 `$:`（美元符号和冒号）包含一个字符串数组，表示 Ruby 在查找加载或引入文件时搜索的目录。
</div>

如果成功加载指定的文件，`require` 方法返回 `true` 值；否则返回 `false`。如果有疑问，你只需显示结果：

	puts(require( "testmod.rb" ))

当需要该文件时，将执行在文件运行时通常执行的任意代码。因此，如果文件 `testmod.rb` 包含此代码...

<div class="code-file clearfix"><span>testmod.rb</span></div>

	def sing
	  puts( "Tra-la-la-la-la....")
	end

	puts( "module loaded")
	sing

...文件 **require_module.rb** 包含它...

<div class="code-file clearfix"><span>require_module.rb</span></div>

	require( "testmod.rb")

...然后，当运行 **require_module.rb** 时，这将输出：

	module loaded
	Tra-la-la-la-la....

在所需文件中声明的模块，可以将其混入：

	require( "testmod.rb")
	include MyModule #mix in MyModule declared in testmod.rb

<div class="code-file clearfix"><span>load_module.rb</span></div>

Ruby 还允许你使用 `load` 方法加载文件。在大多数情况下，`require` 和 `load` 可视为可互换的。但是有一些微妙的差异。特别是，`load` 可以使用可选的第二个参数，如果为 `true`，则加载并执行代码作为未命名或匿名模块：

	load( "testmod.rb", true)

加载的文件不会将新命名空间（namespace）引入主程序，你将无法访问已加载文件中的所有模块。当 `load` 的第二个参数为 `false` 或没有第二个参数时，你将可以访问已加载文件中的模块。请注意，使用 `load` 时你必须输入完整的文件名（"testmod" 减去 ".rb" 扩展名是不行的）。

另一个区别是 `require` 仅加载一次文件（即使你的代码多次引入该文件），而 `load` 会导致每次调用 `load` 时重新加载指定的文件。我们假设你在文件 **test.rb** 中有这个：

<div class="code-file clearfix"><span>test.rb</span></div>

	MyConst = 1
	if @a == nil then
	  @a = 1
	else
	  @a += MyConst
	end

	puts @a

我们假设你现在引入（require）这个文件三次：

<div class="code-file clearfix"><span>require_again.rb</span></div>

	require "test"
	require "test"
	require "test"

这里将会输出：

	1

但是如果你加载（load）文件三次...

<div class="code-file clearfix"><span>load_again.rb</span></div>

	load "test.rb"
	load "test.rb"
	load "test.rb"

...那么这将会输出：

	1
	./test.rb:1: warning: already initialized constant MyConst
	2
	./test.rb:1: warning: already initialized constant MyConst
	3

## 深入探索

### 模块与类

我们已经研究了模块的行为。现在我们来看看模块的本质是什么。事实证明，与 Ruby 中的大多数其它东西一样，模块是对象（object）。事实上，每个命名模块都是 Module 类的实例：

<div class="code-file clearfix"><span>module_inst.rb</span></div>

	module MyMod
	end

	puts( MyMod.class )  #=> Module

你无法创建命名模块的后代，因此不允许这样做：

	module MyMod
	end

	module MyOtherMod < MyMod
	end

但是，与其它类一样，允许创建 Module 类的后代：

	class X < Module
	end

实际上，`Class` 类本身就是 `Module` 类的后代。它继承了 Module 的行为并添加了一些重要的新行为 - 特别是创建对象的能力。你可以通过运行 **modules_classes.rb** 程序来显示此继承链以验证 Module 是 Class 的超类：

<div class="code-file clearfix"><span>modules_classes.rb</span></div>

	Class
	Module  #=> is the superclass of Class
	Object  #=> is the superclass of Module

### 预定义模块

以下模块内置于 Ruby 解释器中：

	Comparable, Enumerable, FileTest, GC, Kernel, Math, ObjectSpace, Precision, Process, Signal

`Comparable` 是一个 mixin 模块，允许包含类以实现比较运算符。包含类必须定义 `<=>` 运算符，该运算符将接收器对象与另一个对象进行比较，返回 -1，0 或 +1，具体取决于接收器对象是否小于，等于或大于另一个对象。Comparable 使用 `<=>` 来实现常规的比较运算符（`<`，`<=`，`==`，`>=` 和 `>`）和 `between?` 方法。

`Enumerable` 是为枚举（enumeration）提供的 mix-in 模块。包含类必须提供 `each` 方法。

`FileTest` 是一个包含文件测试功能的模块；它的方法也可以从 File 类访问。

`GC` 模块为 Ruby 的标记和垃圾回收清除机制提供了一个接口。一些底层方法也可以通过 ObjectSpace 模块获得。

`Kernel` 是 Object 类包含的模块；它定义了 Ruby 的“内置”（‘built-in）方法。

`Math` 是一个包含了基本三角函数和超越函数功能的模块函数（module functions）的模块。它具有相同定义和名称的“实例方法”（instance methods）和模块方法。

`ObjectSpace` 是一个包含了与垃圾回收工具交互的例程，并且允许你使用迭代器遍历所有活动对象的模块。

`Precision` 是为有具体精度的数字（numeric）类提供的 mixin 模块。这里，"Precision"　表示近似的实数精度，因此，该模块不应包含在不是　Real（实数）子集的任何类中（因此它不应包含在诸如　Complex（复数）或　Matrix（矩阵）之类的类中）。

`Process ` 是操纵进程（processes）的模块。它的所有方法都是模块方法（module methods）。

`Signal` 是用于处理发送到正在运行的进程的信号的模块。可用信号名称列表及其解释取决于操作系统。

以下是三个最常用的Ruby模块的简要概述...

#### Kernel

最重要的预定义模块是 Kernel，它提供了许多“标准”（standard）Ruby 方法，如 `gets`，`puts`，`print` 和 `require`。与许多 Ruby 类库一样，Kernel 是用 C 语言编写的。虽然 Kernel 实际上是“内置于”（built into）Ruby 解释器，但从概念上讲它可以被视为一个 mixed-in 模块，就像普通的 Ruby mixin 一样，它使得它的方法可以直接用于任何需要它的类。由于它混入到了 Object 类中，所有其它 Ruby 类都继承自该类，因此 Kernel 的方法是全部可访问的。

#### Math

<div class="code-file clearfix"><span>math.rb</span></div>

Math 模块的方法以“模块”（module）和“实例”（instance）方法的形式同时提供，因此可以通过将 Math 混入到类中或从外部通过使用模块名称，点和方法名来访问模块方法；你可以使用双冒号访问常量：

	puts( Math.sqrt(144) )
	puts( Math::PI )

#### Comparable

<div class="code-file clearfix"><span>compare.rb</span></div>

Comparable 模块通过将模块混入到你的类中并定义 `<=>` 方法，来提供一种巧妙的方式定义你自己的比较'运算符'（operators）`<`，`<=`，`==`，`>=` 和 `>`。然后，你可以指定将当前对象中的某些值与其他值进行比较的规则。例如，你可能会比较两个整数，两个字符串的长度或一些更不常用的值（例如数组中字符串的位置）。我在我的示例程序 **compare.rb** 中选择了这种不常用的比较类型。这里使用了一个虚构的数组中的字符串索引，以便将一个人的名字与另一个人的名字进行比较。小的索引（例如位于索引 0 处的 "hobbit"）被认为是小于大的索引（例如位于索引 6 处的 "dragon"）：

	class Being
	  include Comparable
	  BEINGS = ['hobbit','dwarf','elf','orc','giant','oliphant','dragon']

	  attr_accessor :name

	  def <=> (anOtherName)
		BEINGS.index[@name]<=>BEINGS.index[anOtherName]
	  end

	  def initialize( aName )
		@name = aName
	  end
	end

	elf = Being.new('elf')
	orc = Being.new('orc')
	giant = Being.new('giant')

	puts( elf.name < orc.name ) 	#=> true
	puts( elf.name > giant.name ) 	#=> false

### 作用域解析

与类一样，你可以使用双冒号作用域解析运算符（scope resolution operator）来访问模块内声明的常量（包括类和其它模块）。例如，假设你有嵌套的模块和类，如下所示：

	module OuterMod
	  moduleInnerMod
		class Class1
	    end
	  end
	end

你可以使用 `::` 运算符访问 Class1，如下所示：

	OuterMod::InnerMod::Class1

<div class="note">

有关类中常量的作用域解析的介绍，请参见第 2 章...
</div>

每个模块和类都有自己的作用域，这意味着可以在不同的作用域中使用单个常量名称。既然如此，你可以使用 `::` 运算符在确定的作用域内指定常量：

	Scope1::Scope2::Scope3 	#...etc

如果在常量名称的最开头使用此运算符，则会产生“打破”（breaking out）当前作用域并访问“顶级”（top level）作用域的效果：

	::ACONST 	# refers to ACONST at „top level‟ scope

以下程序提供了作用域运算符的一些示例：

<div class="code-file clearfix"><span>scope_resolution.rb</span></div>

	ACONST = "hello" 		# We'll call this the "top-level" constant

	module OuterMod
	  module InnerMod
		ACONST=10
		class Class1
		  class Class2
			module XYZ
			  class ABC
				ACONST=100
				def xyz
				  puts( ::ACONST ) #<= this prints the „top-level‟ constant
				end
			  end
			end
		  end
		end
	  end
	end

	puts(OuterMod::InnerMod::ACONST)
	#=> displays 10

	puts(OuterMod::InnerMod::Class1::Class2::XYZ::ABC::ACONST)
	#=> displays 100

	ob = OuterMod::InnerMod::Class1::Class2::XYZ::ABC.new
	ob.xyz
	#=> displays hello

### 模块函数

<div class="code-file clearfix"><span>module_func.rb</span></div>

如果你希望函数既可用作实例方法又可用作模块方法，则可以使用 `module_function` 方法，传入与实例方法的名称相匹配的符号（symbol）即可，如下所示：

	module MyModule
	  def sayHi
		return "hi!"
	  end

	  def sayGoodbye
		return "Goodbye"
	  end

	  module_function :sayHi
	end

现在，`sayHi` 方法可以混入到一个类中并用作实例方法：

	class MyClass
	  include MyModule
	  def speak
		puts(sayHi)
		puts(sayGoodbye)
	  end
	end

它可以用作模块方法，使用点符号：

	ob = MyClass.new
	ob.speak
	puts(MyModule.sayHi)

由于这里的 `sayGoodbye` 方法不是模块函数（module function），因此不能以这种方式使用：

	puts(MyModule.sayGoodbye)  #=> Error: undefined
	method

Ruby 在其一些标准模块，例如 Math（在 Ruby 库文件，**complex.rb** 中）），中使用 `module_function` 来创建模块和实例方法的“匹配对”（matching pairs）。

### 扩展对象

你可以使用 `extend` 方法将模块的方法添加到特定对象（而不是整个类），如下所示：

<div class="code-file clearfix"><span>extend.rb</span></div>

	module A
	  def method_a
		puts( 'hello from a' )
	  end
	end

	class MyClass
	  def mymethod
		puts( 'hello from mymethod of class MyClass' )
	  end
	end

	ob = MyClass.new
	ob.mymethod
	ob.extend(A)

现在对象 `ob` 用模块 `A` 进行了扩展（extend），它可以访问该模块的实例方法 `method_a`：

	ob.method_a

实际上，你可以一次用多个模块来扩展对象。这里，模块 `B` 和 `C` 扩展了对象 `ob`：

	ob.extend(B, C)

当使用包含了与对象类中方法同名的方法的模块扩展对象时，模块中的方法将替换该类中的方法。所以，我们假设 `ob` 用这个类来扩展...

	module C
	  def mymethod
		puts( 'hello from mymethod of module C' )
	  end
	end

现在，当你调用 `ob.mymethod` 时，将显示字符串 'hello from mymethod of module C' 而不是之前显示的 'hello from mymethod of class MyClass'。

你可以通过使用 `freeze` 方法来“冻结”（freezing）它，以阻止对象被扩展：

	ob.freeze

任何进一步扩展此对象的尝试都将导致运行时错误（runtime error）。为了避免这样的错误，你可以使用 ` frozen?` 方法来测试对象是否已被冻结：

	if !(ob.frozen?)
	  ob.extend( D )
	  ob.method_d
	else
	  puts( "Can't extend a frozen object" )
	end


# 第十三章

***

## Files 与 IO

Ruby 提供了专门用于处理 IO – 输入和输出的类。其中最主要的是一个名为 IO 的类，这不足为奇。IO 类允许你打开和关闭 IO “流”（streams，字节序列），并向它们读写数据。

例如，假设你有一个名为 'textfile.txt' 的文件，它包含一些文本行，这就是你打开文件并在屏幕上显示每一行文本的方法：

<div class="code-file clearfix"><span>io_test.rb</span></div>

	IO.foreach("testfile.txt") {|line| print( line ) }

这里 `foreach` 是 IO 类的类方法，因此你不需要创建新的 IO 对象来使用它；相反，你只需将文件名指定为参数。`foreach` 方法接收一个块，从文件中读取的每一行都作为参数传递给它。你不必打开文件进行读操作，并在完成后关闭它（正如你根据其它语言的使用经验所预料的那样），因为 Ruby 的 `IO.foreach` 方法会为你完成这些操作。

IO 有许多其它有用的方法。例如，你可以使用 `readlines` 方法将文件内容读入数组以进行进一步处理。这是一个简单的示例，它再次将文本行打印到屏幕：

	lines = IO.readlines("testfile.txt")
	lines.each{|line| print( line )}

File 类是 IO 类的子类，上面的示例可以使用 File 类重写：

<div class="code-file clearfix"><span>file_test.rb</span></div>

	File.foreach("testfile.txt") {|line| print( line ) }

	lines = File.readlines("testfile.txt")
	lines.each{|line| print( line )}

### 打开和关闭文件

虽然一些标准方法会自动打开和关闭文件，但在处理文件内容时，你需要显式的打开和关闭文件。你可以使用 `new` 或 `open` 方法打开文件。你必须将两个参数传递给其中一个方法 - 文件名和文件 'mode' - 同时将返回一个新的 File 对象。文件模式（modes）可以是由操作系统指定的常量或字符串所定义的整数。该模式通常指示文件是打开以进行读取（'r'），写入（'w'）还是读取和写入（'rw'）。这是可用字符串模式的列表：

| Mode | Meaning |
| ------ | ------ |
| "r" | 只读，从文件开头开始（默认模式）。 |
| "r+" | 读写，从文件开头开始。 |
| "w" | 只写，将现有文件截断为零长度或创建用于写入的新文件。 |
| "w+" | 读写，将现有文件截断为零长度或创建新文件以进行读写。 |
| "a" | 只写，如果文件存在则从文件末尾开始，否则创建一个用于写入的新文件。 |
| "a+" | 读写，如果文件存在则从文件末尾开始，否则创建一个用于读写的新文件。 |
| "b" | （仅限 DOS/Windows）二进制文件模式（可能与上面列出的任何关键字母一起出现）。 |

<div class="code-file clearfix"><span>open_close.rb</span></div>

让我们看一下打开，处理和关闭文件的实际示例。在 **open_close.rb** 中，我首先打开一个文件 'myfile.txt'，用于写入（'w'）。打开文件进行写入时，如果该文件尚不存在，则将创建该文件。我使用 `puts()` 在文件中写入六个字符串，在六行中分别写一个字符串。最后我关闭了文件。

	f = File.new("myfile.txt", "w")
	f.puts( "I", "wandered", "lonely", "as", "a", "cloud" )
	f.close

关闭文件不仅会释放“文件句柄”（file handle，指向文件数据的指针），还会“刷新”（flushes）内存中的数据，以确保它全部保存到磁盘上的文件中。未能关闭文件可能会导致不可预测的副作用（尝试注释掉上面显示的 `f.close` 以便你自己查看！）。

现在，将文本写入文件后，让我们看看如何打开该文件并重新读取数据。这次我将一次读取一个数据中的字符。在我这样做的时候，我将保留已读过的字符数。我还会保留行数，每当我读入一个换行符时，行数都会递增（给定 ASCII 码 10）。为了清楚起见，我将在每行读取的末尾添加一个字符串，显示其行号。我将在屏幕上显示文件字符加上我的行结束字符串，当从文件中读取所有内容后，我将关闭它并显示我计算的统计数据。这是完整的代码：

	charcount = 0
	linecount = 0
	f = File.new("myfile.txt", "r")
	while !( f.eof ) do 		# while not at end of file...
	  c = f.getc()  			# getc gets a single character
	  if ( c == 10 ) then 		# ...whose ASCII code is tested
		linecount += 1
		puts( " <End Of Line #{linecount}>" )
	  else
		putc( c )  				# putc here puts the char to screen
		charcount += 1
	  end
	end

	if f.eof then
	  puts( "<End Of File>" )
	end
	f.close
	puts("This file contains #{linecount} lines and #{charcount} characters." )

### 文件和目录...

你还可以使用 File 类来操作磁盘上的文件（files）和目录（directories）。在尝试对文件执行某些操作之前，你必须自然地确保该文件存在。毕竟，它可能在程序启动后被重命名或删除 - 或者用户可能错误地输入了文件或目录名称。

你可以使用 `File.exist?` 方法验证文件是否。这是 FileTest 模块提供给 File 类的几种测试方法之一。就 `File.exist?` 方法而言，一个目录记为一个文件，所以你可以使用下面的代码来测试是否存在 C:\ 驱动器（注意你必须在字符串中使用双文件分隔符 '\\'，单个 '\' 将被视为转义字符）：

<div class="code-file clearfix"><span>file_ops.rb</span></div>

	if File.exist?( "C:\\" ) then
	  puts( "Yup, you have a C:\\ directory" )
	else
	  puts( "Eeek! Can't find the C:\\ drive!" )
	end

如果要区分目录和数据文件，请使用 `directory?` 方法：

	def dirOrFile( aName )
	  if File.directory?( aName ) then
		puts( "#{aName} is a directory" )
	  else
		puts( "#{aName} is a file" )
	  end
	end

### 复制文件

让我们通过编写一个简单的文件备份程序将 File 类用于实际用途。当你运行 **copy_files.rb** 时，将要求你选择要从中复制的目录（源目录）和要复制到的另一个目录（目标目录）。假设两个目录都存在，程序将把所有文件从源目录复制到目标目录。如果目标目录不存在，它将询问你是否要创建它（你应该输入，'Y' 接受）。我已经为你提供了一个源目录；只需在提示时输入名称 **srcdir**。当询问目标目录时，输入 **targetdir** 以在当前目录下创建该名称的子目录。

程序使用源目录的路径初始化变量 `sourcedir`，并使用目标目录的名称初始化 `targetdir`。这是执行文件复制的代码：

<div class="code-file clearfix"><span>copy_files.rb</span></div>

	Dir.foreach( sourcedir ){
	  |f|
	  filepath = "#{sourcedir}\\#{f}"
	  if !(File.directory?(filepath) ) then
	    if File.exist?("#{targetdir}\\#{f}") then
		  puts("#{f} already exists in target directory (not copied)" )
	    else
		  FileUtils.cp( filepath, targetdir )
		  puts("Copying... #{filepath}" )
	    end
	  end
	}

在这里，我使用了 Dir 类的 `foreach` 方法，该方法将指定目录中每个文件的文件名传递给块变量 `f`。我很快就会说到关于 Dir 类的东西。该代码通过将文件名附加到 `sourcedir` 变量给出的目录名来构造合适的文件路径 `filepath`。我只想复制数据文件而不是目录，所以我测试文件路径是文件而不是目录：

	if !(File.directory?(filepath) )

此程序不会复制已存在的文件，因此它首先检查目标目录 `targetdir` 中是否已存在名称为 `f` 的文件：

	if File.exist?("#{targetdir}\\#{f}")

最后，假设满足所有指定条件，源文件 `filepath` 将复制到 `targetdir`：

	FileUtils.cp( filepath, targetdir )

这里的 `cp` 是 FileUtils 模块中的文件复制方法。该模块还包含许多其它有用的文件处理例程，例如 `mv(source，target)` 用于将文件从 `source` 移动到 `target`；`rm(files)` 将删除 files 参数列出的一个或多个文件，`mkdir` 将创建一个目录，就像我在当前程序中创建 `targetdir` 时所做的那样：

	FileUtils.mkdir( targetdir )

### 目录查询

我的备份程序一次只处理一个目录级别 - 这就是为什么它在尝试复制之前测试文件 `f` 不是目录的原因。但是，有很多次，你可能想要遍历子目录。举个例子，让我们编写一个程序来计算指定根目录下所有子目录的大小。例如，如果你想要找到最大的文件和目录，以便通过存档或删除它们来释放磁盘空间，这可能很有用。

浏览子目录为我们提供了一个有趣的编程问题。当我们开始搜索存在的子目录时，我们不知道我们是否会找到一个，没有或者多个。此外，我们找到的任何子目录可能包含另一级子目录，每个子目录可能包含其它子目录，依此类推，通过许多可能的级别。

### 关于递归的讨论

我们的程序需要能够将整个子目录树向下导航到任意数量的级别。为了能够做到这一点，我们必须使用递归。

<div class="note">
	<p class="h4"><b>什么是递归（Recursion）？</b></p>

简单的说，递归方法就是调用它自己的。如果你不熟悉递归编程，请参阅本章末尾的“深入探索”部分中的“简单递归”。
</div>

<div class="code-file clearfix"><span>file_info.rb</span></div>

在程序 **file_info.rb** 中，`processfiles` 方法是递归的：

	def processfiles( aDir )
	  totalbytes = 0
	  Dir.foreach( aDir ){
		|f|
		mypath = "#{aDir}\\#{f}"
		s = ""
		if File.directory?(mypath) then
		  if f != '.' and f != '..' then
			bytes_in_dir = processfiles(mypath)  # <==== recurse!
			puts( "<DIR> ---> #{mypath} contains [#{bytes_in_dir/1024}] KB" )
		  end
		else
		  filesize = File.size(mypath)
		  totalbytes += filesize
		  puts ( "#{mypath} : #{filesize/1024}K" )
		end
	  }
	  $dirsize += totalbytes
	  return totalbytes
	end

你将看到，当首次调用该方法时，向下到源代码的底部，它将在变量 `dirname` 中传递一个目录的名称：

	processfiles( dirname )

我已经将当前目录的父级（由两个点给出，`".."`）分配给 `dirname`。如果你在其原始位置运行此程序（即，从本书的源代码存档中提取其位置），则将引用包含所有示例代码文件的子目录的目录。或者，你可以将硬盘上某个目录的名称分配给代码中指定的变量 `dirname`。如果你这样做，不要指定包含大量文件和目录的目录（**"C：\ Program Files"** 不是一个好的选择！），因为程序需要一些时间来执行。

让我们仔细看看 `processfiles` 方法中的代码。再次，我使用 `Dir.foreach` 查找当前目录中的所有文件，并一次传递一个文件 `f`，由花括号之间的块中的代码处理。如果 `f` 是一个目录但不是当前目录（`"."`）或其父目录（`".."`），那么我将目录的完整路径传递回 `processfiles` 方法：

	if File.directory?(mypath) then
	  if f != '.' and f != '..' then
		bytes_in_dir = processfiles(mypath)

如果 `f` 不是目录，而只是一个普通的数据文件，我用 `File.size` 计算它的大小（以字节为单位）并将其分配给变量 `filesize`：

	filesize = File.size(mypath)

由于每个连续文件 `f` 由代码块处理，因此计算其大小并将此值添加到变量 `totalbytes`：

	totalbytes += filesize

将当前目录中的每个文件传递到块后，`totalbytes` 将等于目录中所有文件的总大小。

但是，我还需要计算所有子目录中的字节数。由于该方法是递归的，因此这是自动完成的。请记住，当 `processfiles` 方法中大括号之间的代码确定当前文件f是一个目录时，它会将此目录名称传递回自身 -  `processfiles` 方法。

让我们假设首先使用 **C:\test** 目录调用 `processfiles`。在某些时候，变量 `f` 被赋予其子目录之一的名称 - 比如 **C:\test\dir_a**。现在这个子目录被传递回 `processfiles`。在 **C:\test\dir_a** 中找不到更多目录，因此 `processfiles` 只计算该子目录中所有文件的大小。当它完成计算这些文件时，`processfiles` 方法结束并将当前目录中的字节数 `totalbytes` 返回到首先调用该方法的代码位置：

	return totalbytes

在这种情况下，`processfiles` 方法本身内部的这段代码以递归方式调用 `processfiles` 方法：

	bytes_in_dir = processfiles(mypath)

因此，当 `processfiles` 完成处理子目录 **C:\test\dir_a** 中的文件时，它返回在那里找到的所有文件的总大小，并将其分配给 `bytes_in_dir` 变量。`processfiles` 方法现在从它停止的地方继续（也就是说，它从它自己处理子目录的地方继续）以处理原始目录 **C:\test** 中的文件。

无论此方法遇到多少级别的子目录，每当它找到目录时都会调用它自己的事实确保它会自动沿着它找到的每个目录路径向下移动，计算每个子目录中的总字节数。

最后要注意的是，在每个递归级别完成时，分配给 `processfiles` 方法内部声明的变量的值将更改回其“之前”的值。因此，`totalbytes` 变量首先包含 **C:\test\test_a\test_b** 的大小，然后是 **C:\test\test_a** 的大小，最后是 **C:\test** 的大小。为了保证运行结果总和是所有目录的组合大小，我们需要将值分配给在方法外部声明的变量。为此，我使用全局变量 `$dirsize` 来实现这个目的，将处理的每个子目录计算的 `totalbytes` 值增加到该变量：

	$dirsize += totalbytes

顺便提一下，虽然字节（byte）对于非常小的文件来说可能是很方便的测量单位，但通常更好的是以千字节（kilobyte）描述更大的文件，以兆字节（megabytes）描述非常大的文件或目录。要将字节转换为千字节或将千字节转换为兆字节，你需要除以 1024。要将字节转换为兆字节，除以 1048576。

我程序中的最后一行代码执行这些计算，并使用 Ruby 的 `printf` 方法以格式化字符串显示结果：

	printf( "Size of this directory and subdirectories is #{$dirsize} bytes, #{$dirsize/1024}K, %0.02fMB", "#{$dirsize/1048576.0}" )

请注意，我在第一个字符串中嵌入了格式化占位符 "％0.02fMB"，并在逗号后面添加了第二个字符串：

	"#{$dirsize/1048576.0}".

第二个字符串计算目录大小（以兆字节为单位），然后将该值替换为第一个字符串中的占位符。占位符的格式选项 `"％0.02f"` 确保兆字节值显示为浮点数 `"f"`，带有两个小数位，`"0.02"`。

### 根据大小排序

目前，该程序按字母顺序打印文件和目录名称及其大小。但我对它们的相对大小更感兴趣。因此，如果文件按大小而不是按名称排序，则会更有用。

为了能够对文件进行排序，我们需要一些方法来存储所有文件大小的完整列表。一种显而易见的方法是将文件大小添加到数组中。在 **file_info2.rb** 中，我创建了一个空数组 `$files`，并且每次处理文件时，我都会将其大小附加到数组中：

<div class="code-file clearfix"><span>file_info2.rb</span></div>

	$files << fsize

然后，我可以对文件大小进行排序，以显示从低到高的值或（通过排序然后反转数组），从高到低的值：

	$files.sort # sort low to high
	$files.sort.reverse # sort high to low

唯一的问题是我现在最终得到一个没有相关文件名的文件大小数组。更好的解决方案是使用 Hash 而不是 Array。我在 **file_info3.rb** 中完成了这个。首先，我创建两个空 Hash：

<div class="code-file clearfix"><span>file_info3.rb</span></div>

	$dirs = {}
	$files = {}

现在，当 `processfiles` 方法遇到目录时，它会向 `$dirs` 哈希添加一个新元素，使用完整目录路径 `mypath` 作为键，目录大小 `dsize` 作为值：

	$dirs[mypath] = dsize

同样的将键值对添加到 `$files` 哈希中。当通过递归调用 `processfiles` 方法处理子目录和文件的整个结构时，`$dirs` 哈希变量将包含目录名和大小的键值对，`$files` 哈希将包含文件名的键值对和大小。

现在剩下的就是对这些哈希进行排序和显示。Hash 的标准排序方法是对键进行排序，而不是值。 我想根据值（大小）排序，而不是根据键（名称）。为了做到这一点，我已经定义了这个自定义排序方法：

	$files.sort{|a,b| a[1]<=>b[1]

这里 `sort` 遍历将（directory-walking） `$files` 哈希转换为 `[key，value]` 对的嵌套数组，并将其中的两个作为 `a` 和 `b` 传递到花括号之间的块中。每个 `[key，value]` 对的第二项（在索引 `[1]` 处）提供值。使用 Ruby 的 `<=>` 比较方法对值进行排序。最终结果是，该程序现在首先按升序（按大小）显示文件列表，然后类似的显示排序的目录列表。

## 深入探索

### 简单递归

<div class="code-file clearfix"><span>recursion.rb</span></div>

如果你之前从未使用过递归（recursion），则本章中的递归“目录遍历”（directory-walking）方法可能需要一些说明。为了阐明递归是如何工作的，让我们看一个更简单的例子。加载 **recursion.rb** 程序：

	$outercount = 0

	def addup( aNum )
	  aNum += 1
	  $outercount +=1
	  puts( "aNum is #{aNum}, $outercount is #{$outercount}" )
	  if $outercount < 3 then
		addup( aNum ) #<= recursive call to addup method
	  end
	  puts( "At END: aNum is #{aNum}, outercount is #{$outercount}" )
	end

	addup( 0 ) #<= This is where it all begins

这包含递归方法 `addup`，其唯一的目的是从 1 到 3 计数。`addup` 方法接收一个整数值作为传入参数 `aNum`。

	addup( aNum )

还有全局变量 `$outercount`，它存在于 `addup` 方法之外。每当 `addup` 方法执行时，1 将添加到 `aNum`，1 也会添加到 `$outercount`。然后，只要 `$outercount` 小于 3，`addup` 方法中的代码就会再次调用相同的方法（`addup`），并将 `aNum` 的新值传递给它：

	if $outercount < 3 then
	  addup( aNum )
	end

让我们来看看会发生什么。通过值 0 来调用 `addup` 以启动整个过程：

	addup( 0 )

`addup` 方法将 `aNum` 和 `$outercount` 都加 1，因此两个变量现在都具有值 1。测试 `test($outercount < 3)` 的计算结果为 true，因此 `aNum` 作为参数传递给 `addup`。再次向两个变量添加 1，因此 `aNum` 现在为 2，`$outercount` 也为 2。现在 `aNum` 再次传递给 `addup`。然后再将 1 添加到两个变量中，给出每个值 3。然而，这次测试条件失败，因为 `$outercount` 不再小于 3。因此调用 `addup` 的代码被跳过，我们到达方法的最后一行：

	puts( "At END: aNum is #{aNum}, outercount is #{$outercount}" )

这会打印出 `aNum` 和 `$outercount` 的值，正如我们所料，它们都是 3。

现在已经到达此方法的末尾，“控制流”会在最初调用该方法的代码之后立即返回到代码行。这里，调用 `addup` 方法的代码行恰好位于方法本身内部。这里是：

	addup( aNum )

此后的第一个可执行代码行是（再次）方法的最后一行，它打印出两个变量的值：

	puts( "At END: aNum is #{aNum}, outercount is #{$outercount}" )

所以我们回到了之前的“执行点” - 我们递归调用 `addup` 方法的点。那时，`aNum` 的值是 2，也是它现在的值。如果这看起来令人困惑，那就试着想想如果 `aNum` 已经是 2 ，然后我们调用其它一些不相关的方法，那么会发生什么。从该方法返回时，`aNum` 当然仍然具有值 2。这就是发生在这里的一切。唯一的区别是这种方法恰好调用自己而不是其它方法。

该方法再一次退出，控制再次返回到调用该方法的代码之后的下一个可执行代码行 - 并且 `aNum` 的值又回到了自己的历史记录中 - 它现在具有值 1。但是，`$outercount` 变量存在于方法之外，不受递归的影响，因此它仍然是 3。

<div class="note">

如果你可以访问可视化调试器，那么如果在第 9 行放置一个断点（`if $outercount < 3 then`），将 `aNum` 和 `$outercount` 添加到 Watch 窗口，并在你命中断点之后重复进入代码，整个过程将变得更加清晰。

<div class="text-center">
	<img src="./images/chapter13_debug_recursion.png" />
	<p class="small">
		此屏幕截图显示了在 <a href="http://www.sapphiresteel.com/" target="_blank">Ruby In Steel</a> 中调试的递归程序。我可以单步执行源代码，使用调用堆栈来跟踪当前递归的“级别”（调用 <code>addup</code> 方法的次数），并使用Watch 窗口监视变量的当前值。
	</p>
</div>
</div>

# 第十四章

***

## YAML

在某些时候，大多数桌面应用程序都希望在磁盘上保存和读取结构化数据。我们已经看到了如何使用简单的 IO 例程（如 `gets` 和 `puts`）读取和写入数据。但是，如何编写保存和恢复混合对象类型列表中的数据？使用 Ruby 执行此操作的一种简单方法是使用 YAML。

<div class="note">

**YAML** 是 "Yet An-other Markup Language"（仍是一种标记语言，有争议）或 "YAML Ain't Markup Language" （不是标记语言，递归的）的首字母缩写。
</div>

### 转换成 YAML

YAML 定义了一种序列化（数据保存）格式，它将信息存储为人类可读的文本。YAML 可以与各种编程语言一起使用，为了在 Ruby 中使用它，你的代码需要使用 **yaml.rb** 文件。通常，这可以通过在代码单元的顶部加载或“引入”（requiring）文件来完成，如下所示：

	require 'yaml'

完成此操作后，你将可以访问各种方法将 Ruby 对象转换为 YAML 格式，以便将其数据写入文件。随后，你将能够回读已保存的数据并使用它来重新构造 Ruby 对象。

要将对象转换为 YAML 格式，可以使用 `to_yaml` 方法。它可以转换任何对象 - 字符串，整数，数组，哈希等。例如，这是转换字符串的方式：

<div class="code-file clearfix"><span>to_yaml1.rb</span></div>

	"hello world".to_yaml

这是如何转换数组：

	["a1", "a2" ].to_yaml

这是你通过此数组转换获得的 YAML 格式：

	---
	- a1
	- a2

请注意定义新 YAML '文档'的开头的三个破折号以及定义列表中每个新元素的单个破折号。有关 YAML 格式的更多信息，请参阅本章末尾的“深入探索”部分。

你还可以将非标准类型的对象转换为 YAML。例如，假设你创建了此类和对象...

<div class="code-file clearfix"><span>to_yaml2.rb</span></div>

	class MyClass
	  def initialize( anInt, aString )
		@myint = anInt
		@mystring =aString
	  end
	end

	ob1 = MyClass.new( 100, "hello world" ).to_yaml

此对象的 YAML 表示形式将以文本 `!ruby/object:` 开头，后跟类名，每行一个变量名称附加冒号（但减去 `@`）及其值：

	--- !ruby/object:MyClass
	myint: 100
	mystring: hello world

如果要打印出对象的 YAML 表示，可以使用方法 `y()`，它是一种 YAML 的方法，等同于我们熟知的用来查看并打印正常的 Ruby 对象的 `p()` 方法：

<div class="code-file clearfix"><span>yaml_test1.rb</span></div>

	y( ['Bert', 'Fred', 'Mary'] )

这将显示：

	---
	- Bert
	- Fred
	- Mary

你可以同样的显示一个哈希对象...

	y( { 'fruit' => 'banana', :vegetable => 'cabbage', 'number' => 3 } )

...在这种情况下，每个键/值对都放在一个新行上：

	---
	number: 3
	fruit: banana
	:vegetable: cabbage

或者你可以显示自己的“自定义”对象...

	t = Treasure.new( 'magic lamp', 500 )
	y( t )

...它显示的数据，如前面我使用 `to_yaml` 的示例一样，顶部是类名以及连续行上是一对变量名和值：

	--- !ruby/object:Treasure
	name: magic lamp
	value: 500

<div class="code-file clearfix"><span>yaml_test2.rb</span></div>

你甚至可以使用 `y()` 来显示非常复杂的对象，例如嵌套数组：

	arr1 = [ ["The Groovesters", "Groovy Tunes", 12 ],
			[ "Dolly Parton", "Greatest Hits", 38 ]
		]

	y( arr1 )

...或包含任意类型对象的数组：

	arr2 = [ CD.new("The Beasts", "Beastly Tunes", 22),
			CD.new("The Strolling Bones", "Songs For Senior Citizens", 38)
		]

	y( arr2 )

### 嵌套序列

当相关的数据序列（例如数组）嵌套在其它数据序列中时，这种关系由缩进表示。所以，例如，假设我们在 Ruby 中声明了这个数组...

<div class="code-file clearfix"><span>nested_arrays.rb</span></div>

	arr = [1,[2,3,[4,5,6,[7,8,9,10],"end3"],"end2"],"end1"]

当呈现为 YAML（例如，通过 `y(arr)`）时，这变为：

	---
	- 1
	- - 2
	  - 3
	  - - 4
	    - 5
	    - 6
	    - - 7
	      - 8
	      - 9
	      - 10
	    - end3
	  - end2
	- end1

### 保存 YAML 数据

`dump` 方法提供了另一种方便的方式将 Ruby 对象转换为 YAML 格式。最简单的是，它会将你的 Ruby 数据转换为 YAML 格式并将其转储为字符串：

<div class="code-file clearfix"><span>yaml_dump1.rb</span></div>

	arr = ["fred", "bert", "mary"]
	yaml_arr = YAML.dump( arr )	# yaml_arr is now: "--- \n- fred\n- bert\n- mary\n"

更有用的是，`dump` 方法可以接收第二个参数，它是某种 IO 对象，通常是文件（file）。你可以打开文件并将数据转储给它...

<div class="code-file clearfix"><span>yaml_dump2.rb</span></div>

	f = File.open( 'friends.yml', 'w' )
	YAML.dump( ["fred", "bert", "mary"], f )
	f.close

...或者你可以打开文件（或其它类型的 IO 对象）并将其传递到关联的块中：

	File.open( 'morefriends.yml', 'w' ){ |friendsfile|
		YAML.dump( ["sally", "agnes", "john" ], friendsfile )
	}

如果使用块，则退出块时文件将自动关闭，否则应使用 `close` 方法显式关闭文件。顺便提一下，你也可以以类似的方式使用块来打开文件并读入 YAML 数据：

	File.open( 'morefriends.yml' ){ |f|
		$arr= YAML.load(f)
	}

### 保存时忽略变量

如果由于某种原因，在序列化对象时要省略某些实例变量，可以通过定义名为 `to_yaml_properties` 的方法来实现。

在此方法的主体中，放置一个字符串数组。每个字符串应与要保存的实例变量的名称匹配。任何未指定的变量都不会被保存。看看这个示例：

<div class="code-file clearfix"><span>limit_y.rb</span></div>

	class Yclass
	  def initialize(aNum, aStr, anArray)
		@num = aNum
		@str = aStr
		@arr = anArray
	  end

	  def to_yaml_properties
		["@num", "@arr"] #<= @str will not be saved!
	  end
	end

这里 `to_yaml_properties` 限制了当你调用 `YAML.dump` 时被保存的变量仅为 `@num` 和 `@arr`。字符串变量 `@str` 将不会被保存。如果你以后希望根据保存的 YAML 数据重建对象，则你有义务确保“缺失”变量是不被需要的（在这种情况下可以忽略它们），或者如果需要，它们应该用一些有意义的值初始化：

	ob = Yclass.new( 100, "fred", [1,2,3] )	# ...creates object with @num=100, @str="fred", @arr=[1,2,3]

	yaml_ob = YAML.dump( ob ) #...dumps to YAML only the @num and @arr data (omits @str)

	ob2 = YAML.load( yaml_ob )	#...creates ob2 from dumped data with @num=100, @arr=[1,2,3] , but without @str

### 一个文件中多个文档

早些时候，我提到过三个破折号用于标记新的 YAML “文档”（document）的开头。在 YAML 术语中，文档是离散的组或片段。单个文件可能包含许多此类“文档”。

例如，假设你要将两个数组 `arr1` 和 `arr2` 保存到文件 **'multidoc.yml'**。 这里 `arr1` 是一个包含两个嵌套数组的数组，`arr2` 是一个包含两个 CD 对象的数组：

<div class="code-file clearfix"><span>multi_docs.rb</span></div>

	arr1 = [ ["The Groovesters", "Groovy Tunes", 12 ],
			[ "Dolly Parton", "Greatest Hits", 38 ]
		]

	arr2 = [ CD.new("Gribbit Mcluskey", "Fab Songs", 22),
			CD.new("Wayne Snodgrass", "Singalong-a-Snodgrass", 24)
		]

这是我将这些数组转储到 YAML 并将它们写入文件的例程（如第 13 章所述，`'w'` 参数导致文件以写入模式被打开）：

	File.open( 'multidoc.yml', 'w' ){ |f|
		YAML.dump( arr1, f )
		YAML.dump( arr2, f )
	}

查看文件 **'multidoc.yml'**，你将看到数据已保存为两个单独的'文档' - 每个文档以三个破折号开头：

	---
	- - The Groovesters
	  - Groovy Tunes
	  - 12
	- - Dolly Parton
	  - Greatest Hits
	  - 38
	---
	- !ruby/object:CD
	  artist: Gribbit Mcluskey
	  name: Fab Songs
	  numtracks: 22
	- !ruby/object:CD
	  artist: Wayne Snodgrass
	  name: Singalong-a-Snodgrass
	  numtracks: 24

现在，我需要找到一种通过将数据作为两个文档读取来重建这些数组的方法。`load_documents` 方法提供了该解决方式。

`load_documents` 方法调用一个块并将每个连续文档传递给它。下面是一个如何使用此方法从两个 YAML 文档重建两个数组（放在另一个数组 `$new_arr` 中）的示例：

	File.open( 'multidoc.yml' ) {|f|
	  YAML.load_documents( f ) { |doc|
		$new_arr << doc
	  }
	}

你可以通过执行以下操作来验证是否已使用两个数组初始化 `$new_arr`：

	puts( "$new_arr contains #{$new_arr.size} elements" )
	p( $new_arr[0] )
	p( $new_arr[1] )

或者，这是一种更通用的做同样事情的方法，它适用于任何长度的数组：

	$new_arr.each{ |arr| p( arr ) }

### YAML 数据库

有关以 YAML 格式保存和加载数据的稍微复杂的应用程序的示例，你可能需要查看 **cd_db.rb**。这里实现了一个简单的 CD 数据库。它定义了三种类型的 CD 对象 - 一个基本 CD，其中包含有关名称，艺术家和轨道数量的数据以及两个更专业的后代类 - PopCD，它添加了关于类型（例如“摇滚”或“乡村”）的数据以及 ClassicalCD 添加了导师和作曲家的数据。

当程序运行时，用户可以输入数据以创建这三种类型中的任何一种的新 CD 对象。还有一个将数据保存到磁盘的选项。随后运行应用程序时，将重新加载现有数据。

数据本身在代码中被组织得非常简单（甚至微不足道！），在创建对象本身之前将每个对象的数据读入数组。整个 CD 对象数据库被保存到全局变量 `$cd_arr` 中，并将其写入磁盘并使用 YAML 方法重新加载到内存中：

<div class="code-file clearfix"><span>cd_db.rb</span></div>

	def saveDB
	  File.open( $fn, 'w' ) {
		  |f|
		  f.write($cd_arr.to_yaml)
	  }
	end

	def loadDB
	  input_data = File.read( $fn )
	  $cd_arr = YAML::load( input_data )
	end

在现实世界的应用程序中，我确信你希望创建一些更优雅的数据结构来管理你的 Dolly Parton 集合！

### YAML 冒险游戏

作为使用 YAML 的最后一个示例，我为冒险游戏（**gamesave_y.rb**）提供了一个基本框架。这会创建一些 Treasure 对象和一些 Room 对象。Treasure 对象被放入 Room 对象中（也就是说，它们被放置在 Rooms 包含的数组中），然后 Room 对象被放入 Map 对象中。这具有构造中等复杂数据结构的效果，其中一种类型的对象（Map）包含任意数量的另一种类型的对象（Rooms），每个 Room 对象可以包含零个或多个其它类型的对象（Treasures））。

乍一看，找到一种将混合对象类型的整个网络存储到磁盘并在稍后重建该网络的方法可能看起来像编程噩梦。

事实上，由于 Ruby 的 YAML 库提供的序列化功能，保存和恢复这些数据几乎没有更容易的了。这是因为序列化（serialization）减轻了你逐个保存每个对象的繁琐工作。相反，你只需要“转储”（dump）顶级对象 - 这里就是 Map 对象 `mymap`。

完成此操作后，将自动为你保存顶级对象“包含”的任何对象（如 Rooms）或被包含对象本身包含的对象（如 Treasures）。然后可以通过在单个操作中加载所有已保存的数据并将其分配给“顶级”对象（此处为 map）来重建它们：

<div class="code-file clearfix"><span>gamesave_y.rb</span></div>

	# Save mymap
	File.open( 'game.yml', 'w' ){ |f|
	  YAML.dump( mymap, f )
	}

	# Reload mymap
	File.open( 'game.yml' ){ |f|
	  mymap = YAML.load(f)
	}

## 深入探索

### YAML 的简要指南

在 YAML 中，数据被分成包含“序列”（sequences）数据的“文档”。每个文档以三个短划线字符 `---` 开头，列表中的每个单独元素都以单个短划线字符 `-` 开头。因此，例如，这是一个 YAML 数据文件，包含一个文档和两个列表项：

	---
	- artist: The Groovesters
	  name: Groovy Tunes
	  numtracks: 12
	- artist: Dolly Parton
	  name: Greatest Hits
	  numtracks: 38

在上面的示例中，你可以看到每个列表项由两部分组成 - 名称如 `artist:`（在每个列表项中相同）和右侧的一段数据，例如 `Dolly Parton`，可能因每个列表项而异。这些项类似于 Ruby 的 Hash 中的键值对。YAML 将键值列表称为“映射”（maps）。

下面是一个包含两个项目的列表的 YAML 文档，每个项目包含三个项目 - 换句话说，它是包含两个三项“嵌套”数组的数组的 YAML 表示形式：

	---
	- - The Groovesters
	  - Groovy Tunes
	  - 12
	- - Dolly Parton
	  - Greatest Hits
	  - 38

现在让我们看看 YAML 如何处理嵌套的哈希（Hashes）。

思考这个 Hash：

<div class="code-file clearfix"><span>hash_to_yaml.rb</span></div>

	hsh = { :friend1 => 'mary',
			:friend2 => 'sally',
			:friend3 => 'gary',
			:morefriends => { :chap_i_met_in_a_bar => 'simon',
							  :girl_next_door => 'wanda'
							}
	}

正如我们已经看到的，Hash 在 YAML 中很自然地表示为键值对列表。但是，在上面显示的示例中，关键字 `:morefriends` 与嵌套哈希值相关联。YAML 如何表示？事实证明，与数组一样（参见本章前面的*“嵌套序列”*），它只是缩进嵌套的哈希：

	:friend1: mary
	:friend2: sally
	:friend3: gary
	:morefriends:
	  :chap_i_met_in_a_bar: simon
	  :girl_next_door: wanda

<div class="note">

有关 YAML 的详细信息，请访问 **http://yaml.org**
</div>

随 Ruby 提供的 YAML 库非常庞大且复杂，并且有许多方法可供你使用，而不仅是本章所述的。但是，你现在应该对 YAML 有了足够的了解，以便在你自己的程序中使用它。你可以在闲暇时慢慢地探索 YAML 库。

但事实证明，YAML 并不是在 Ruby 中序列化数据的唯一方法。我们将在下一章中讨论另一种方式。

# 第十五章

***

## Marshal

Ruby 的 Marshal 库提供了另一种保存和加载数据的方式。它有一组类似于 YAML 中的方法，使你可以将数据保存到磁盘上，并可以从磁盘上加载数据。

### 保存与加载数据

将该程序与前一章中的 **yaml_dump2.rb** 进行比较：

<div class="code-file clearfix"><span>marshal1.rb</span></div>

	f = File.open( 'friends.sav', 'w' )
	Marshal.dump( ["fred", "bert", "mary"], f )
	f.close

	File.open( 'morefriends.sav', 'w' ){ |friendsfile|
	  Marshal.dump( ["sally", "agnes", "john" ], friendsfile )
	}

	File.open( 'morefriends.sav' ){ |f|
	  $arr= Marshal.load(f)
	}

	myfriends = Marshal.load(File.open( 'friends.sav' ))
	morefriends = Marshal.load(File.open( 'morefriends.sav' ))

	p( myfriends )
	p( morefriends )
	p( $arr )

除了每次出现的 `YAML`（如 `YAML.dump` 和 `YAML.load`）都已被 Marshal 替换之外，这两个程序几乎完全相同。此外，Marshal 作为标准“内置”（built in）于 Ruby 中，因此你无需“引入”（require）任何额外的文件即可使用它。

但是，如果你查看生成的数据文件（例如 'friends.sav'），你会立即看到存在的重要差异。YAML 文件采用纯文本格式，而 Marshal 文件采用二进制格式。因此，虽然你可以阅读某些字符，例如字符串中的字符，但你不能简单地在文本编辑器中加载已保存的数据并对其进行修改。

与 YAML 一样，大多数数据结构都可以使用 Marshal 自动序列化，只需转储顶级（top-level）对象并在想要重建其下的所有对象时加载它。举个例子，看看我的小冒险游戏程序。在上一章中，我解释了如何通过转储和加载 Map 对象 `mymap`（参见 **gamesave_y.rb**）来保存和恢复包含了包含 Treasures 的 Rooms 的 Map 对象。使用 Marshal 替代 YAML 可以做同样的事情：

<div class="code-file clearfix"><span>gamesave_m.rb</span></div>

	File.open( 'game.sav', 'w' ){ |f|
	  Marshal.dump( mymap, f )
	}

	File.open( 'game.sav' ){ |f|
	  mymap = Marshal.load(f)
	}

在一些特殊情况下，对象不能如此容易地被序列化。Ruby 的 Marshal 模块（**marshal.c**）中的代码记录了这些异常：如果要转储的对象包括绑定（bindings），例程（procedure）或方法（method）对象，IO 类的实例或单例对象（singleton objects），则会抛出 TypeError。稍后在考虑如何通过编排（marshaling）来保存单例（singletons）对象时，我会看一个与之相关的示例。

### 保存时忽略变量

与 YAML 序列化一样，可以限制使用 Marshal 进行序列化时要保存的变量。在 YAML 中，我们通过编写一个名为 `to_yaml_properties` 的方法来完成此目的。而在使用 Marshal 时，我们需要编写一个名为 `marshal_dump` 的方法。在这个方法的代码中，你应该创建一个包含要保存的实际变量名的数组（在 YAML 中，我们创建了一个包含变量名的字符串数组）。这是一个示例：

<div class="code-file clearfix"><span>limit_m.rb</span></div>

	def marshal_dump
	  [@num, @arr]
	end

另一个不同之处在于，使用 YAML 我们只需加载数据即可重新创建对象。而使用 Marshal 时，我们需要添加一个名为 `marshal_load` 的特殊方法，任何已加载的数据都作为参数传递给该方法。当你调用 `Marshal.load` 时，它将被自动调用，它将以数组的形式传递加载的数据。可以从此数组中解析之前保存的对象。你还可以为任何在保存数据时被省略的变量（例如 `@str`）赋值：

	def marshal_load(data)
	  @num = data[0]
	  @arr = data[1]
	  @str = "default string"
	end

这是一个完整的程序，它保存并恢复了变量 `@num` 和 `@arr` 但省略了 `@str`：

	class Mclass
	  def initialize(aNum, aStr, anArray)
		@num = aNum
		@str = aStr
		@arr = anArray
	  end

	  def marshal_dump
		[@num, @arr]
	  end

	  def marshal_load(data)
		@num = data[0]
		@arr = data[1]
		@str = "default string"
	  end
	end

	ob = Mclass.new( 100, "fred", [1,2,3] )
	p( ob )

	marshal_data = Marshal.dump( ob )
	ob2 = Marshal.load( marshal_data )
	p( ob2 )

请注意，尽管序列化在内存中完成，但使用 Marshal 在磁盘上保存和加载对象时可以使用相同的技术。

### 保存单例对象

让我们看一下前面提到的问题的一个具体示例 - 即，无法使用编排（marshaling）来保存和加载单例对象（singleton）。在 **singleton_m.rb** 中，我创建了一个 Object 的实例 `ob`，然后以单例类的形式扩展它，附加了方法 `xxx`：

<div class="code-file clearfix"><span>singleton_m.rb</span></div>

	ob = Object.new

	class << ob
	  def xxx( aStr )
		@x = aStr
	  end
	end

当我尝试使用 `Marshal.dump` 将此数据保存到磁盘时会抛出该问题。Ruby 显示一条错误消息，指出：“单例对象不能被转储（类型错误，TypeError）”。

### YAML 与单例对象

在思考我们如何处理这个问题之前，让我们先简单地看看 YAML 将如何应对这种情况。程序 **singleton_y.rb** 尝试使用 `YAML.dump` 保存上面显示的单例对象，并且与 `Marshal.dump` 不同，它成功了 - 嗯，可以说是的...

<div class="code-file clearfix"><span>singleton_y.rb</span></div>

	ob.xxx( "hello world" )

	File.open( 'test.yml', 'w' ){ |f|
	  YAML.dump( ob, f )
	}

	ob.xxx( "new string" )

	File.open( 'test.yml' ){ |f|
	  ob = YAML.load(f)
	}

如果你看一下保存的 YAML 文件 **'test.yml'**，你会发现它定义了一个普通泛类型（vanilla）对象的实例，它附加了一个名为 `x` 的变量，它有一个字符串值 "hello world"。这一切都很好。除了通过加载保存的数据重建对象时，新的 `ob` 将是恰好包含一个额外的实例变量 `@x` 的 Object 的标准实例。然而，它不再是原来的单例对象，所以新的 `ob` 会无法访问该单例中定义的任何方法（此处为 `xxx` 方法）。因此，虽然 YAML 序列化更容易保存和加载在单例中创建的数据项，但在重新加载被保存的数据时，它不会自动重新创建单例本身。

现在让我们回到这个程序的 Marshal 版本。我需要做的第一件事是找到一种至少使它可以保存和加载数据项的方法。一旦我做完了，我将试着弄清楚如何在重新加载时重建单例对象。

为了保存特定的数据项，我可以定义 `marshal_dump` 和 `marshal_load` 方法，如前所述（参见 **limit_m.rb**）。这些通常应该在单例的派生类中定义 - 而不是单例本身。

这是因为，如已经说明的那样，当保存数据时，它将被存储为单例的派生类的表示。这意味着，虽然你确实可以将 `marshal_dump` 添加到从类 `X` 派生的单例中，但在重构对象时，你将加载泛型类型 `X` 的对象的数据，而不是特定单例实例的对象。

此代码创建类 `X` 的单例 `ob`，保存其数据，然后重新创建类 `X` 的通用对象：

<div class="code-file clearfix"><span>singleton_m2.rb</span></div>

	class X
	  def marshal_dump
		[@x]
	  end

	  def marshal_load(data)
		@x = data[0]
	  end
	end

	ob = X.new

	class << ob
	  def xxx( aStr )
		@x = aStr
	  end
	end

	ob.xxx( "hello" )

	File.open( 'test2.sav', 'w' ){ |f|
	  Marshal.dump( ob, f )
	}

	File.open( 'test2.sav' ){ |f|
	  ob = Marshal.load(f)
	}

就其包含的数据而言，保存的对象和重新加载的对象是相同的。但是，重新加载的对象对单例类没有任何了解，并且单例类包含的方法 `xxx` 不构成重构对象的一部分。然后，以下将失败：

	ob.xxx( "this fails" )

因此，该 Marshal 版本的代码等同于之前给出的 YAML 版本。它可以正确保存和恢复数据，但不会重建单例。

那么，如何从保存的数据中重建单例呢？毫无疑问，有许多聪明而巧妙的方式可以实现这一目标。但是，我会选择一种非常简单的方式：

<div class="code-file clearfix"><span>singleton_m3.rb</span></div>

	FILENAME = 'test2.sav'

	class X
	  def marshal_dump
		[@x]
	  end

	  def marshal_load(data)
		@x = data[0]
	  end
	end

	ob = X.new

	if File.exists?(FILENAME) then
	  File.open(FILENAME){ |f|
		ob = Marshal.load(f)
	  }
	else
	  puts( "Saved data can't be found" )
	end

	# singleton class
	class << ob
	  def xxx=( aStr )
		@x = aStr
	  end

	  def xxx
		return @x
	  end
	end

此代码首先检查是否可以找到包含已保存数据的文件（此示例有意保持简单 - 在实际的应用程序中，你当然需要编写一些异常处理代码来处理可能读取无效数据的问题）。如果找到该文件，则将数据加载到通用 `X` 类型的对象中：

	ob = X.new

	if File.exists?(FILENAME) then
	  File.open(FILENAME){ |f|
		ob = Marshal.load(f)
	  }

只有在完成此操作后，此对象才会“转换”为单例对象。完成此操作后，代码可以在重构单例上使用单例方法 `xxx`。然后，我们可以将新数据保存回磁盘并在稍后重新加载并重新创建修改后的单例：

	if ob.xxx == "hello" then
	  ob.xxx = "goodbye"
	else
	  ob.xxx = "hello"
	end

	File.open( FILENAME, 'w' ){ |f|
	  Marshal.dump( ob, f )
	}

如果你希望在实际的应用程序中保存和加载单例，单独的“重建”代码自然可以给出自己的方法：

<div class="code-file clearfix"><span>singleton_m4.rb</span></div>

	def makeIntoSingleton( someOb )
	  class << someOb
		def xxx=( aStr )
		  @x = aStr
		end

		def xxx
		  return @x
		end
	  end
	  return someOb
	end

## 深入探索

### Marshal 版本号

Marshal 库（一个名为 **'marshal.c'** 的 C 语言文件）的嵌入式文档说明如下：

> 编排（Marshaled）数据具有与对象信息一起存储的主要（major）和次要（minor）版本号。 在正常使用中，编排只能加载使用相同主版本号和相同或较低版本号编写的数据。

这显然提出了通过编排（marshaling）创建的数据文件格式可能与当前 Ruby 应用程序不兼容的潜在问题。另外地，Marshal 版本号不依赖于 Ruby 版本号，因此仅基于 Ruby 版本进行兼容性假设是不安全的。

这种不兼容的可能性意味着我们应该尝试在加载已保存数据之前检查其版本号。但是我们如何获得版本号呢？嵌入式文档再一次提供了线索。它指出：

> 你可以通过读取编排（marshaled ）数据的前两个字节来提取版本号。

它提供了这个示例：

	str = Marshal.dump("thing")
	RUBY_VERSION  #=> "1.8.0"
	str[0] 		  #=> 4
	str[1]        #=> 8

好的，让我们在一段完整的代码中尝试这一点。开始...

<div class="code-file clearfix"><span>version_m.rb</span></div>

	x = Marshal.dump( "hello world" )
	print( "Marshal version: #{x[0]}:#{x[1]}\n" )

打印出：

	"Marshal version: 4:8"

当然，如果你使用的是不同版本的 Marshal 库，则显示的数字会有所不同。在上面的代码中，`x` 是一个字符串，它的前两个字节是主要和次要版本号。Marshal 库还声明了两个常量 `MAJOR_VERSION` 和 `MINOR_VERSION`，它们存储了当前正在使用的 `Marshal` 库的版本号。因此，乍一看，似乎很容易将保存数据的版本号与当前版本号进行比较。

只有一个问题：当你将数据保存到磁盘上的文件中时，`dump` 方法接受 的是IO 或 File 对象，它返回 IO（或 File）对象而不是字符串：

<div class="code-file clearfix"><span>version_error.rb</span></div>

	f = File.open( 'friends.sav', 'w' )
	x = Marshal.dump( ["fred", "bert", "mary"], f )
	f.close  #=> x is now: #<File:friends.sav (closed)>

如果你现在尝试获取 `x[0]` 和 `x[1]` 的值，你将收到错误消息。从文件加载数据不再具有意义：

	File.open( 'friends.sav' ){ |f|
	  x = Marshal.load(f)
	}
	puts( x[0] )
	puts( x[1] )

这里的两个 `puts` 语句没有（如我希望）打印出编排（marshaled）数据的主要和次要版本号；事实上，它们打印出了名称，"fred" 和 "bert"，即从数据文件 'friends.sav' 加载到数组 `x` 中的前两项。

那么我们如何才能从保存的数据中获取版本号？我必须承认，我被迫在 **marshal.c** 中的 C 代码中获取可能的方式（不是我最喜欢的活动！）并检查保存的文件中的十六进制数据以便弄清楚这一点。事实证明，正如文档所述，你可以通过读取编排（marshaled）数据的前两个字节来提取版本号。但是，你不适合这么做。你必须明确地读取这些数据 - 像这样：

	f = File.open('test2.sav')
	vMajor = f.getc()
	vMinor = f.getc()
	f.close

这里，`getc` 方法从输入流读取下一个 8 位字节。我的示例项目 **version_m2.rb** 给出了一种简单的方法，可以将保存数据的版本号与当前 Marshal 库的版本号进行比较，以确定在尝试重新加载数据之前数据格式是否可能兼容。

<div class="code-file clearfix"><span>version_m2.rb</span></div>

	if vMajor == Marshal::MAJOR_VERSION then
	  puts( "Major version number is compatible" )
	  if vMinor == Marshal::MINOR_VERSION then
		puts( "Minor version number is compatible" )
	  elsif vMinor < Marshal::MINOR_VERSION then
		puts( "Minor version is lower - old file format" )
	  else
		puts( "Minor version is higher - newer file format" )
	  end
	else
	  puts( "Major version number is incompatible" )
	end

# 第十六章

***

## 正则表达式

正则表达式（Regular expressions）为你提供了在文本中进行查找和模式修改的强大方式 - 不仅是短文本（例如在命令提示符下输入的），还可以是庞大的储存文本（例如在磁盘上的文件中的）。

正则表达式采用了与字符串进行比较的模式形式。正则表达式还提供了修改字符串的方法，例如，你可以将特定字符转换为大写；或者你可以用 "Ruby" 替换每一次出现的 "Diamond"；或者读取一个代码文件，提取所有注释并写出包含所有注释但不包含任何代码的新文档文件。我们将很快了解如何编写注释提取工具。首先，让我们来看一些非常简单的正则表达式。

### 进行匹配

最简单的正则表达式（regular expression）几乎是一系列字符，例如 'abc'，你会感觉到它是一个字符串。只需简单的将这些字母放在两个正斜杠（forward-slash）分隔符之间 `/abc/`，就可以创建匹配 'abc' 的正则表达式。你可以使用 `=〜` 运算符方法来测试匹配（match），如下所示：

<div class="code-file clearfix"><span>regex0.rb</span></div>

	puts( /abc/ =~ 'abc' )  #=> returns 0

如果匹配，则返回表示字符串中字符位置的整数。如果不匹配，则返回 `nil`。

	puts( /abc/ =~ 'xyzabcxyzabc' ) #=> returns 3
	puts( /abc/ =~ 'xycab' ) 		#=> returns nil

你还可以在方括号之间指定一组字符，在这种情况下，将在字符串中匹配这组字符中的任何一个。例如，在这里，首先匹配到了 'c'，并返回该字符在字符串中的位置：

	puts( /[abc]/ =~ 'xycba' )  #=> returns 2

虽然我在上面的示例中使用了正斜杠分隔符，但还有其它方式可以定义正则表达式：你可以专门创建一个使用字符串初始化的新的 Regexp 对象，或者你可以在正则表达式之前使用 `％r` 并使用自定义分隔符 - 非字母数字字符 - 就像字符串一样（参见第 3 章）。在下面的示例中，我使用花括号分隔符：

<div class="code-file clearfix"><span>regex1.rb</span></div>

	regex1 = Regexp.new('^[a-z]*$')
	regex2 = /^[a-z]*$/
	regex3 = %r{^[a-z]*$}

上面的每一个，都定义了一个匹配全小写字符串的正则表达式（我将很快解释表达式的细节）。 这些表达式可用于测试这样的字符串：

	def test( aStr, aRegEx )
	  if aRegEx =~ aStr then
		puts( "All lower case" )
	  else
		puts( "Not all lower case" )
	  end
	end

	test( "hello", regex1 )  	#=> matches: "All lower case"
	test( "hello", regex2 ) 	#=> matches: "All lower case"
	test( "Hello", regex3 ) 	#=> no match: "Not all lower case"

要测试匹配（match），可以使用 `if` 和 `=〜` 运算符：

<div class="code-file clearfix"><span>if_test.rb</span></div>

	if /def/ =~ 'abcdef'

如果匹配，则上面的表达式求值为 true（并返回一个整数）；如果没有匹配则会计算为 false（并返回 `nil`）：

	RegEx = /def/
	Str1 = 'abcdef'
	Str2 = 'ghijkl'

	if RegEx =~ Str1 then
	  puts( 'true' )
	else
	  puts( 'false' )
	end #=> displays: true

	if RegEx =~ Str2 then
	  puts( 'true' )
	else
	  puts( 'false' )
	end #=> displays: false

通常，尝试从字符串的最开头匹配某个表达式是有用的；字符 `^` 后跟匹配项用于指定这个（前缀匹配）。从字符串的末尾进行匹配也可能很有用；字符 `$` 前置一个匹配项用于指定这个（后缀匹配）。

<div class="code-file clearfix"><span>start_end1.rb</span></div>

	puts( /^a/ =~ 'abc' )  #=> returns 0
	puts( /^b/ =~ 'abc' )  #=> returns nil
	puts( /c$/ =~ 'abc' )  #=> returns 2
	puts( /b$/ =~ 'abc' )  #=> returns nil

当字符串构成更复杂模式（pattern）的一部分时，从字符串的开头或结尾进行匹配会变得更有用。通常，这种模式会尝试匹配指定模式的零个或多个实例。`*` 字符用于表示其所遵循的模式的零个或多个匹配（matches）。形式上，这被称为“量词”（quantifier）。思考这个示例：

<div class="code-file clearfix"><span>start_end2.rb</span></div>

	puts( /^[a-z 0-9]*$/ =~ 'well hello 123' )

这里，正则表达式在方括号之间指定了字符范围。此范围包括所有小写字符，a-z，所有数字，0-9，加上空白字符（即此表达式中 "z" 和 "0" 之间的空格）。`^` 字符表示必须从字符串的开头进行匹配，范围之后的 `*` 表示必须与范围中的字符进行零次或多次匹配，而 `$` 字符表示必须匹配到字符串的末尾。换句话说，此模式（pattern）仅匹配从字符串的起始位置到结尾处包含小写字符，数字和空格的字符串：

	puts( /^[a-z 0-9]*$/ =~ 'well hello 123' ) # match at 0
	puts( /^[a-z 0-9]*$/ =~ 'Well hello 123' ) # no match due to ^ and uppercase 'W'

实际上，这个模式也会匹配一个空字符串，因为 `*` 表示可以接受*零个或多个*匹配：

	puts( /^[a-z 0-9]*$/ =~ '' ) # this matches!

如果要排除空字符串，请使用 `+`（以匹配模式的*一个或多个*匹配项）：

	puts( /^[a-z 0-9]+$/ =~ '' ) # no match

尝试使用 **start_end2.rb** 中的代码，了解更多示例，其中 `^`，`$`，`*` 和 `+` 可以与范围组合以创建各种不同的匹配模式（match-patterns）。

你可以使用这些技术来确定字符串的特定特征，例如给定字符串是大写的，小写的还是大小写混合的：

<div class="code-file clearfix"><span>regex2.rb</span></div>

	aStr = "HELLO WORLD"

	case aStr
	  when /^[a-z 0-9]*$/
		puts( "Lower case" )
	  when /^[A-Z 0-9]*$/
		puts( "Upper case" )
	  else
		puts( "Mixed case\n" )
	end

通常，正则表达式用于处理磁盘上文件中的文本。例如，假设你希望在 Ruby 文件中显示所有单行（full-line）注释，但省略所有代码或行内注释。你可以通过尝试从每行的开头匹配（`^`）零个或多个空格字符（空格字符由 `\s` 表示）直到注释字符（`'#'`）：

<div class="code-file clearfix"><span>regex3a.rb</span></div>

	# displays all the full-line comments in a Ruby file
	File.foreach( 'regex1.rb' ){ |line|
	  if line =~ /^\s*#/ then
		puts( line )
	  end
	}

### 匹配组

你还可以使用正则表达式来匹配一个或多个子字符串。为此，那你应该将正则表达式的一部分放在圆括号之间。这里我有两个组（有时称为'捕获'），第一个尝试匹配字符串 'hi'，第二个尝试匹配以 'h' 开头的字符串，后跟任意三个字符（一个点表示'匹配任何一个字符'所以这里的三个点将匹配任何三个连续的字符）并以 'o' 结尾：

<div class="code-file clearfix"><span>groups.rb</span></div>

	/(hi).*(h...o)/ =~ "The word 'hi' is short for 'hello'."

在对正则表达式中的组进行计算之后，将为这些组的匹配值分配等于组数的多个变量。这些变量采用 `$` 后跟数字的形式：`$1`，`$2`，`$3` 等等。执行上面的代码后，我可以像这样访问变量 `$1` 和 `$2`：

	print( $1, " ", $2, "\n" ) #=> displays: hi hello

请注意，如果整个正则表达式不匹配，则不会初始化任何组变量。例如，如果 'hi' 在字符串中但 'hello' 不在，则组变量都不会被初始化。两者都是 `nil`。

这是另一个示例，它返回三个组，每个组包含一个字符（由点给出）。然后显示组 `$1` 和 `$3`：

	/(.)(.)(.)/ =~ "abcdef"
	print( $1, " ", $3, "\n" ) #=> displays: a c

这是之前给出的注释匹配程序的新版本（**regex3a.rb**）；现在这已经采用了使用组 `(*.)` 的值，它返回正则表达式前缀匹配字符串后面的所有字符（零个或更多）（这里是：`^\s*#`）。这匹配从当前行（`^`）的开头到第一次出现的哈希或磅字符 `#` 的零个或多个空格（`\s*`）字符：

<div class="code-file clearfix"><span>regex3b.rb</span></div>

	File.foreach( 'regex1.rb' ){ |line|
	  if line =~ /^\s*#(.*)/ then
		puts( $1 )
	  end
	}

最终结果是只匹配第一个可打印字符 `#` 所在的行；并且 `$1` 打印出那些行文本减去 `#` 字符本身之后的文本。我们很快就会看到，这种简单的技术为从 Ruby 文件中提取文档提供了有用的工具。

你不仅仅限于逐字提取和显示字符；你也可以修改文本。此示例显示 Ruby 文件中的文本，但将行注释之前的所有 Ruby 行注释字符（`'#'`）更改为 C 样式的行注释字符（`'//'`）：

<div class="code-file clearfix"><span>regex4.rb</span></div>

	File.foreach( 'regex1.rb' ){ |line|
	  line = line.sub(/(^\s*)#(.*)/, '\1//\2')
	  puts( line )
	}

在此示例中，使用了 String 类的 `sub` 方法；它接受一个正则表达式作为它的第一个参数（这里是 `/(*\s*)#(.*)/`）和一个替换字符串作为第二个参数（这里是 `'\1//\2'`）。替换字符串可能包含编号的占位符，例如 `\1` 和 `\2`，以匹配正则表达式中的任何组 - 此处圆括号之间有两组：`(^\s*)` 和 `(.*)`。`sub` 方法返回一个新字符串，其中正则表达式所匹配的字符串被替换为替换字符串，而任何未匹配的元素（此处为 `#` 字符，都会被省略）。

### MatchData

`=~` '运算符'不是找到匹配的唯一方式。Regexp 类也有 `match` 方法。这与 `=~` 类似，但是，当匹配时，它返回 MatchData 对象而不是整数。MatchData 对象包含模式匹配的结果。乍一看，这似乎是一个字符串...

<div class="code-file clearfix"><span>match.rb</span></div>

	puts( /cde/ =~ 'abcdefg' )     #=> 2
	puts( /cde/.match('abcdefg') ) #=> cde

实际上，它是 MatchData 类的一个实例，它包含一个字符串：

	p( /cde/.match('abcdefg') )  #=> #<MatchData:0x28cedc8>

MatchData 对象可以包含组或“捕获”（captures），这些可以使用 `to_a` 或 `capture` 方法在数组中返回，如下所示：

<div class="code-file clearfix"><span>matchdata.rb</span></div>

	x = /(^.*)(#)(.*)/.match( 'def myMethod # This is a very nice method' )
	x.captures.each{ |item| puts( item ) }

以上显示：

	def myMethod
	#
	This is a very nice method

请注意，`captures` 和 `to_a` 方法之间存在细微差别。第一个只返回捕获值：

	x.captures #=>["def myMethod ","#"," This is a very nice method"]

第二个返回原始字符串（在索引 0 处），然后是捕获值：

	x.to_a  #=>["def myMethod # This is a very nice method","def myMethod ","#"," This is a very nice method"]

### 前后匹配

MatchData 类提供 `pre_match` 和 `post_match` 方法以返回匹配之前或之后的字符串。例如，我们用注释字符 `"#"` 进行匹配：

<div class="code-file clearfix"><span>pre_post_match.rb</span></div>

	x = /#/.match( 'def myMethod # This is a very nice method' )
	puts( x.pre_match )   #=> def myMethod
	puts( x.post_match )  #=> This is a very nice method

或者，你可以使用特定变量 <code>$`</code>（带反引号）和 <code>$'</code>（带正常引号）分别访问前后匹配值：

	x = /#/.match( 'def myMethod # This is a very nice method' )
	puts( $` ) #=> def myMethod
	puts( $' ) #=> This is a very nice method

使用组匹配时，可以使用数组形式的索引来获取特定项。索引 0 是原始字符串；更大的索引是组的匹配值：

<div class="code-file clearfix"><span>match_groups.rb</span></div>

	puts( /(.)(.)(.)/.match("abc")[2] ) #=> "b"

特殊变量 `$~` 可用于访问最后一个 MatchData 对象，你可以再次使用数组形式索引来引用组的匹配值：

	puts( $~[0], $~[1], $~[3] )

但是，为了使用 Array 类的所有方法，必须使用 `to_a` 或 `capture` 方法来将匹配组作为数组返回：

	puts( $~.sort ) 			# this doesn't work!
	puts( $~.captures.sort ) 	# this does

### 贪婪匹配

当一个字符串包含多个潜在的匹配值时，你有时可能希望该字符串返回第一个匹配项（即，尽可能少的字符串与匹配模式一致），并且在其它时候你可能希望该字符串返回一直到最后一个匹配项（也就是尽可能多的字符串）。

在后一种情况下（获得尽可能多的字符串），这中匹配被称为“贪婪的”（greedy）。`*` 和 `+` 模式量词是贪婪的。你可以通过在其后放置 `?` 让它们节制一点，以使它们尽可能少地返回匹配值：

<div class="code-file clearfix"><span>greedy1.rb</span></div>

	puts( /.*at/.match('The cat sat on the mat!') ) 	#=> returns: The cat sat on the mat
	puts( /.*?at/.match('The cat sat on the mat!') )	#=> returns: The cat

你可以控制模式匹配的贪婪性，以执行诸如处理目录路径之类的操作：

<div class="code-file clearfix"><span>greedy2.rb</span></div>

	puts( /.+\\/.match('C:\mydirectory\myfolder\myfile.txt') )		#=> C:\mydirectory\myfolder\
	puts( /.+?\\/.match('C:\mydirectory\myfolder\myfile.txt') )		#=> C:\

### 字符串方法

到目前为止，我们在处理字符串时使用了 Regexp 类的方法。事实上，由于 String 类有一些自己的正则表达式方法，因此模式匹配可以双向进行。这些包括 `=~` 和 `match`（所以你可以在匹配时切换 String 和 Regexp 对象的顺序），以及遍历字符串的 `scan` 方法，该方法寻找尽可能多的匹配。每个匹配都添加到一个数组中。例如，我正在寻找字母 'a'，'b' 或 'c' 的匹配。`match` 方法返回的 MatchData 对象中包含第一个匹配项（'a'）；但 `scan` 方法会继续扫描字符串并在数组中返回它找到的所有匹配项：

<div class="code-file clearfix"><span>match_scan.rb</span></div>

	TESTSTR = "abc is not cba"
	puts( "\n--match--" )
	b = /[abc]/.match( TESTSTR )	#=> MatchData: "a"
	puts( "--scan--" )
	a = TESTSTR.scan(/[abc]/)		#=> Array: ["a", "b", "c", "c", "b", "a"]

可选地，可以给 `scan` 方法传递一个块，以便可以以某种方式处理后扫描创建的数组元素：

	a = TESTSTR.scan(/[abc]/){|c| print( c.upcase ) }	#=> ABCCBA

许多其它 String 方法可以与正则表达式一起使用。`String.slice` 方法的一个版本接受一个正则表达式作为参数并返回任何匹配到的子字符串。`String.slice!` 方法（注意最后的 `!`）从接收字符串中删除匹配的子字符串并返回子字符串：

<div class="code-file clearfix"><span>string_slice.rb</span></div>

	s = "def myMethod # a comment "

	puts( s.slice( /m.*d/ ) ) 	#=> myMethod
	puts( s )  					#=> def myMethod # a comment
	puts( s.slice!( /m.*d/ ) ) 	#=> myMethod
	puts( s )  					#=> def # a comment

`split` 方法基于模式（pattern）将字符串拆分为子字符串。结果（减去模式）作为数组返回；空模式将字符串拆分为字符：

<div class="code-file clearfix"><span>string_ops.rb</span></div>

	s = "def myMethod # a comment"

	p( s.split( /m.*d/ ) )	# => ["def ", " # a comment"]
	p( s.split( /\s/ ) )	#=> ["def", "myMethod", "#", "a", "comment"]
	p( s.split( // ) )		# => ["d", "e", "f", " ", "m", "y", "M", "e", "t", "h", "o", "d", " ", "#", " ", "a", " ", "c", "o", "m", "m", "e", "n", "t"]

你可以使用 `sub` 方法匹配正则表达式，并将其第一个匹配项替换为字符串。如果未匹配到，则返回不变的字符串：

	s = "def myMethod # a comment"
	s2 = "The cat sat on the mat"
	p( s.sub( /m.*d/, "yourFunction" ) ) 	#=> "def yourFunction # a comment"
	p( s2.sub( /at/, "aterpillar" ) )  		#=> "The caterpillar sat on the mat"

`sub!` 方法与 `sub` 类似，但会修改原始（接收）字符串。或者，你可以使用 `gsub` 方法（或 `gsub!` 来修改接收字符串）用字符串替换所有出现的模式匹配项：

	p( s2.gsub( /at/, "aterpillar" ) )	#=> "The caterpillar saterpillar on the materpillar"

### 文件操作

我之前说过，正则表达式通常用于处理存储在磁盘上文件中的数据。在一些之前的示例中，我们从磁盘文件中读取数据，进行一些模式匹配并在屏幕上显示结果。这是另一个我们计算文件中单词的示例。通过扫描每一行来创建一个单词数组（即字母数字字符序列）然后将每个数组的大小添加到变量，`count`：

<div class="code-file clearfix"><span>wordcount.rb</span></div>

	count = 0
	File.foreach( 'regex1.rb' ){ |line|
	  count += line.scan( /[a-z0-9A-Z]+/ ).size
	}
	puts( "There are #{count} words in this file." )

我在示例程序中包含了一些可替换代码（被注释掉），它显示每个单词及其编号：

	File.foreach( 'regex1.rb' ){ |line|
	  line.scan( /[a-z0-9A-Z]+/ ).each{ |word|
		count +=1
		print( "[#{count}] #{word}\n" )
	  }
	}

现在让我们看看如何同时处理两个文件 - 一个用于读取，另一个用于写入。第一个示例打开文件 **testfile1.txt** 进行写入，并将文件变量 `f` 传递到块中。我现在打开第二个文件 **regex1.rb** 进行读取，并使用 `File.foreach` 将从该文件读取的每行文本传递到第二个块中。我使用一个简单的正则表达式来创建一个新的字符串，以匹配具有 Ruby 风格注释的行；当该字符是一行中的第一个非空白字符时，代码将 Ruby 注释字符（`'#'`）替换为 C 风格的注释字符（`'//'`）；并将每行写入 **testfile1.txt**，代码行未经修改（因为没有匹配到）并且注释行更改为 C 风格：

<div class="code-file clearfix"><span>regexp_file1.rb</span></div>

	File.open( 'testfile1.txt', 'w' ){ |f|
	  File.foreach( 'regex1.rb' ){ |line|
		f.puts( line.sub(/(^\s*)#(.*)/, '\1//\2') )
	  }
	}

这说明了使用正则表达式和非常少的编码可以完成多少工作。

下一个示例显示了如何读取一个文件（此处为文件 **regex1.rb**）并写出两个新文件 - 其中一个（**comments.txt**）仅包含行注释，而另一个（**nocomments.txt**）包含文件中所有其它行：

<div class="code-file clearfix"><span>regexp_file2.rb</span></div>

	file_out1 = File.open( 'comments.txt', 'w' )
	file_out2 = File.open( 'nocomments.txt', 'w' )

	File.foreach( 'regex1.rb' ){ |line|
	  if line =~ /^\s*#/ then
		file_out1.puts( line )
	  else
	    file_out2.puts( line )
	  end
	}

	file_out1.close
	file_out2.close

## 深入探索

### 正则表达式

这是可以在正则表达式中使用的一些元素的列表...

| 元素 | 解释说明 |
| ------ | ------ |
| ^ | 一行或一个字符串的开头 |
| $ | 一行或一个字符串的结尾 |
| . | 除换行符之外的任何字符 |
| * | 0 个或多个前一个正则表达式 |
| *? | 0 个或多个前一个正则表达式（非贪婪） |
| + | 1 个或多个前一个正则表达式 |
| +? | 1 个或多个前一个正则表达式（非贪婪） |
| [] | 范围规范（例如 [a-z] 表示 "a" 到 "z" 范围内的字符） |
| \w | 一个字母数字字符 |
| \W | 一个非字母数字字符 |
| \s | 一个空白字符 |
| \S | 一个非空白字符 |
| \d | 一个数字 |
| \D | 一个非数字字符 |
| \b | 退格（在范围规范中时） |
| \b | 单词边界（不在范围规范中时） |
| \B | 非单词边界 |
| * | 前面的零个或多个重复 |
| + | 前面的 1 个或多个重复 |
| {m,n} | 前面的至少 m 次且至多 n 次重复 |
| ? | 前面的至多 1 次重复 |
| \| | 前一个或下一个表达式可以匹配 |
| () | 一个匹配组 |

以下是一些示例正则表达式...

<div class="code-file clearfix"><span>overview.rb</span></div>

	# match chars...
	puts( 'abcdefgh'.match( /cdefg/ ) ) # literal chars
		#=> cdefg
	puts( 'abcdefgh'.match( /cd..g/ ) ) # dot matches any char
		#=> cdefg

	# list of chars in square brackets...
	puts( 'cat'.match( /[fc]at/ )
		#=> cat
	puts( "batman's father's cat".match( /[fc]at/ ) )
		#=> fat
	puts( 'bat'.match( /[fc]at/ ) )
		#=> nil

	# match char in a range...
	puts( 'ABC100x3Z'.match( /[A-Z][0-9][A-Z0-9]/ ) )
		#=> C10
	puts( 'ABC100x3Z'.match( /[a-z][0-9][A-Z0-9]/ ) )
		#=> x3Z

	# escape 'special' chars with \
	puts( 'ask who?/what?'.match( /who\?\/w..t\?/ ) )
		#=> who?/what?
	puts( 'ABC 100x3Z'.match( /\s\S\d\d\D/ ) )
		#=> " 100x" (note the leading space)

	# scan for all occurrences of pattern 'abc' with at least 2 and
	# no more than 3 occurrences of the letter 'c'
	p( 'abcabccabcccabccccabccccccabcccccccc'.scan( /abc{2,3}/ ) )
		#=> ["abcc", "abccc", "abccc", "abccc", "abccc"]

	# match either of two patterns
	puts( 'my cat and my dog'.match( /cat|dog/ ) ) 		#=> cat
	puts( 'my hamster and my dog'.match( /cat|dog/ ) ) 	#=> dog

# 第十七章

***

## 线程（Threads）

有时你的程序可能需要一次执行多个操作。例如，你可能想要执行一些磁盘操作（IO）并同时向用户显示一些反馈。或者你可能希望在后台复制或上传某些文件时，同时仍允许用户继续执行“前台”中的其它任务。

在 Ruby 中，如果你希望一次执行多个任务，则可以在自己的“线程”（thread）中运行每个任务。线程就像程序中的程序。它独立于任何其它线程运行一些特定的代码。

但是，正如我们稍后将看到的，多个线程可能需要找到相互协作的方法，例如，它们可以共享相同的数据，并且不会占用自己可用的所有处理时间，从而阻止其它线程运行。

### 创建线程

可以使用 `new` 方法像任何其它对象一样创建线程。执行此操作时，必须将包含你希望线程运行的代码块传递给 Thread。

接下来是我首先尝试创建两个线程，其中一个应该打印四个字符串，而另一个打印十个数字：

<div class="code-file clearfix"><span>threads1.rb</span></div>

	# This is a simple threading example which, however, doesn't work as anticipated!

	words = ["hello", "world", "goodbye", "mars" ]
	numbers = [1,2,3,4,5,6,7,8,9,10]

	Thread.new{
	  words.each{ |word| puts( word ) }
	}

	Thread.new{
	  numbers.each{ |number| puts( number ) }
	}

很可能，当你运行它时，你可能看不到任何东西，或者，无论如何都会很少。我已经添加了一个关于程序执行时间的报告，这表明这个事情在它有时间开始之前就已经完成了！

### 运行线程

这是对线程运行（thread-running）问题的简单修复。在代码的末尾，添加以下内容：

<div class="code-file clearfix"><span>threads2.rb</span></div>

	sleep( 5 )

现在，当你再次运行代码时，你应该会看到所有字符串和所有数字，尽管有点混乱。事实上，这正是我们想要的，因为它表明时间现在正在两个线程之间划分；这就是为什么单词和数字交错出现 - 首先第一个线程执行并显示一个单词，然后下一个线程执行并显示一个数字，然后执行返回到第一个线程，依此类推，直到第一个线程结束（当所有四个单词都显示后），此时第二个线程可以不间断地运行。

现在将其与该程序的第一个版本进行比较。在那个程序中我创建了两个线程，但是就像 Ruby 刚刚准备好运行它们包含的代码一样，*迷惑（bam）*！它到达了程序的末尾并关闭（中断）了所有事情 - 包括我的两个线程。所以，实际上，线程在它们有时间做任何事情之前都被杀掉了。

通过添加一个 `sleep(5)` 我给了 Ruby 五秒钟的延迟 - 在程序退出之前有足够的时间运行这些线程。这种技术只有一个问题 - 也是一个很大的问题。为让线程运行而向程序添加不必要的延迟会偏离练习的目的。这里的计时器显示的表明程序运行了整整五秒钟 - 比绝对必要的时间长约 4.99 秒！我们将很快看到更加优雅的处理线程的方法。

<div class="note">
	<p class="h4"><b>本地化？</b></p>

目前（在 Ruby 1.8x 中）Ruby 的线程不是“原生的”（native）。简而言之，这意味着 Ruby 线程存在于 Ruby 程序的封闭世界中 - 多个线程在单个进程中分配时间（使用称为“时间切片”（time-slicing）的例程）。Ruby 没有利用操作系统处理的“本地线程”（native threads）的优势，以允许在一个或多个处理器上更有效地执行（使用“抢占式多任务处理”，pre-emptive multitasking）。虽然 Ruby 线程牺牲了效率，但它们至少可以从可移植性中获益；写在一个操作系统上的线程也将可以在不同的操作系统上运行。后续版本的 Ruby（在编写 Ruby 1.9 时，可能被视为主导 Ruby 2.0 的“实验性”版本）将支持本地线程。
</div>

### 主线程

即使你没有显式创建任何线程，也始终至少执行一个线程 - 运行 Ruby 程序的主（main）线程。你可以输入以下内容来验证这一点：

	p( Thread.main )

这将显示如下：

	#<Thread:0x28955c8 run>

在这里，Thread 是它的线程类，`0x28955c8`（或其它一些数字）是它的十六进制对象标识符，`run` 是线程的当前状态。

### 线程状态

每个线程的状态可以是以下之一：

| 状态 | 解释说明 |
| ------ | ------ |
| run | 当线程正在执行时 |
| sleep | 当线程正在休眠或等待 I/O 时 |
| aborting | 当线程正在中止 |
| false | 当线程正常终止时 |
| nil | 当线程以异常终止时 |

你可以使用 `status` 方法获取线程的状态。查看线程时也会显示状态，在这种情况下，`nil` 或 `false` 状态显示为 `'dead'`。

<div class="code-file clearfix"><span>thread_status.rb</span></div>

	puts( Thread.main.inspect )  				#=> #<Thread:0x28955c8 run>
	puts( Thread.new{ sleep }.kill.inspect ) 	#=> #<Thread:0x28cddc0 dead>
	puts( Thread.new{ sleep }.inspect ) 		#=> #<Thread:0x28cdd48 sleep>
	thread1 = Thread.new{ }
	puts( thread1.status )  					#=> false
	thread2 = Thread.new{ raise( "Exception raised!" ) }
	puts( thread2 ) 							 #=> nil

### 确保线程执行

让我们回到我们之前的程序中遇到的问题。回想一下，我们创建了两个线程，但程序在其中任何一个线程运行之前就已完成。我们通过使用 `sleep` 方法插入固定长度的延迟来解决这个问题。但是有意在你的程序中引入没有额外作用的延迟并不是你想要做的通用规则。幸运的是，Ruby 有一种更加优雅的方式来确保线程有时间执行。`join` 方法强制调用线程（例如主线程）挂起自己的执行（因此它不只是终止程序），直到调用 `join` 的线程完成：

<div class="code-file clearfix"><span>join.rb</span></div>

	words = ["hello", "world", "goodbye", "mars" ]
	numbers = [1,2,3,4,5,6,7,8,9,10]

	Thread.new{
	  words.each{ |word| puts( word ) }
	}.join

	Thread.new{
	  numbers.each{ |number| puts( number ) }
	}.join

乍一看，这似乎是进步了，因为两个线程都需要执行它们所需的时间，我们不必引入任何不必要的延迟。但是，当你查看输出时，你将看到线程按顺序运行 - 第二个线程在第一个线程完成后开始运行。但我们真正想做的是让两个线程同时运行，Ruby 从一个线程切换到另一个线程，为每个线程提供一小部分可用的处理时间。

下一个程序 **threads3.rb** 显示了实现这一目标的一种方式。它像之前一样创建两个线程；但是，这次它将每个线程分配给一个变量：`wordsThread` 和 `numbersThread`：

<div class="code-file clearfix"><span>threads3.rb</span></div>

	wordsThread = Thread.new{
	  words.each{ |word| puts( word ) }
	}

	numbersThread = Thread.new{
	  numbers.each{ |number| puts( number ) }
	}

现在它将这些线程放入一个数组中并调用 `each` 方法将它们传递到一个块中，块变量 `t` 接收它们，它只是在每个线程上调用 `join` 方法：

	[wordsThread, numbersThread].each{ |t| t.join }

正如你将从输出中看到的那样，两个线程现在“并行”（in parallel）运行，因此它们的输出混乱，但没有人为延迟，总执行时间可以忽略不计。

### 线程优先级

到目前为止，我们已经让 Ruby 完全自由地以任何方式切换线程之间的时间。但有时候一个线程比其它线程更重要。例如，如果你正在编写一个文件复制程序，其中一个线程用于执行实际复制，而另一个线程用于显示进度条，那么给文件复制线程更多时间是有意义的。

<div class="note">

有时候当前正在执行的线程特别想要将执行时间给予其它线程。原则上，这是通过调用 `Thread.pass` 方法完成的。然而，在实践中，这可能无法产生你期望的结果。`pass` 方法将在本章末尾的**“深入探索”**部分中详细讨论。
</div>

Ruby 允许你分配整数值以指示每个线程的优先级（priority）。理论上，具有较高优先级的线程比具有较低优先级的线程分配更多的执行时间。在实践中，事情并不那么简单，因为其它因素（例如运行线程的顺序）可能会影响给予每个线程的时间量。而且，在非常短的程序中，可能无法确定改变优先级的效果。我们到目前为止使用的单词和数字线程示例太短，无法显示任何差异。因此，让我们来看一个稍微工作密集的程序 - 一个运行三个线程的程序，每个线程调用一个方法五十次，以便计算 50 的阶乘。

<div class="code-file clearfix"><span>threads4.rb</span></div>

	def fac(n)
	  n == 1 ? 1 : n * fac(n-1)
	end

	t1 = Thread.new{
	  0.upto(50) {fac(50); print( "t1\n" )}
	}

	t2 = Thread.new{
	  0.upto(50) {fac(50); print( "t2\n" )}
	}

	t3 = Thread.new{
	  0.upto(50) {fac(50); print( "t3\n" )}
	}

我们现在可以为每个线程设置特定的优先级：

	t1.priority = 0
	t2.priority = 0
	t3.priority = 0

在这种情况下，每个线程的优先级是相同的，因此没有线程将被赋予操作的最大时间切片，并且所有三个线程的结果将像通常一样混乱的出现。现在尝试更改 `t3` 的优先级：

	t3.priority = 1

这次运行代码时，`t3` 将占用大部分时间并且（很大可能）在其它线程之前执行。其它线程可能会在一开始就得到关注，因为它们是以相同的优先级创建的，并且优先级仅在它们开始运行后才会更改。当 `t3` 结束时，`t1` 和 `t2` 应该或多或少平等地分享时间。

因此，假设你希望 `t1` 和 `t2` 首先运行，或多或少地共享时间，并且仅在这两个线程完成后运行 `t3`。这是我的第一次尝试；你可能想自己尝试一下：

	t1.priority = 2
	t2.priority = 2
	t3.priority = 1

嗯，最终结果不是我想要的！似乎线程是按顺序运行的，根本没有时间切片！好的，只是为了它，让我们尝试一些负数：

	t1.priority = -1
	t2.priority = -1
	t3.priority = -2

欢呼！这还差不多。这次，`t1` 和 `t2` 同时运行（在设置线程优先级之前，你可能还会看到 `t3` 短暂运行）；然后 `t3` 运行。那么为什么负值会起作用但正值却不会呢？

负值本身没有什么特别之处。但是，你需要记住，每个进程至少有一个运行的线程 - 主线程 - 这也有优先级。它的优先级恰好为 0。

### 主线程优先级

你可以很容易验证主线程（main thread）的优先级：

<div class="code-file clearfix"><span>threads4.rb</span></div>

	puts( Thread.main.priority )  	#=> 0

因此，在上一个程序（**threads4.rb**）中，如果将 `t1` 的优先级设置为 2，它将“超出”主线程本身优先级，然后将获得所需的所有执行时间，直到下一个线程 `t2` 到来等等。通过将优先级设置为低于主线程的优先级，你可以强制三个线程仅与它们自己竞争，因为主线程总是会超过它们。如果你更喜欢使用正数，则可以将主线程的优先级设置为高于所有其它线程的值：

	Thread.main.priority=100

<div class="code-file clearfix"><span>threads5.rb</span></div>

那么，现在，如果我们希望 `t2` 和 `t3` 具有相同的优先级，并且 `t1` 具有较低的优先级，我们需要为这三个线程和主线程设置优先级：

	Thread.main.priority = 200
	t1.priority = 0
	t2.priority = 1
	t3.priority = 1

如果仔细观察输出，可能会发现一个微小但不良的副作用。有可能（不确定，但可能）你会在开始时发现 `t1` 线程的一些输出，就在 `t2` 和 `t3` 开始并确定它们的优先级之前。这与我们前面提到的问题相同：每个线程一旦创建就尝试开始运行，并且 `t1` 可能会在其他线程的优先级被“提升”之前获得自己的运行时间切片。为了防止这种情况，我们可以使用 `Thread.stop` 在创建时专门挂起线程，如下所示：

<div class="code-file clearfix"><span>stop_run.rb</span></div>

	t1 = Thread.new{
	  Thread.stop
	  0.upto(50){print( "t1\n" )}
	}

现在，当我们想要启动线程运行时（在这种情况下，在设置线程优先级之后），我们调用它的 `run` 方法：

	t1.run

### 互斥

有时两个或多个线程可能都需要访问某种全局资源。由于全局资源的当前状态可能被一个线程修改，并且该修改的值在被某个其它线程使用时可能是不可预测的，因此这可能产生错误的结果。举一个简单的例子，看看这段代码：

<div class="code-file clearfix"><span>no_mutex.rb</span></div>

	$i = 0

	a = Thread.new {
	  1000000.times{ $i += 1 }
	}

	b = Thread.new {
	  1000000.times{ $i += 1 }
	}

	a.join
	b.join
	puts( $i )

我的目的是运行两个线程，每个线程递增全局变量 `$i` 一百万次。在这结束时 `$i` 的预期结果（自然）将是 200 万。但是，事实上，当我运行它时，`$i` 的结束值是 1088237（你可能会看到不同的结果）。

对此的解释是，这两个线程实际上正在竞争对全局变量 `$i` 的访问。这意味着，在某些时候，线程 `a` 可能获得 `$i` 的当前值（假设它恰好是 100）并且同时线程 `b` 也获得 `$i` 的当前值（仍然是 100）。现在，`a` 增加它刚刚获得的值（`$i` 变为 101）并且 `b` 增加它刚刚获得的值（因此 `$i` 再次变为 101！）。换句话说，当多个线程同时访问共享资源时，其中一些线程可能正在使用过时的值 - 即，没有考虑其它线程对该资源所做的任何修改。随着时间的推移，这些操作产生的错误会累积，直到我们得出的结果与我们预期的结果大不相同。

为了解决这个问题，我们需要确保当一个线程可以访问全局资源时，它会阻止其它线程的访问。另一种说法，即授予多个线程对全局资源的访问应该是“互斥的”（mutually exclusive）。你可以使用 Ruby 的 Mutex 类来实现它，该类使用信号量来指示当前资源是否正在被访问，并提供同步方法以防止（外部）访问块内的资源。请注意，你必须引入（require）*'thread'* 才能使用 Mutex 类。这是我重写的代码：

<div class="code-file clearfix"><span>mutex.rb</span></div>

	require 'thread'
	$i = 0

	semaphore = Mutex.new

	a = Thread.new {
	  semaphore.synchronize{
		1000000.times{ $i += 1 }
	  }
	}

	b = Thread.new {
	  semaphore.synchronize{
		1000000.times{ $i += 1 }
	  }
	}

	a.join
	b.join
	puts( $i )

这次，`$i` 的最终结果是 2000000。

最后，有关使用 Threads 的更有用的示例，请查看 **file_find2.rb**。此示例程序使用 Ruby 的 Find 类遍历磁盘上的目录。有关非线程示例，请参阅 **file_find.rb**。将其与第 13 章中的 **file_info3.rb** 程序进行比较，其使用的 Dir 类。

这会设置两个线程运行。第一个，`t1`，调用 `processFiles` 方法来查找和显示文件信息（你需要编辑对 `processFiles` 的调用以将系统上的目录名传递给它）。第二个线程 `t2` 只打印出一条消息，当 `t1` 处于“活着”状态（即运行或休眠）时，该线程运行：

<div class="code-file clearfix"><span>file_find2.rb</span></div>

	t1 = Thread.new{
	  Thread.stop
	  processFiles( '..' ) # edit this directory name
	}

	t2 = Thread.new{
	  Thread.stop
	  while t1.alive? do
		print( "\n\t\tProcessing..." )
		Thread.pass
	  end
	}

每个线程使用 `Thread.pass` 完成让步控制（`t1` 线程在 `processFiles` 方法内进行让步控制）。在实际应用程序中，你可以采用此技术以提供某种类型的用户反馈，同时进行一些密集的过程（例如目录遍历）。

## 深入探索

### 传递执行权给其它线程

在某些情况下，你可能特别希望某个线程（thread）能够让步执行权（execution）给任何其它线程以让其运行。例如，如果你有多个线程正在进行稳定的更新图形操作或显示各种“正在发生的”统计信息，你可能需要确保一旦一个线程绘制了 X 个像素或显示了 Y 个统计数据，另一个线程保证有机会做一些其它事情。

从理论上讲，`Thread.pass` 方法可以解决这个问题。根据 Ruby 的源代码文档，`Thread.pass` 调用线程调度程序将执行权传递给另一个线程。这是 Ruby 文档提供的示例：

<div class="code-file clearfix"><span>pass0.rb</span></div>

	a = Thread.new { print "a"; Thread.pass;
					 print "b"; Thread.pass;
					 print "c" }

	b = Thread.new { print "x"; Thread.pass;
					 print "y"; Thread.pass;
					 print "z" }

	a.join
	b.join

根据文档，此代码在运行时会生成以下输出：

	axbycz

是的，确实如此。理论上，这似乎表明，通过在每次调用 `print` 之后调用 `Thread.pass`，这些线程将执行权传递给另一个线程，这就是两个线程的输出交替的原因。

出于我心中的疑问，我想知道 `Thread.pass` 的调用被删除后会产生什么影响？第一个线程是否会一直占用，只有在结束后才让步于第二个线程？找出答案的最佳方法是尝试：

<div class="code-file clearfix"><span>pass1.rb</span></div>

	a = Thread.new { print "a";
					 print "b";
					 print "c" }

	b = Thread.new { print "x";
					 print "y";
					 print "z" }

	a.join
	b.join

如果我的理论是正确的（该线程将一直占用，直到它完成），这将是预期的输出：

	abcdef

事实上，（令我惊讶的是！），实际产生的输出是：

	axbycz

换句话说，无论是否调用 `Thread.pass`，结果都是相同的。那么，`Thread.pass` 做什么呢？其宣称 `pass`方法，调用线程调度程序将执行权传递给另一个线程，该文档是错误的吗？

在一个短暂而愤怒的时刻，我承认我轻率的认为有一种可能性，文档是不正确的，并且 `Thread.pass` 根本没有做任何事情。深入研究 Ruby 的 C 语言源代码很快消除了我的疑虑；`Thread.pass` 确实做了一些事情，但它的行为并不像 Ruby 文档暗示的那样可预测。在解释原因之前，让我们尝试一下我自己的示例：

<div class="code-file clearfix"><span>pass2.rb</span></div>

	s = 'start '
	a = Thread.new { (1..10).each{
	    s << 'a'
	    Thread.pass
	  }
	}

	b = Thread.new { (1..10).each{
		s << 'b'
		Thread.pass
	  }
	}

	a.join
	b.join
	puts( "#{s} end" )

乍一看，这可能看起来与前面的示例非常相似。它设置两个线程运行，但不是反复打印东西出来，而是重复地将一个字符附加到字符串中 - 'a' 由线程 `a` 添加，'b' 由线程 `b` 添加。每次操作后，`Thread.pass` 将执行权传递给另一个线程。最后显示整个字符串。字符串包含 'a' 和 'b' 的交替序列应该不足为奇：

	abababababababababab

现在，请记住，在上一个程序中，即使我删除了对 `Thread.pass` 的调用，我也获得了完全相同的交替输出。基于这种经历，如果我在这个程序中删除 `Thread.pass`，我想我应该期望得到类似的结果。我们来试试吧：

<div class="code-file clearfix"><span>pass3.rb</span></div>

	s = 'start '
	a = Thread.new { (1..10).each{
	    s << 'a'
	  }
	}

	b = Thread.new { (1..10).each{
		s << 'b'
	  }
	}

	a.join
	b.join
	puts( "#{s} end" )

这次，输出如下：

	aaaaaaaaaabbbbbbbbbb

换句话说，这个程序显示了我最初在第一个程序中预料的那种不同的行为（我从 Ruby 的嵌入式文档中复制出来的那个）- 也就是说当两个线程在它们自己的时间片下运行时，第一个线程，`a`，抢占所有时间为它自己所用，只有当它完成时第二个线程 `b` 才会得到关注。但是通过显式添加对 `Thread.pass` 的调用，我们可以强制每个线程将执行权传递给任何其它线程。

那么我们如何解释这种行为上的差异呢？从本质上讲，**pass0.rb** 和 **pass3.rb** 正在做同样的事情 - 运行两个线程并显示每个线程的字符串。唯一真正的区别在于，在 **pass3.rb** 中，字符串在线程内连接而不是打印。这可能看起来不是什么大不了的事，但事实证明，打印字符串比连接字符串需要更多的时间。实际上，`print` 调用会引入时间延迟。正如我们之前发现的那样（当我们有意使用 `sleep` 引入延迟时），时间延迟对线程产生了深远的影响。

如果你仍然不相信，请尝试我重写的 **pass0.rb** 版本，我创造性地命名为 **pass0_new.rb**。这只是用连接替换了打印。现在，如果你对 `Thread.pass` 的调用进行注释和取消注释，你确实会看到不同的结果。

<div class="code-file clearfix"><span>pass0_new.rb</span></div>

	s = ""
	a = Thread.new { s << "a"; Thread.pass;
	  s << "b"; Thread.pass;
	  s << "c" }

	b = Thread.new { s << "x"; Thread.pass;
	  s << "y"; Thread.pass;
	  s << "z" }

	a.join
	b.join
	puts( s )

顺便说一句，我的测试是在运行 Windows 的 PC 上进行的。很可能在其它操作系统上会看到不同的结果。这是因为控制分配给线程的时间量的 Ruby 调度程序的实现在 Windows 和其它操作系统上是不同的。在 Unix 上，调度程序每 10 毫秒运行一次，但在 Windows 上，通过在某些操作发生时递减计数器来控制时间共享，因此精确的间隔是不确定的。

作为最后一个示例，你可能需要查看 **pass4.rb** 程序。这会创建两个线程并立即挂起它们（`Thread.stop`）。在每个线程的主体中，线程的信息（包括其 `object_id`）被附加到数组 `arr`，然后调用 `Thread.pass`。最后，运行并连接两个线程，并显示数组 `arr`。尝试通过取消注释 `Thread.pass` 来验证其效果（密切注意其 `object_id` 标识符指示的线程的执行顺序）：

<div class="code-file clearfix"><span>pass4.rb</span></div>

	arr = []
	t1 = Thread.new{
	  Thread.stop
	  (1..10).each{
		arr << Thread.current.to_s
		Thread.pass
	  }
	}
	t2 = Thread.new{
	  Thread.stop
	  (1..10).each{ |i|
		arr << Thread.current.to_s
		Thread.pass
	  }
	}
	puts( "Starting threads..." )
	t1.run
	t2.run
	t1.join
	t2.join
	puts( arr )

# 第十八章

***

## 调试与测试

任何实际应用程序的开发都是逐步进行的。我们大多数人宁愿采取更多的步骤向前推进而不是向后返工。为了最大限度地减少由编码错误或不可预见的副作用引起的后向返工的步骤，我们可以利用测试（testing）和调试（debugging）技术。本章旨在简要概述 Ruby 程序员可用的一些最有用的调试工具。但请记住，如果你使用专用的 Ruby IDE（如 Ruby in Steel），你将拥有更强大的可视化调试工具。我将在本章仅讨论 Ruby 可用的“标准”（standard）工具。我不会讨论 IDE 提供的工具。

### IRB - 交互式 Ruby

有时候你只想用 Ruby “试试某些东西”。标准的 Ruby 解释器 **Ruby.exe** 远非用于此目的理想工具。虽然可以从命令提示符运行 Ruby，并一次输入一行代码，但只有在输入文件结束符（Windows 上为 <kbd>CTRL+Z</kbd>，其它操作系统时为 <kbd>CTRL+D</kbd>）时才会执行代码。要获得与 Ruby 交互的更好方法，请使用 Interactive Ruby shell，**IRB.exe**。要开始使用这个，请转到命令提示符并输入：

	irb

你现在应该看到类似于以下内容的提示：

	irb(main):001:0>

现在开始输入一些 Ruby 代码。你可以在多行上输入表达式；表达式完成后，**irb** 将对其进行计算并显示结果。尝试以下（在 `+` 后按 <kbd>Enter</kbd> 键）：

	x = ( 10 +
	( 2 * 4 ) )

在闭合括号后按 <kbd>Enter</kbd> 键时，**irb** 将计算表达式并显示结果：

	=> 18

你现在可以计算 `x`。输入：

	x

**irb** 显示：

	=> 18

但要小心。尝试输入：

	x = (10
	+ (2*4))

这次的结果是：

	=> 8

事实上，这是正常的 Ruby 行为。这可以通过以下事实来解释：换行符作为终止符，而 `+` 运算符在开始新行时充当一元运算符（它只是声明后面的表达式为正）。你将在本章末尾的*深入探索*中找到更全面的解释。现在，请注意，当一次输入一行时，换行符的位置很重要！使用 **irb** 时，你可以判断解释器是否认为你已结束输入语句。如果你这样做，则显示以 `">"` 结尾的普通提示：

	irb(main):013:1>

如果语句不完整，则提示以星号结束：

	irb(main):013:1*

在上面的两个示例中，当你输入没有加号的第一行时会显示 `>` 提示符：

	x = ( 10

但是当你使用加号输入时会显示 `*` 提示符：

	x = ( 10 +

前一种情况表明，**irb** 认为该语句是完整的；后一种情况表明它正在等待语句被完成。

如果你希望，可以将 Ruby 程序加载到 **irb** 中，方法是将程序名称传递给它：

	irb myprogram.rb

你也可以使用各种选项调用它，如下一页所示：

	Usage: irb.rb [options] [programfile] [arguments]

| 选项 | 解释说明 |
| ------ | ------ |
| -f | 禁止阅读 ~/.irbrc |
| -m | Bc 模式（加载可用的 mathn，fraction 或 matrix） |
| -d | 将 $DEBUG 设置为 true（与 `ruby -d' 相同） |
| -r load-module | 与 `ruby -r' 相同 |
| -I path | 指定 $LOAD_PATH 目录 |
| --inspect | 使用 `inspect' 作为输出（默认除了 bc 模式） |
| --noinspect | 不使用 inspect 输出 |
| --readline | 使用 Readline 扩展模块 |
| --noreadline | 不使用 Readline 扩展模块 |
| --prompt | 提示模式，prompt-mode |
| --prompt-mode | prompt-mode 切换提示模式（prompt mode）。预定义的提示模式是“默认”，“简单”，“xmp” 和 “inf-ruby” 的 |
| --inf-ruby-mode | 在 emacs 上使用适合 inf-ruby-mode 的提示符。 禁止 --readline |
| --simple-prompt | 简单的提示模式 |
| --noprompt | 没有提示模式 |
| --tracer | 显示每次执行命令的轨迹 |
| --back-trace-limit | 显示回溯顶部 n 个和尾部 n 个。默认值价值是 16。 |
| --irb_debug n | 将内部调试级别设置为 n（不适合常用） |
| -v, --version | 打印 irb 的版本 |

你可以通过在命令行输入以下内容来查看这些选项的列表：

	irb --help

你可以通过在提示符处输入单词 `quit` 或按 <kbd>CTRL+BREAK</kbd> 结束 **irb** 会话。

虽然 **irb** 可能对尝试某些代码很有用，但它并不提供调试程序所需的所有功能。但是，Ruby 提供了一个命令行调试器。

### 调试

默认的 Ruby 调试器允许你在程序执行时设置断点和监视点并计算变量。要在调试器中运行程序，请在启动 Ruby 解释器时使用 `-r debug` 选项（其中 `-r` 表示 'require'，`debug` 是调试库的名称）。例如，这是调试一个名为 **debug_test.rb** 的程序的方法：

	ruby –r debug debug_test.rb

<div class="note">
	<p class="h4"><b>Ubygems？什么是 Ubygems ...？</b></p>

在某些情况下，如果运行上述命令，你可能会看到类似于以下令人费解的消息：

	c:/ruby/lib/ruby/site_ruby/1.8/ubygems.rb:4:require 'rubygems'

当你开始调试时，你会发现自己试图调试文件 **'ubygems.rb'** 而不是你的程序！这似乎是一个困扰使用一键安装程序安装 Ruby 的 Windows 用户的主要问题：(http://rubyforge.org/projects/rubyinstaller/)

此安装程序设置环境变量 `RUBYOPT=-rubygems`。在大多数情况下，这具有允许 Ruby 程序使用 ruby gems “打包系统”来安装 Ruby 库的理想效果。但是，当你尝试使用 `-r` 选项时，会将其解释为 `-r ubygems`，这就是加载文件 **ubygems.rb** 的原因。Ruby 顺便（可能令人困惑？）提供了一个名为 **ubygems.rb** 的文件，它除了引入（requiring）**rubygems.rb** 之外什么都不做！有两种方法可以解决这个问题。你可以永久删除 `RUBYOPT`，也可以暂时禁用它。但是，如果你选择永久删除它，则以后使用 ruby gems 时可能会遇到副作用。要永久删除它，请加载“开始”菜单，（如果使用 XP 则为“设置”）“控制面板”；（如果使用 Vista，则为“系统和维护”）；单击系统（在 Vista 上，你现在应该单击“高级系统设置”）；在“系统属性”对话框中，选择“高级”选项卡；单击环境变量；在“系统变量”面板中，找到 `RUBYOPT` 并删除它。更安全的替代方法是在加载调试器之前在命令提示符处禁用该变量。为此，请输入：

	set RUBYOPT=

这将仅为此命令会话禁用 `RUBYOPT` 环境变量。你可以输入以下命令验证这一点：

	set RUBYOPT

你应该会看到以下消息：

	Environment variable RUBYOPT not defined

但是，打开另一个命令窗口并输入 `set RUBYOPT`，你将看到此处的环境变量保留其默认值。
</div>

一旦调试器启动后，你可以输入各种命令来逐步执行代码，设置断点以使执行暂停在特定行，设置监视以监视变量值等等。在下一页是可用的调试命令列表：

| 命令 | 解释说明 |
| ------ | ------ |
| b[reak] [file&#124;class:]&lt;line&#124;method&gt; | 在某个位置设置断点 |
| b[reak] [class.]&lt;line&#124;method&gt; | 在某个位置设置断点 |
| wat[ch] &lt;expression&gt; | 为某个表达方式设置监视点 |
| cat[ch] &lt;an Exception&gt; | 为异常设置捕获点 |
| b[reak] | 列出断点 |
| cat[ch] | 显示捕获点 |
| del[ete][ nnn] | 删除部分或全部断点 |
| disp[lay] &lt;expression&gt; | 将表达式添加到显示表达式列表 |
| undisp[lay][ nnn] | 删除一个特定或所有显示表达式 |
| c[ont] | 运行到结束或遇到断点 |
| s[tep][ nnn] | 前进（代码）1 行或 nnn 行 |
| n[ext][ nnn] | 跨越一行或直到 nnn 行 |
| w[here] | 显示帧 |
| f[rame] | where 别名 |
| l[ist][ (-&#124;nn-mm)] | 程序列表，- 向后列出给定行 nn-mm 的列表 |
| up[ nn] | 移到更大的帧 |
| down[ nn] | 移到更小的框帧 |
| fin[ish] | 回到外部帧 |
| tr[ace] (on&#124;off) | 设置当前线程为跟踪模式 |
| tr[ace] (on&#124;off) all | 设置所有线程为跟踪模式 |
| q[uit] | 退出调试器 |
| v[ar] g[lobal] | 显示全局变量 |
| v[ar] l[ocal] | 显示局部变量 |
| v[ar] i[nstance] &lt;object&gt; | 显示对象的实例变量 |
| v[ar] c[onst] &lt;object&gt; | 显示对象的常量 |
| m[ethod] i[nstance] &lt;obj&gt; | 显示对象的方法 |
| m[ethod] &lt;class&#124;module&gt; | 显示类或模块的实例方法 |
| th[read] l[ist] | 列出所有线程 |
| th[read] c[ur[rent]] | 列出当前线程 |
| th[read] [sw[itch]] &lt;nnn&gt; | 将线程上下文切换为 nnn |
| th[read] stop &lt;nnn&gt; | 停止线程 nnn |
| th[read] resume &lt;nnn&gt; | 恢复线程 nnn |
| p expression | 计算表达式并打印其值 |
| h[elp] | 打印帮助信息 |
| &lt;everything else&gt; | 执行计算 |

让我们看看如何在真正的调试会话中使用其中一些命令。打开系统提示符并导航到包含文件 **debug_test.rb** 的目录。输入以下命令启动调试器：

<div class="code-file clearfix"><span>debug_test.rb</span></div>

	ruby –r debug debug_test.rb

现在，让我们尝试一些命令。 在这些示例中，我写了 *[Enter]* 以显示你应该在每个命令后按 <kbd>Enter</kbd> 键。首先让我们看一下代码列表：

	l [Enter]

这显示了该程序的前几行。`l`（小写 "L"）或 `list` 命令列会出小块代码。实际行数将随调试代码而变化。列出更多：

	l [Enter]
	l [Enter]

或列出特定行数（此处字母 'l' 后跟数字 1，连字符和 100）：

	l 1-100 [Enter]

我们在第 78 行放一个断点（breakpoint）：

	b 78 [Enter]

Ruby 调试器应该回复：

	Set breakpoint 1 at debug_test.rb:78

我们也可能设置一个或多个监视点（watchpoints）。监视点可用于触发简单变量的中断（例如，当创建 `@t2` 对象时，输入 `wat @t2` 会中断）；或者它可以设置为匹配特定值（例如 `i == 10`）。在这里，我想设置一个在 `@t4` 的 `name` 属性为 "wombat" 时中断的监视点：

	wat @t4.name == "wombat" [Enter]

调试器应该确认这一点：

	Set watchpoint 2:@t4.name == "wombat"

请注意观察点编号为 2.如果你随后决定删除监视点，则需要该编号。好的，现在让我们继续执行：

	c [Enter]

程序将一直运行，直到它到达断点。你将看到类似于以下内容的消息：

	Breakpoint 1, toplevel at debug_test.rb:78
	debug_test.rb:78: puts( "Game start" )

这里显示了它停在的行号和该行的代码。让我们继续：

	c [Enter]

这次它在这里中断了：

	Watchpoint 2, toplevel at debug_test.rb:85
	debug_test.rb:85: @t5 = Treasure.new("ant", 2)

这是在成功计算监视点条件之后的行。通过列出指示的行号来查看：

	l 85

调试器高亮显示了一组行，在当前执行（86）：

	[80, 89] in debug_test.rb
	  80 # i) Treasures
	  81 @t1 = Treasure.new("A sword", 800)
	  82 @t4 = Treasure.new( "potto", 500 )
	  83 @t2 = Treasure.new("A dragon Horde", 550)
	  84 @t3 = Treasure.new("An Elvish Ring", 3000)
	  85 @t4 = Treasure.new("wombat", 10000)
	=> 86 @t5 = Treasure.new("ant", 2)
	  87 @t6 = Treasure.new("sproggit", 400)
	  88
	  89 # ii) Rooms

如你所见，第 85 行包含与监视点条件匹配的代码。请注意，在最初创建 `@t4` 的第 82 行之后，执行没有停止，因为那里没有满足监视点条件（它的 `name` 属性是 "potto"，而不是 "wombat"）。如果要在断点或监视点处暂停时查看变量的值，只需输入其名称即可。试试这个：

	@t4 [Enter]

调试器将显示：

	#<Treasure:0x315617c @value=10000, @name="wombat">

你可以同样输入要执行的其它表达式：

	@t1.value [Enter]
	10+4/2 [Enter]

现在删除监视点（回想一下它的编号是 2）：

	del 2 [Enter]

并继续，直到程序退出：

	c [Enter]

还有更多的命令可用于以这种方式调试程序，你可能想要尝试上表中显示的那些。你还可以通过输入 `help` 或 `h` 在调试会话期间查看命令列表：

	h [Enter]

要退出调试会话，请输入 `quit` 或 `q`：

	q [Enter]

虽然标准的 Ruby 调试器有其用途，但它不如使用集成开发环境提供的图形调试器简单或方便。而且，它很慢。在我看来，调试简单脚本很好，但不建议用于调试大型和复杂的程序。

### 单元测试

单元测试是一种后调试（post-debugging）测试技术，它允许你试运行程序的各个部分，以验证它们是否按预期工作。基本思想是你可以编写一些“断言”（assertions），说明某些行为应该获得某些结果。例如，你可能断言特定方法的返回值应为 100，或者它应该是布尔值（Boolean），或者它应该是特定类的实例。当测试运行时，如果断言被证明是正确的，即它通过了测试；如果不正确，则测试失败。

这是一个示例，如果对象 `t` 的 `getVal` 方法返回 100 以外的任何值，则会失败：

	assert_equal(100, t.getVal)

但是你不能只用这种断言来编写你的代码。测试有精确的规则。首先，你必须引入（require）**test/unit** 文件。然后，你需要从 TestCase 类派生一个测试类，该类位于 Unit 模块中，该模块本身则位于 Test 模块中：

	class MyTest < Test::Unit::TestCase

在这个类中，你可以编写一个或多个方法，每个方法构成一个包含一个或多个断言的测试。方法名称必须以 **test** 开头（因此名为 `test1` 或 `testMyProgram` 的方法都可以，但是名为 `myTestMethod` 的方法不行）。这是一个测试，包含 `TestClass.new(100).getVal` 的返回值为 1000 的单个断言：

	def test2
	  assert_equal(1000,TestClass.new(100).getVal)
	end

这里有一个完整的（虽然很简单）测试套件，我在其中定义了一个名为 MyTest 的 TestCase 类，它测试类 TestClass。在这里（有点想象力！），TestClass 可以用来代表我想要测试的整个程序：

<div class="code-file clearfix"><span>test1.rb</span></div>

	require 'test/unit'

	class TestClass
	  def initialize( aVal )
		@val = aVal * 10
	  end

	  def getVal
		return @val
	  end
	end

	class MyTest < Test::Unit::TestCase
	  def test1
		t = TestClass.new(10)
		assert_equal(100, t.getVal)
		assert_equal(101, t.getVal)
		assert(100 != t.getVal)
	  end

	  def test2
		assert_equal(1000,TestClass.new(100).getVal)
	  end
	end

此测试套件包含两个测试：`test1`（包含三个断言）和 `test2`（包含一个）。为了运行测试，你只需要运行该程序；你不必创建 MyClass 的实例。

你将看到结果报告，其中指出有两个测试，三个断言和一个失败。事实上，我做了四个断言。但是，在给定的测试中不会执行计算失败后的断言。在 `test1` 中，此断言失败：

	assert_equal(101, t.getVal)

失败后，下一个断言被跳过。如果我现在纠正这个（断言 100 而不是 101，那么下一个断言也将被测试：

	assert(100 != t.getVal)

这也失败了。这次报告指出已经执行计算了四个断言，其中一个失败。当然，在现实生活中，你应该设法写出正确的断言，当报告任何失败时，它应该是重写失败代码 - 而不是断言！

有关稍微复杂的测试示例，请参阅 **test2.rb** 程序（需要一个名为 buggy.rb 的文件）。这是一款小型冒险游戏，包括以下测试方法：

<div class="code-file clearfix"><span>test2.rb</span></div>

	def test1
	  @game.treasures.each{ |t|
	    assert(t.value < 2000, "FAIL: #{t} t.value = #{t.value}" )
	  }
	end

	def test2
	  assert_kind_of( TestMod::Adventure::Map, @game.map)
	  assert_kind_of( Array, @game.map)
	end

这里第一个方法对传递给块的对象数组执行断言测试，当 value 属性不小于 2000 时，它会失败。第二个方法使用 `assert_kind_of` 方法测试两个对象的类类型。当发现 `@game.map` 属于 `TestMod::Adventure::Map` 而不是被断言的 `Array` 时，此方法中的第二个测试会失败。

该代码还包含另外两个名为 `setup` 和 `teardown` 的方法。定义时，将在每个测试方法之前和之后运行具有这些名称的方法。换句话说，在 **test2.rb** 中，以下方法将按以下顺序运行：`setup`，`test1`，`teardown`，`setup`，`test2`，`teardown`。这使你有机会在运行每个测试之前将任何变量重新初始化为特定值，或者在这种情况下，重新创建对象以确保它们处于已知状态：

	def setup
	  @game = TestMod::Adventure.new
	end

	def teardown
	  @game.endgame
	end

## 深入探索

### 单元测试时可用的断言

	assert(boolean, message=nil)

断言 boolean 不是 false 或 nil。

	assert_block(message="assert_block failed.") {|| ...}

所有其它断言所依据的断言。如果块产生 true 则通过。

	assert_equal(expected, actual, message=nil)

如果 expected == +actual 为 true，则通过。

	assert_in_delta(expected_float, actual_float, delta, message="")

如果 expected_float 和 actual_float 在增量公差内相等，则通过。

	assert_instance_of(klass, object, message="")

如果 `object .instance_of? klass` 为 true，则通过。

	assert_kind_of(klass, object, message="")

如果 `object .kind_of? klass` 为 true，则通过。

	assert_match(pattern, string, message="")

如果 string =~ pattern，则通过。

	assert_nil(object, message="")

如果 object 为 nil，则通过。

	assert_no_match(regexp, string, message="")

如果 regexp !~ string，则通过。

	assert_not_equal(expected, actual, message="")

如果 expected != actual，则通过。

	assert_not_nil(object, message="")

如果 !object .nil?，则通过。

	assert_not_same(expected, actual, message="")

如果 !actual .equal? expected，则通过。

	assert_nothing_raised(*args) {|| ...}

如果块没有抛出异常，则通过。

	assert_nothing_thrown(message="", &proc)

如果块不抛出任何东西，则通过。

	assert_operator(object1, operator, object2, message="")

用 operator 比较 object1 与 object2。如果 object1.send(operator, object2) 为 true，则通过。

	assert_raise(*args) {|| ...}

如果块抛出给定的异常之一，则通过。

	assert_raises(*args, &block)

`assert_raise` 的别名。（在 Ruby 1.9 版本中弃用，在 2.0 版本中移除）。

	assert_respond_to(object, method, message="")

如果 object.respond_to? method 为 true，则通过。

	assert_same(expected, actual, message="")

如果 actual.equal? expected 为 true，则通过。（例如它们是同一个实例）。

	assert_send(send_array, message="")

如果方法 send 返回 true 值，则通过。

	assert_throws(expected_symbol, message="", &proc)

如果块抛出 expected_symbol，则通过。

	build_message(head, template=nil, *arguments)

构建失败消息。在模板之前添加 head，并且用参数将模板中的 '?' 位置替换。

	flunk(message="Flunked")

`flunk` 总是失败。

### 换行很重要

我之前说过，在交互式 Ruby 控制台（IRB）中输入换行符时需要注意，因为换行符的位置可能会改变 Ruby 代码的含义。例如，这个：

<div class="code-file clearfix"><span>linebreaks.rb</span></div>

	x = ( 10 +
	( 2 * 4 ) )

...将 `18` 分配给 `x`，但是这个：

	x = (10
	+ (2*4))

...将 `8` 指定给了 `x`。

这不是 IRB 的一个问题。这是 Ruby 代码的正常行为，即使在文本编辑器中并由 Ruby 解释器执行时也是如此。上面显示的第二个例子计算 `10`，发现它是一个完全可以接受的值，并迅速遗忘它；然后它计算 `+ (2*4)`，它也发现它是一个可接受的值（`8`），但它与前一个值（`10`）没有连接，因此返回 `8` 并分配给 `x`。

如果你想告诉 Ruby 来计算分割成多行的表达式并将其“联系在一起”（tie lines together），忽略换行符，你可以使用行连续符 `\`。这就是我在这里所做的：

	x = (10 \
	+ (2*4) )

这次，`x` 被赋值为 `18`。

### 图形化调试器

对于正式的调试，我强烈建议使用图形化调试器（graphical debugger）。例如，Ruby In Steel IDE 中的调试器允许你通过单击编辑器的边沿来设置断点和监视点。它允许你在单独的停靠窗口中监视所选“监视变量”（watch variables）或所有局部变量的值。它保留当前执行点所有方法调用的“调用栈”（callstack），并允许你通过调用栈“向后”导航以查看变量的变化值。它还具有完整的“向下钻取”变量扩展，允许您扩展数组和散列并查看“内部”复杂对象。它还可以完整的“向下探查”（drill-down）以展开变量，允许你扩展数组和散列并查看“内部”复杂对象。这些功能远远超出了标准 Ruby 调试器的功能。

<div class="text-center">
	<img src="./images/chapter18_debugger.png" />
	<p class="small"><i>The Ruby In Steel debugger</i></p>
</div>

# 第十九章

***

## Ruby On Rails

Rails 已与 Ruby 紧密相连，以至于现在人们用 Ruby on Rails 谈论编程已经司空见惯，就好像 Ruby on Rails 是编程语言的名称一样。

实际上，Rails 是一个框架 - 一组工具和代码库 - 可以与 Ruby 一起使用。它有助于创建基于 Web 前端的数据库驱动的应用程序。Rails 使你能够开发响应用户交互的网站：例如，用户能够在一个页面上输入和保存数据，并在其它页面上搜索和显示已有数据。这使得 Rails 适合创建动态网站，可以“动态”（on the fly）生成网页，而不是加载静态的，预先设计的页面。典型应用包括：协作网站，如在线社区，多作者书籍或维基，购物网站，论坛和博客。

我们将尽快通过动手指南来创建博客。首先，让我们仔细看看 Rails 框架的细节。

<div class="note">

本章旨在让你体验 Ruby On Rails 的开发。但请记住，Rails 是一个庞大而复杂的框架。我们将只涵盖最基本的要点！我们在本章中使用的是 Rails 的第 2 版。这些示例不适用于 Rails 1，并且不保证可以与任何未来的 Rails 版本一起使用。
</div>

### 首先安装 Rails

#### A）自己安装...

Rails 不是 Ruby 标准的一部分，因此你需要单独操作来进行安装。你可以通过多种方式这么做。最简单的方法是使用 all-one 安装程序（下面介绍了一些替代方法）。但是，你也可以一次安装 Rails 及其所需的工具。可以使用 Ruby Gem '包管理器'（package manager）安装 Rails。只要你连接到互联网，就会在线查找并安装最新版本的 Rails。在命令提示符下，输入：

	gem install rails

或者，你可以从 Ruby On Rails 网站 **http://www.rubyonrails.org** 下载并安装 Rails。大多数 Rails 应用程序也需要数据库。你需要单独操作来安装数据库。免费的 MySQL 数据库服务器广泛用于此目的。你可以在附录中找到有关 MySQL 的基本的安装帮助。

#### B）或者使用'一体化'安装器...

Mac 用户可以使用 Locomotive 安装程序设置带有附加实用程序的 Rails 环境（**http://sourceforge.net/projects/locomotive**）。Windows 用户可以通过使用 InstantRails 安装程序或 Ruby In Steel 一体化（All-in-One）安装程序来节省一些精力。InstantRails 安装包括 Ruby，Rails，Apache Web 服务器和 MySQL。所有内容都放在一个单独的目录结构中，因此原则上应该能够与同一台 PC 上的任何其它 Ruby 安装共存，而不会造成任何无法预料的副作用。事实上，如果你已经单独安装了 Apache，我发现 InstantRails 有时会出现问题。不然的话，它提供了一种简单的方法来启动和运行 Rails。

从以下位置下载 InstantRails：

> **http://instantrails.rubyforge.org/wiki/wiki.pl**

Ruby In Steel 一体化（All-in-One）安装程序安装包括 Ruby，Gems，Rails，WEBrick，MySQL plus（可选）免费版 Microsoft Visual Studio。你还可以安装 Ruby In Steel IDE 的免费或商业版（或试用版）。本章中的示例均使用 Ruby In Steel 的“一体化安装程序”（all in one installer）默认配置进行了测试。

从以下位置下载 Ruby In Steel 一体化（All-in-One）安装程序：

> **http://www.sapphiresteel.com/spip?page=download**

### MVC - 模型，视图，控制器

Rails 应用程序分为三个主要部分 - 模型（Model），视图（View）和控制器（Controller）。简而言之，模型（Model）是数据部分 - 数据库和对该数据进行的任何编程操作（例如计算）；视图（View）是最终用户看到的 - 在 Rails 术语中表示浏览器中出现的网页；控制器（Controller）是将模型连接到视图的编程粘合剂。

模型-视图-控制器方法由各种编程语言和框架以各种形式使用。在*“深入探索”*部分中对此进行了更全面的描述。为了简洁起见，我今后将其称为 MVC。

### 第一个 Ruby On Rails 应用

不需要更多的麻烦，让我们开始用 Rails 编程。我假设你已经安装了 Rails 以及 Web 服务器。我碰巧使用 WEBrick 服务器，但你可以使用其它服务器，如 LightTPD 或 Mongrel。有关 Web 服务器的更多信息，请参阅附录。

本章假设你仅使用“原始”（raw）的 Rails 开发工具，至少使用文本编辑器和 Web 浏览器；因此，你会发现你经常需要启动单独的程序并在系统提示符下输入命令。集成开发环境应该可以让你更轻松地完成这些任务。

<div class="note">

*注意：*与本书中的普通Ruby代码示例不同，我没有提供了本章所述的 Ruby On Rails 应用程序的所有代码。这有三个原因：

1）每个 Rails 应用程序包含大量的文件和文件夹。
2）我还必须为每个数据库提供数据，你必须在使用它之前导入它。
3）不仅是为了让你自己创建自己的 Rails 应用程序，这样做也可以帮助你了解 Rails 的工作原理。但是，我提供了一些示例文件 - 一个完整应用程序的组件部分 - 在你运行过程中遇到问题时你可以使用它来比较自己的代码。
</div>

### 创建一个 Rails 应用

为简单起见，第一个应用程序根本不使用数据库（database）。这将让我们探索视图（View ）和控制器（Controller），而不必担心模型（Model）的复杂性。

首先，打开系统提示符（在 Windows 上，选择“开始”菜单，然后在*“运行”*或*“搜索”*框中输入 **cmd**）。导航到你打算将 Rails 应用程序放入的目录。我们假设这是 **C\railsapps**。检查是否已安装 Rails 并在系统环境变量（path）中。

	rails

一切都很好，现在你应该会在屏幕上看到关于使用 `rails` 命令的帮助信息。如果没有，则在继续之前需要修复 Rails 安装问题。

假设 Rails 可以正常工作，现在就可以创建一个应用程序。输入：

	rails helloworld

在硬盘发出一点呜呜声后，你应该会看到 Rails 刚刚创建的文件列表：

	create app/controllers
	create app/helpers
	create app/models
	create app/views/layouts
	create config/environments
	...(etcetera)

使用计算机的文件管理器（例如 Windows 资源管理器）查看这些文件。在运行 Rails 命令的目录下面（**\helloworld**），你将看到已创建了几个新目录：**\app**，**\config**，**\db** 等。其中一些有子目录。例如，**\app** 目录包含 **\controllers**，**\helpers**，**\models** 和 **\views**。 **\views** 目录本身包含一个子目录，**\layouts** 等。

Rails 应用程序中的目录结构并非随机生成；目录（或“文件夹”）及其包含的文件名称定义了应用程序各个部分之间的关系。这背后的想法是，通过采用定义明确的文件和文件夹结构，你可以避免编写大量配置文件以将应用程序的各个部分链接在一起。本章末尾的*深入探索*中提供了 rails 的默认目录结构的简化指南。

现在，在系统提示符下，将目录更改为新生成的 Rails 应用程序的顶级文件夹（**\helloworld**）。假设你仍然在 **C:\railsapps** 目录中，并且你按照之前的建议命名了 Rails 应用程序 **helloworld**，那么你将（在 Windows 上）输入此命令以更改为那个目录：

	cd helloworld

现在运行服务器。 如果（像我一样）你使用 WEBrick，你应该输入：

	ruby script/server

请注意，其它服务器可能以不同的方式启动，如果上述方法不起作用，则需要查阅服务器的文档。在上面的示例中，**script** 是在运行 `rails` 命令时创建的目录，而 **server** 是运行 WEBrick 服务器的代码文件的名称。你现在应该看到类似于以下的内容：

	=> Booting WEBrick...
	=> Rails application started on http://0.0.0.0:3000
	=> Ctrl-C to shutdown server; call with --help for options
	[2006-11-20 13:46:01] INFO WEBrick 1.3.1
	[2006-11-20 13:46:01] INFO ruby 1.8.4 (2005-12-24) [i386-mswin32]
	[2006-11-20 13:46:01] INFO WEBrick::HTTPServer#start: pid=4568 port=3000

<div class="note">
	<p class="h4"><b>问题...？</b></p>

如果你看到错误消息而不是上面的输出，请检查你是否完全从相应目录（**\helloworld**）输入了服务器命令：

	ruby script/server

如果仍有问题，则可能默认端口（3000）被占用了 - 例如，如果你已在同一台 PC 上安装了 Apache 服务器。在这种情况下，请尝试其它一些值，例如 3003，在运行脚本时将此数字放在 `-p` 之后：

	ruby script/server –p3003
</div>

现在启动一个 Web 浏览器。输入主机名，然后在其地址栏中输入冒号和端口号。主机名应该（通常）是 *localhost*，端口号应该与启动服务器时使用的端口号相匹配，否则默认为 3000。这是一个示例：

	http://localhost:3000/

浏览器现在应该显示一个欢迎您登录 Rails 的页面。如果没有，请验证你的服务器是否在 URL 中指定的端口上运行...

<div class="text-center">
	<img src="images/chapter19_rails.png" />
</div>

### 创建控制器

如前所述，控制器（Controller）是你的大部分 Ruby 代码所在的位置。它是应用程序中视图（View，浏览器中显示的内容）和模型（Model，数据发生了什么改变）之间的一部分。这是一个 "Hello world" 应用程序，让我们创建一个控制器来说 "hello"。本着原创的精神，我将其称为 **SayHello** 控制器。再一次，你可以通过在系统提示符下运行脚本来创建它。你需要在先前运行服务器脚本的目录中打开另一个命令窗口（例如，**C\railsapps\helloworld**）。你无法重新使用现有的命令窗口，因为该命令窗口正在运行服务器，你需要将其关闭以返回到提示符 - 这将阻止我们的 Rails 应用程序工作！

在提示符下输入这个（使用 **SayHello** 的首字母大写方式，如下所示）：

	ruby script/generate controller SayHello

片刻之后，你将被告知已创建以下文件和目录（该脚本还将告诉你某些目录已存在且因此未创建）：

	app/views/say_hello
	app/controllers/say_hello_controller.rb
	test/functional/say_hello_controller_test.rb
	app/helpers/say_hello_helper.rb

<div class="note">

**注意：**这个 **generate controller** 脚本还会创建另一个 Ruby 文件，**application.rb**，它是整个应用程序的控制器，加上一个文件夹 **/views/say_hello**，我们将在稍后使用它。
</div>

注意 Rails 如何将名称 SayHello 解析成两个小写的单词，say 和 hello，并用下划线分隔，它使用这个名字作为三个独立的 Ruby 文件的第一部分。这只是 Rails 使用的“按惯例配置”方法的一个示例。

这里的 Ruby 文件 **say_hello_controller_test.rb** 和 **say_hello_helper.rb** 作为存储库提供（如果你愿意），分别用于测试和实用程序（'helper'）代码。但更重要的是控制器文件本身 **say_hello_controller.rb**，它是在 **\helloworld\app\controllers** 中创建的。在文本编辑器中打开此文件。这个空方法已自动生成：

	class SayHelloController < ApplicationController
	end

在这个类中，我们可以编写一些代码，以便在显示某个页面时执行。编辑类定义以匹配以下内容：

	class SayHelloController < ApplicationController
	  def index
		render :text => "Hello world"
	  end

	  def bye
		render :text => "Bye bye"
	  end
	end

现在包含两个方法，`index` 和 `bye`。每个方法都包含一行代码。尽管我已经省略了括号（许多 Rails 开发人员喜欢使用 bracket-light 风格编码），但你可以推断出 `render` 是一种以哈希为参数的方法；哈希本身包含一个包含一个由符号和字符串构成的键值对。对于 bracket-lovers ，`index` 方法可以像这样重写：

	def index
	  render( { :text => "Hello world" } )
	end

你有了第一个真正的 Rails 应用程序。要试用它，你需要返回到 Web 浏览器并输入你刚刚编写的两个函数的完整“地址”。但首先你可能需要重新启动服务器。只需在服务器运行的命令窗口中按 <kbd>CTRL+C</kbd>。当服务器存在时，输入以下命令重启：

	ruby script/server

现在输入一个地址来访问控制器方法。地址采用主机和端口的形式（与你之前输入的相同 - 例如，**http://localhost:3000**），加上控制器的名称（**/say_hello**），最后是特定方法的名称（**/index** 或 **/bye**）。尝试将这些输入到浏览器的地址字段中，再次确保使用相应的端口号，如果不是 3000：

	http://localhost:3000/say_hello/index
	http://localhost:3000/say_hello/bye

你的浏览器应分别为每个地址显示 "Hello world" 和 "Bye bye"。如果所有都如期运行，你可以继续阅读*“剖析简单 Rails 应用”*一节。但是，如果你看到数据库错误，请首先阅读下一节_“无法找到数据库？”_...

### 无法找到数据库？

如果你选择使用 SQLite3 数据库，则应首先按照此处的说明进行安装：

	http://wiki.rubyonrails.org/rails/pages/HowtoUseSQLite

如果（像我一样）你已经决定使用 MySQL 数据库，并且假设 MySQL 已正确安装（参见附录），那么当你尝试运行应用程序时，Rails 可能会显示类似于以下内容的错误消息：

	no such file to load -- mysql

某些版本的 Rails（例如，Rails 2.2）要求将 MySQL gem 作为单独的操作安装。为此，请在系统提示符下输入：

	gem install mysql

在 Windows 上，当你现在运行应用程序时，你可能会看到与此类似的其它错误消息：

	The specified module could not be found.
	c:/ruby/lib/ruby/gems/1.8/gems/mysql-2.7.3-x86-mswin32/ext/mysql.so

如果你遇到此问题，你应该能够通过将 MySQL 二进制目录（例如，*C:\Program Files\MySQL\MySQL Server 5.0\bin*）中名为 **libmySQL.dll** 的文件的副本复制到 Ruby 二进制目录（例如，*C:\ruby\bin*）中来修复它。重新启动应用程序（关闭并重新启动服务器），然后再次尝试运行它。

我们简单的 "hello world" 应用程序不需要数据库。验证是否正确指定了数据库适配器（例如 *sqlite3* 或 *mysql*），但在数据库配置文件 *\app\config\database.yml* 的 'development' 部分中没有给出数据库名称。

当我使用 MySQL 时，我的配置如下（其中 'root' 是我的 MySQL 用户名，'mypassword' 是我的 MySQL 密码：

	development:
	  adapter: mysql
	  host: localhost
	  username: root
	  database:
	  password: mypassword

### 剖析简单的 Rails 应用

Rails 使用 `index` 方法作为默认值，因此在将地址输入浏览器时可以省略 URL 的那一部分：

	http://localhost:3000/say_hello

在继续之前，让我们仔细看看我们正在使用的类。Rails 通过将 Controller 附加到我们在运行控制器生成器脚本（**HelloWorld**）时指定的名称来命名该类，并使它成为 `ApplicationController` 类的后代：

	class SayHelloController < ApplicationController

究竟什么是 `ApplicationController` 类？现在，你可能还记得我提到我们之前运行的 **generate/controller** 脚本在 **/app/controllers** 文件夹中静默创建了一个名为 **application.rb** 的文件。此文件是应用程序控制器，如果打开它，你将看到它包含一个类名为：

	ApplicationController < ActionController::Base

因此，我们的 `SayHelloController` 类继承自 `ApplicationController` 类，该类本身继承自 `ActionController` 模块中的 `Base` 类。你可以通过遍历层次结构并要求每个类显示自己来证明这一点。

顺便说一句，这也让我们有机会尝试在 `SayHelloController` 类中进行一些真正的 Ruby 编程。

只需编辑 **say_hello_controller.rb** 文件的内容以匹配以下内容（或者复制并粘贴本章代码存档中的 **sayhello1.rb** 文件中的代码）：

<div class="code-file clearfix"><span>sayhello1.rb</span></div>

	class SayHelloController < ApplicationController
	  def showFamily( aClass, msg )
		if (aClass != nil) then
		  msg += "<br />#{aClass}"
		  showFamily( aClass.superclass, msg )
		else
		  render :text => msg
		end
	  end

	  def index
		showFamily( self.class, "Class Hierarchy of self..." )
	  end
	end

要查看结果，请在浏览器中输入此地址（如果需要，请再次更改端口号）：

	http://localhost:3000/say_hello/

你的 Web 浏览器现在应该显示...

	Class Hierarchy of self...
	SayHelloController
	ApplicationController
	ActionController::Base
	Object

如你所见，Rails 控制器文件包含 Ruby 代码。你可以在控制器中使用所有常用的 Ruby 类，例如 String 和 Hash，你可以调用方法并传递参数。

但请记住，最终结果需要显示在网页中。这有一定的后果。例如，不要将换行符 "\n" 放入字符串中，而应使用 HTML 段落，`<P>` 或换行 `<br/>` 标记，并且每次显示页面时只允许调用 `render` 一次，这就解释了为什么我在递归调用方法的过程中构造了一个字符串，然后将其传递给最后的 `render` 方法：

	def showFamily( aClass, msg )
	  if (aClass != nil) then
		msg += "<br />#{aClass}"
		showFamily( aClass.superclass, msg )
	  else
		render :text => msg
	  end
	end

<div class="note">
	<p class="h4"><b>生成控制器脚本摘要</b></p>

让我们简要回顾一下当我们运行 **generate controller** 脚本时发生的事情。每次生成一个新控制器时，它都会在 **app/controllers** 目录中创建一个 Ruby 代码文件，其名称与你输入的名称相匹配，但全部为小写，并且你指定的任何非初始大写字母前面都加了下划线并且最后附加 **_controller**。因此，如果你输入 **SayHello**，则控制器文件将被命名为 **say_hello_controller.rb** 。控制器将包含一个类定义，例如：`SayHelloController`。你还可以通过在执行脚本时包含这些视图名称来指定“视图”，例如 `index` 和 `bye` ...

	ruby script/generate controller SayHello index bye

在这种情况下，Controller 类将自动提供名称与这些视图匹配的方法（`def index` 和 `def bye`）。无论如何，无论你是否指定视图，都会在 **/views** 目录中创建一个的文件，其名称与控制器匹配（**views/say_hello**）。实际上，该脚本还会创建一些其它文件，包括 **/helpers** 文件夹中的一些更多 Ruby 文件，但我们现在可以忽略它们。

如果在运行控制器脚本时指定了视图名称，则某些名称匹配且扩展名为 **.html.erb** 的文件将添加到相应的视图文件夹中。例如，如果你输入了命令...

	ruby script/generate controller SayHello xxx

...**/views/say_hello** 目录现在应该包含一个名为 **xxx.html.erb** 的文件。另一方面，如果你输入了...

	ruby script/generate controller Blather xxx bye snibbit

...**views/blather** 目录现在应该包含三个文件：

**xxx.html.erb**, **bye.html.erb** 和 **snibbit.html.erb**。
</div>

### 创建视图

虽然通过对 Controller 内的所有内容进行编码来创建整个应用程序是可能的，但最终会出现一些非常难看的网页。要应用更多格式，您需要创建一个视图（View）。

你可以将视图（View）视为 HTML 页面，当有人登录到特定的 Web 地址时将显示该页面 - 在这种情况下，视图的名称构成了地址的最后部分，如前面的示例所示 URL 的 **/index** 和 **/bye** 部分将为我们匹配相应视图，这些视图显示的数据由控制器中的 `index` 和 `bye` 方法提供。

你可以创建与这些 Web 地址和相应方法名称匹配的 HTML 视图“模板”。使用 HTML（或纯文本）编辑器，在 **\app\views\say_hello** 目录中创建名为 **index.html.erb** 的文件。

<div class="note">

请记住，在最初生成控制器时，你可以选择创建一个或多个视图模板。这是通过在用于运行脚本以生成控制器的命令末尾附加几个名称来完成的：

	ruby script/generate controller Blather index bye snibbit

这个脚本将创建 Blather 控制器和三个视图：*index*，*bye* 和 *snibbit*。

现在我们有了一个视图模板，我们可以对其进行编辑，以便控制数据在网页中的显示方式。这意味着从现在开始我们不需要使用控制器中的 `render` 方法显示简单，无格式的文本。

</div>

但是，由于视图不受控制器的控制（可以这么说），控制器如何将数据传递给视图？事实证明，它可以通过将数据分配给实例变量来实现。

编辑 **say_hello_controller.rb** 中的代码（或删除它并粘贴来自文件 **sayhello2.rb** 中我的代码），使其与以下内容匹配：

<div class="code-file clearfix"><span>sayhello2.rb</span></div>

	class SayHelloController < ApplicationController
	  def showFamily( aClass, msg )
		if (aClass != nil) then
		  msg += "<li>#{aClass}</li>"
		  showFamily( aClass.superclass, msg )
	    else
		  return msg
	    end
	  end

	  def index
		@class_hierarchy = "<ul>#{showFamily( self.class, "" )}</ul>"
	  end
	end

此版本调用 `showFamily()` 方法，以便在两个 HTML'无序列表'标记 `<ul>` 和 `</ul>` 中构建一个字符串。每次找到类名时，它都放在两个 HTML '列表项'标签 `<li>` 和 `</li>` 之间。完整的字符串形成一个有效的 HTML 片段，`index` 方法只是将该字符串分配给一个名为 `@class_hierarchy` 的变量。

<div class="note">
	<p class="h4"><b>控制器中的 HTML 标记...？</b></p>

一些 Ruby On Rails 开发人员反对在控制器（Controller）代码中包含任何 HTML 标记，无论多少。在我看来，如果你打算在网页中显示最终结果，那么你把奇怪的 `<p>`，`<ul>` 或 `<li>` 标签放在哪里就没那么重要了。虽然 MVC 模式鼓励控制器（Controller）的程序代码与视图（View）的布局定义之间存在强烈的可拆分性，但你将不可避免地要做出一些妥协 - 至少可以通过将一些程序代码放入视图（View）中来实现。避免在控制器（Controller）中使用 HTML 标签在很大程度上是一种美学而非实用的反对意见。我个人对这个问题没有非常强烈的看法，但是（被警告！）其他人这样做...
</div>

我们现在需要做的就是找到一种方法将 HTML 片段放入一个完整组成的 HTML 页面。这就是视图（View）的用处。在 **app/views/say_hello** 文件夹中打开刚刚创建的视图文件 **index.html.erb**。根据 Rails 命名约定 - 这是与 **say_hello_controller.rb** 文件关联的默认视图（'index'页面）。由于 Rails 根据文件，文件夹，类和方法名称计算出依赖关系，因此我们不必按名称加载（load）或引入（require）任何文件，也不必编写任何详细配置信息。

在 **index.html.erb** 文件中添加：

	<h1></h1>
	<%= @class_hierarchy %>

第一行只是纯 HTML 格式，它将 `<h1></h1>` 标记所包含的文本定义为标题。下一行更有趣。它包含变量 `@class_hierarchy`。回顾一下 **say_hello_controller.rb** 中的 `index` 方法，你会发现这是我们为其分配字符串的变量。在视图中，`@class_hierarchy` 位于两个奇怪的限定符 `<%=` 和 `%>` 两者之间。这些是特殊的 Rails 标签。它们用于嵌入 Ruby 代码，这些 Ruby 代码将在浏览器中显示网页之前被执行。最终显示的页面将是一个完整格式的 HTML 页面，其中包含视图模板文件中的任何HTML片段以及任何嵌入式 Ruby 代码执行后的结果。现在尝试一下，在浏览器中输入页面地址：

	http://localhost:3000/say_hello/

现在，它应该以粗体字母显示标题 "This is the Controller's Class Hierarchy"，后跟一个类列表，每个类的前面都有一个点：

- SayHelloController
- ApplicationController
- ActionController::Base
- Object

如果你愿意，从视图文件中删除所有 HTML，可以通过在控制器中创建标题并将结果字符串分配给另一个变量。你可以通过编辑 **say_hello_controller.rb** 中的 `index` 方法来执行此操作：

	def index
	  @heading = "<h1>This is the Controller's Class Hierarchy</h1>"
	  @class_hierarchy = "<ul>#{showFamily( self.class, "" )}</ul>"
	end

然后将视图文件（**/app/views/say_hello/index.html.erb**）编辑为：

<div class="code-file clearfix"><span>say_hello.html.erb</span></div>

	<%= @heading %>
	<%= @class_hierarchy %>

如果执行此操作，则网页中显示的最终结果将保持不变。

### Rails 标记

Rails 标记（tags）有两种变体，你可以将它们放入 Rails HTML “嵌入式 Ruby”（*ERb*）模板文件中。到目前为止我们使用的那些在开头包含一个等号，`<％=`。

这些标记使 Rails 不仅可以计算 Ruby 表达式，还可以在网页中显示结果。如果在开始分隔符 `<％` 中省略等号，则将执行代码，但不会显示结果。

<div class="note">

**ERB**（'嵌入式Ruby'）文件包含混合了 HTML 标记和标记之间的 Ruby 代码，例如 `<％=` 和 `％>`。Rails 在 Web 浏览器中显示最终页面之前处理这些文件，执行嵌入式R uby 代码并构造 HTML 页面。
</div>

如果你愿意，你可以在 `<％` 和 `％>` 标记之间放置相当长的代码 - 甚至整个 Ruby 程序！然后在要在网页中显示内容时使用 `<％=` 和 `％>`。实际上，我们可以通过完全省略控制器并将所有内容放入视图来重写我们的应用程序。通过编辑 **app/views/say_hello/index.html.erb** 以匹配以下内容（或复制并粘贴 **embed_ruby.html.erb** 文件中的代码）来尝试：

<div class="code-file clearfix"><span>embed_ruby.rhtml</span></div>

	<% def showFamily( aClass, msg )
	  if (aClass != nil) then
		msg += "<li>#{aClass}</li>"
		showFamily( aClass.superclass, msg )
	  else
		return msg
	  end
	end %>

	<%= "<ul>#{showFamily( self.class, "" )}</ul>" %>

在这种特殊情况下，网页上显示的文本与之前略有不同，因为它现在显示视图类的类层次结构，而不是控制器类的类层次结构。正如你将看到的，视图继承自 `ActionView::Base` 类。

你还可以通过在 `<％` 和 `％>` 标记之间放置单独的行来分割连续的代码块，而不是将整个块放在一对标记之间。这样做的好处是它允许你将标准 HTML 标记放在单独分隔的 Ruby 代码行之外。例如，你可以将其放入视图中：

	<% arr = ['h','e','l','l','o',' ','w','o','r','l','d'] %>

	<% # sort descending from upper value down to nil
	reverse_sorted_arr = arr.sort{
	  |a,b|
	  b.to_s <=> a.to_s
	} %>

	<% i = 1 %>
	<ul>
	<% reverse_sorted_arr.each{ |item| %>
	<li><%= "Item [#{i}] = #{item}" %></li>
	<% i += 1 %>
	<% } %>
	</ul>

在这里，我为一组标签之间的变量 `arr` 分配了一组字符；我已经编写了一个块来对数组进行反向排序，并将结果分配给第二组标记之间的另一个变量；然后我给变量 `i` 分配了 1；最后我写完了这个方法：

但是，我没有将方法包含在一组标记之间，而是将每个单独的行包含在它自己的标记对中。我为什么要这样做？嗯，有两个原因。首先，我希望块中间的字符串显示在网页上，所以我需要在那里使用 `<％=` 标记：

	<%= "Item [#{i}] = #{item}" %>

其次，我希望整个字符串集显示为 HTML 列表。所以我在 Ruby 代码块之前和之后放置了 `<ul>` 和 `</ul>` 标记；并且，我已经在 `<li>` 和 `</li>` 标记中放置了显示每个数组项的代码行。请注意，这些标记位于 Ruby 代码块中，但在此特定行上的嵌入式Ruby标记之外：

	<li><%= "Item [#{i}] = #{item}" %></li>

因此，通过将一个连续的 Ruby 代码块划分为单独分隔的行，我已经掌握了能够将 HTML 混合到 Ruby 代码中的有用技巧！说实话，我根本没有把它混合在一起 -  Ruby 代码仍然在标签内部闭合；我所做的是告诉 Rails 在 Web 浏览器中显示页面之前在特定点混合使用 HTML。

顺便提一下，你可能会发现将所有嵌入式 Ruby 代码放入视图（**index.html.erb**）的应用程序的版本与将代码全部放入控制器的先前的版本（**say_hello_controller.rb**）进行比较会很有趣。并且只有很少的嵌入式 Ruby（几个变量）代码被放入视图中：

	<%= @heading %>
	<%= @class_hierarchy %>

你可能会赞成第一个版本，其中代码和格式保持独立，是整洁的。总的来说，Ruby 代码属于 Ruby 代码文件，HTML 格式属于 HTML 文件。虽然嵌入式 Ruby 提供了一种让视图和控制器进行通信的简单方法，但通常最好保持嵌入式 Ruby 代码简洁明了，并将更复杂的 Ruby 代码放入 Ruby 代码文件中。

### 让我们创建一个博客

对于许多人来说，真正“将它们吸引到” Ruby On Rails 的一件事是由 Rails 创建者 David Heinemeier Hansson 提供的二十分钟演示，其中他演示了如何创建一个简单的博客。

	http://www.rubyonrails.com/screencasts

博客是一种来展示使用 Rails 创建相当复杂的应用程序是多么容易的很好的方式。在本章的最后一部分中，我将解释如何创建一个非常简单的博客（Blog）应用程序。我将使用一个名为 "migrations"（迁移）的功能，它将减少创建“模型”（Model）数据库结构的大量工作。

请记住，我已尽力使这个应用程序的创建尽可能简单，并且它不具备功能齐全的博客的所有功能（例如，没有用户注释和管理界面）。完成我的基本博客应用程序后，你可能想要学习前面提到的截屏教程。这将带你进一步创建更复杂的博客。

<div class="code-file clearfix"><span>\blog</span></div>

<div class="note">

你可以将博客应用程序的代码与我创建的代码进行比较。这是在本章随附的代码的 **\blog** 子目录中提供的。但是，这个博客应用程序并不是“就绪状态”，因为它需要一个你必须创建的数据库。不要“按原样”运行它，使用我的博客应用程序作为参考来检查你创建的文件是否与我创建的文件匹配。
</div>

在保存 Rails 应用程序的目录中打开命令提示符（例如 **C:\railsapps**）并执行命令以创建名为 Blog 的应用程序：

	rails blog

#### 创建数据库

现在让我们创建一个数据库。再一次，我假设你正在使用 MySQL 数据库。打开 MySQL 提示符（如前所述，从 MySQL 程序组打开 MySQL 命令行客户端）。出现提示时输入你的 MySQL 密码。现在你应该看到提示：

	mysql>

在提示符处输入此内容（请记住结尾处的分号）：

	create database blog_development;

MySQL 应该回复“查询确定”（Query OK）以确认已创建。现在确保 Rails 应用程序的数据库配置文件包含开发数据库的相应配置。如果你使用的是其它数据库（而不是 MySQL），则配置条目必须引用该数据库。

打开 *\app\config\database.yml*。假设你正在使用 MySQL，请输入*'mysql'*作为适配器，_'localhost'_作为主机，输入你的 MySQL 用户名（例如*'root'*）和密码（如果有）。数据库名称应与你刚刚创建的数据库匹配。这是一个示例：

	development:
	  adapter: mysql
	  host: localhost
	  username: root
	  database: blog_development
	  password: mypassword

<div class="note">

**记住：**如果在更改 **database.yml** 时服务器正在运行，你应该在之后重新启动服务器！
</div>

#### 脚手架

我们将使用一个名为 "scaffolding"（脚手架）的功能来一次创建模型，视图和控制器。脚手架是一种快速启动和运行简单应用程序的便捷方式。进入新的 **\blog** 目录并在系统提示符下输入以下内容：

	ruby script/generate scaffold post title:string body:text created_at:datetime

这告诉脚手架生成器创建一个包含 Ruby 代码的模型来访问一个名为 'post' 的数据库表，其中有三列，'title'，'body' 和 'created_at'，每个列都有数据类型（*字符串*，*文本*和*日期时间*）在冒号后指定。

为了基于此模型创建数据库结构，我们需要运行 "migration" 来更新数据库表本身。

#### 迁移

scaffold 脚本为我们创建了一个数据库迁移（migration）文件。导航到 **\db\migrate** 目录。你将看到它包含一个带编号的迁移文件，其名称以 *_create_posts.rb* 结尾。如果打开此文件，你可以看到如何在 Ruby 代码中表示表结构：

	def self.up
	  create_table :posts do |t|
		t.string :title
		t.text :body
		t.datetime :created_at

		t.timestamps
	  end
	end

随着时间的推移，应用程序可能会获得大量迁移文件，每个迁移文件都包含有关模型特定迭代的信息 - 对数据库表结构所做的更改和添加。经验丰富的 Rails 开发人员可以有选择地使用迁移来激活不同版本的模型。但是，在这里，我们将使用此迁移来创建数据库的初始结构。在应用程序主目录（例如 **/blog**）中的系统提示符下，你可以使用 "rake" 工具运行迁移。输入以下命令：

	rake db:migrate

片刻之后，你应该看到一条消息，指出 rake 任务已完成并且 CreatePosts 已被迁移。

#### Partial

现在让我们创建一个新的 "partial" 视图模板。Partial 是网页模板的片段，Rails 可以在运行时将其插入到一个或多个完整的网页中。

例如，如果你计划在站点的多个页面中使用相同的数据输入表单，则可以在 partial 模板中创建该表单。partial 模板的名称以下划线开头。

在 *\app\views\posts\directory* 目录中创建一个名为 **_post.html.erb** 的新文件。打开此文件并编辑其内容以匹配以下内容：

	<div>
	<h2><%= link_to post.title, :action => 'show', :id => post %></h2>
	<p><%= post.body %></p>
	<p><small>
	<%= post.created_at.to_s %>
	</small></p>
	</div>

保存更改。然后打开名为 **show.html.erb** 的文件。此文件由脚手架脚本自动创建。从文件中删除以下“样板”（boilerplate）代码...

	<b>Title:</b>
	  <%=h @post.title %>
	</p>

	<p>
	  <b>Body:</b>
	  <%=h @post.body %>
	</p>

	<p>
	  <b>Created at:</b>
	  <%=h @post.created_at %>
	</p>

并用这些替换它...

	<%= render :partial => "post", :object => @post %>

这告诉 Rails 此时渲染 **_post** 局部模板。**show.html.erb** 中的代码现在应该如下所示...

	<%= render :partial => "post", :object => @post %>

	<%= link_to 'Edit', edit_post_path(@post) %> |
	<%= link_to 'Back', posts_path %>

#### 试一试

就是这样！现在你已准备好测试你的应用程序。首先，运行服务器。在 **\blog** 目录中的提示符下，输入：

	ruby script/server

<div class="note">

回想一下，如果你没有使用默认端口 3000，则需要在 `-p` 之后指定实际端口号，如本章前面所述。 例如：`ruby script/server -p3003`
</div>

进入你的 Web 浏览器并输入以下地址（如果不是 3000，请再次使用实际端口号）：

**http://localhost:3000/posts**

你应该看到你的页面的 index 页面处于活动状态。这是应该出现的...

<div class="text-center">
	<img src="images/chapter19_blog_test_index.png" />
</div>

现在单击 *New Post* 链接。在“新建帖子”页面中，输入标题和正文。然后单击 *Create*。

<div class="text-center">
	<img src="images/chapter19_blog_test_create.png" />
</div>

显示的下一页是“显示页面”。这是由 **show.html.erb** 视图和 **_post.html.erb** 部分组合定义的。现在继续输入帖子并单击链接以浏览各种已定义的视图...

<div class="text-center">
	<img src="images/chapter19_blog_test_list.png" />
</div>

<div class="note">

如前所述，本章假定你以"原始的方式"使用 Rails，在系统提示符下输入所有必需的命令。某些 IDE 提供了更加集成的环境，允许你使用内置工具和实用程序生成和编写应用程序代码。你将在附录中找到一些 Ruby 和 Rails IDE 的概述。如果你使用的是 Ruby In Steel，你可以在 SapphireSteel Software 网站上找到本博客教程的替代版本，该教程使用 Ruby In Steel 的集成工具：

**http://www.sapphiresteel.com/How-To-Create-A-Blog-With-Rails-2**
</div>

## 深入探索

### MVC

如前所述，Rails 采用模型（Model），视图（View）和控制器（Controller）（或 MVC）模式。虽然这三个组成部分在理论上是分开的实体，但实际上不可避免地存在一定程度的重叠。例如，一些计算可以在模型中完成，其它计算在控制器中完成；影响数据格式化的操作可能发生在控制器或视图中。没有硬性和绝对的规则 - 只是一个一般原则，即尽可能地在模型中发生“接近数据”的操作，“接近显示”的操作应该在视图中发生，其它一切应该进入控制器。

那是理论上的 MVC。现在让我们看看 Rails 如何实现它...

#### Model

Ruby On Rails 中的模型（Model）是数据库中表的组合 - 由 MySQL 等数据库服务器处理 - 以及一组匹配的 Ruby 类来处理这些表。例如，在 Blog 中，你可能拥有一个包含名为 Posts 的表的数据库。在这种情况下，Rails 模型还将包含一个名为 Post 的 Ruby 类（注意 Rails 与复数一起使用 - Posts 表可以包含许多 Post 对象！）。Ruby Post 类通常包含从 Posts 数据库中查找，保存或加载单个 Post 记录的方法。数据库表和相应的 Ruby 类的这种组合构成了 Rails 模型（Model）。

#### View

视图（View）几乎就是它的样子 - Ruby On Rails 应用程序的可视化表示。它（通常不一定）以 HTML 模板的形式创建，其中混合了一些 Ruby 代码。事实上，其它视图类型（例如，使用 Adobe 的 Flex 或 Microsoft 的 Silverlight 制作的图形视图）是可能的，但 Rails 默认为 HTML。这些模板，通常具有扩展名 *.html.erb*（但也可能使用扩展名 *.rhtml*，这是 Rails 1 默认的），不会直接加载到 Web 浏览器中 - 毕竟，Web 浏览器没有任何运行 Ruby 代码的方法。相反，它们由一个单独的工具预处理，该工具执行 Ruby 代码以便与模型交互（根据需要查找或编辑数据），然后，作为最终结果，它创建一个新的 HTML 页面，其基本布局由一个 ERb 模板定义，但其实际数据（即博客文章，购物车项目或其它内容）由模型提供。

#### Controller

控制器（Controller）采用 Ruby 代码文件的形式，作为链接模型和视图的中间件。例如，在网页（视图）中，用户可以单击按钮将新帖子添加到博客；使用普通的 HTML，这个按钮提交一个名为 'Create' 的值。这导致一个名为 `create` 的方法，在一个帖子'控制器'（一个 Ruby 代码文件）中将已经输入网页（视图）的新博客条目（一些文本）保存到数据库中（该模型的数据存储库）。

### Rails 文件夹

这是 Rails 生成的顶级文件夹的简化指南，简要描述了它们包含的文件和文件夹：

- **app**

	它包含特定于此应用程序的代码。子子文件夹是：*app\controllers*, *app\models*, *app\views* 和 *app\helpers*。

- **config**

	Rails 环境的配置文件，路由映射，数据库和其它依赖项；一旦我定义了数据库，会包含配置文件 *database.yml*。

- **db**

	包含 *schema.rb* 中的数据库概要信息，并且可能包含作用于数据库中数据的代码。

- **doc**

	可能包含 RDOC 文档（有关 RDOC 的更多信息，请参阅附录）。

- **lib**

	可能包含应用程序的代码库（即，逻辑上不属于 *\controllers*，*\models* 或 *\helpers* 的代码）。

- **log**

	可能包含错误日志。

- **public**

	该目录包含可由 Web 服务器使用的“静态”文件。它有图片，样式表和 javascripts 的子目录。

- **script**

	包含 Rails 用于执行各种任务的脚本，例如生成某些文件类型和运行 Web 服务器。

- **test**

	这可能包含由 Rails 生成或由用户指定的测试文件。

- **tmp**

	Rails 使用的临时文件。

- **vendor**

	可能包含第三方库，这些库不构成 Rails 默认安装的一部分。

### 其它 Ruby 框架

Rails 可能是最著名的 Ruby 框架，但它肯定不是唯一的。其它如 Ramaze，Waves 和 Sinatra 也有专门的追随者。一个名为 Merb 的框架曾被视为最接近 Rails 的竞争对手。然而，在 2008 年 12 月，Rails 和 Merb 团队宣布他们将合作进行 Rails 的下一次迭代 - "Rails 3"。

如果你有兴趣探索其它 Ruby 框架，请点击以下链接：

Merb: http://merbivore.com/

Ramaze: http://ramaze.net/

Sinatra: http://sinatra.rubyforge.org/

Waves: http://rubywaves.com/

Ramaze 开发人员维护了一个更全面的 Ruby 框架列表，你可以在他们的主页上找到它们。

# 第二十章

***

## 动态编程

在前面的 19 章中，我们介绍了 Ruby 语言的大量特性。我们还没有详细研究过的一件事就是 Ruby 的“动态编程”（dynamic programming）功能。

如果你只使用了非动态语言（比如 C 或 Pascal 系列中的一种语言），那么编程中的动态可能需要一点时间来习惯。在进一步讨论之前，让我们用“动态”语言来澄清我的意思。事实上，这个定义有点模糊，并不是所有声称“动态”的语言拥有所有相同的特征。然而，在一般意义上，提供一些可以在运行时修改程序的手段的语言可以被认为是动态的。动态语言的另一个特征是它能够改变给定变量的类型 - 这是我们在本书的例子中无数次做过的事情。

可以区分“动态类型”语言（如 Ruby）和“静态类型语言”（其中变量的类型是预先声明和固定的），如 C，Java 或 Pascal。在本章中，我将集中讨论 Ruby 的自修改（self-modifying）功能。

### 自修改程序

在大多数编译语言和许多解释语言中，编写和运行程序是两个完全不同的操作。换句话说，你编写的代码是固定的，并且没有程序运行时更改的可能性。

Ruby 的情况并非如此。Ruby程序 - 它的实际代码 - 可以在程序运行时进行修改。甚至可以在运行时输入新的 Ruby 代码并执行新代码而无需重新启动程序。

将数据视为可执行代码的能力称为元编程（ meta-programming）。在本书中，我们一直在进行元编程，尽管是一种相当简单的编程。每次在双引号字符串中嵌入表达式时，你都在进行元编程。毕竟，嵌入式表达式并不是真正的程序代码 - 它是一个字符串 - 然而 Ruby 显然必须“将其转换为”程序代码才能够对其进行计算执行。

大多数情况下，你可能会在双引号字符串中的 `#{` 和 `}` 分隔符之间嵌入相当简单的代码。通常你可以嵌入变量名，或数学表达式：

<div class="code-file clearfix"><span>str_eval.rb</span></div>

	aStr = 'hello world'
	puts( "#{aStr}" )
	puts( "#{2*10}" )

但是你不仅限于这种简单的表达方式。如果你愿意，你可以将任何东西嵌入双引号字符串中。实际上，你可以用字符串编写整个程序。你甚至不需要使用 `print` 或 `puts` 显示最终结果。只需将双引号字符串放入程序中就会使得 Ruby 对其进行计算执行：

	"#{def x(s)
		puts(s.reverse)
	  end;
	(1..3).each{x(aStr)}}"

在字符串中编写整个程序可能是一个非常毫无意义的努力。但是，在其它情况下，这种类似的特性可以更有效地使用。例如，Rails 框架广泛使用元编程。你可以使用元编程来探索人工智能和“机器学习”（machine learning）。实际上，任何因程序执行过程中由于交互而修改程序行为进而受益的应用程序本质上都是元编程。

<div class="note">

动态（元编程）特性在 Ruby 中无处不在。例如，思考属性访问器：将符号（例如 `:aValue`）传递给 `attr_accessor` 方法会最终创建两个方法（`aValue` 和 `aValue=`）。
</div>

### eval 魔法

`eval` 方法提供了一种执行字符串的 Ruby 表达式的简单方法。乍一看，`eval` 可能看起来与双引号字符串中的 `#{}` 标记限定表达式完全相同。以下两行代码产生相同的结果：

<div class="code-file clearfix"><span>eval.rb</span></div>

	puts( eval("1 + 2" ) )
	puts( "#{1 + 2}" )

但是，有时候结果可能不是你所期望的。请看以下内容，例如：

<div class="code-file clearfix"><span>eval_string.rb</span></div>

	exp = gets().chomp()
	puts( eval( exp ))
	puts( "#{exp}" )

假设你输入 `2 * 4` 并将其分配给 `exp`。当你使用 `eval` 计算 `exp` 时，结果为 8，但是当你在双引号字符串中计算 `exp` 时，结果为 **'2 * 4'**。这是因为 `gets()` 读入的任何内容都是字符串，`"＃{exp}"` 将其作为*字符串*而不是表达式进行计算，而 `eval(exp)` 将字符串作为*表达式*求值。

为了强制在字符串中进行求值，你可以在字符串中放置 `eval`（尽管如此，可能会偏离我们的目标）：

	puts( "#{eval(exp)}" )

这是另一个例子。尝试一下，并在出现提示时按照说明操作：

<div class="code-file clearfix"><span>eval2.rb</span></div>

	print( "Enter the name of a string method (e.g. reverse or upcase): " )   # user enters: upcase
	methodname = gets().chomp()
	exp2 = "'Hello world'."<< methodname
	puts( eval( exp2 ) )  													  #=> HELLO WORLD
	puts( "#{exp2}" ) 														  #=> "Hello world".upcase
	puts( "#{eval(exp2)}" )													  #=> HELLO WORLD

`eval` 方法可以执行计算跨越多行的字符串，从而可以执行嵌入字符串中的整个程序：

<div class="code-file clearfix"><span>eval3.rb</span></div>

	eval( 'def aMethod( x )
	  return( x * 2 )
	end

	num = 100
	puts( "This is the result of the calculation:" )
	puts( aMethod( num ))' )

有了所有这些 `eval` 的能力，现在让我们看看编写一个它自己可以编写程序的程序是多么容易。这里：

<div class="code-file clearfix"><span>eval4.rb</span></div>

	input = ""
	until input == "q"
	  input = gets().chomp()
	  if input != "q" then eval( input ) end
	end

这可能看起来不多，但是这个程序允许你从命令提示符中创建和执行真正可用的 Ruby 代码。试试看。运行程序并一次一行地输入这两个方法（但是*不要点 'q' 来退出* - 我们稍后会写一些代码）：

	def x(aStr); puts(aStr.upcase);end
	def y(aStr); puts(aStr.reverse);end

请注意，你必须在一行中输入每个整个方法代码，因为我的程序在输入时按行执行。我将在后面解释如何解决这个限制。归功于 `eval`，每个方法都变成了真实可行的 Ruby 代码。你可以通过输入以下内容来证明这一点：

	x("hello world")
	y("hello world")

现在，这些表达式本身已被执行，它们将调用我们刚刚编写的两个方法，从而产生以下输出：

	HELLO WORLD
	dlrow olleh

仅仅五行代码很不错了！

### 特殊类型的 eval

`eval` 相关的一些变体以名为 `instance_eval`，`module_eval` 和 `class_eval` 方法的形式出现。可以从特定对象调用 `instance_eval` 方法，并且它提供对该对象的实例变量的访问。它可以用块或字符串调用：

<div class="code-file clearfix"><span>instance_eval.rb</span></div>

	class MyClass
	  def initialize
		@aVar = "Hello world"
	  end
	end

	ob = MyClass.new
	p( ob.instance_eval { @aVar } ) 	#=> "Hello world"
	p( ob.instance_eval( "@aVar" ) ) 	#=> "Hello world"

另一方面，`eval` 方法不能以这种方式从对象调用，因为它是 Object 的私有方法（而 `instance_eval` 是公有方法）。实际上，你可以通过将其名称（符号 `:eval`）发送到 `public` 方法来显式更改 `eval` 的可见性，尽管通常建议不要在基类中没有理由的去更改方法可见性！

<div class="note">

严格地说，`eval` 是 Kernel 模块的一个方法，它是被混入到 Object 类中的。事实上，Kernel 模块提供了大多数可用作 Object 方法的函数。
</div>

你可以通过以这种方式添加到 Object 类定义来更改 `eval` 的可见性：

	class Object
	  public :eval
	end

实际上，请记住，当你编写“独立”的代码时，你实际上是在 Object 的作用域内工作，只需输入此代码（没有类 Object 包装器）就会产生相同的效果：

	public :eval

现在你可以使用 `eval` 作为 `ob` 变量的方法：

	p( ob.eval( "@aVar" ) )  #=> "Hello world"

`module_eval` 和 `class_eval` 方法分别对模块和类而不是对象进行操作。例如，此代码将 `xyz` 方法添加到 X 模块（此处 `xyz` 在块中定义，并通过 `define_method` 作为接收对象的实例方法添加，这是 Module 类的方法）；并将 `abc` 方法添加到 Y 类：

<div class="code-file clearfix"><span>module_eval.rb</span></div>

	module X
	end

	class Y
	  @@x = 10
	  include X
	end

	X::module_eval{ define_method(:xyz){ puts("hello" ) } }
	Y::class_eval{ define_method(:abc){ puts("hello, hello" ) } }

<div class="note">

访问类和模块方法时，你可以使用作用域解析运算符 `::` 或单个点。访问常量时，作用域解析运算符是必需的，访问方法时是可选的。
</div>

所以，现在作为 Y 实例的对象将有权访问 Y 类的 `abc` 方法和已混合到 Y 类中的 X 模块的 `xyz` 方法：

	ob = Y.new
	ob.xyz  #=> "hello"
	ob.abc  #=> "hello, hello"

尽管名称不同，但 `module_eval` 和 `class_eval` 在功能上是相同的，并且每个都可以与模块或类一起使用：

	X::class_eval{ define_method(:xyz2){ puts("hello again" ) } }
	Y::module_eval{ define_method(:abc2){ puts("hello, hello again" ) } }

你也可以以相同的方式将方法添加到 Ruby 的标准类中：

	String::class_eval{ define_method(:bye){ puts("goodbye" ) } }
	"Hello".bye #=> "goodbye"

### 添加变量和方法

`module_eval` 和 `class_eval` 方法也可用于获取类变量的值（但请记住，你越这么做，代码就越依赖于类的实现细节，从而破坏封装性）：

	Y.class_eval( "@@x" )

实际上，`class_eval` 可以计算任意复杂度的表达式。例如，你可以通过计算字符串将其用于向类中添加新方法...

	ob = X.new
	X.class_eval( 'def hi;puts("hello");end' )
	ob.hi  #=> "hello"

回到前面从类外部添加和获取类变量的示例（使用 `class_eval`）；事实证明，还有一些方法可以从类中实现。这些方法称为 `class_variable_get`（这需要一个表示变量名的符号参数，它返回变量的值）和 `class_variable_set`（这需要一个表示变量名的符号参数和一个要赋给变量的值作为第二个参数）。这是这些方法的一个示例：

<div class="code-file clearfix"><span>classvar_getset.rb</span></div>

	class X
	  def self.addvar( aSymbol, aValue )
		class_variable_set( aSymbol, aValue )
	  end

	  def self.getvar( aSymbol )
		return class_variable_get( aSymbol )
	  end
	end

	X.addvar( :@@newvar, 2000 )
	puts( X.getvar( :@@newvar ) ) #=> 2000

要获取类变量名称列表作为字符串数组，请使用 `class_variables` 方法：

	p( X.class_variables )  #=> ["@@abc", "@@newvar"]

你还可以使用 `instance_variable_set` 为类和对象在它们被创建后添加实例变量：

	ob = X.new
	ob.instance_variable_set("@aname", "Bert")

将此与添加方法的能力相结合，大胆的（或者可能是鲁莽的？）程序员可以完全改变“来自外部”类的内部结构。这里我以类 X 中名为 `addMethod` 的方法的形式实现了这个方法，它使用 `send` 方法创建一个新方法 `m`，该方法使用 `define_method` 和由 `&block` 定义的方法体：

<div class="code-file clearfix"><span>dynamic.rb</span></div>

	def addMethod( m, &block )
	  self.class.send( :define_method, m , &block )
	end

<div class="note">

`send` 方法调用第一个参数（符号）标识的方法，并将指定的其它参数传递给它。
</div>

现在，X 对象可以调用 `addMethod` 将新方法插入到 X 类中：

	ob.addMethod( :xyz ) { puts("My name is #{@aname}") }

虽然从类的特定实例（此处为 `ob`）调用此方法，但它会影响类本身，因此新定义的方法也可用于后续从 X 类创建的任何实例（此处为 `ob2`）：

	ob2 = X.new
	ob2.instance_variable_set("@aname", "Mary")
	ob2.xyz

如果你不关心对象中数据的封装性，你还可以使用 `instance_variable_get` 方法获取实例变量的值：

	ob2.instance_variable_get( :@aname )

你可以类似地设置和获取常量：

	X::const_set( :NUM, 500 )
	puts( X::const_get( :NUM ) )

`const_get` 可以返回常量的值，所以你可以使用此方法获取类名的值，然后附加新方法以从该类创建新对象。这甚至可以通过提示用户输入类名和方法名来为你提供在运行时（runtime）创建对象的方法。通过运行此程序试试这个：

<div class="code-file clearfix"><span>dynamic2.rb</span></div>

	class X
	  def y
		puts( "ymethod" )
	  end
	end

	print( "Enter a class name: ")  			#<= Enter: X
	cname = gets().chomp
	ob = Object.const_get(cname).new
	p( ob )
	print( "Enter a method to be called: " ) 	#<= Enter: y
	mname = gets().chomp
	ob.method(mname).call

### 在运行时创建类

到目前为止，我们已经可以修改类并从现有类中创建新对象。但是你如何在运行时（runtime）创建一个全新的类呢？好吧，正如 `const_get` 可用于访问现有类一样，`const_set` 可用于创建新类。下面是一个示例，说明如何在创建该类之前提示用户输入新类的名称，向其中添加方法（`myname`），创建该类的实例 `x`，并调用其 `myname` 方法：

<div class="code-file clearfix"><span>create_class.rb</span></div>

	puts("What shall we call this class? ")
	className = gets.strip().capitalize()
	Object.const_set(className, Class.new)
	puts("I'll give it a method called 'myname'" )
	className = Object.const_get(className)
	className::module_eval{ define_method(:myname){
	  puts("The name of my class is '#{self.class}'" ) }
	}
	x = className.new
	x.myname

### 绑定

`eval` 方法可以接收可选的“绑定”（binding）参数，如果提供该参数，则使得执行计算在特定作用域或“上下文”（context）内完成。在 Ruby 中，发现绑定是 Binding 类的一个实例可能不会让人感到意外。你可以使用 `binding` 方法返回绑定。Ruby 类库中的 `eval` 文档提供了这个示例：

<div class="code-file clearfix"><span>binding.rb</span></div>

	def getBinding(str)
	  return binding()
	end
	str = "hello"
	puts( eval( "str + ' Fred'" ) ) 					#=> "hello Fred"
	puts( eval( "str + ' Fred'", getBinding("bye") ) ) 	#=> "bye Fred"

这里的 `binding` 是 Kernel 的私有方法。`getBinding` 方法能够在当前上下文中调用 `binding` 并返回 `str` 的当前值。在第一次调用 `eval` 时，上下文是 *main* 对象，并使用局部变量 `str` 的值；在第二次调用中，上下文移动到了 `getBinding` 方法内，`str` 的局部值现在是方法的 `str` 参数。

上下文也可以由类定义。在 **binding2.rb** 中，你可以看到实例变量 `@mystr` 和类变量 `@@x` 的值根据类而不同：

<div class="code-file clearfix"><span>binding2.rb</span></div>

	class MyClass
	  @@x = " x"
	  def initialize(s)
		@mystr = s
	  end
	  def getBinding
		return binding()
	  end
	end

	class MyOtherClass
	  @@x = " y"
	  def initialize(s)
		@mystr = s
	  end
	  def getBinding
		return binding()
	  end
	end

	@mystr = self.inspect
	@@x = " some other value"

	ob1 = MyClass.new("ob1 string")
	ob2 = MyClass.new("ob2 string")
	ob3 = MyOtherClass.new("ob3 string")

	puts(eval("@mystr << @@x", ob1.getBinding)) #=> ob1 string x
	puts(eval("@mystr << @@x", ob2.getBinding)) #=> ob2 string x
	puts(eval("@mystr << @@x", ob3.getBinding)) #=> ob3 string y
	puts(eval("@mystr << @@x", binding)) 		#=> main some other value

### Send

你可以使用 `send` 方法调用与指定符号同名的方法：

<div class="code-file clearfix"><span>send1.rb</span></div>

	name = "Fred"
	puts( name.send( :reverse ) )  #=> derF
	puts( name.send( :upcase ) )   #=> FRED

虽然 `send` 方法被记录为需要符号参数，但你也可以使用字符串参数。或者，为了保持一致性，你可以使用 `to_sym` 进行转换，然后使用相同的名称调用该方法：

	name = MyString.new( gets() )
	methodname = gets().chomp.to_sym 	#<= to_sym is not strictly necessary
	name.send(methodname)

下面是在运行时使用 `send` 调用输入的命名方法的示例：

<div class="code-file clearfix"><span>send2.rb</span></div>

	class MyString < String
	  def initialize( aStr )
		super aStr
	  end

	  def show
		puts self
	  end

	  def rev
		puts self.reverse
	  end
	end

	print("Enter your name: ")  	#<= Enter: Fred
	name = MyString.new( gets() )
	print("Enter a method name: " ) #<= Enter: rev
	methodname = gets().chomp.to_sym
	puts( name.send(methodname) )	#=> derF

回想一下我们先前（**dynamic.rb**）如何使用 `send` 来创建一个新方法，通过调用 `define_method` 并向其传递要创建的方法的名称 `m` 和包含新方法代码的块 `&block`：

<div class="code-file clearfix"><span>dynamic.rb</span></div>

	def addMethod( m, &block )
	  self.class.send( :define_method, m , &block )
	end

### 移除方法

除了创建新方法之外，有时你可能希望移除现有方法。你可以通过在特定类作用域内使用 `remove_method` 执行此操作。这将删除特定类中指定符号的方法：

<div class="code-file clearfix"><span>rem_methods1.rb</span></div>

	puts( "hello".reverse )
	class String
	  remove_method( :reverse )
	end
	puts( "hello".reverse )  #=> "undefined method" error!

如果为该类的祖先类定义了具有相同名称的方法，则不会删除祖先类中的同名方法：

<div class="code-file clearfix"><span>rem_methods2.rb</span></div>

	class Y
	  def somemethod
		puts("Y's somemethod")
	  end
	end

	class Z < Y
	  def somemethod
		puts("Z's somemethod")
	  end
	end

	zob = Z.new
	zob.somemethod 		#=> "Z's somemethod"

	class Z
	  remove_method( :somemethod )
	end

	zob.somemethod 		#=> "Y's somemethod"

相反，`undef_method` 阻止指定的类响应方法调用，即使在其一个祖先类中定义了一个具有相同名称的方法：

<div class="code-file clearfix"><span>undef_methods.rb</span></div>

	zob = Z.new
	zob.somemethod 		#=> "Z's somemethod"

	class Z
	  undef_method( :somemethod )
	end

	zob.somemethod 		#=> "undefined method" error

### 处理未定义方法的调用

当 Ruby 尝试执行未定义的方法时（或者，在 OOP 术语中，当一个对象被发送了一个它无法处理的消息时），该错误会导致程序终止退出。你可能更愿意你的程序能从这样的错误中恢复。你可以通过编写一个名为 `method_missing` 的方法来完成此操作，该方法接收一个值为缺失的方法名称的参数。这将在调用不存在的方法时执行：

<div class="code-file clearfix"><span>nomethod1.rb</span></div>

	def method_missing( methodname )
	  puts( "#{methodname} does not exist" )
	end
	xxx 	#=> displays: "xxx does not exist"

`method_missing` 方法还可以在缺失的方法名称后获取传入的参数列表（`args`）：


<div class="code-file clearfix"><span>nomethod2.rb</span></div>

	def method_missing( methodname, *args )
	  puts( "Class #{self.class} does not understand: #{methodname}( #{args.inspect} )" )
	end

`method_missing` 方法甚至可以动态创建未定义的方法：

	def method_missing( methodname, *args )
	  self.class.send( :define_method, methodname, lambda{ |*args| puts( args.inspect) } )
	end

### 在运行时写程序

最后，让我们回到我们之前看过的程序（**eval4.rb**），它提示用户输入字符串以在运行时（runtime）定义代码，执行这些字符串并从中创建新的可运行方法。

该程序的一个缺点是它必须要求每个方法的代码都输入一行中。事实上，编写一个允许用户输入跨越多行的方法的程序非常简单。例如，这里是一个程序，它执行输入的所有代码，直到输入一个空行：

<div class="code-file clearfix"><span>writeprog.rb</span></div>

	program = ""
	input = ""
	line = ""
	until line.strip() == "q"
	  print( "?- " )
	  line = gets()
	  case( line.strip() )
	  when ''
		puts( "Evaluating..." )
		eval( input )
		program += input
		input = ""
	  when 'l'
		puts( "Program Listing..." )
		puts( program )
	  else
		input += line
	  end
	end

你可以通过输入整个方法然后输入空行来尝试一下（当然只输入代码，而不是注释）：

	def a(s) 			# <= press Enter after each line
	  return s.reverse 	# <= press enter (and so on...)
	end
		# <- Enter a blank line here to eval these two methods
	def b(s)
	  return a(s).upcase
	end
		# <- Enter a blank line here to eval these two methods
	puts( a("hello" ) )
		# <- Enter a blank line to eval
		#=> Displays "olleh"
	puts( b("goodbye" ) )
		# <- Enter a blank line to eval
		#=> Displays "EYBDOOG"

输入每行后，会出现提示符（`'?-'`），除非程序正在执行代码的过程中，在这种情况下，它会显示 *"Evaluating"* 或显示执行结果，例如 *"olleh"*。

如果你完全按照上面的说明输入文本，那么你应该看到：

	Write a program interactively.
	Enter a blank line to evaluate.
	Enter 'q' to quit.
	?- def a(s)
	?- return s.reverse
	?- end
	?-
	Evaluating...
	?- def b(s)
	?- return a(s).upcase
	?- end
	?-
	Evaluating...
	?- puts(a("hello"))
	?-
	Evaluating...
	olleh
	?- b("goodbye")
	?-
	Evaluating...
	EYBDOOG

这个程序还很简单。它没有任何基本的错误恢复功能，更不用说花哨的东西，如文件保存和加载功能。即便如此，这个小示例也证明了在 Ruby 中编写自修改（self-modifying）程序是多么容易。使用本章概述的技术，你可以从给定语法规则的自然语言解析器为冒险游戏创造任何东西，这个过程中你可以探索新的困惑。

在本书中，我们涵盖了很多基础内容 - 从 "hello world" 到动态编程（dynamic programming）。剩下的要靠你自己了。

这才是冒险真正开始的地方。

## 深入探索

### 冻结对象

通过了解所有这些修改对象的方法，你可能会担心对象有被无意中修改掉的风险。实际上，你可以通过“冻结”它（使用 `freeze` 方法）来专门固定住对象的状态。一旦冻结，就无法修改对象包含的数据，如果尝试这样做，将抛出 TypeError 异常。然而，在冻结对象时要小心，因为一旦冻结，它就不能“解冻”（unfrozen）。

<div class="code-file clearfix"><span>freeze.rb</span></div>

	s = "Hello"
	s << " world"
	s.freeze
	s << " !!!" # Error: "can't modify frozen string (TypeError)"

你可以使用 `frozen?` 来专门检查对象是否被冻结：

	a = [1,2,3]
	a.freeze
	if !(a.frozen?) then
	  a << [4,5,6]
	end

请注意，虽然无法修改冻结对象的数据，但可以修改定义它的类。假设你有一个类 X，它包含一个方法 `addMethod`，它可以使用给出的符号名称创建新的方法 `m`：

<div class="code-file clearfix"><span>cant_freeze.rb</span></div>

	def addMethod( m, &block )
	  self.class.send( :define_method, m , &block )
	end

现在，如果你有一个从 M 类创建的对象 `ob`，那么调用 `addMethod` 来向 M 类添加一个新方法是完全合法的：

	ob.freeze
	ob.addMethod( :abc ) { puts("This is the abc method") }

当然，如果你想阻止冻结的对象它的所属类，可以使用 `frozen?` 方法来测试它的状态：

	if not( ob.frozen? ) then
	  ob.addMethod( :def ) { puts("'def' is not a good name for a method") }
	end

你也可以冻结类本身（记住，类也是一个对象）：

	X.freeze
	if not( X.frozen? ) then
	  ob.addMethod( :def ) { puts("'def' is not a good name for a method") }
	end


# 附录

***

## 附录 A：使用 RDOC 记录 Ruby

RDoc 是描述源代码文档格式的工具。RDoc 工具是 Ruby 的标准工具，可以处理 Ruby 代码文件和 C 代码 Ruby 类库，以便提取文档并对其进行格式化，以便它可以显示，例如在 Web 浏览器中。可以以源代码注释的形式显式添加 RDoc 文档。RDoc 工具还可以提取源代码本身的元素，以提供类，模块和方法的名称以及方法所需的任何参数的名称。

以 RDoc 处理器可访问的方式记录你自己的代码很容易。你可以在记录代码之前编写一组普通的单行注释（例如类或方法），也可以编写由 `=begin rdoc` 和 `=end` 分隔的嵌入式多行注释。请注意，`rdoc` 必须跟随 `=begin` 后，否则 RDoc 处理器将忽略注释块。

如果要生成文档，只需从命令提示符运行 RDoc 处理器即可。要为单个文件生成文档，请输入 `rdoc`，后跟文件名：

	rdoc rdoc1.rb

要为多个文件生成文档，请在 `rdoc` 命令后输入以空格分隔的文件名：

	rdoc rdoc1.rb rdoc2.rb rdoc3.rb

Rdoc 工具将创建一个格式良好的 HTML 文件（*index.html*），顶部有三个窗格，底部有四个较大的窗格。三个顶部窗格显示文件，类和方法的名称，而底部窗格显示文档。

HTML 包含超链接，以便你可以单击类和方法名称以导航到关联的文档。文档放在它自己的子目录 *\doc* 中，还有许多必需的 HTML 文件和一个样式表来应用格式。

你可以通过在单个单词或多个单词周围的标签周围放置格式字符来为 RDoc 注释添加额外的格式。使用 `*` 和 `*` 表示粗体，`_` 和 `_` 表示斜体，`+` 和 `+` 表示'打字机'字体。较长文本的等效标记为 `<b>` 和 `</b>` 表示粗体，`<em>` 和 `</em>` 表示斜体，`<tt>` 和 `</tt>` 表示打字机字体。

如果要从 RDoc 文档中排除注释或注释的一部分，可以将它放在 `#--` 和 `#++` 注释标记之间，如下所示：

	#--
	# This comment won‟t appear
	# in the documentation
	#++
	# But this one will

还有各种特殊说明，包含在冒号对之间。例如，如果要添加要在浏览器栏中显示的标题，请使用 `:title:`，像这样：

	#:title: My Fabulous RDoc Document

RDoc 提供了更多选项，使你能够以各种方式格式化文档，并以其它格式输出来替代 HTML 格式。如果你真的想要掌握 RDoc，请务必阅读完整的文档：

**http://rdoc.sourceforge.net/doc/index.html**

## 附录 B：为 Ruby On Rails 安装 MySQL

如果你正在使用 Rails，则需要安装数据库。虽然有很多可能的选择，但最广泛使用的是 MySQL。如果你之前从未使用过 MySQL，你可能会发现一些设置选项令人困惑。在这里，我将尝试引导你完成整个过程，以避免潜在的问题。

<div class="note">

本指南基于 Windows 下的 MySQL 5.0 安装。在其它操作系统上安装其它版本时可能会有所不同。有关其它指南，请参阅 MySQL 站点。
</div>

MySQL 主站点位于 **http://www.mysql.com/**，你可以从此处导航到当前版本的下载页面。

### 下载 MySQL

我假设你将使用 MySQL 的免费版本。可以从 **http://dev.mysql.com/downloads** 下载。在撰写本文时，当前版本是 MySQL 5 Community Server。当然，名称和版本号会随着时间的推移而变化。下载当前（即将发布的，alpha 或 beta）版本。选择为你的操作系统推荐的特定版本（例如，Win32 和 Win64 可能有不同的版本）。

你需要在此页面的某处向下滚动以找到适用于你的操作系统的安装程序。对于 Windows，你可以下载完整的 MySQL 包或较小的 Windows Essentials 包。完整的包包含数据库开发人员的额外工具，但这些不是简单的 Rails 开发所必需的。因此，对于大多数人来说，可以获得较小的 Windows Essentials 下载文件。

你应该单击此选项旁边的“选择镜像”（Pick A Mirror）链接。然后，你将看到一份问卷，如果你愿意，可以填写。如果你不希望这样做，只需向下滚动页面并选择一个区域下载站点。单击一个链接并保存文件，其名称类似（数字可能不同）：**mysql-essential-5.0.41-win32.msi**，磁盘上任意合适的目录。

### 安装 MySQL

下载完成后，通过在下载对话框中选择*“打开”（Open）*或_“运行”（Run）_（如果仍然可见）或通过 Windows 资源管理器双击安装文件来运行程序。

<div class="note">

**注意：**在安装 MySQL 期间，可能会在屏幕上出现一些广告。单击按钮以浏览这些。某些安全警告还可能会提示你验证是否有意安装该软件。出现提示时，应单击必要的选项以继续安装。
</div>

现在将出现安装向导的第一页。 单击<em>“下一步”（Next）</em>按钮。如果你愿意将软件安装到 <b>C:\Program Files\ </b> 下的默认 MySQL 目录中，则可以选择<em>“典型”（Typical）</em>设置选项。但是，如果要安装到其它目录，请选择<em>“自定义”（Custom）</em>。然后单击<em>下一步（Next）</em>。单击<em>“更改”（Change）</em>以更改目录。

准备好继续后，单击*“下一步”（Next）*。

你将看到屏幕上显示“准备安装程序”。验证目标文件夹是否正确，然后单击*“安装”（Install）*按钮。

根据 MySQL 的版本，你现在可能会看到显示的一些营销信息，或者可能会提示你创建一个新的 MySQL 帐户，以便你收到更改和更新的消息。这些不是软件安装的重要部分，你可以单击*“下一步”（Next）*或_“跳过”（Skip）_按钮继续安装。

现在出现向导已完成对话框。

点击*“完成”（Finish）*按钮。

### 配置 MySQL

事实上，安装并没有结束。对于某些安装程序，会弹出一个新屏幕，欢迎你使用 MySQL 服务器实例配置向导。如果没有发生这种情况，你需要自己加载。在 Windows 上，单击“开始”菜单，然后在程序组中导航到 MySQL-> MySQL Server 5.0（或你使用的任何版本号），然后再运行 MySQL Server Instance Config Wizard。点击*下一步*。

假设这是你第一次在此计算机上安装 MySQL，你可以选择标准配置（如果你要从旧版本的 MySQL 升级，则需要选择详细配置 - 这超出了此简单设置指南的范围）。点击*下一步*。在下一个对话框中，保留选中的默认选项（即，*安装为 Windows 服务；服务名称 = 'MySQL' 并自动启动 MySQL 服务器*）。然后单击*下一步*。

在下一个屏幕中，选中“修改安全设置”（Modify Security Settings），然后在前两个文本字段中输入相同的密码（你选择的密码）。你将在以后需要此密码，因此请记住它或将其记录在安全的位置。如果你可能需要从另一台计算机访问 MySQL，可以选中“从远程计算机启用 root 访问权限”（Enable root access from remote machines）。然后单击*下一步*。

<div class="note">

**注意：**默认的 MySQL 用户名是 "root"。密码是你刚输入的密码。以后在创建 Rails 应用程序时，你将需要这两项信息。
</div>

下一个屏幕只显示有关即将执行的任务的一些信息。单击*“执行”（Execute）*按钮。

<div class="note">

如果你以前安装或配置了 MySQL，则可能会看到一条错误消息，告诉你跳过安装。你可以单击*“重试”（Retry）*以查看是否可以绕过此问题。如果没有，请按 _Skip_ 键，然后重新启动 MySQL 配置过程，在出现提示时选择 <em>Reconfigure Instance</em> 和 Standard Instance。
</div>

安装完所有内容后，将出现此屏幕。单击*完成*。

就是这样！

## 附录 C：进一步阅读

以下是 Ruby 和 Rails 上一些最有用的书籍的简短列表...

### 书籍

- **Beginning Ruby: From Novice To Professional**

	by Peter Cooper $39.99 <br />
	APress: http://www.apress.com <br />
	ISBN: 1590597664 <br />
	这本书编写得很好，布局合理，解释清楚，代码示例很有用。简而言之，如果你已经拥有一些编程经验并希望获得 Ruby 世界的可访问介绍，那么这本书就是你的最佳选择。

- **Programming Ruby: The Pragmatic Programmer’s Guide**

	by Dave Thomas, with Chad Fowler and Andy Hunt $44.95 <br />
	ISBN: 0-9745140-5-5 (2 nd edition) <br />
	ISBN: 9781934356081 (3 rd edition) <br />
	Pragmatic: http://www.pragmaticprogrammer.com/titles/ruby/index.html
	有关 Ruby 语言和库的大量（超过 860 页）指南，所谓的“镐书”（pickaxe book）通常被认为是必不可少的 Ruby 参考。但是，阅读起来并不是轻松，（在我看来）它不是学习 Ruby 最好的“第一本书”。尽管如此，你可能迟早会需要它。第二版涵盖Ruby 1.8；第 3 版涵盖了 Ruby 1.9。

- **The Ruby Way**

	by Hal Fulton $39.99 <br />
	Addison-Wesley: http://www.awprofessional.com/ruby <br />
	ISBN: 0-672-32884-4 <br />
	在介绍部分，作者指出，由于相对缺乏“教程”材料，“你可能不会从这本书中学习 Ruby”。他把它描述为一种“反向引用”。不是按方法或类的名称查找，而是按功能或目的查找。我认为他大大低估了 The Ruby Way 的教程价值。然而，作者假定只你在编程方面相当熟练。

- **Ruby On Rails Up and Running**

	by Bruce A. Tate & Curt Hibbs $29.99 <br />
	O’Reilly: www.oreilly.com <br />
	ISBN: 0-596-10132-5 <br />
	我更喜欢编程书籍，而不需要太多的华夫饼干。坦率地说，我没有耐心通过 1000 多页的书籍或按照逐步指南来构建应用程序。所以这书吸引了我。在七章中，它涵盖了有关 Rails 的所有重要内容 - 它的设计模式和类；它的脚本和应用程序生成工具；它的模型，视图，控制器和脚手架; 以及使用 Ajax 和单元测试的概述。

- **Ruby For Rails**

	by David A. Black $44.95 <br />
	Manning : www.manning.com/black <br />
	ISBN 1-932394-69-9 <br />
	虽然本书主要关注 Rails 开发，但在每一步中都深入研究底层 Ruby 代码的内部工作方式。在十七章和不到 500 页的篇幅中，它将带你从第一次看从 Ruby 语言到创建 Rails 模型，控制器，视图，帮助和模板的细节。在此过程中，它解释了很多关于 Ruby 的内容，包括它的数组和散列，类，方法，块和迭代器。简而言之，如果你是 Ruby 新手，但想尽快加快 Rails 的学习速度，那么本书可能就是你所需要的。

- **Agile Web Development With Rails (3 rd edition)**

	by Sam Ruby, Dave Thomas and David Heinemeier Hansson $43.95 <br />
	Pragmatic:  http://pragprog.com/titles/rails3/agile-web-development-with-rails-third-edition <br />
	ISBN: 9781934356166 <br />
	这是关于 Rails “必备”的一本书。有几本 Ruby 编程书籍可能会争夺“必不可少”的主张，但我知道没有任何其它 Rails 书可以与 Agile Web Development 相媲美，因为它可以全面覆盖其主题。'Nuff 说：如果你认真对待 Ruby On Rails，那么买这本书吧！第 3 版涵盖了 Rails 2。

### E-Books

- **Learn To Program**

	第一版 Chris Pine 的书提供了对 Ruby 的简单介绍。 <br />
	http://pine.fm/LearnToProgram/

- **Programming Ruby: The Pragmatic Programmer’s Guide**

	可能是你读过的最奇怪的编程书 - 连同会说话的狐狸！ <br />
	http://poignantguide.net/ruby/

- **The Little Book Of Ruby**

	你正在读的这本书的配套书。 <br />
	http://www.sapphiresteel.com/The-Little-Book-Of-Ruby

## 附录 D：Web 站点

有无数的网站致力于 Ruby，Rails 和相关技术。以下是一些开始探索的内容...

### Ruby 和 Rails 信息

- **Ruby 语言站点**

	http://www.ruby-lang.org

- **Ruby 文档站点**

	http://www.ruby-doc.org/

- **Ruby 类库参考 (在线)**

	http://www.ruby-doc.org/core/

- **Ruby 类库参考（下载地址）**

	http://www.ruby-doc.org/downloads

- **Ruby On Rails 站点**

	http://www.rubyonrails.org/

- **The Book Of Ruby 作者的博客...**

	http://www.sapphiresteel.com/-Blog-

## 附录 E：Ruby 和 Rails 的开发软件

### 集成开发环境 IDE/编辑器

- **3rd Rail**

	http://www.codegear.com/products/3rdrail/ <br />
	Eclipse 的商业版 Rails-centric 集成开发环境。

- **Aptana IDE/ RADRails**

	http://www.aptana.com/ <br />
	Eclipse 的免费版 Rails-centric 集成开发环境。

- **Komodo**

	http://www.activestate.com/ <br />
	多语言（Ruby，Python，PHP，Perl，Pcl），跨平台商业集成开发环境。免费版也可用。

- **NetBeans**

	http://www.netbeans.org/products/ruby/ <br />
	NetBeans 的免费 Ruby 集成开发环境。

- **Ruby In Steel**

	http://www.sapphiresteel.com/ <br />
	适用于 Visual Studio 的商业 Ruby 和 Rails 集成开发环境。免费版也可用。

- **TextMate**

	http://www.macromates.com/ <br />
	Mac OS X 的 Ruby 编辑器。

### Web 服务器

以下是一些与 Ruby On Rails 一起使用的流行 Web 服务器。

- **WEBrick**

	http://www.webrick.org/

- **LightTPD**

	http://www.lighttpd.net/

- **Mongrel**

	http://mongrel.rubyforge.org/

- **Apache**

	http://www.apache.org/

### 数据库

- **MySQL**

	http://www.mysql.com/

- **SQLite**

	http://www.sqlite.org/

- **PostgreSQL**

	http://www.postgresql.org/

- **SQL Server Express**

	http://www.microsoft.com/sql/editions/express/

## 附录 F：Ruby 实现

在撰写本文时，Ruby 1.8.x 和 1.9.1 的版本都可用，并且将在未来某个日期发布版本 2.0。目前 Ruby 1.8.6 可能是 Ruby 使用最广泛的版本，本书的大部分内容适用于 Ruby 1.8。实际上，尽管 Ruby 1.9 已经发布，但为了支持使用该版本开发的项目，Ruby 1.8.x 仍将是一个重要的 Ruby 平台，包括使用 Ruby On Rails 和其它框架构建的应用程序。其它 Ruby 解释器，编译器和虚拟机也可用或正在开发中。以下是一个简短的网站列表，它将提供有关 Ruby 实现的更多信息（和下载）...

- **Ruby**

	“标准” Ruby 实现。 <br />
	http://www.ruby-lang.org/en/downloads/

- **JRuby**

	Ruby For Java。 <br />
	http://www.headius.com/

- **Iron Ruby**

	微软正在开发的 "Ruby For .NET"。 <br />
	http://www.ironruby.net/

- **Rubinius**

	用于 Ruby 的编译器/虚拟机（主要用 Ruby 编写）。 <br />
	http://rubini.us/

- **Maglev**

	快速 Ruby 实现（开发中）。 <br />
	http://maglev.gemstone.com/
